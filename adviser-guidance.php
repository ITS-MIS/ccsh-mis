<?php
	
	getDatatablesLink();
	getModalPageLink();

	getDatatablesScript();	

?>

<div class="container-fluid">

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Disciplinary Action</label>
			</div>
			<div class="col-md-1"></div>
		</div>	
	
	<div class="row">
		<form method="post">
			<table id="pg" class="display" cellspacing="0" width="100%" style="background-color:gold;">
		     
		        <thead>
		            <tr>
		            	<th style="display: none;">ID</th>
		            	<th>LRN </th>
		            	<th>Name</th>
		                <th>Date</th>
		                <th>Penalty</th>
		                <th style="display: none;">PTD</th>
		                <th style="display: none;">Remarks</th>
		            </tr>
		                
		        </thead>

		        <tbody>

			        <?php  


						$query = "SELECT DISTINCT(a.id), a.lrn, lastname, firstname, middlename, date, d.penalty_desc, ptd2 from tbl_guidancerecord a 
									LEFT JOIN tbl_studentinfo b ON a.lrn=b.lrn
									LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn
									LEFT JOIN tbl_penalty d ON a.penalty_id=d.penalty_id
									LEFT JOIN tbl_advisers e ON c.section_id=e.section_id WHERE e.emp_no=$emp_no AND e.sy_id=$sy_id
									AND b.remarks!='TO' AND b.remarks!='EXPELLED' ORDER BY date desc";



						$result = mysql_query($query) or die(mysql_error());


						if(mysql_num_rows($result) > 0) {
														
							while ($row = mysql_fetch_assoc($result)) {

								$id = $row['id'];
								$lrn = $row['lrn'];
								$lastname = $row['lastname'];
								$firstname = $row['firstname'];
								$middlename = $row['middlename'];
								$dater = $row['date'];
								$penalty_desc = $row['penalty_desc'];
								$ptd2 = $row['ptd2'];
								//$offense_desc = $row['offense_desc'];
								//$remarks = $row['remarks'];
								//$count = $row['count'];
					?>
					
			            <tr data-id="<?php echo $id ?>" data-lrn="<?php echo $lrn ?>"
		            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
		            						data-middlename="<?php echo $middlename ?>" data-dater="<?php echo $dater ?>"
		            						data-offense="<?php echo $offense_desc ?>" data-penalty="<?php echo $penalty_desc ?>"
		            						data-remarks="<?php echo $remarks ?>"  class="data">
			                
			                <td style="display: none;"><?php echo $id; ?></td>
			                <td><?php echo $lrn; ?></td>
			                <td><?php echo $lastname .', '. $firstname .' '. $middlename; ?></td>
			                <td><?php echo $dater; ?></td>
			                <td><?php echo $penalty_desc; ?></td>
			                <td style="display: none;"><?php echo $ptd2; ?></td>
			                <td style="display: none;"><?php echo $remarks; ?></td>
			            </tr>
		           	
				
				<?php
			            }
			        }

			    ?>

				</tbody>

			</table>
		</form>
	</div>
<div id="da" class="modal fade" data-backdrop="static">
		<div id="yearlevel-content">
			<div class="modal-dialog modal-lg">
		        <div class="modal-content">
		           
		   		</div>
		   	</div>
		</div>
	</div>

	

</div>


 		
<script type="text/javascript">

	
	$(document).ready(function() {
		var table=$('#pg').dataTable({
			"ordering": false,
		});
		

		$("#pg tr:contains(run)").attr("style","color:red");
 		
    	$('#pg tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
			$(".modal-content").html('');
			$('#da').modal('show');

			 //var clickedlrn= 'adasdadadasd';
			 $.post('modal-ag.php',{lrn:$(this).attr('data-lrn'),id:$(this).attr('data-id')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ); 

			 
			var lrn= $(this).attr('data-lrn');
			var id= $(this).attr('data-id');
			 console.info(lrn);

	
		});
	} );



</script>