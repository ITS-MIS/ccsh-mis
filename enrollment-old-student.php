<?php	
	getDatatablesLink();
	getModalPageLink();
	getDatatablesScript();
?>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Old Students  Enrollment</label>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
			<label>Year Level</label>
			<div class="btn-group btn-left-padding"> <a class="btn btn-default dropdown-toggle btn-select" data-toggle="dropdown" href="#">
				
				<?php 
				 	$result=get_db_array("SELECT year_level FROM tbl_yearlevel where  year_level!='Grade 10' ")or die(mysql_error());

					if(!isset($_GET['year_level'])) {
					
						$_GET['year_level'] = $result[0]['year_level'];
					
						echo $_GET['year_level'];
						$yearlvl=$_GET['year_level'];		

					}
					else {
					
						echo $_GET['year_level'];	
						$yearlvl=$_GET['year_level'];	 												
					}
				?>
					
				<span class="caret"></span></a>
		            <ul class="dropdown-menu">
			
			            <?php

						 	foreach ($result as $key => $filedname) {
						 		foreach ($filedname as $key => $value) {
						 			?>
							 		<li>
				                		<a href="index2.php?mode=<?php echo $_GET['mode']?>&category=<?php echo $_GET['category']?>&page=<?php echo $_GET['page']?>&year_level=<?php echo $value?>"><?php echo $value?></a>
				                	</li>
				                <?php
		           		
						 		}					
						 	}
			            ?>

	           	 	</ul>
	        </div>
<br><br><br>
<div class="row">
	<form method="post" >
		<table id="student" class="display" cellspacing="0" width="100%" /*data-page-length="25"*/ style="background-color:gold;">	     
	        <thead>
	            <tr> 
	                <th>LRN</th>
	                <th>Last Name</th>
	                <th>First Name</th>
	                <th>Middle Name</th>
	                <th>Status</th>				        
	            </tr>	                
	        </thead>	      

	        <tbody>

	        <?php  
	      
		        $sy_idcurrent=get_last_sy_id();
				$sy_idlast=$sy_idcurrent-1;


				$result =get_db_array("SELECT a.id,a.lrn,lastname,firstname,middlename,year_id,b.remarks from tbl_studentinfo a left join tbl_studentstatus b on a.lrn=b.lrn 
					where b.sy_id=$sy_idlast and b.year_id =(SELECT year_id from tbl_yearlevel where year_level='$yearlvl') and b.remarks!='Advise Transfer' ");		
				
				foreach ($result as $key => $fieldname) {

					$lrn=$fieldname['lrn'];
					$record=get_db_array("SELECT lrn from tbl_studentstatus where lrn=$lrn and sy_id=$sy_idcurrent");
					$record=count($record);
					if($record==0){
				
						?>
		           		<tr class="data" data-toggle="modal" data-traget="#studentmodal" data-id="<?php echo $result[$key]['id']; ?>" data-lrn="<?php echo $result[$key]['lrn']; ?>" data-yrlvl="<?php echo $yearlvl; ?>">
							<?php
							foreach ($fieldname as $key2 => $value) {
								if ($key2!='id' && $key2!='remarks') {
										
									?>
			        				<td><?php echo $value ?></td>
			        				<?php
								}
								
		        			}
		        			?>	                             
		            	</tr>
		          	<?php
	            	}
	            }

			?>

			</tbody>
		</table>
	</form><br>
</div>

<div id="studentmodal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
    	</div>
    </div>
</div>

<!--================ Delete Confirm Modal =================================================================-->

<div id="delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Do you want to delete the record?</p>
                <p class="text-warning"><small>If you proceed, deletion will be immediate!</small></p>
            </div>

            <div class="modal-footer">
            
            	<form method="post" action="students-submit.php">
		            <input type="hidden"  id="deleteid2" name="id">
		            <input type="hidden"  id="deletelrn" name="lrn">
		            <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			
		   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-danger warning" name="btnDelete">Confirm</button>
                </form>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
	$('#student').dataTable({
		bInfo: true,
		"bFilter": true,
		"ordering": false
		});
	} );

	$(document).ready(function() {
		$('#student').dataTable();
		 
		$('#student tbody').on('click', 'tr', function (e) {
			e.preventDefault();

			$.post('modal-enrollment-old-student.php',{id:$(this).data('id'),lrn:$(this).data('lrn'),year_level:$(this).data('yrlvl')},
					 function(html){
					 	$(".modal-content").html(html);
					 	$('#studentmodal').modal('show');
					 }
			); 
				        
		});

	});

		  $(function(){
		    $('#delete').modal({
		        keyboard: true,
		        backdrop: "static",
		        show:false,

		    }).on('show.bs.modal', function(){
		        var getIdFromRow = $(event.target).closest('a').data('id');
		 			$('#deleteid2').val(getIdFromRow);

			        console.info(getIdFromRow);

			});
		});

</script>