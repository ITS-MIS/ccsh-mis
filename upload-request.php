<?php
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10"><label id="header">List of Upload Image Request</label></div>
		<div class="col-sm-1"></div>
	</div>
	
	<div class="row"><br>
		<form method="post">
			<table id="student" class="display" cellspacing="0" width="100%" style="background-color:gold;">
		        <thead>
		            <tr>
		            	<th style="display: none;">ID</th>
		                <th>LRN</th>
		                <th>Student's Name</th>
		                <th>Upload Request Date</th>
		            </tr>		                
		        </thead>
		 
		        <tbody>
			        <?php  

						$query = "SELECT a.image_id, a.lrn, lastname,firstname,middlename, submission_date FROM tbl_images a 
									LEFT JOIN tbl_studentinfo b on a.lrn=b.lrn WHERE verify=0 ORDER BY submission_date desc";

						$result = mysql_query($query) or die(mysql_error());

						if(mysql_num_rows($result) > 0) {														
							while ($row = mysql_fetch_assoc($result)) {
								$image_id = $row['image_id'];
								$lrn= $row['lrn'];
								$lastname = $row['lastname'];
								$firstname = $row['firstname'];
								$middlename = $row['middlename'];
								$submission_date= $row['submission_date'];
					?>

		            <tr data-toggle="modal" data-lrn="<?php echo $lrn ?>" data-image_id="<?php echo $image_id ?>"
		            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
		            						data-middlename="<?php echo $middlename ?>" 
		            						data-target="#studentmodal" class="data">
		                
		                <td style="display: none;"><?php echo $image_id; ?></td>
		                <td><?php echo $lrn; ?></td>
		                <td><?php echo $lastname .', '. $firstname .' '. $middlename ?></td>
		                <td><?php echo $submission_date; ?></td>
					 	
		            </tr>
				
					<?php
				            }
				        }
				    ?>

				</tbody>

			</table>
		</form>
	</div>

	<br><br>

	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10"><label id="header2">Upload CCSHS School Policies</label></div>
		<div class="col-sm-1"></div>
	</div>

	<div class="row">
		<form action="upload.php" method="post" enctype="multipart/form-data">
			<input type="file" accept="application/pdf" class="btn btn-default" name="file" required/><br>
		 	<button type="submit" name="btn-upload" class="btn btn-success success" onclick="return confirm('Are you sure you want to Submit?');">Upload</button>
		</form>

 		<?php
 
			if(isset($_GET['fail'])) {
		?>
        	<label>Problem While File Uploading !</label>
        <?php
 			}
 		?>

 		<br><br>

		<table id="upload" class="display" cellspacing="0" width="100%" style="background-color:gold;">
			<thead>
		            <tr>
		            	<th>File Name</th>
		                <th>File Type</th>
		                <th>File Size</th>
		                <th>View</th>
		            </tr>
		                
		        </thead>
		 
		        <tbody>
    
				    <?php
				 		$sql="SELECT * FROM tbl_uploads";
				 		$result_set=mysql_query($sql);
				 		while($row=mysql_fetch_array($result_set)) {
				  	?>
				  	<tr>
				        <td><?php echo $row['file'] ?></td>
				        <td><?php echo $row['type'] ?></td>
				        <td><?php echo $row['size'] ?></td>
				        <td><a href="uploads/<?php echo $row['file'] ?>" target="_blank">view file</a></td>
				    </tr>
				    
				    <?php
				 		}
				 	?>
				</tbody>
    	</table>
	</div>

</div>

	<div id="da" class="modal fade">
		<div id="yearlevel-content">
			<div class="modal-dialog">
		        <div class="modal-content">
		           
		   		</div>
		   	</div>
		</div>
	</div>

</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#student').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#student tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	        $(".modal-content").html('');
			$('#da').modal('show');

			 $.post('modal-upload-request.php',{lrn:$(this).attr('data-lrn'),image_id:$(this).attr('data-image_id')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ); 

			 
			var lrn= $(this).attr('data-lrn');
			var image_id= $(this).attr('data-image_id');
			 console.info(lrn);

	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#upload').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    	} );
    } );

</script>