<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');
	
	date_default_timezone_set('Asia/Taipei');
	$currentmonth = date('F');
	$cmonth = date('m');
	$currentyear = date('Y');
	$currentday = date('d');
	$currentdate=$currentyear."-".$cmonth."-".$currentday;
	$day=1;

	$pdf = new FPDI();

	$pdf->AddPage('L','letter'); 

	$pageCount = $pdf->setSourceFile("forms/schoolstats.pdf");
	$tplIdx = $pdf->importPage(1); 

	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true); 

// schoolyear-------------------------------------------------------------

	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(220, 30);
	$pdf->SetFont('Arial','', '9');
	$pdf->Write(0, $sy);

//month---------------------------------------------------------------------

$pdf->SetXY(25, 190);
$pdf->SetFont('Arial','', '9');
$pdf->Write(0, $currentdate);

// grade 7 ToTAL--------------------------------------------------------------
	$year7=get_db("SELECT count(year_id) as total FROM tbl_studentinfo a, tbl_studentstatus b WHERE  a.lrn=b.lrn and year_id=7 and sy_id =$sy_id ");
	$year7=$year7['total'];
	$pdf->SetXY(242, 85);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $year7);
	

	$GF7=get_db("SELECT count(gender) as genderf FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=7 and sy_id = $sy_id  and gender='F'; ");
	$GF7=$GF7['genderf'];
	$pdf->SetXY(195, 85);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GF7);

	$GM7=get_db("SELECT count(gender) as genderm FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=7 and sy_id = $sy_id  and gender='M'; ");
	$GM7=$GM7['genderm'];
	$pdf->SetXY(135, 85);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GM7);

	$S7=get_db("SELECT count(section_id) as section7 FROM tbl_section_yearlevel WHERE year_id=7 and sy_id = $sy_id");
	$S7=$S7['section7'];
	$pdf->SetXY(82, 85);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $S7);

// grade 8 ToTAL--------------------------------------------------------------
	$year8=get_db("SELECT count(year_id) as total8 FROM tbl_studentstatus WHERE year_id=8 and sy_id = $sy_id");
	$year8=$year8['total8'];
	
	$pdf->SetXY(242, 103);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $year8);


	$GF8=get_db("SELECT count(gender) as genderf8 FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=8 and sy_id = $sy_id  and gender='F'; ");
	$GF8=$GF8['genderf8'];
	$pdf->SetXY(195, 103);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GF8);

	$GM8=get_db("SELECT count(gender) as genderm8 FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=8 and sy_id = $sy_id  and gender='M'; ");
	$GM8=$GM8['genderm8'];
	$pdf->SetXY(135, 103);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GM8);

	$S8=get_db("SELECT count(section_id) as section8 FROM tbl_section_yearlevel WHERE year_id=8 and sy_id = $sy_id");
	$S8=$S8['section8'];
	$pdf->SetXY(82, 103);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $S8);


// grade 9 ToTAL--------------------------------------------------------------
	$year9=get_db("SELECT count(year_id) as total FROM tbl_studentstatus WHERE year_id=9 and sy_id = $sy_id");
	$year9=$year9['total'];
	
	$pdf->SetXY(242, 120);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $year9);

	$GF9=get_db("SELECT count(gender) as genderf9 FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=9 and sy_id = $sy_id  and gender='F'; ");
	$GF9=$GF9['genderf9'];
	$pdf->SetXY(195, 120);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GF9);

	$GM9=get_db("SELECT count(gender) as genderm9 FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=9 and sy_id = $sy_id  and gender='M'; ");
	$GM9=$GM9['genderm9'];
	$pdf->SetXY(135, 120);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GM9);

	$S9=get_db("SELECT count(section_id) as section9 FROM tbl_section_yearlevel WHERE year_id=9 and sy_id = $sy_id");
	$S9=$S9['section9'];
	$pdf->SetXY(82, 120);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $S9);

// grade 10 ToTAL--------------------------------------------------------------
	$year10=get_db("SELECT count(year_id) as total FROM tbl_studentstatus WHERE year_id=10 and sy_id = $sy_id");
	$year10=$year10['total'];
	
	$pdf->SetXY(242, 138);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $year10);

	$GF10=get_db("SELECT count(gender) as genderf10 FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=10 and sy_id = $sy_id  and gender='F'; ");
	$GF10=$GF10['genderf10'];
	$pdf->SetXY(195, 138);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GF10);

	$GM10=get_db("SELECT count(gender) as genderm10 FROM tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and year_id=10 and sy_id = $sy_id  and gender='M'; ");
	$GM10=$GM10['genderm10'];
	$pdf->SetXY(135, 138);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $GM10);

	$S10=get_db("SELECT count(section_id) as section10 FROM tbl_section_yearlevel WHERE year_id=10 and sy_id = $sy_id");
	$S10=$S10['section10'];
	$pdf->SetXY(82, 138);
	$pdf->SetFont('Arial','', '12');
	$pdf->Write(0, $S10);
	
	$pdf->Output();