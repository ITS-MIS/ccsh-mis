<?php
	getDatatablesLink();
	getDatatablesScript();
	getRegistrarPageScript();
?>	
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Input Entrance Exam Result</label>
			</div>
			<div class="col-md-1"></div>
		</div>

<div class="row">
	<?php 
		$sy_id=get_db("SELECT sy_id FROM tbl_sy ORDER BY sy_id DESC LIMIT 1");
		$sy_id=$sy_id['sy_id'];
		$result=get_db_array("SELECT id, lastname, firstname, middlename,score from tbl_applicantinfo WHERE enrolled=0 AND sy_id=$sy_id");		
	?>
		<form method="post" action="admission-input-entrance-exam-submit.php" class="form-horizontal" >
			<table id="applicants" class="display" cellspacing="0" width="100%" style="background-color:gold;" >
				<thead >
					<tr>
						<th>ID</th>
						<th>Last Name</th>
						<th>First Name</th>
						<th>Middle Name</th>
						<th>Score</th>				
					</tr>								
				</thead>
				<tbody>
					<?php
					if(count($result)>0){
						foreach ($result as $key => $fieldname) {
							?>
							<tr>
							<?php
							foreach ($fieldname as $key2 => $value) {

								if ($key2=='score') {
									if ($value==0 or $value =='' or $value==null) {
										?>
										<td >
											<input type="text" class="form-control" id="score" placeholder="Score" name="grade[<?php echo $result[$key]['id'] ?>]" style="width:80px;height:25px" maxlength="3" onkeypress="return isNumber(event)">
										           	
										<?php
									}
									else{
										?>												
										<td class="grade"style="width:200px;" >									
											<div>
												<div style="text-align:right;width:40px;float:left">	
													<?php echo $value; ?>
												</div>

												<div style="width:80px;float:left">
													<a id="edit" style=" margin-left:20px; width:800px" data-grade="<?php echo $value ?>" data-id="<?php echo $result[$key]['id'] ?>" class="edit" href="#">edit</a>
											    
												</div>
											</div>
											
									    </td>
										<?php
									}
								}
								else{
									?>
									<td ><?php echo $value;  ?></td>
									<?php										
								}
							}
							
							?>
							</tr>
							<?php								
						}
					}
					?>			
				</tbody>
			</table>	
				
			<div class="form-group">												
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
			</div>
				
			<div class="form-group " style="alighn:right;">
				<div class="col-xs-offset-3 col-xs-9">
					    <button type="submit" class="btn btn-success success left-margin" name="submit" style="float: right;" onclick="return confirm('Are you sure you want to submit?');">Submit</button>
				</div>
			</div>
		</form>		
	</div>
	
<script type="text/javascript">
		
	$(document).on("click", "td.grade a", function() {
	    var parent = $(this).parent();
	    $(this).replaceWith("<input type='text' class='form-control'  value='" + $(this).data('grade') + "' name='grade[" + $(this).data('id') + "]' style='margin-left:20px;width:80px;height:25px;' maxlength='3' onkeypress='return isNumber(event)'>"); //closing angle bracket added
	    parent.children(":text").focus();
	    return false;
	});

</script>		
<script type="text/javascript" src="./js/number.js"></script>