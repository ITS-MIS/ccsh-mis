<?php
	include_once'connect.php';
	include 'current-year.php';
	include 'user-access-filter.php';

	getMeta();

	getBsLink();
	getCcshsPageLink();
	getModalPageLink();

	getJqueryScript();
	getBsScript();
	getValidationScipt();
?>

<!DOCTYPE html>
<html>
	<head>	
		<title>CCSHS SIS</title>
		<link rel="icon" href="images/logo.ico" type="image/x-icon" sizes="64x64">
		<style type="text/css">			
					.badge {color:white;
							background-color: red;
					}
					.badge2{color:white;
							background-color: red;
					}
					.badge3{color:white;
							background-color: red;
					}
					.error {color: #FF0000;}
		</style>		
	</head>
	
			<?php 
				include('badge-adviser.php');
				include('badge-principal.php');
				include('badge-adviser-guidance.php');
				include('badge-upload.php');
			 ?>
			
	<body>
			<div class="col-md-12 " >
				<nav id="myNavbar" class="navbar navbar-default navbar-fixed-top" >
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<img src="images/logo.png" height=95px; width=95px; style="margin-left: 70px; margin-right:15px; margin-top:3px; margin-bottom:3px">	
						</div>
						
						<div class="collapse navbar-collapse" id="navbarCollapse">
							<ul class="nav navbar-nav ">

								<li <?php if (count($modes)>1) echo 'class="dropdown"'?>>
										<a href="#" <?php if (count($modes)>1) echo 'class="dropdown-toggle" data-toggle="dropdown"';?>>
									<?php 
										if (!isset($_GET['mode'])){

											 $_GET['mode']=$modes[0];
											 echo $_GET['mode'];
										}
										else 
											echo $_GET['mode'];
										
										if (count($modes)>1){
											echo '<b class="caret"></b>';
										}
									?>
									
										</a>

										<?php
										if (count($modes)>1){
											?>
											<ul class="dropdown-menu">
											<?php 
												
												foreach($modes as $acc){
													?>
													<li><a href="index2.php?mode=<?php echo $acc ?>"><?php echo $acc ?></a></li>
													<?php
												}
											?>
											</ul>
										<?php	
										}	
									?>
								</li>
							</ul>


							<ul class="nav navbar-nav navbar-right ">
								<?php
									if(isset($_GET['mode'])){
										switch ($_GET['mode']) {

											case 'Administrator':
												if($emp_no==1413914){
													$tabs=array("Faculty","Students","Subjects", "Sections", "Grant Access","Audit Trail", "Important Dates", "Grade Access", "Upload Access", "Account");
												}
												else{
													$tabs=array("Faculty","Students","Subjects", "Sections", "Audit Trail", "Upload Access", "Account");
												}
												break;

											case 'Registrar':
												$tabs=array("Admission","Enrollment","Students","Forms and Reports","Account");

												$admissionSubTabs=array("Application Form","Input Entrance Exam Result","Entrance Exam Result");

												$studentsSubTabs=array("Master List","Requirements","Grades");
												
												$enrolmentSubTabs=array("Freshmen","Old Student","Transferee");
													
												$formsAndReportsSubTabs=array("Form 137","Good Moral Character");

												break;

											case 'Principal':
												
												$tabs=array("Teacher-Subject Loading","Student-Section Loading","Ranking","Under Probation","Guidance","Account");
												
												break;

											case 'Faculty':
												if($adviser==true){
													$tabs=array("Input Grades","Advisory Class","Ranking","Under Probation","Guidance Record","Reports","Account");
													$advisoryclassSubTabs=array("Advisory Grade","Attendance Monitoring","Class List");
													$reportsSubTabs=array("SF1","SF2","Form 138");
												}
												else{
													$tabs=array("Input Grades","Ranking","Under Probation","Account");
												
												}
												
												break;

											case 'Guidance':
												$tabs=array("Students Record","Violation","Under Probation","Account");
												break;

											case 'Student':
												$tabs=array("Home","Online Grades","Requirement","Offenses","Account");
												break;

											case 'Parent':
												$tabs=array("Home","Online Grades","Requirement","Offenses","Account");
												break;

											default:
												$tabs='not a valid mode';
												break;  
										}

											$accountSubtabs=array("<span class='glyphicon glyphicon-edit'> <font face='arial'>Edit Account</font><span>","Logout");
								
										foreach($tabs as $key=>$navitem){
											
											
		 									switch ($_GET['mode']) {
		 										case 'Registrar':
													
													switch ($navitem) {

														case 'Admission':

															$drplist=$admissionSubTabs;
															break;

														case 'Enrollment':

															$drplist=$enrolmentSubTabs;
															break;
															
														case 'Students':

															$drplist=$studentsSubTabs;
															break;

														case 'Forms and Reports':

															$drplist=$formsAndReportsSubTabs;
															break;

													}
													break;

												case 'Faculty':
													
													switch ($navitem) {

														case 'Reports':
															$drplist=$reportsSubTabs;
															break;

														case 'Advisory Class':
															$drplist=$advisoryclassSubTabs;
															break;

													}
													break;
											}	

											if($navitem=='Account'){

												$drplist=$accountSubtabs;
											}

										if(isset($drplist)){
											?>

											<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<?php 
												echo $navitem; 
												if($navitem=='Advisory Class'){ 
													?> 
													<span style="float:right" class="badge"><?php echo $counter; ?></span>
													<?php
												}
												?>
												<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<?php
														if (isset($drplist)){

															foreach($drplist as $key => $listitem){

																if ($listitem=='divider'){
																	?>
																	<li class="divider"></li>
																	<?php
																}

																elseif ($listitem=='Logout') {
																	?>
																	<li><a href="#logout" data-toggle="modal" data-url="logoutdata.php"><span class="glyphicon glyphicon-log-out"> <font face="arial"><?php echo $listitem?></font></span></a></li>
																	<?php
																}

																elseif ($listitem=='Advisory Grade') {
																	?>
																	<li ><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $listitem ?> <span style="float:right" class="badge"><?php echo $counter; ?></span></a></li>
																	<?php
																}
																														
																else{

																	if ($navitem=='Reports'){
																		if ($listitem=='Form 138') {
																			?>
																			<li ><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $listitem ?> </a></li>
																			<?php
																		}
																		else{

																			?>
																			<li><a target = '_blank' href="reports-<?php echo $listitem ?>.php"><?php echo $listitem ?> </a></li>
																			<?php
																		}
																		
																	}
																	else{

																		?>
																		<li ><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $listitem ?> </a></li>
																		<?php
																	}
																}
															}
														}
														unset($drplist);

													?>
													
												</ul>
											</li>

											<?php

										}
										
										else{

										
											switch ($navitem) {

												case 'Advisory Class':
													?>
													<li><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $navitem."    "?>
														<span class="badge"><?php echo $counter; ?></span></a></li>
													<?php
													break;

												case 'Guidance':
													?>
													<li><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $navitem."    "?>
														<span class="badge"><?php echo $pcounter; ?></span></a></li>
													<?php
													break;

												case 'Guidance Record':
													?>
													<li><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $navitem."    "?>
														<span class="badge2">&nbsp;<?php echo $agcounter; ?>&nbsp;</span></a></li>
													<?php
													break;

												case 'Upload Access':
													?>
													<li><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $navitem."    "?>
														<span class="badge3">&nbsp;<?php echo $upcounter; ?>&nbsp;</span></a></li>
													<?php
													break;

												default:
													?>
													<li><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $navitem ?>&page=<?php echo $key ?>"><?php echo $navitem ?></a></li>
													<?php
													break;

											}

										}
					
									}	
								}		

								?>

							</ul>
						</div>
					</div>					
				</nav>
			</div>
		
			<div class="col-md-12 " style="margin-top:100px; margin-bottom:200px">

				<div class="col-md-1">
				</div>

				<div class="col-md-10" >

					<?php
					
						include 'index2-navbar-links.php'					
					?>

				</div>
				
				<div class="col-md-1">
					<div id="logout" class="modal fade">
		        		<div id="modal-content"></div>
		   			</div>

				</div>

			</div>
			
		<div id="footer"><?php include('ccshs-footer.php'); ?></div>	
		
		<script type="text/javascript">
			$(function(){
			
				$(".dropdown").hover(            
				function() {
					$('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
					$(this).toggleClass('open');
					$('b', this).toggleClass("caret caret-up");                
				},
				function() {
					$('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
					$(this).toggleClass('open');
					$('b', this).toggleClass("caret caret-up");                
				});
			});

			$(function(){
			
				$(".dropdown").hover(            
				function() {
					$('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
					$(this).toggleClass('open');
					$('b', this).toggleClass("caret caret-up");                
				},
				function() {
					$('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
					$(this).toggleClass('open');
					$('b', this).toggleClass("caret caret-up");                
				});
			});

			$('#logout').on('show.bs.modal', function (event) {
				var url = $(event.relatedTarget).data('url');
				$.get( url, function( data ) {
				  	$( "#modal-content" ).html( data );
				});
			});
  			
		</script>	
	</body>
</html>
	
	