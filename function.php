<?php 
	
	function getMeta()
	{
		?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">	
		<?php
	}

// Jquery script -------------------------------------------------------------------

	function getJqueryScript()
	{
		?>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		
		<?php
	}

// Bootstrap css and script -------------------------------------------------------------------

	function getBsLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">	
		<?php
	}

	function getBsScript()
	{
		?>
		
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/bootstrap-contextmenu.js"></script>
				<?php
	}

// Bootstrap css and script VALIDATOR -------------------------------------------------------------------

	function getBsValidator()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/bootstrapValidator.css"/>
	    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	    <script type="text/javascript" src="js/bootstrapValidator.js"></script>
		<?php
	}

// Datatables css and script------------------------------------------------------

	function getDatatablesLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.dataTables_themeroller.css">	
		<?php
	}

	function getDatatablesScript(){
		?>
		<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/datatables.js"></script>
		<?php

	}

// Datatables Editor css and script------------------------------------------------------

	function getDatatablesEditorLink(){

		?>
		<link rel="stylesheet" type="text/css" href="css/dataTables.tableTools.css">
		<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">	
		<link rel="stylesheet" type="text/css" href="css/editor.bootstrap.css">	
		<?php

	}

	function getDatatablesEditorScript(){
		?>
		<script type="text/javascript" src="js/dataTables.tableTools.min.js"></script>
		<script type="text/javascript" src="js/dataTables.editor.min.js"></script>
		<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="js/editor.bootstrap.js"></script>
		<?php
	}
	
// Homepage css and script------------------------------------------------------
	
	function getCcshsPageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/ccshs.css">	
		<link rel="stylesheet" type="text/css" href="css/ccshs-header.css">
		<link rel="icon" href="images/logo.ico" type="image/x-icon" sizes="64x64">
		<?php
	}
	
// Admin css and script------------------------------------------------------	
	
	function getAdminPageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/admin.css">	
		<link rel="stylesheet" type="text/css" href="css/faculty-loading.css">
		<?php
	}
	
	function getAdminPageScript()
	{
		?>
		<script type="text/javascript" src="js/admin.js"></script>
		<?php
	}
	
// Registrar css and script------------------------------------------------------
	
	function getRegistrarPageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/registrar.css">
		<link rel="stylesheet" type="text/css" href="css/php_checkbox.css" />
		<?php
	}
	
		function getRegistrarPageScript()
	{
		?>
		<script type="text/javascript" src="js/registrar.js"></script>		
		<?php
	}	
	
// Adviser css and script------------------------------------------------------
	
	function getAdviserPageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/adviser.css">		
		<?php
	}
	
	function getAdviserPageScript()
	{
		?>
		<script type="text/javascript" src="js/adviser.js"></script>		
		<?php
	}
	
// Faculty css and script------------------------------------------------------

	function getFacultyPageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/faculty.css">
		<?php
	}
	
// Guidance css and script------------------------------------------------------
	
	function getGuidancePageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/guidance.css">
		<?php
	}
	
	function getGuidancePageScript()
	{
		?>
		<script type="text/javascript" src="js/guidance.js"></script>
		<?php
	}
	
// Student css and script------------------------------------------------------
	
	function getStudentPageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/student.css">	
		<?php
	}

// ------------------MODAL----------------------------------------------------
	
	function getModalPageLink()
	{
		?>
		<link rel="stylesheet" type="text/css" href="css/modal.css">	
		<?php
	}
	
// ------------------------------------------------------
	
	function echoActiveClassIfRequestMatches($requestUri)
	{
		$current_page = basename($_SERVER['REQUEST_URI'], ".php");
		//Given a string containing the path to a file or directory, this function will return the trailing name component.

		if ($current_page == $requestUri)
			echo 'class="active"';
	}

//login functions-/database------------------------------------------------------------------------------------------------

	function get_db_array($sql) {
		$resource = mysql_query($sql);

		$db_array= array();

		while($row=mysql_fetch_assoc($resource)){
			array_push($db_array, $row);
		}
		return $db_array;
	}

	function get_db_array2($sql) {
		$resource = mysql_query($sql);

		$db_array= array();

		while($row=mysql_fetch_array($resource)){
			array_push($db_array, $row);
		}
		return $db_array;
	}
	function get_db($sql) {
		$resource = mysql_query($sql);

		return mysql_fetch_assoc($resource);
	}

	function get_db2($sql) {
		$resource = mysql_query($sql);

		return mysql_fetch_array($resource);
	}

	function query_db($sql) {
		mysql_query($sql);
	}

	function redirect($url) {
		header('Location: ' . $url);
		exit;
	}

	function verify_user_pass($user, $pass) {
		$user = mysql_real_escape_string($user);
		$pass = mysql_real_escape_string($pass);
		return get_db("SELECT * FROM tbl_loginemployee WHERE emp_no = '$user' AND password = '$pass'");
	}

	function verify_user_pass2($lrn, $pass) {
		$lrn = mysql_real_escape_string($lrn);
		$pass = mysql_real_escape_string($pass);
		return get_db("SELECT * FROM tbl_loginstudent WHERE username = $lrn AND password = '$pass'");
	}

	function verify_user_pass3($lrn2, $pass) {
		$lrn2 = mysql_real_escape_string($lrn2);
		$pass = mysql_real_escape_string($pass);
		return get_db("SELECT * FROM tbl_loginparent WHERE username = $lrn2 AND password = '$pass'") ;
	}

	function set_logged_in_user(array $user) {
		$_SESSION['emp_no'] = $user['emp_no'];

	}

	function set_logged_in_user2(array $lrn) {
		$_SESSION['lrn'] = $lrn['lrn'];

	}

	function set_logged_in_user3(array $lrn2) {
		$_SESSION['lrn2'] = $lrn2['lrn'];

	}

	function logout() {
	
		$_SESSION['emp_no'] = null;
		$_SESSION['lrn'] = null;
		$_SESSION['lrn2'] = null;		
		
		session_destroy();
		
		redirect('index.php');
	}	
	
//other validation functions-------------------------------------------------------------------------------------------------	

	function getValidationScipt()
	{
		?>
		<script type="text/javascript" src="./js/input.js"></script>
		<?php
	}
	
	//other---------------------------------------------------------------------------------------------------------------------
	
	function getother()
	{
	?>
	<script type="text/javascript">
	function showfield1(name){
	  if(name=='Other')document.getElementById('div1').innerHTML='<input type="text" class="form-control" id="mothertounge2" name="info[mothertounge]" placeholder="Mother Tounge" maxlength="20" onkeypress="return isLetterDashPoint(event)">';
	  else document.getElementById('div1').innerHTML='';
	}

	function showfield2(name){
	  if(name=='Other')document.getElementById('div2').innerHTML='<input type="text" class="form-control" id="ip2" name="info[ip]" placeholder="Ethnic Group" maxlength="20" onkeypress="return isLetterDashPoint(event)">';
	  else document.getElementById('div2').innerHTML='';
	}

	function showfield3(name){
	  if(name=='Other')document.getElementById('div3').innerHTML='<input type="text" class="form-control" id="religion2" name="info[religion]" placeholder="Religion" maxlength="20" onkeypress="return isLetterDashPoint(event)">';
	  else document.getElementById('div3').innerHTML='';
	}

	function showfield4(name){
	  if(name=='Other')document.getElementById('div4').innerHTML='<input type="text" class="form-control" id="relationship2" name="info[relationship]" placeholder="Relationship" maxlength="20" onkeypress="return isLetterDashPoint(event)">';
	  else document.getElementById('div4').innerHTML='';
	}
</script>
	<?php
	
	}
	
//--misc-------------------------------------------------------------------------

	function get_last_sy_id(){
		$sy_id=get_db("SELECT sy_id FROM tbl_sy ORDER BY sy_id DESC LIMIT 1");
		$sy_id=$sy_id['sy_id'];
		return $sy_id;
	}

	function get_last_sy(){
		$sy=get_db("SELECT sy FROM tbl_sy ORDER BY sy_id DESC LIMIT 1");
		$sy=$sy['sy'];
		return $sy;
	}
					
?>