<?php	
	getDatatablesLink();
	getDatatablesScript();
?>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Freshmen Student Enrollment</label>
			</div>
			<div class="col-md-1"></div>
		</div>	
<div>	

	<label>School year</label>

	<div class="btn-group btn-left-padding"> <a class="btn btn-default dropdown-toggle btn-select" data-toggle="dropdown" href="#">
		<?php 

			$result=get_db_array("SELECT sy FROM tbl_sy order by sy_id desc limit 2");	

			if(!isset($_GET['schoolyr'])) {
				
				$_GET['schoolyr'] = $result[0]['sy'];		
				echo $result[0]['sy'];
				$sy=$_GET['schoolyr'];	
				
			}
			else{
				echo $_GET['schoolyr'];
				$sy=$_GET['schoolyr'];		 												
			}
		?>

		<span class="caret"></span></a>
	        <ul class="dropdown-menu">
	            <?php	        		   
	            foreach ($result as $key => $column) {	
	           		foreach ($column as $key => $value) {	       
						?>
			            <li><a href="index2.php?mode=Registrar&category=Enrollment&page=0&schoolyr=<?php echo $value?>"><?php echo $value?></a></li>
		                <?php	 
		            }     
	            }
	            ?>
	   	 	</ul>
	</div>
</div>

<div class="row" style="background-color:;"><br>
	<form method="post" >
		<table id="student" class="display" cellspacing="0" width="100%" /*data-page-length="25"*/ style="background-color:gold;">   
	        <thead>
	            <tr>
	            	<th>ID</th>
	                <th>Last Name</th>
	                <th>First Name</th>
	                <th>Middle Name</th>
	            </tr>	      
	        </thead>

	        <tbody>
	        <?php  
				
				if(isset($_GET['schoolyr'])){
					$sy_id=get_db("SELECT sy_id FROM tbl_sy where sy='$sy'");

				}
				else{
					$sy_id=get_db("SELECT sy_id FROM tbl_sy ORDER BY sy_id DESC LIMIT 1");
							
				}
				$sy_id=$sy_id['sy_id'];

				$studentarray = get_db_array("SELECT id,lrn,lastname,firstname,middlename from tbl_applicantinfo  where score>=85  and enrolled =0 and sy_id=$sy_id order by lastname");
				
				if(count($studentarray)>0){
	 				foreach ($studentarray as $key => $fieldname) {
						?>

		           		<tr  data-toggle="modal" data-traget="#studentmodal" data-id="<?php echo $studentarray[$key]['id']; ?>" data-data="true" class="data">
		     			
							<?php
							foreach ($fieldname as $key2 => $value) {
								if ($key2!='lrn') {
									?>
		            				<td><?php echo $value ?></td>
		            				<?php
								}
								
	            			}
	            			?>	                             
		            	</tr>
		            <?php
			        
			        }
			    }
		    ?>

			</tbody>
		</table>
	</form><br>
</div>

<div id="studentmodal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
		</div>
	</div>	
</div>

<script type="text/javascript">

	$(document).ready(function() {
		var table=$('#student').dataTable({
			bInfo: true,
			"bFilter": true,
			"ordering": false
			});

		$('#student tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

    		$(".data").removeClass('selected');

	        if ( $(this).removeClass('selected') ) {
	            $(this).addClass('selected');
	         }

	        if($(this).data('data')){
	        	$("#studentmodal .modal-content").html('');
			
				$.post('modal-enrollment-freshman.php',{id:$(this).data('id')},
					 function(html){
					 	$(".modal-content").html(html);
					 	$('#studentmodal').modal('show');
					 }
				 ); 
	        }
	
		});
	});

</script>