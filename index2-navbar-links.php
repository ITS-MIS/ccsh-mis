<?php
	if (isset($_GET['mode']) && !isset($_GET['category']) && !isset($_GET['page'])) {

		switch ($_GET['mode']) {
			case 'Administrator':				
				include'faculty.php';
				break;

			case 'Principal':
				include'teacher-subject-loading.php';
				break;

			case 'Registrar':
				include'admission-application-form.php';
				break;

			case 'Faculty':				
				include'input-grades.php';
				break;

			case 'Guidance':				
				include'students-record.php';
				break;

			case 'Student':				
				include'student-page.php';
				break;

			case 'Parent':				
				include'parent-page.php';
				break;
		}
	}

	if (isset($_GET['mode']) && isset($_GET['category']) && isset($_GET['page'])) {

		switch ($_GET['mode']. $_GET['category'].$_GET['page']) {

//--------------------------Administrator-------------------------------------------------------------------

			case 'AdministratorFaculty0':				
				include'Faculty.php';
				break;

			case 'AdministratorStudents1':
				include'students.php';
				break;

			case 'AdministratorSubjects2':
				include'subjects.php';
				break;

			case 'AdministratorSections3':
				include'sections.php';
				break;

			case 'AdministratorYear Level4':
				include'year-level.php';
				break;

			case 'AdministratorGrant Access4':
				include'grant-access.php';
				break;

			case 'AdministratorAudit Trail5':
				include'audit-trail.php';
				break;

			case 'AdministratorImportant Dates6':
				include'important-dates.php';
				break;

			case 'AdministratorGrade Access7':
				include'grade-access.php';
				break;

			case 'AdministratorUpload Access8':
				include'upload-request.php';
				break;

			case 'AdministratorAccount0':
				include'account-admin.php';
			break;

//--------------Admin without grant access-------------------------------

			case 'AdministratorAudit Trail4':
				include'audit-trail.php';
				break;
			
//--------------------------Registrar-------------------------------------------------------------------
		
			case 'RegistrarAdmission0':
				include 'admission-application-form.php';
				break;
				
			case 'RegistrarAdmission1':
				include 'admission-input-entrance-exam-result.php';
				break;

			case 'RegistrarAdmission2':
				include 'admission-entrance-exam-result.php';
				break;
				
			case 'RegistrarEnrollment0':
				include 'enrollment-freshman.php';
				break;	

			case 'RegistrarEnrollment1':
				include 'enrollment-old-student.php';
				break;
				
			case 'RegistrarEnrollment2':
				include 'enrollment-transferee.php';
				break;

			case 'RegistrarStudents0':
				include 'students-master-list.php';
				break;
				
			case 'RegistrarStudents1':
				include 'student-requirements.php';
				break;

			case 'RegistrarStudents2':
				include 'students-grades.php';
				break;

			case 'RegistrarForms and Reports0':
				include 'Form137.php';
				break;	

			case 'RegistrarForms and Reports1':
				include 'Goodmoral.php';
				break;	

			case 'RegistrarAccount0':
				include 'account-registrar.php';
				break;

//--------------------------Principal-------------------------------------------------------------------

			case 'PrincipalTeacher-Subject Loading0':
				include'teacher-subject-loading.php';
				break;

			case 'PrincipalStudent-Section Loading1':
				include'student-section-loading.php';
				break;

			case 'PrincipalRanking2':
				include'ranking.php';
				break;
				
			case 'PrincipalUnder Probation3':
				include'under-probation.php';
				break;

			case 'PrincipalAudit Trail - Grades4':					
				include'audit-trail-grades.php';
				break;

			case 'PrincipalGuidance4':					
				include'principal-guidance.php';
				break;

			case 'PrincipalAccount0':					
				include'account-principal.php';
				break;

//--------------------------Faculty-------------------------------------------------------------------
				
			case 'FacultyInput Grades0':					
				include'input-grades.php';
				break;										

			case 'FacultyAdvisory Class0':					
				include'advisory-grade.php';
				break;

			case 'FacultyAdvisory Class1':
				include'attendance-monitoring.php';
				break;

			case 'FacultyAdvisory Class2':					
				include'advisory-class.php';
				break;

			case 'FacultyRanking2':					
				include'ranking.php';
				break;

			case 'FacultyUnder Probation3':
				include 'under-probation.php';
				break;

			case 'FacultyGuidance Record4':
				include 'adviser-guidance.php';
				break;

			case 'FacultyReports0':
				include 'reports-sf1.php';
				break;
				
			case 'FacultyReports1':				
				break;

			case 'FacultyReports2':
				include 'Reports-Form 138.php';
				break;

			case 'FacultyAccount0':					
				include'account-faculty.php';
				break;
				
//--------------------------Faculty (not advier)-------------------------------------------------------------

			case 'FacultyRanking1':					
				include'ranking.php';
				break;

			case 'FacultyUnder Probation2':
				include 'under-probation.php';
				break;

//--------------------------Guidance-------------------------------------------------------------------
			
			case 'GuidanceStudents Record0':
				include 'students-record.php';
				break;

			case 'GuidanceViolation1':
				include 'Violation.php';
				break;	

			case 'GuidanceUnder Probation2':
				include 'under-probation.php';
				break;

			case 'GuidanceAccount0':
				include 'account-guidance.php';
				break;

//--------------------------Student-------------------------------------------------------------------
		
			case 'StudentHome0':
				include 'student-page.php';
				break;

			case 'StudentOnline Grades1':
				include 'online-grades.php';
				break;

			case 'StudentRequirement2':
				include 'stud-req.php';
				break;

			case 'StudentOffenses3':
				include 'stud-offenses.php';
				break;
				
			case 'StudentAccount0':
				include 'account-student.php';
				break;

			default:
				# code...
				break;
		
//--------------------------Parent-------------------------------------------------------------------
		
			case 'ParentHome0':
				include 'parent-page.php';
				break;

			case 'ParentOnline Grades1':
				include 'onlinep-grades.php';
				break;

			case 'ParentRequirement2':
				include 'studp-req.php';
				break;

			case 'ParentOffenses3':
				include 'studp-offenses.php';
				break;
				
			case 'ParentAccount0':
				include 'accountp-parent.php';
				break;

			default:
				# code...
				break;
		}
	}
?>