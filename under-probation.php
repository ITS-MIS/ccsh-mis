<link rel="stylesheet" href="css/ccshs-header.css">
	
	<?php
		getDatatablesLink();
		getModalPageLink();		
		getDatatablesScript();
	?>
		
<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Under Probation</label>
			</div>
			<div class="col-md-1"></div>
		</div>
	
		<div class="row">
			<form method="post">
				<table id="under-probation" /*data-page-length="25"*/ class="display" cellspacing="0" width="100%" style="background-color:gold">
					<thead>
						<tr>
							<th>LRN</th>
							<th>Name</th>
						</tr>
					</thead>
					
					<tbody>
					
					<?php 
						
						include('current-year.php');
					
						$sql_search="SELECT lrn, lastname, firstname, middlename FROM tbl_underprobation a left join tbl_studentinfo b  on a.student_id=b.id
										WHERE b.remarks!='EXPELLED' AND b.remarks!='TO'";
						
						$result = mysql_query($sql_search) or die(mysql_error());
						
							if (mysql_num_rows($result)>0) { 
							
								while(($row = mysql_fetch_assoc($result))!= null){
									$lrn = $row['lrn'];
									$lastname = $row['lastname'];
									$firstname = $row['firstname'];
									$middlename = $row['middlename'];
					?>
					
									<tr data-lrn="<?php echo $lrn ?>" class="data">
										<td><?php echo $lrn;  ?></td>
										<td><?php echo $lastname .", ". $firstname ." ". $middlename;  ?></td>
									</tr>
						
						<?php
								}
							}
						?>
					
					
					</tbody>
				</table>
			</form>
		</div>

		<div id="da" class="modal fade">
			<div id="yearlevel-content">
				<div class="modal-dialog modal-lg">
			        <div class="modal-content">
			           
			   		</div>
			   	</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">

$('#under-probation').css('visibility','visible');
	
	$(document).ready(function() {
		var table=$('#under-probation').dataTable();
 		
    	$('#under-probation tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }


			$(".modal-content").html('');
			$('#da').modal('show');
			
			 $.post('modal-under-probation.php',{lrn:$(this).attr('data-lrn')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ) 
			var lrn= $(this).attr('data-lrn');
			 console.info(lrn);

	
		});
	} );

</script>