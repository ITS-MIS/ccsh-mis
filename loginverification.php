 <?php
	include 'connect.php';
	
	getMeta();
	getBsLink();
	getCcshsPageLink();
	getJqueryScript();
	getBsScript();

	if( !isset($_POST['emp_no']) || empty($_POST['emp_no']) || !isset($_POST['pass']) || empty($_POST['pass'])){		
?>

		<div class="alert alert-warning" role="alert">Check username and password or fill all fields
			<a href="index.php"><input type="submit" class="btn btn-warning" value="Try Again" name="btnLoad" id="btnload"></a>
		</div>
						
		<?php
	}

	else {
		$user = verify_user_pass($_POST['emp_no'], $_POST['pass']);
   	 	
		if ($user) {
   	 			set_logged_in_user($user);
   	 
				//audit trail login

   	 			$emp_type = 'Employee';
				$table = 'tbl_loginemployee';
				$action = 'Login';
				include('audit-query.php');

				?>
				<div class="alert alert-success" role="alert">Login success!</div>
				
				<?php redirect('index2.php'); ?>
				
				<?php
		} else {		
				
			redirect('index.php?loginfailed');
		}
	}