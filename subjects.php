<style type="text/css">
	.error {color: #FF0000;}
</style>

<?php	
	getDatatablesLink();
	getDatatablesScript();

	include('current-year.php');
?>

<div class="container-fluid">

	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">List of Subject Electives</label>
			</div>
			<div class="col-md-1"></div>
	</div>

	<!--<button data-target="#insertmodal" data-toggle="modal" type="button" class="btn btn-success success">Add Subject</button>-->
	<button data-target="#generatecsymodal" data-toggle="modal" type="button" class="btn btn-success success"> <font face="arial">Generate Curriculum</font></button>

	<!--<button data-target="#generatesubjectmodal" data-toggle="modal" type="button" class="btn btn-success success"> <font face="arial">Generate Subjects</font></button>-->	

	<div class="bs-studgrade"><!--=============START of TABS--======================-->
		    <ul class="nav nav-tabs" id="subjectTab">
		    	<li class="active"><a href="#grade7">Grade 7</a></li>
		        <li class><a href="#grade8">Grade 8</a></li>
		        <li class><a href="#grade9">Grade 9</a></li>
		        <li class><a href="#grade10">Grade 10</a></li>
		    </ul><br>

		    <div class="tab-content">
		        <div id="grade7" class="tab-pane fade in active"><!--=============START of  TAB7--======================-->
		        	<form method="post">
						<table id="subject7" class="display" cellspacing="0" width="100%" style="background-color:gold;">
					        <thead>
					            <tr>
					            	<th style="display: none;">ID</th>
					                <th>Subject Code</th>
					                <th>Title</th>
					                <th>Description</th>
					                <th>Unit</th>
					                <th>Year Level</th>
					            </tr>					                
					        </thead>
					 
					        <tbody>

						        <?php  

									$query = "SELECT a.*, c.year_level FROM tbl_subject a 
												LEFT JOIN tbl_subject_yearlevel b ON a.subject_code=b.subject_code
												LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id WHERE a.subject_code LIKE 'ELEC%' AND b.year_id=7 AND b.sy_id=$sy_id ORDER BY subject_title";

									$result = mysql_query($query) or die(mysql_error());

									if(mysql_num_rows($result) > 0) {
																	
										while ($row = mysql_fetch_assoc($result)) {

											$id = $row['id'];
											$subject_code = $row['subject_code'];
											$subject_title = $row['subject_title'];
											$subject_desc = $row['subject_desc'];
											$subject_unit = $row['subject_unit'];
											$year_level = $row['year_level'];
									
								?>

					            <tr data-toggle="modal" data-id="<?php echo $id ?>" data-code="<?php echo $subject_code ?>"
            						data-title="<?php echo $subject_title ?>" data-desc="<?php echo $subject_desc ?>" 
            						data-unit="<?php echo $subject_unit ?>" data-year="<?php echo $year_level ?>"
            						data-target="#subjectmodal" class="data">
					                
					        		<td style="display: none;"></td>
					                <td><?php echo $subject_code; ?></td>
					                <td><?php echo $subject_title; ?></td>
					                <td><?php echo $subject_desc; ?></td>
					                <td><?php echo $subject_unit; ?></td>
					               	<td><?php echo $year_level; ?></td>
					            </tr>
							
								<?php
							            }
							        }
							    ?>

							</tbody>

						</table>
					</form><br>	
					
		        </div><!--===============================================END of TAB7=========================-->
		       
		        <div id="grade8" class="tab-pane fade in"><!--=============START of  TAB8--======================-->
		        	<form method="post">
						<table id="subject8" class="display" cellspacing="0" width="100%" style="background-color:gold;">
					     
					        <thead>
					            <tr>
					            	<th style="display: none;">ID</th>
					                <th>Subject Code</th>
					                <th>Title</th>
					                <th>Description</th>
					                <th>Unit</th>
					                <th>Year Level</th>
					            </tr>
					                
					        </thead>
					 
					        <tbody>

						        <?php  

									$query = "SELECT a.*, c.year_level FROM tbl_subject a 
												LEFT JOIN tbl_subject_yearlevel b ON a.subject_code=b.subject_code
												LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id WHERE a.subject_code LIKE 'ELEC%' AND b.year_id=8 AND b.sy_id=$sy_id ORDER BY subject_title";

									$result = mysql_query($query) or die(mysql_error());

									if(mysql_num_rows($result) > 0) {
																	
										while ($row = mysql_fetch_assoc($result)) {

											$id = $row['id'];
											$subject_code = $row['subject_code'];
											$subject_title = $row['subject_title'];
											$subject_desc = $row['subject_desc'];
											$subject_unit = $row['subject_unit'];
											$year_level = $row['year_level'];
									
								?>

					            <tr data-toggle="modal" data-id="<?php echo $id ?>" data-code="<?php echo $subject_code ?>"
					            						data-title="<?php echo $subject_title ?>" data-desc="<?php echo $subject_desc ?>" 
					            						data-unit="<?php echo $subject_unit ?>" data-year="<?php echo $year_level ?>"
					            						data-target="#subjectmodal" class="data">
					                
					        		<td style="display: none;"></td>
					                <td><?php echo $subject_code; ?></td>
					                <td><?php echo $subject_title; ?></td>
					                <td><?php echo $subject_desc; ?></td>
					                <td><?php echo $subject_unit; ?></td>
					               	<td><?php echo $year_level; ?></td>
					            </tr>
							
								<?php
							            }
							        }
							    ?>

							</tbody>

						</table>
					</form><br>	
					
		        </div><!--===============================================END of TAB8=========================-->

		        <div id="grade9" class="tab-pane fade in"><!--=============START of  TAB9--======================-->
		        	<form method="post">
						<table id="subject9" class="display" cellspacing="0" width="100%" style="background-color:gold;">
					     
					        <thead>
					            <tr>
					            	<th style="display: none;">ID</th>
					                <th>Subject Code</th>
					                <th>Title</th>
					                <th>Description</th>
					                <th>Unit</th>
					                <th>Year Level</th>
					            </tr>
					                
					        </thead>
					 
					        <tbody>

						        <?php  

									$query = "SELECT a.*, c.year_level FROM tbl_subject a 
												LEFT JOIN tbl_subject_yearlevel b ON a.subject_code=b.subject_code
												LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id WHERE a.subject_code LIKE 'ELEC%' AND b.year_id=9 AND b.sy_id=$sy_id ORDER BY subject_title";

									$result = mysql_query($query) or die(mysql_error());

									if(mysql_num_rows($result) > 0) {
																	
										while ($row = mysql_fetch_assoc($result)) {

											$id = $row['id'];
											$subject_code = $row['subject_code'];
											$subject_title = $row['subject_title'];
											$subject_desc = $row['subject_desc'];
											$subject_unit = $row['subject_unit'];
											$year_level = $row['year_level'];
									
								?>

					            <tr data-toggle="modal" data-id="<?php echo $id ?>" data-code="<?php echo $subject_code ?>"
					            						data-title="<?php echo $subject_title ?>" data-desc="<?php echo $subject_desc ?>" 
					            						data-unit="<?php echo $subject_unit ?>" data-year="<?php echo $year_level ?>"
					            						data-target="#subjectmodal" class="data">
					                
					        		<td style="display: none;"></td>
					                <td><?php echo $subject_code; ?></td>
					                <td><?php echo $subject_title; ?></td>
					                <td><?php echo $subject_desc; ?></td>
					                <td><?php echo $subject_unit; ?></td>
					               	<td><?php echo $year_level; ?></td>
					            </tr>
							
								<?php
							            }
							        }
							    ?>

							</tbody>

						</table>
					</form><br>	
					
		        </div><!--===============================================END of TAB9=========================-->

		        <div id="grade10" class="tab-pane fade in"><!--=============START of  TAB10--======================-->
		        	<form method="post">
						<table id="subject10" class="display" cellspacing="0" width="100%" style="background-color:gold;">
					     
					        <thead>
					            <tr>
					            	<th style="display: none;">ID</th>
					                <th>Subject Code</th>
					                <th>Title</th>
					                <th>Description</th>
					                <th>Unit</th>
					                <th>Year Level</th>
					            </tr>
					                
					        </thead>
					 
					        <tbody>

						        <?php  

									$query = "SELECT a.*, c.year_level FROM tbl_subject a 
												LEFT JOIN tbl_subject_yearlevel b ON a.subject_code=b.subject_code
												LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id WHERE a.subject_code LIKE 'ELEC%' AND b.year_id=10 AND b.sy_id=$sy_id ORDER BY subject_title";

									$result = mysql_query($query) or die(mysql_error());

									if(mysql_num_rows($result) > 0) {
																	
										while ($row = mysql_fetch_assoc($result)) {

											$id = $row['id'];
											$subject_code = $row['subject_code'];
											$subject_title = $row['subject_title'];
											$subject_desc = $row['subject_desc'];
											$subject_unit = $row['subject_unit'];
											$year_level = $row['year_level'];
									
								?>

					            <tr data-toggle="modal" data-id="<?php echo $id ?>" data-code="<?php echo $subject_code ?>"
					            						data-title="<?php echo $subject_title ?>" data-desc="<?php echo $subject_desc ?>" 
					            						data-unit="<?php echo $subject_unit ?>" data-year="<?php echo $year_level ?>"
					            						data-target="#subjectmodal" class="data">
					                
					        		<td style="display: none;"></td>
					                <td><?php echo $subject_code; ?></td>
					                <td><?php echo $subject_title; ?></td>
					                <td><?php echo $subject_desc; ?></td>
					                <td><?php echo $subject_unit; ?></td>
					               	<td><?php echo $year_level; ?></td>
					            </tr>
							
								<?php
							            }
							        }
							    ?>

							</tbody>

						</table>
					</form><br>	
					
		        </div><!--===============================================END of TAB10=========================-->
		    </div>
		</div><!--=============END of TABS--======================-->

		<br>
		
		

</div><!--=============END of CONTAINE FLUID--======================-->

<div id="subjectmodal" class="modal fade">
	<div id="subject-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Subject Information</h4>
	            </div>
	            
	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="subjects-submit.php">
						
						<br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id" name="id" >
							</div>
					  	</div>
					
						<div class="form-group">
							<label for="code" class="col-sm-4 control-label">Code</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="subject_code" name="subject_code" placeholder="Subject Code" maxlength="7" onkeypress="return isPassword(event)" onkeyup="caps(this)" readonly>
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="title" class="col-sm-4 control-label">Title</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="subject_title" name="subject_title" placeholder="Subject Name" maxlength="50" onkeypress="return isTitle(event)" readonly>
							</div>
						</div>

						<div class="form-group">
							<label for="desc" class="col-sm-4 control-label">Description</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="subject_desc" name="subject_desc" placeholder="Subject Description" maxlength="50" onkeypress="return isTitle(event)" readonly>
							</div>
						</div>

						<div class="form-group">
							<label for="unit" class="col-sm-4 control-label">Unit</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="subject_unit" name="subject_unit" placeholder="Subject Unit" maxlength="1" onkeypress="return isNumber(event)" readonly>
							</div>
						</div>

						<div class="form-group">
							<label for="yearlevel" class="col-sm-4 control-label">Year Level</label>
							<div class="col-sm-7">
						
								<select style="width: 320px;" class="form-control" id="year_level" placeholder="Year Level" name="year_level" >
								
										<?php
											$yl = "SELECT year_level FROM tbl_yearlevel";
											$result = mysql_query($yl);
										?>
										
									
									<?php
										while($row = mysql_fetch_array($result)) {
									?> 
											<option><?php echo $row['year_level']; ?> </option>
									<?php		
										}
									?>
									
								</select>
							</div>
						</div>

						<br>
						
						<div class="form-group">

							<div class="col-sm-4"></div>
							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#update" data-toggle="modal" type="button" class="btn btn-success success" >Update</button>
								<input type="hidden"  class="form-control" id="deleteid" >						
								<!--<button  data-target="#delete" data-toggle="modal" type="button" class="btn btn-danger warning confirm-delete-modal" name="btnDelete">Delete</button>-->
							</div>
							
							<div class="col-sm-4"></div>
							
						</div>

						<div class="form-group">															
						
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
							
						</div>
						
					</form>
				</div>


			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>
	

<!--================ Add Record Modal =================================================================-->
<div id="insertmodal" class="modal fade">
	<div id="subject-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Add Subject Record</h4>
	            </div>
	            
	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="subjects-submit.php">
						
						<br><br>
					
						<div class="form-group">
							<label for="code" class="col-sm-4 control-label">Code</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="asubject_code" name="subject_code" placeholder="Subject Code" maxlength="7" onkeypress="return isPassword(event)" onkeyup="caps(this)" required>
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="title" class="col-sm-4 control-label">Title</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="asubject_title" name="subject_title" placeholder="Subject Name" maxlength="50" onkeypress="return isTitle(event)" required>
							</div>
						</div>

						<div class="form-group">
							<label for="desc" class="col-sm-4 control-label">Description</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="asubject_desc" name="subject_desc" placeholder="Subject Description" maxlength="50"onkeypress="return isTitle(event)" required>
							</div>
						</div>

						<div class="form-group">
							<label for="unit" class="col-sm-4 control-label">Unit</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="asubject_unit" name="subject_unit" placeholder="Subject Unit" maxlength="1"  onkeypress="return isNumber(event)" required>
							</div>
						</div>

						<div class="form-group">
							<label for="yearlevel" class="col-sm-4 control-label">Year Level</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
						
								<select style="width: 320px;" class="form-control" id="ayear_level" placeholder="Year Level" name="year_level" required>
								
										<?php
											$yl = "SELECT year_level FROM tbl_yearlevel";
											$result = mysql_query($yl);
										?>
										
									
									<?php
										while($row = mysql_fetch_array($result)) {
									?> 
											<option><?php echo $row['year_level']; ?> </option>
									<?php		
										}
									?>
									
								</select>
							</div>
						</div>

						<br>
						
						<div class="form-group">

							<div class="col-sm-4"></div>
							
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnInsert" onclick="return confirm('Are you sure you want to submit?');">Add</button>
								<!--<button data-target="#insert" data-toggle="modal" type="button" class="btn btn-success success">Add</button>-->
								<button type="reset" class="btn btn-default">Reset</button>
							</div>
							
							<div class="col-sm-4"></div>
							
						</div>

						<div class="form-group">															
						
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
							
						</div>
						
					</form>
				</div>

				<p><span class="error">&nbsp;* required field</span></p>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================ Insert Confirm Modal =================================================================-->
<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Record</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to add the record?</p>               
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="subjects-submit.php">

            		<input type="hidden" id="insertcode" name="subject_code">
            		<input type="hidden" id="inserttitle" name="subject_title"  >		
					<input type="hidden" id="insertdesc" name="subject_desc" >							
					<input type="hidden" id="insertunit" name="subject_unit"  >												
					<input type="hidden" id="insertyear" name="year_level"> 					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-insert-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-insert" name="btnInsert">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>
                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="subjects-submit.php">

            		<input type="hidden" id="updateid" name="id">
            		<input type="hidden" id="updatecode"name="subject_code">
            		<input type="hidden" id="updatetitle" name="subject_title"  >		
					<input type="hidden" id="updatedesc" name="subject_desc" >							
					<input type="hidden" id="updateunit" name="subject_unit"  >												
					<input type="hidden" id="updateyear" name="year_level"> 					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnUpdate">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================ Delete Confirm Modal =================================================================-->
<div id="delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Do you want to delete the record?</p>
                <p class="text-warning"><small>If you proceed, deletion will be immediate!</small></p>
            </div>

            <div class="modal-footer">
            
            	<form method="post" action="subjects-submit.php">
		            <input type="hidden"  id="deleteid2" name="id">
		            <input type="hidden"  id="deletecode" name="subject_code">
		            <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			
		   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-danger warning" name="btnDelete">Confirm</button>
                </form>

            </div>

        </div>
    </div>
</div>
 
 <!--================ generateSubjectmodal Confirm Modal =================================================================-->
<div id="generatesubjectmodal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Generate Subjects</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to generate a new list of subjects for new SY?</p>               
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="generate-subject-yearlevel.php">

            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-insert-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-insert" name="btnGenerateSection">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>	

<!--================ generateCurriculumSY Confirm Modal =================================================================-->
<div id="generatecsymodal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Generate Curriculum</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to generate a new list of curriculum for new SY?</p>               
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="generate-curriculum-sy.php">

            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-insert-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-insert" name="btnGenerateSection">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>	

<script type="text/javascript">

	$(document).ready(function() {
		var table=$('#subject7').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#subject7 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#subject8').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#subject8 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#subject9').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#subject9 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#subject10').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#subject10 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );

	$(function(){
	 	console.info('getIdFromRow');
	    $('#subjectmodal').modal({
	        keyboard: true,
	        show:false,


	    }).on('show.bs.modal', function(){
	        var getIdFromRow = $(event.target).closest('tr').data('id');
				console.info(getIdFromRow);
     			$('#id').val(getIdFromRow);

	        var getcodeFromRow = $(event.target).closest('tr').data('code');
	         	console.info(getcodeFromRow);
	         	$('#subject_code').val(getcodeFromRow);
	         	$('#deletecode').val(getcodeFromRow);
	        
	        var gettitleFromRow = $(event.target).closest('tr').data('title');
	         	console.info(gettitleFromRow);
	         	$('#subject_title').val(gettitleFromRow);
	        
	        var getdescFromRow = $(event.target).closest('tr').data('desc');
	         	console.info(getdescFromRow);
	         	$('#subject_desc').val(getdescFromRow);
	        
	        var getunitFromRow = $(event.target).closest('tr').data('unit');
	         	console.info(getunitFromRow);
	         	$('#subject_unit').val(getunitFromRow);

	        var getyearFromRow = $(event.target).closest('tr').data('year'); 
	        	console.info(getyearFromRow);
	        	$('#year_level').val(getyearFromRow);


			$('#deleteid').val(getIdFromRow);
			         	console.info(getIdFromRow);

				        
		    });
		});

	$(function(){
    	$('#insert').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getcodeFromRow = $('#asubject_code').val();;	         
	         	$('#insertcode').val(getcodeFromRow);
	         
	        var gettitleFromRow = $('#asubject_title').val();;
	         	$('#inserttitle').val(gettitleFromRow);	 

	        var getdescFromRow = $('#asubject_desc').val();;
	         	$('#insertdesc').val(getdescFromRow);

	        var getunitFromRow = $('#asubject_unit').val();;
	         	$('#insertunit').val(getunitFromRow);

	        var getyearFromRow = $('#ayear_level').val();;
	         	$('#insertyear').val(getyearFromRow); 
	 			
		});
	});

  	$(function(){
    	$('#delete').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){
         	var getIdFromRow = $('#deleteid').val();
       
 			$('#deleteid2').val(getIdFromRow); 			
	        console.info(getIdFromRow);
		});
	});
  	
  	$(function(){
    	$('#update').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getIdFromRow =$('#id').val();;
				$('#updateid').val(getIdFromRow);

	        var getcodeFromRow = $('#subject_code').val();;	         
	         	$('#updatecode').val(getcodeFromRow);
	         
	        var gettitleFromRow = $('#subject_title').val();;
	         	$('#updatetitle').val(gettitleFromRow);	 

	        var getdescFromRow = $('#subject_desc').val();;
	         	$('#updatedesc').val(getdescFromRow);

	        var getunitFromRow = $('#subject_unit').val();;
	         	$('#updateunit').val(getunitFromRow);

	        var getyearFromRow = $('#year_level').val();;
	         	$('#updateyear').val(getyearFromRow); 
	 			
		        console.info(getIdFromRow);
		});
	});

	$(document).ready(function(){ 
    	$("#subjectTab a").click(function(e){
    		e.preventDefault();
    		$(this).tab('show');
    	});
	});

</script>

<script type="text/javascript">
	jQuery(document).ready(function(){
	jQuery(".chosen").chosen();
});
</script>