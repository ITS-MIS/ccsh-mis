<?php

	if(isset($_POST['info'])){
		$info=$_POST['info'];
	}
	
	$errlist= array('lrn' => 'LRN is required',
					'year_level'=>'Year Level is required',
					'lastname' => 'Last Name is required',
					'firstname' => 'First Name is required',
					'middlename' => 'Middle Name is required',
					'gender' => 'Gender is required',

					'birthdate' => 'Birthdate is required',
					'mothertounge' => 'Mothertounge is required',
					'ip' => 'Ethnic Group is required',
					'religion' => 'Religion is required',
					'addnost' => 'Home no. and Street is required',

					'brgy' => 'Barangay is required',
					'city' => 'City is required',
					'province' => 'Province is required',
					'flastname' => 'Father Lastname is required',
					'ffirstname' => 'Father Firstname is required',

					'fmiddlename' => 'Father Middlename is required',
					'mlastname' => 'Mother Lastname is required',
					'mfirstname' => 'Mother Firstname is required',
					'mmiddlename' => 'Mother Middlename is required',
					'guardianname' => 'Guardian is required',
					'guardianmail' => 'Guardian email is required',

					'relationship' => 'Relationship is required', 
					'contactnumber' => 'Contact Number is required', 
					'gwa' => 'GWA is required', 
					'school' => 'Last School attended is required' );


	$emptyErr=array();

	// Control variables
	$app_state = "empty";  //empty, processed, logged in
	$valid = 0;

	// Validate input and sanitize
	if ($_SERVER['REQUEST_METHOD']== "POST") {
	  

		foreach ($info as $key => $value) {
	  		if (empty($value)) {
				$emptyErr[$key]= $errlist[$key];  	
   			} else {
		      	$valid++;
	   		}		  	
		}

		if ($valid >= 18) {

				$test=get_db("SELECT lastname from tbl_studentinfo WHERE lrn={$info['lrn']} ");
				$test=$test['lastname'];
				
				if(count($test)==1) {
					$confirm=("LRN " .$info['lrn']. " is not available!" );
					echo "<script type='text/javascript'>alert(\"$confirm\");</script>";

				}
				else{
		      $app_state = "processed";
		   }
		}
	}

	// Sanitize data
	function test_input($data) {

		$data = trim($data);
	   	$data = stripslashes($data);
	   	$data = htmlspecialchars($data);
	   	return $data;
	}	