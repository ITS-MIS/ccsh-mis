<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
	<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<label id="header">List of Year Level</label>
			</div>
			<div class="col-md-4"></div>
		</div>	

	<button data-target="#insertmodal" data-toggle="modal" type="button" class="btn btn-success success">Add New Year Level</button>

	<div class="row"><br>
		<form method="post">
			<table id="year" class="display" cellspacing="0" width="100%" style="background-color:gold;">		     
		        <thead>
		            <tr>
		            	<th style="display: none;">ID</th>
		                <th>Year ID</th>
		                <th>Year Level</th>
		                <th>Description</th>
		            </tr>		                
		        </thead>	 

		        <tbody>
			        <?php
						$query = "SELECT * FROM tbl_yearlevel";
						$result = mysql_query($query) or die(mysql_error());

						if(mysql_num_rows($result) > 0) {													
							while ($row = mysql_fetch_assoc($result)) {
								$id = $row['id'];
								$year_id = $row['year_id'];
								$year_level = $row['year_level'];
								$year_desc = $row['year_desc'];								
					?>

		            <tr data-toggle="modal" data-id="<?php echo $id ?>" data-yearid="<?php echo $year_id ?>"
		            						data-level="<?php echo $year_level ?>" data-desc="<?php echo $year_desc ?>" 
		            						data-target="#yearmodal" class="data">
		                
		                <td style="display: none;"></td>
		                <td><?php echo $year_id; ?></td>
		                <td><?php echo $year_level; ?></td>
		                <td><?php echo $year_desc; ?></td>		                
		            </tr>
				
					<?php
				            }
				        }
				    ?>

				</tbody>

			</table>
		</form><br>
	</div>
</div>

<div id="yearmodal" class="modal fade">
	<div id="yearlevel-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Year Level</h4>
	            </div>
	            

	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="year-level-submit.php">
						
						<br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id" name="id" >
							</div>
					  	</div>
					
						<div class="form-group">
							<label for="yearid" class="col-sm-4 control-label">Year ID</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="year_id" name="year_id" placeholder="Year ID" maxlength="2" onkeypress="return isNumber(event)">
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="level" class="col-sm-4 control-label">Year Level</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="year_level" name="year_level" placeholder="Year Level" maxlength="20" onkeypress="return isTitle(event)">
							</div>
						</div>

						<div class="form-group">
							<label for="desc" class="col-sm-4 control-label">Description</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="year_desc" name="year_desc" placeholder="Year Level Description" maxlength="20" onkeypress="return isTitle(event)">
							</div>
						</div>

						<br>
						
						<div class="form-group">

							<div class="col-sm-4"></div>
							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#update" data-toggle="modal" type="button" class="btn btn-success success"  >Update</button>				
								<button  data-target="#delete" data-toggle="modal" type="button" class="btn btn-danger warning confirm-delete-modal" name="btnDelete">Delete</button>							
							</div>
							
							<div class="col-sm-4"></div>
							
						</div>
					</form>	
				
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================ Add Record Modal =================================================================-->
<div id="insertmodal" class="modal fade">
	<div id="yearlevel-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Year Level</h4>
	            </div>	            

	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="year-level-submit.php"><br><br>
					
						<div class="form-group">
							<label for="yearid" class="col-sm-4 control-label">Year ID</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="ayear_id" name="year_id" placeholder="Year ID" maxlength="2" onkeypress="return isNumber(event)">
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="level" class="col-sm-4 control-label">Year Level</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="ayear_level" name="year_level" placeholder="Year Level" maxlength="20" onkeypress="return isTitle(event)">
							</div>
						</div>

						<div class="form-group">
							<label for="desc" class="col-sm-4 control-label">Description</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="ayear_desc" name="year_desc" placeholder="Year Level Description" maxlength="20" onkeypress="return isTitle(event)">
							</div>
						</div>

						<br>
						
						<div class="form-group">

							<div class="col-sm-4"></div>
							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#insert" data-toggle="modal" type="button" class="btn btn-success success">Add</button>
								<button type="reset" class="btn btn-default">Reset</button>								
							</div>
							
							<div class="col-sm-4"></div>
							
						</div>
					</form>	
				
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================ Insert Confirm Modal =================================================================-->
<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>
                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="year-level-submit.php">

            		<input type="hidden" id="insertyid"name="year_id">
            		<input type="hidden" id="insertlevel" name="year_level" >
            		<input type="hidden" id="insertdesc" name="year_desc" >		 					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnInsert">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>
                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="year-level-submit.php">

            		<input type="hidden" id="updateid" name="id">
            		<input type="hidden" id="updateyid"name="year_id">
            		<input type="hidden" id="updatelevel" name="year_level" >
            		<input type="hidden" id="updatedesc" name="year_desc" >		 					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnUpdate">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================ Delete Confirm Modal =================================================================-->
<div id="delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Do you want to delete the record?</p>
                <p class="text-warning"><small>If you proceed, deletion will be immediate!</small></p>
            </div>

            <div class="modal-footer">
            
            	<form method="post" action="year-level-submit.php">
		            <input type="hidden" class="form-control" id="deleteid" name="id">
		            <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			
		   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-danger warning" name="btnDelete">Confirm</button>
                </form>

            </div>

        </div>
    </div>
</div>
	
<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#year').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#year tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );


	 $(function(){
	    $('#yearmodal').modal({
	        keyboard: true,
	        show:false,


	    }).on('show.bs.modal', function(){
	        var getIdFromRow = $(event.target).closest('tr').data('id');
     			$('#id').val(getIdFromRow);

	        var getyearidFromRow = $(event.target).closest('tr').data('yearid');	      	
	         	$('#year_id').val(getyearidFromRow);

	        var getlevelFromRow = $(event.target).closest('tr').data('level');    
	         	$('#year_level').val(getlevelFromRow);

	        var getdescFromRow = $(event.target).closest('tr').data('desc');        
         		$('#year_desc').val(getdescFromRow);
	        
	    });
	});


	 $(function(){
    	$('#insert').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getyIdFromRow =$('#ayear_id').val();;
				$('#insertyid').val(getyIdFromRow);

			var getlevelFromRow =$('#ayear_level').val();;
				$('#insertlevel').val(getlevelFromRow);
	 		
	 		var getdescFromRow =$('#ayear_desc').val();;
				$('#insertdesc').val(getdescFromRow);

		});
	});


  	$(function(){
    	$('#delete').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){
         	var getIdFromRow = $('#id').val();
 				$('#deleteid').val(getIdFromRow); 			
	        	console.info(getIdFromRow);
		});
	});

  	
  	$(function(){
    	$('#update').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

    		var getIdFromRow =$('#id').val();;
				$('#updateid').val(getIdFromRow);

	        var getyIdFromRow =$('#year_id').val();;
				$('#updateyid').val(getyIdFromRow);

			var getlevelFromRow =$('#year_level').val();;
				$('#updatelevel').val(getlevelFromRow);
	 		
	 		var getdescFromRow =$('#year_desc').val();;
				$('#updatedesc').val(getdescFromRow);
	 			
		        console.info(getIdFromRow);
		});
	});

</script>