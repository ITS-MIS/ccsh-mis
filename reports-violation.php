<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');
	
	date_default_timezone_set('Asia/Taipei');

	$lrn=$_POST['lrn'];
	
	$currentmonth = date('F');
	$cmonth = date('m');
	$currentyear = date('Y');
	$currentday = date('d');
	$currentdate=$currentyear."-".$cmonth."-".$currentday;
	$day=1;

	$pdf = new FPDI();

	$pdf->AddPage('P','a5'); 

	$pageCount = $pdf->setSourceFile("forms/violation.pdf");
	$tplIdx = $pdf->importPage(1); 

	$pdf->useTemplate($tplIdx, 1.5, 0, 0, 0, true); 

// schoolyear-------------------------------------------------------------

	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(110, 21);
	$pdf->SetFont('Arial','', '9');
	$pdf->Write(0, $sy);

//month---------------------------------------------------------------------

$pdf->SetXY(18, 180);
$pdf->SetFont('Arial','', '9');
$pdf->Write(0, $currentdate);

//student--------------------------------------------------------------------
	$result=mysql_query("SELECT a.lrn, lastname, firstname, c.offense_desc, d.penalty_desc,a.remarks FROM tbl_guidancerecord a, tbl_studentinfo b,
	tbl_offense c, tbl_penalty d
	WHERE a.lrn=b.lrn and a.offense_id=c.offense_id and a.lrn=$lrn  ORDER BY a.id DESC LIMIT 1;");
	
	while($row=mysql_fetch_assoc($result)){
		$l=$row['lrn'];
		$pdf->SetXY(20, 40);
		$pdf->SetFont('Arial', 'b', '10');
	 	$pdf->Write(0, $l);
		
		$lastname=$row['lastname'];
		$pdf->SetXY(20, 55);
		$pdf->SetFont('Arial', 'b', '10');
	 	$pdf->Write(0, $lastname);


	 	$firstname=$row['firstname'];
		$pdf->SetXY(20, 73);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $firstname);

	
		
		$offense=$row['offense_desc'];
		$pdf->SetXY(20, 87);
		$pdf->SetFont('Arial', 'b', '8');
		$pdf->Write(0, $offense);

		$penalty=$row['penalty_desc'];
		$pdf->SetXY(20, 105);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $penalty);

		$remarks=$row['remarks'];
		$pdf->SetXY(20, 120);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $remarks);
	
		$pdf->Output();

	}