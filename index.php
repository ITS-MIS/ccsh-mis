<?php include'connect.php'; ?>

<!DOCTYPE html>
<html lang="en">
	<head>
	<link rel="icon" href="images/logo.ico" type="image/x-icon" sizes="64x64">
		<?php
			getMeta();
			getBsLink();
			getCcshsPageLink();
			getModalPageLink();
			
			getJqueryScript();
			getBsScript();
			getBsValidator();
				
		?>
		
		<title>CCSHS SIS</title>
		
		<style type="text/css"> 
			body{background-color: white;}
		</style>	
	</head>
	
	<body>
	
		<div id="container-navbar">
			<?php include('ccshs-nav.php'); ?>
		</div>
		
		<div id="container-fluid">
			<?php include('ccshs-homepage.php'); ?>
		</div>
		
		<div id="footer">
			<?php include('ccshs-footermain.php'); ?>
		</div>	

		<div id="myModal2" class="modal fade">
        	<div class="modal-dialog">
        		<div class="modal-content">
		            <div class="modal-header" style="background-color:gold;">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Employee Login</h4>
		            </div>
		            
		            <div class="modal-body modal-height"> 
						<form id="empForm" class="form-horizontal" method="post" action="loginverification.php"><br><br>
							<div class="form-group">
								<label for="usernamer" class="col-sm-4 control-label">Username</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="username" placeholder="Employee&#39;s No." name="emp_no">
								</div>
						  	</div>
						  
							<div class="form-group">
								<label for="inputpass" class="col-sm-4 control-label">Password</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" id="inputpass" placeholder="Password" name="pass">
								</div>
							</div><br>
							
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-10">
									<button type="submit" class="btn btn-success success left-margin" name="btnSubmit" >Login</button>
									<button type="reset" class="btn btn-default" name="reset" id="resetBtn1">Reset</button>
								</div>
							</div>							
						</form>
					</div>

				    <div class="modal-footer" style="background-color:gold;">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				      
			        </div>
	   			</div>
			</div>
        </div>
		
		<div id="myModal" class="modal fade">
			<div class="modal-dialog">
	        	<div class="modal-content">
		            <div class="modal-header" style="background-color:gold;">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Student Login</h4>
		            </div>

		            <div class="modal-body modal-height"> 
						<form id="studForm" class="form-horizontal" method="post" action="loginverification-student.php"><br><br>						
							<div class="form-group">
								<label for="usernamer" class="col-sm-4 control-label">Username</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="username" placeholder="LRN" name="lrn">
								</div>
						  	</div>
						  
							<div class="form-group">
								<label for="inputpass" class="col-sm-4 control-label">Password</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" id="inputpass" placeholder="Password" name="pass">
								</div>
							</div><br>
							
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-10">
									<button type="submit" class="btn btn-success success left-margin" name="btnSubmit" >Login</button>
									<button type="reset" class="btn btn-default" name="reset" id="resetBtn2">Reset</button>
								</div>
							</div>							
						</form>
					</div>

				    <div class="modal-footer" style="background-color:gold;">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				      
			        </div>
	   			</div>
	   		</div>
    	</div>

    	<div id="myModal3" class="modal fade">
			<div class="modal-dialog">
	        	<div class="modal-content">
		            <div class="modal-header" style="background-color:gold;">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Parents/Guardian Login</h4>
		            </div>

		            <div class="modal-body modal-height"> 
						<form id="parentForm" class="form-horizontal" method="post" action="loginverification-parent.php"><br><br>						
							<div class="form-group">
								<label for="usernamer" class="col-sm-4 control-label">Username</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="username" placeholder="Child&#39;s LRN" name="lrn2">
								</div>
						  </div>
						  
							<div class="form-group">
								<label for="inputpass" class="col-sm-4 control-label">Password</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" id="inputpass" placeholder="Password" name="pass">
								</div>
							</div><br>
							
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-10">
									<button type="submit" class="btn btn-success success left-margin" name="btnSubmit" >Login</button>
									<button type="reset" class="btn btn-default" name="reset" id="resetBtn3">Reset</button>
								</div>
							</div>							
						</form>
					</div>

				    <div class="modal-footer" style="background-color:gold;">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				      
			        </div>
	   			</div>
	   		</div>
    	</div>

	<script type="text/javascript">

	$(document).ready(function() {
    
	    $('#studForm').bootstrapValidator({
	        message: 'This value is not valid',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	            
	            lrn: {
	                message: 'The username is not valid',
	                validators: {
	                    notEmpty: {
	                        message: 'The username is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 1,
	                        max: 12,
	                        message: 'The username must not be more than 12 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[0-9]+$/,
	                        message: 'The username is your LRN'
	                    },
	                }
	            },
	            
	            pass: {
	                validators: {
	                    notEmpty: {
	                        message: 'The password is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 1,
	                        max: 20,
	                        message: 'The password must be more than 20 characters long'
	                    },
	                     regexp: {
	                        regexp: /^[a-zA-Z0-9_]+$/,
	                        message: 'The password only accepts letters, numbers and underscores'
	                    },
	                }
	            },
	        }
	    });
	    
	    $('#empForm').bootstrapValidator({
	        message: 'This value is not valid',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	            
	            emp_no: {
	                message: 'The username is not valid',
	                validators: {
	                    notEmpty: {
	                        message: 'The username is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 1,
	                        max: 12,
	                        message: 'The username must not be more than 12 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[0-9]+$/,
	                        message: 'The username is your LRN'
	                    },
	                }
	            },
	            
	            pass: {
	                validators: {
	                    notEmpty: {
	                        message: 'The password is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 1,
	                        max: 20,
	                        message: 'The password must be more than 20 characters long'
	                    },
	                     regexp: {
	                        regexp: /^[a-zA-Z0-9_]+$/,
	                        message: 'The password only accepts letters, numbers and underscores'
	                    },
	                }
	            },
	        }
	    });

		$('#parentForm').bootstrapValidator({
	        message: 'This value is not valid',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	            
	            lrn2: {
	                message: 'The username is not valid',
	                validators: {
	                    notEmpty: {
	                        message: 'The username is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 1,
	                        max: 12,
	                        message: 'The username must not be more than 12 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[0-9]+$/,
	                        message: 'The username is your childs LRN'
	                    },
	                }
	            },
	            
	            pass: {
	                validators: {
	                    notEmpty: {
	                        message: 'The password is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 1,
	                        max: 20,
	                        message: 'The password must be more than 20 characters long'
	                    },
	                     regexp: {
	                        regexp: /^[a-zA-Z0-9_]+$/,
	                        message: 'The password only accepts letters, numbers and underscores'
	                    },
	                }
	            },
	        }
	    });
		
			$('#resetBtn1').click(function() {
			        $('#empForm').data('bootstrapValidator').resetForm(true);
			    });

			$('#resetBtn2').click(function() {
			        $('#studForm').data('bootstrapValidator').resetForm(true);
			    });

			$('#resetBtn3').click(function() {
			        $('#parentForm').data('bootstrapValidator').resetForm(true);
			    });
	});
			
	</script>

	</body>
</html>