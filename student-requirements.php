<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10"><label id="header">List of Incomplete Requirements</label>
		<div class="col-sm-1"></div>
	</div>
	
	<div class="row"><br>
		<form method="post">
			<table id="student" class="display" cellspacing="0" width="100%" style="background-color:gold;">
		        <thead>
		            <tr>
		            	<th style="display: none;">ID</th>
		                <th>LRN</th>
		                <th>Name</th>
		                <th>Form 137</th>
						<th>Form 138</th>
		                <th>Certificate of Good Moral</th>
		                <th>NSO</th>
						<th>Medical Certificate</th>
						<th>ID Picture</th>		                
		            </tr>		                
		        </thead>
		 
		        <tbody>
			        <?php  
						$query = "SELECT a.student_id, lastname,firstname,middlename,nso,f137,f138,cgmc,idpic,medc FROM tbl_requirement a 
									LEFT JOIN tbl_studentinfo b on a.student_id=b.id WHERE nso=0 OR f137=0 OR f138=0 OR cgmc=0 OR idpic=0 OR medc=0";
						$result = mysql_query($query) or die(mysql_error());

						if(mysql_num_rows($result) > 0) {														
							while ($row = mysql_fetch_assoc($result)) {
								$lrn= $row['student_id'];
								$lastname = $row['lastname'];
								$firstname = $row['firstname'];
								$middlename = $row['middlename'];
								$nso= $row['nso'];
								$f138= $row['f138'];
								$f137= $row['f137'];
								$cgmc= $row['cgmc'];
								$idpic= $row['idpic'];
								$medc= $row['medc'];
					?>
		            <tr data-toggle="modal" data-lrn="<?php echo $lrn ?>"
						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
						data-middlename="<?php echo $middlename ?>" 
						data-nso="<?php echo $nso ?>" data-f137="<?php echo $f137 ?>" data-f138="<?php echo $f138 ?>"
						data-idpic="<?php echo $idpic ?>" data-cgmc="<?php echo $cgmc ?>" data-medc="<?php echo $medc ?>"
						data-target="#studentmodal" class="data">
		                
		                <td style="display: none;"></td>
		                <td><?php echo $lrn; ?></td>
		                <td><?php echo $lastname .', '. $firstname .' '. $middlename ?></td>		                
					 	<td style="text-align:center"><?php echo $f137; ?></td>
						<td style="text-align:center"><?php echo $f138; ?></td>
						<td style="text-align:center"><?php echo $cgmc; ?></td>
						<td style="text-align:center"><?php echo $nso; ?></td>
						<td style="text-align:center"><?php echo $medc; ?></td>
						<td style="text-align:center"><?php echo $idpic; ?></td>						
		            </tr>
				
					<?php
				            }
				        }
				    ?>
				</tbody>
			</table>
		</form>
	</div>

	<div class="row">
		<div class="col-sm-8">
			<br>	
			<label><i>Legend:</i></label> <br>
				<p>1-passed</p>
				<p>0-incomplete</p>
		</div>
	</div>
</div>

	<div id="da" class="modal fade">
		<div id="yearlevel-content">
			<div class="modal-dialog modal-lg">
		        <div class="modal-content">
		           
		   		</div>
		   	</div>
		</div>
	</div>

</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#student').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#student tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	        $(".modal-content").html('');
			$('#da').modal('show');

			$.post('modal-requirements.php',{lrn:$(this).attr('data-lrn'),id:$(this).attr('data-id')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ); 

			 
			var lrn= $(this).attr('data-lrn');
			var id= $(this).attr('data-id');
			 console.info(lrn);

	       
    	} );
	} );

</script>