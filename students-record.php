<?php	
	getDatatablesLink();
	getModalPageLink();
	getDatatablesScript();

	include('current-year.php');
?>

<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Student's Record <?php if (isset($_POST['clickedlrn']) )echo $clickedlrn; ?></label>
			</div>
			<div class="col-md-1"></div>
		</div>	

	<div class="bs-studgrade"><!--=============START of TABS--======================-->
		    <ul class="nav nav-tabs" id="guidanceTab">
		    	<li class="active"><a href="#grade7">Grade 7</a></li>
		        <li class><a href="#grade8">Grade 8</a></li>
		        <li class><a href="#grade9">Grade 9</a></li>
		        <li class><a href="#grade10">Grade 10</a></li>
		    </ul><br>

		    <div class="tab-content">
		        <div id="grade7" class="tab-pane fade in active"><!--=============START of  TAB7--======================-->
		        	<form method="post">
						<table id="student7" /*data-page-length='25'*/ class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>LRN</th>
									<th>Name</th>
								</tr>
							</thead>
							
							<tbody>
							
							<?php 
				
								$sql_search = "SELECT id, lrn, lastname, firstname, middlename from tbl_studentinfo WHERE lrn IN
												(SELECT lrn from tbl_studentstatus WHERE (remarks='New Student' OR remarks='Transferee' OR remarks = 'Old Student') AND sy_id=$sy_id AND year_id=7) 
												AND remarks!='EXPELLED' AND remarks!='TO' order by lastname asc, firstname asc";		
												
								$result = mysql_query($sql_search) or die('SQL Error :: '.mysql_error());
								
									if (mysql_num_rows($result)>0) { 
									
										while(($row = mysql_fetch_assoc($result))!= null){
											$lrn = $row['lrn'];
											$lastname = $row['lastname'];
											$firstname = $row['firstname'];
											$middlename = $row['middlename'];
							?>
							
											<tr data-id="<?php echo $id ?>" data-lrn="<?php echo $lrn ?>"
					            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
					            						data-middlename="<?php echo $middlename ?>"
					            						class="data">

												<td width="35%"><?php echo $lrn;  ?></td>
												<td><?php echo $lastname .", ". $firstname ." ". $middlename;  ?></td>
											</tr>
								
								<?php
										}
									}
								?>
							
							</tbody>
						</table>
					</form>
		        </div><!--===============================================END of TAB7=========================-->
		       		
		        <div id="grade8" class="tab-pane fade in"><!--=============START of  TAB8--======================-->
		        	<form method="post">
						<table id="student8" /*data-page-length='25'*/ class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>LRN</th>
									<th>Name</th>
								</tr>
							</thead>
							
							<tbody>
							
							<?php 
				
								$sql_search = "SELECT id, lrn, lastname, firstname, middlename from tbl_studentinfo WHERE lrn IN
												(SELECT lrn from tbl_studentstatus WHERE (remarks='New Student' OR remarks='Transferee' OR remarks = 'Old Student') AND sy_id=$sy_id AND year_id=8) 
												AND remarks!='EXPELLED' AND remarks!='TO' order by lastname asc, firstname asc";		
												
								$result = mysql_query($sql_search) or die('SQL Error :: '.mysql_error());
								
									if (mysql_num_rows($result)>0) { 
									
										while(($row = mysql_fetch_assoc($result))!= null){
											$lrn = $row['lrn'];
											$lastname = $row['lastname'];
											$firstname = $row['firstname'];
											$middlename = $row['middlename'];
							?>
							
											<tr data-id="<?php echo $id ?>" data-lrn="<?php echo $lrn ?>"
					            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
					            						data-middlename="<?php echo $middlename ?>"
					            						class="data">

												<td width="35%"><?php echo $lrn;  ?></td>
												<td><?php echo $lastname .", ". $firstname ." ". $middlename;  ?></td>
											</tr>
								
								<?php
										}
									}
								?>
							
							</tbody>
						</table>
					</form>
		        </div><!--===============================================END of TAB8=========================-->

		        <div id="grade9" class="tab-pane fade in"><!--=============START of  TAB9--======================-->
		        	<form method="post">
						<table id="student9" /*data-page-length='25'*/ class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>LRN</th>
									<th>Name</th>
								</tr>
							</thead>
							
							<tbody>
							
							<?php 
				
								$sql_search = "SELECT id, lrn, lastname, firstname, middlename from tbl_studentinfo WHERE lrn IN
												(SELECT lrn from tbl_studentstatus WHERE (remarks='New Student' OR remarks='Transferee' OR remarks = 'Old Student') AND sy_id=$sy_id AND year_id=9) 
												AND remarks!='EXPELLED' AND remarks!='TO' order by lastname asc, firstname asc";		
												
								$result = mysql_query($sql_search) or die('SQL Error :: '.mysql_error());
								
									if (mysql_num_rows($result)>0) { 
									
										while(($row = mysql_fetch_assoc($result))!= null){
											$lrn = $row['lrn'];
											$lastname = $row['lastname'];
											$firstname = $row['firstname'];
											$middlename = $row['middlename'];
							?>
							
											<tr data-id="<?php echo $id ?>" data-lrn="<?php echo $lrn ?>"
					            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
					            						data-middlename="<?php echo $middlename ?>"
					            						class="data">

												<td width="35%"><?php echo $lrn;  ?></td>
												<td><?php echo $lastname .", ". $firstname ." ". $middlename;  ?></td>
											</tr>
								
								<?php
										}
									}
								?>
							
							</tbody>
						</table>
					</form>
		        </div><!--===============================================END of TAB9=========================-->

		        <div id="grade10" class="tab-pane fade in"><!--=============START of  TAB10--======================-->
		        	<form method="post">
						<table id="student10" /*data-page-length='25'*/ class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>LRN</th>
									<th>Name</th>
								</tr>
							</thead>
							
							<tbody>
							
							<?php 
				
								$sql_search = "SELECT id, lrn, lastname, firstname, middlename from tbl_studentinfo WHERE lrn IN
												(SELECT lrn from tbl_studentstatus WHERE (remarks='New Student' OR remarks='Transferee' OR remarks = 'Old Student') AND sy_id=$sy_id AND year_id=10) 
												AND remarks!='EXPELLED' AND remarks!='TO' order by lastname asc, firstname asc";		
												
								$result = mysql_query($sql_search) or die('SQL Error :: '.mysql_error());
								
									if (mysql_num_rows($result)>0) { 
									
										while(($row = mysql_fetch_assoc($result))!= null){
											$lrn = $row['lrn'];
											$lastname = $row['lastname'];
											$firstname = $row['firstname'];
											$middlename = $row['middlename'];
							?>
							
											<tr data-id="<?php echo $id ?>" data-lrn="<?php echo $lrn ?>"
					            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
					            						data-middlename="<?php echo $middlename ?>"
					            						class="data">

												<td width="35%"><?php echo $lrn;  ?></td>
												<td><?php echo $lastname .", ". $firstname ." ". $middlename;  ?></td>
											</tr>
								
								<?php
										}
									}
								?>
							
							</tbody>
						</table>
					</form>
		        </div><!--===============================================END of TAB10=========================-->
		    </div>
		</div><!--=============END of TABS--======================-->

</div>
	
	<div id="da" class="modal fade">
		<div id="yearlevel-content">
			<div class="modal-dialog modal-lg">
		        <div class="modal-content">
		           
		   		</div>
		   	</div>
		</div>
	</div>
 		
<script type="text/javascript">

	
	$(document).ready(function() {
		var table=$('#student7').dataTable();
 		
    	$('#student7 tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }


			$(".modal-content").html('');
			$('#da').modal('show');

			 $.post('modal-guidance.php',{lrn:$(this).attr('data-lrn')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ) 
			var lrn= $(this).attr('data-lrn');
			 console.info(lrn);

	
		});
	} );


	$(document).ready(function() {
		var table=$('#student8').dataTable();
 		
    	$('#student8 tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }


			$(".modal-content").html('');
			$('#da').modal('show');

			 $.post('modal-guidance.php',{lrn:$(this).attr('data-lrn')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ) 
			var lrn= $(this).attr('data-lrn');
			 console.info(lrn);

	
		});
	} );


	$(document).ready(function() {
		var table=$('#student9').dataTable();
 		
    	$('#student9 tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }


			$(".modal-content").html('');
			$('#da').modal('show');

			 $.post('modal-guidance.php',{lrn:$(this).attr('data-lrn')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ) 
			var lrn= $(this).attr('data-lrn');
			 console.info(lrn);

	
		});
	} );


	$(document).ready(function() {
		var table=$('#student10').dataTable();
 		
    	$('#student10 tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }


			$(".modal-content").html('');
			$('#da').modal('show');

			 $.post('modal-guidance.php',{lrn:$(this).attr('data-lrn')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ) 
			var lrn= $(this).attr('data-lrn');
			 console.info(lrn);

	
		});
	} );

	$(document).ready(function(){ 
    	$("#guidanceTab a").click(function(e){
    		e.preventDefault();
    		$(this).tab('show');
    	});
	});

</script>