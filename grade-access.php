<?php
	include('current-year.php');
	include('quartercheck.php');
	
	getDatatablesLink();
	getDatatablesScript();
?>
<div 
		<?php if(!isset($_GET['request'])){ echo 'style="display:none"';}?> 
		class="alert alert-danger" role="alert">Request Failed: Double check existing access 
</div>
		
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<label id="header">Input Grade Access</label>
		</div>
		<div class="col-md-1"></div>
	</div>

	<br><br>
	<!--<button data-target="#insertmodal" data-toggle="modal" type="button" class="btn btn-success success">Add Grade Access</button>-->

	<div class="row center-block" style="width:600px;">
		<form method="post">
			<table id="faculty" class="display" cellspacing="0" width="100%" style="background-color:gold;">
				<thead>
					<tr>
						<th style="display: none;">ID</th>
						<th>Employee No.</th>
						<th>Name</th>
					</tr>
				</thead>
				
				<tbody>				
				<?php 
	
					$sql_search = "SELECT id, emp_no, lastname, firstname FROM tbl_facultyinfo WHERE emp_no!=1413914";
									
					$result = mysql_query($sql_search) or die('SQL Error :: '.mysql_error());
					
						if (mysql_num_rows($result)>0) { 
						
							while(($row = mysql_fetch_assoc($result))!= null){
								$id = $row['id'];
								$emp_no = $row['emp_no'];
								$lastname = $row['lastname'];
								$firstname = $row['firstname'];
				?>
								<tr data-id="<?php echo $id ?>" data-emp_no="<?php echo $emp_no ?>"
		            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
		            						data-middlename="<?php echo $middlename ?>"
		            						class="data">

		            				<td style="display: none;"><?php echo $id; ?></td>
									<td width="35%"><?php echo $emp_no;  ?></td>
									<td><?php echo $lastname .", ". $firstname; ?></td>
								</tr>	
					<?php
							}
						}
					?>
				</tbody>
			</table>
		</form>
	</div>

	<br><br>
	<h1 style="color:green; ">______________________________________________________</h1>
	<br><br><br>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<label id="header2">Request for Grade Access</label>
		</div>
		<div class="col-md-1"></div>
	</div>

	<div class="row">	
		<div class="row"><br>
			<form method="post" >
				<table id="request" class="display" cellspacing="0" width="100%" style="background-color:gold;">
						<thead>
							<tr>
								<th style="display:none">id</th>
								<th>Employee no.</th>
								<th>Name</th>								
								<th>Quarter</th>
								<th>Subject Code</th>
								<th>Section ID</th>
								<th>LRN</th>
								<th>End Date</th>
							</tr>
						</thead>
						
						<tbody>
						
						<?php 							
							$result=get_db_array("SELECT a.id,a.emp_no,lastname,quarter,subject_code,section_name,lrn,end_date,firstname,middlename FROM tbl_request a left join tbl_facultyinfo b on a.emp_no=b.emp_no where sy_id=$sy_id ");

							foreach ($result as $key => $columnname) {
								?>
									<tr class="data" >
								<?php
								foreach ($columnname as $key => $value) {
									if($key=='lastname'){
										$name=$columnname['lastname'].', '.$columnname['firstname'].' '.$columnname['middlename'];
										?>
										<td><?php echo $name ?></td>
										<?php
									}
									elseif($key=='id'){
										?>
										<td style="display:none"><?php echo $value ?></td>
										<?php

									}
									elseif($key!='lastname' && $key!='firstname' && $key!='middlename' ){
										?>
										<td><?php echo $value ?></td>
										<?php	
									}								
								}
								?>
								</tr>
								<?php
							}

						?>
						
						</tbody>
				</table>
			</form>
		</div>
	</div>

</div>

<div id="da" class="modal fade">
	<div id="yearlevel-content">
		<div class="modal-dialog modal-lg">
	        <div class="modal-content">
	           
	   		</div>
	   	</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		$('#request').dataTable( {
       		 "order": [[ 0, "desc" ]]
   		 } );
	
		$(function() {
			var table=$('#faculty').dataTable();
	 		
	    	$('#faculty tbody').on( 'click', 'tr', function (e) {
	    		e.preventDefault();

		        if ( $(this).hasClass('selected') ) {
		            $(this).removeClass('selected');
		        }
		        else {
		            table.$('tr.selected').removeClass('selected');
		            $(this).addClass('selected');
		        }

				$(".modal-content").html('');
				$('#da').modal('show');

				 $.post('modal-grades-access.php',{emp_no:$(this).attr('data-emp_no')},
					 function(html){
					 	$(".modal-content").html(html);
					 }
				 ) 
				var emp_no = $(this).attr('data-emp_no');
				 console.info(emp_no);
			});
		} );

	});

	</script>