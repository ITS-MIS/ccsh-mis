
<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');

	$emp_no=$_SESSION['emp_no'];
	$lrn=$_POST['lrn'];

	$pdf = new FPDI();

	$pdf->AddPage('L','letter');

	$pageCount = $pdf->setSourceFile("forms/Form138.pdf");
	$tplIdx = $pdf->importPage(1);

	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);

	$pdf->SetFont('Arial', 'b', '5');
	$pdf->SetTextColor(0,0,0);

	$x=28;
	$y=39.4;
	$y2=107.5;
	$n=2.6;

// schoolyear-------------------------------------------------------------
	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(180, 76);
	$pdf->SetFont('Arial','b', '10');
	$pdf->Write(0, $sy);

//student list--------------------------------------------------------------------
	$result=mysql_query("SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name ,age,gender,b.year_id
								FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
								WHERE b.year_id = c.year_id AND b.section_id = d.section_id AND a.lrn = b.lrn AND a.lrn =$lrn AND b.sy_id=$sy_id");
	
	while($row=mysql_fetch_assoc($result)){
	$name=$row['firstname'].' '.$row['middlename']."   ".$row['lastname'];
		$pdf->SetXY(190, 55);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $name);
	
	$age=$row['age'];
		$pdf->SetXY(180, 62);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $age);
		
		$gender=$row['gender'];
		$pdf->SetXY(230,62 );
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $gender);
		
		$grade=$row['year_id'];
		$pdf->SetXY(180,69 );
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $grade);
		
		$section=$row['section_name'];
		$pdf->SetXY(230,69 );
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $section);
	
		$lrn=$row['lrn'];
		$pdf->SetXY(232,76 );
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $lrn);

		$result=mysql_query("SELECT distinct a.lrn,a.year_id,a.section_id,section_name,sy,d.emp_no,firstname,middlename,lastname
							FROM tbl_studentstatus a,tbl_section b,tbl_sy c,tbl_advisers d,tbl_facultyinfo e
							WHERE a.section_id=b.section_id and a.year_id=d.year_id and a.section_id=d.section_id 
							and e.emp_no=$emp_no and a.sy_id=c.sy_id  and a.lrn=$lrn and d.sy_id=a.sy_id");

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=$row['firstname'].'  '.$row['middlename']." ".$row['lastname'];
		$pdf->SetXY(225, 117); 
		$pdf->SetFont('Arial', '', '9');
		$pdf->Write(0, $name);
	}
	
	$pdf->AddPage('L','letter');

	$pageCount = $pdf->setSourceFile("forms/Form138.pdf");
	$tplIdx = $pdf->importPage(2);
	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);

	$pdf->SetFont('Arial', 'b', '9');
	$pdf->SetTextColor(0,0,0);

//adviser-----------------------------------------------
	$y=43;
	$n=5;
	
	$xarraymale=array(12,56,66,75,85,95,100);

	$pdf->SetFont('Arial', '', '8'); 
	
	$result=get_db_array("SELECT b.subject_title, round(quarter1), round(quarter2), round(quarter3), round(quarter4),round((quarter1+quarter2+quarter3+quarter4)/4) as average 
 							FROM tbl_studentgrade a LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code
							LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn WHERE a.lrn = $lrn and a.sy_id=$sy_id GROUP BY a.subject_code ORDER BY subject_title");
			
	foreach ($result as $key => $columnname) {

			$c=0;
			$remarks="";
		foreach ($columnname as $key2 => $value) {
			
			if ($key2=='average'){
			
				if ($value>74 ) {
				
					if ($remarks==NULL or $remarks==0){

	
							$remarks= "Passed";}
						else {
						
							}
							$remarks = "Failed";	
							
					
							$pdf->SetXY(110, $y);
							$pdf->Write(0, $remarks)  ; 
								}
			}
						
				$pdf->SetXY($xarraymale[$c], $y);
				$pdf->Write(0, $value)  ; 
						
			$c++;
		}
		
		$y=$y+$n;
	}

	$result=mysql_query("SELECT subject_code, 
	round(avg(quarter1)+avg(quarter2) + avg(quarter3)+ avg(quarter4))/4 as gwa FROM tbl_studentgrade a,tbl_studentstatus b where  a.sy_id=$sy_id and a.lrn=b.lrn and a.lrn=$lrn");
	

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=number_format((float)$row['gwa'], 2, '.', '');
		//number_format((float)$name, 2, '.', '');
		$pdf->SetXY(92, 118);
		$pdf->SetFont('Arial', '', '10');
		if ($name==NULL or $name==0){

		}
		else{

		$pdf->Write(0, $name);}

	}

// core values quarter1--------------------------------------------------------------

$result=mysql_query("SELECT makadiyos,makakalikasan,makatao,makabansa  FROM tbl_studentvalues a LEFT JOIN tbl_quarter b ON a.q_id = b.q_id
						LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn WHERE a.lrn = $lrn and a.q_id=1 and a.sy_id=$sy_id");
	
	while($row=mysql_fetch_assoc($result)){
	
	$makadiyos1=$row['makadiyos'];
		$pdf->SetXY(230, 50);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makadiyos1);
	
	$makakalikasan1=$row['makakalikasan'];
		$pdf->SetXY(230, 73);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makakalikasan1);


	$makatao1=$row['makatao'];
		$pdf->SetXY(230, 93);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makatao1);

	$makabansa1=$row['makabansa'];
		$pdf->SetXY(230, 120);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makabansa1);

	}

// core values quarter2--------------------------------------------------------------
	$result=mysql_query("SELECT makadiyos,makakalikasan,makatao,makabansa  FROM tbl_studentvalues a LEFT JOIN tbl_quarter b ON a.q_id = b.q_id
							LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn WHERE a.lrn = $lrn and a.q_id=2 and a.sy_id=$sy_id");
	
	while($row=mysql_fetch_assoc($result)){
	
	$makadiyos1=$row['makadiyos'];
		$pdf->SetXY(242, 50);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makadiyos1);
	
	$makakalikasan1=$row['makakalikasan'];
		$pdf->SetXY(242, 73);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makakalikasan1);


	$makatao1=$row['makatao'];
		$pdf->SetXY(242, 93);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makatao1);

	$makabansa1=$row['makabansa'];
		$pdf->SetXY(242, 120);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makabansa1);


	}
// core values quarter3--------------------------------------------------------------
	$result=mysql_query("SELECT  makadiyos,makakalikasan,makatao,makabansa  FROM tbl_studentvalues a LEFT JOIN tbl_quarter b ON a.q_id = b.q_id
							LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn WHERE a.lrn = $lrn and a.q_id=3 and a.sy_id=$sy_id");

	while($row=mysql_fetch_assoc($result)){

	$makadiyos1=$row['makadiyos'];
		$pdf->SetXY(253, 50);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makadiyos1);
	
	$makakalikasan1=$row['makakalikasan'];
		$pdf->SetXY(253, 73);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makakalikasan1);


	$makatao1=$row['makatao'];
		$pdf->SetXY(253, 93);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makatao1);

	$makabansa1=$row['makabansa'];
		$pdf->SetXY(253, 120);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makabansa1);


}


// core values quarter4--------------------------------------------------------------
	$result=mysql_query("SELECT  makadiyos,makakalikasan,makatao,makabansa  FROM tbl_studentvalues a LEFT JOIN tbl_quarter b ON a.q_id = b.q_id
							LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn WHERE a.lrn = $lrn and a.q_id=4 and a.sy_id=$sy_id");
	
	while($row=mysql_fetch_assoc($result)){
	
	$makadiyos1=$row['makadiyos'];
		$pdf->SetXY(262, 50);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makadiyos1);
	
	$makakalikasan1=$row['makakalikasan'];
		$pdf->SetXY(262, 73);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makakalikasan1);


	$makatao1=$row['makatao'];
		$pdf->SetXY(262, 93);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makatao1);

	$makabansa1=$row['makabansa'];
		$pdf->SetXY(262, 120);
		$pdf->SetFont('Arial', 'b', '10');
		$pdf->Write(0, $makabansa1);

	}

	$pdf->Output();

}