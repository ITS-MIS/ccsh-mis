<?php

// Initialize variables and set to empty strings
$npass = $npass2 = "";
$npassErr = $npass2Err = $matchErr = "";

// Control variables
$app_state = "empty";  //empty, processed, logged in
$valid = 0;

// Validate input and sanitize
if ($_SERVER['REQUEST_METHOD']== "POST") {
   if (empty($_POST["npass"])) {
      $npassErr = "*Password is required";
      $npass = "";
   }
   else {
      $npass = test_input($_POST["npass"]);
      $valid++;
   }
   if (empty($_POST["npass2"])) {
      $npass2Err = "Confirm Password";
      $npass2 = "";
   }
   else {
      $npass2 = test_input($_POST["npass2"]);
      $valid++;
   }

   if (!empty($_POST["npass"]) && !empty($_POST["npass2"]) && $npass != $npass2)     {
				$matchErr = "Password don't match! Try again!";
				$npass = $npass2 = "";
				
		}
	else{
		$npass = test_input($_POST["npass"]);
		$npass2 = test_input($_POST["npass2"]);
		$valid++;
	}

   if ($valid >= 3) {
      $app_state = "processed";
   }
}

// Sanitize data
function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

if ($app_state == "empty") {
?>