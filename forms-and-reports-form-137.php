<?php
	getDatatablesLink();
	getGuidancePageLink();	
	getDatatablesScript();
	getGuidancePageScript();
	getValidationScipt();
?>			
		<div class="container-fluid">
		
			<div class="row">
				<div id="header">Form 137</div>	
			</div><br>
			
			<div class="row">
				<div class="col-sm-3 col-md-5" style="background-color:gold">			
					<table id="ranking" data-page-length="1" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>LRN</th>
								<th>Name</th>
								<th>Number</th>
							</tr>
						</thead>
						
						<tbody>
						
						<?php 
			
							$sql_search = "SELECT a.lrn, lastname, firstname, middlename, avg(quarter1) as q1, avg(quarter2) as q2, avg(quarter3) as q3, avg(quarter4) as q4
											FROM tbl_studentgrade a, tbl_studentinfo b 
											WHERE a.lrn = b.lrn AND a.lrn in
												(SELECT lrn from tbl_studentstatus where year_id)
												GROUP BY a.lrn ORDER BY q1 desc";
											
							$result = mysql_query($sql_search) or die(mysql_error());
							
								if (mysql_num_rows($result)>0) {
								
									$No = 1;
								
									while(($row = mysql_fetch_assoc($result))!= null) {
										$lrn = $row['lrn'];
										$lastname = $row['lastname'];
										$firstname = $row['firstname'];
										$middlename = $row['middlename'];
						?>
										<tr class="data">
											<td width="25%"><?php echo $lrn;  ?></td>
											<td><?php echo $lastname .", ". $firstname ." ". $middlename;  ?></td>
											<td width="20%"><?php echo $No;  ?></td>
											
										</tr>	
							<?php	
										$No++;
									}
								}
							?>
						
						</tbody>
					</table>
				</div>
				
				<div class="col-sm-9 col-md-7" style="background-color:;"><br>
					
					<form role="form" method="post" action=" <?php $_SERVER['PHP_SELF']; ?>" onsubmit="if (!/\S/.test(this.lrn.value)) {alert('Please select student on datatable before loading'); return false}">
						<div class="row">
							<div class="col-md-4"></div>
							
							<div class="col-md-4">
								<input type="text" class="form-control" name="lrn2" id="lrn" readonly>
							</div>
							
							<div class="col-md-4">
								<input type="submit" class="btn btn-success" value="Load" name="btnLoad" id="btnload">
							</div>
							
						</div>
					</form>
					
				<?php
			
					if (isSet($_POST['btnLoad'])) {
					
						$lrn2 = $_POST['lrn2'];
						
						$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
										FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
										WHERE b.year_id = c.year_id and b.section_id = d.section_id and a.lrn = b.lrn and a.lrn=$lrn2";
										
						$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
						
						if (mysql_num_rows($result)>0) {
						
							while($row = mysql_fetch_row($result)) {		
								$lrn2 = $row[0];
								$lname = $row[1];
								$fname = $row[2];
								$mname = $row[3];
								$year = $row[4];
								$section = $row[5];
							}
						}
				?>
					
					<form role="form" method="post" action="#">
					
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="lrn2">LRN</label>
									<input type="text" class="form-control" id="lrn2" name="lrn3" value="<?php echo $lrn2; ?>" readonly> 
								</div>
							</div>
							
							<div class="col-md-4"></div>
							
						</div>
					
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label for="lname">Last Name</label>
									<input type="text" class="form-control" id="lname" name="lname" value="<?php echo $lname; ?>" readonly> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="yr">Year Level</label>
									<input type="text" class="form-control" id="year" name="year" value="<?php echo $year; ?>" readonly> 
								</div>
							</div>
							
						</div>
						
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label for="fname">First Name</label>
									<input type="text" class="form-control" id="fname" name="fname" value="<?php echo $fname; ?>" readonly>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="section">Section</label>
									<input type="text" class="form-control" id="section" name="section" value="<?php echo $section; ?>" readonly> 
								</div>
							</div>
							
						</div>
						
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label for="mname">Middle Name</label>
									<input type="text" class="form-control" id="mname" name="mname" value="<?php echo $mname; ?>" readonly>
								</div>
							</div>
						</div>

					</form>						
					
					<div class="row">
						<div class="col-md-1"></div>
						
						<div class="col-sm-3 col-md-10" style="background-color:gold;">		
							<table id="ranking-spec" data-page-length="10" class="display" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Subject</th>
										<th>1st Quarter</th>
										<th>2nd Quarter</th>
										<th>3rd Quarter</th>
										<th>4th Quarter</th>										
									</tr>								
								</thead>
								
								<tbody>								
									<?php
				
										$sql_load = "SELECT subject_title, quarter1, quarter2, quarter3, quarter4 FROM tbl_studentgrade a, tbl_subject b WHERE a.subject_code = b.subject_code AND lrn = $lrn2";
														
										$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
										
										if (mysql_num_rows($result)>0) {
											while($row = mysql_fetch_row($result)) {
											
												$subject_code = $row[0];
												$quarter1 = $row[1];
												$quarter2 = $row[2];
												$quarter3 = $row[3];
												$quarter4 = $row[4];							
								?>	
											<tr>
												<td><?php echo $subject_code;  ?></td>
												<td><?php echo $quarter1; ?></td>
												<td><?php echo $quarter2; ?></td>
												<td><?php echo $quarter3; ?></td>
												<td><?php echo $quarter4; ?></td>
											</tr>
								
								<?php
											}
										}
								?>

								</tbody>
							</table><br>
						</div>
						<div class="col-md-1"></div>
					</div><br>	
				</div>
			</div>
		</div>

			<?php	}	?>
			
		
		</div>
		
	<script type="text/javascript">
		
		$(document).ready(function() {
			$('#ranking').dataTable({
				"ordering": false,
				scrollY: 835
			});

		$(document).ready(function() {
			$('#ranking').dataTable();
			 
			$('#ranking tbody').on('click', 'tr', function () {
				var lrn = $('td', this).eq(0).text();
				document.getElementById("lrn").value = lrn;
				
			} );
		} );
		
			$(function() {
				$('#ranking-spec').dataTable({
					bInfo: false,
					bFilter: false,
					paging: false,
					"ordering": false
				});
			} );
		} );
		
	</script>