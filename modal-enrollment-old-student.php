 <?php
 	include'connect.php';

 	$id=$_POST['id'];
 	$lrn=$_POST['lrn'];
 	$yearlvl=$_POST['year_level'];
 	$sy_id=get_db("SELECT sy_id FROM tbl_sy ORDER BY sy_id DESC LIMIT 1");
	$sy_id=$sy_id['sy_id']-1;

	$result=get_db_array("SELECT a.id,a.lrn,lastname,firstname,middlename,year_id from tbl_studentinfo a 	left join tbl_studentstatus b on a.lrn=b.lrn 
			where b.sy_id=$sy_id and b.year_id =(SELECT year_id from tbl_yearlevel where year_level='$yearlvl') and a.lrn=$lrn") ;

	$dbfields=array('id','lrn','lastname','firstname','middlename','year_level');
 	$label=array('id'=>'ID','lrn'=>'LRN','lastname'=>'Lastname','firstname'=>'Firstname','middlename'=>'Middlename','year_id'=>'Year Level');
?>
 <div class="modal-content">
    <div class="modal-header" style="background-color:gold;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Old Student Enrollment</h4>
    </div>    

    <div class="modal-body  modal-height"> 
		<form class="form-horizontal" method="post" action="enrollment-old-submit.php"><br><br>
			<?php
				foreach ($result as $key => $columnname) {
					foreach ($columnname as $key2 => $value) {
						if($key2=='id'){
							?>
							<div class="form-group">
								<label  class="col-sm-5 control-label">ID :</label>		
								<label class="col-sm-7" style="margin-top:6px;font"><?php echo $result[$key][$key2]; ?></label>
								<input type="hidden" class="form-control" id="id" name="<?php echo $key2;?>"  value="<?php echo $result[$key][$key2]; ?>">
					
						  	</div>
						  	<?php
						}
						else{
						  	?>
						  	<div class="form-group">
								<label class="col-sm-5 control-label"><?php echo $label[$key2]. " :"; ?></label>
								<label class="col-sm-7 " style="margin-top:6px;"><?php echo $result[$key][$key2]; ?></label>
								<input type="hidden" class="form-control" id="id" name="<?php echo $key2;?>"  value="<?php echo $result[$key][$key2]; ?>">
					
						  	</div>
						  	<?php
						}
					}
				}
		?>
			<br>
			<div class="row text-center" >
				<div class="form-group">
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
					<button type="submit" class="btn btn-success success text-center" name="btnEnroll"  >Enroll</button>
				</div>
			</div>
		</form>	
	</div>

   	<div class="modal-footer" style="background-color:gold;">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>