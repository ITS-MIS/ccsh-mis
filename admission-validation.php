<?php

	// Initialize variables and set to empty strings
	$lrn = $lname = $fname = $mname = $bdate = $rdo_gender = $addnost = $brgy = $city = 
	$province = $mothertounge = $ip = $religion =  $mothertounge2 = $ip2 = $religion2 =$school = $contactnumber = "";	

	$lnameErr = $fnameErr =  $bdateErr = $genderErr = $addnostErr = $brgyErr = $cityErr = 
	$provinceErr = $mothertoungeErr = $ipErr = $religionErr = $schoolErr = $contactnumberErr ="";

	// Control variables
	$app_state = "empty";  //empty, processed, logged in
	$valid = 0;

	// Validate input and sanitize
	if ($_SERVER['REQUEST_METHOD']== "POST") {

		if ((empty($_POST["lrn"])) || (!empty($_POST["lrn"]))) {

				$lrn = test_input($_POST["lrn"]);
	   		}

   		if (empty($_POST["lname"])) {

			$lnameErr = "*Last Name is required";
	     	
   			} else {

				$lname = test_input($_POST["lname"]);
		      	$valid++;
	   		}

	   	if (empty($_POST["fname"])) {

			$fnameErr = "*First Name is required";
	     	
	   		} else {

				$fname = test_input($_POST["fname"]);
		      	$valid++;
		   	}

		if ((empty($_POST["mname"])) || (!empty($_POST["mname"]))) {

				$mname = test_input($_POST["mname"]);
	   		}

		if (empty($_POST["bdate"])) {

			$bdateErr = "*Birth Date is required";
	     	
	   		} else {

				$bdate = test_input($_POST["bdate"]);
		      	$valid++;
		   	}

   		if (empty($_POST["rdo_gender"])) {

			$genderErr = "*required";
	     	
	   		} else {

				$rdo_gender = test_input($_POST["rdo_gender"]);
		      	$valid++;
		   	}

		if (empty($_POST["addnost"])) {

			$addnostErr = "*House No and Street is required";
	     	
	   		} else {

				$addnost = test_input($_POST["addnost"]);
		      	$valid++;
		   	}

	   	if (empty($_POST["brgy"])) {

			$brgyErr = "*Barangay is required";
	     	
	   		} else {

				$brgy = test_input($_POST["brgy"]);
		      	$valid++;
		   	}

	   	if (empty($_POST["city"])) {

			$cityErr = "*City is required";
	     	
	   		} else {

				$city = test_input($_POST["city"]);
		      	$valid++;
		   	}

		
	   	if (empty($_POST["province"])) {

			$provinceErr = "*Province is required";
	     	
	   		} else {

				$province = test_input($_POST["province"]);
		      	$valid++;
		   	}	   	
 		
 		if (empty($_POST["mothertounge"])) {

			$mothertoungeErr = "*Mother Tounge is required";
	     	
	   		} else {
	   			if($_POST["mothertounge"]=='Other'){
	   				$mothertounge='Other';

	   				$mothertounge2=test_input($_POST["mothertounge2"]);
	   				$valid++;
	   			}
	   			else{
	   				$mothertounge=test_input($_POST["mothertounge"]);
	   				$valid++;
	   			}
		   	}

	   	if (empty($_POST["ip"])) {

			$ipErr = "*Ethnic Group is required";
	     	
	   		} else {
				if($_POST["ip"]=='Other'){
	   				$ip='Other';

	   				$ip2=test_input($_POST["ip2"]);
	   				$valid++;
	   			}
	   			else{
	   				$ip=test_input($_POST["ip"]);
	   				$valid++;
	   			}
		   	}

	   	if (empty($_POST["religion"])) {

			$religionErr = "*Religion is required";
	     	
	   		} else {

				if($_POST["religion"]=='Other'){
	   				$religion='Other';

	   				$religion2=test_input($_POST["religion2"]);
	   				$valid++;
	   			}
	   			else{
	   				$religion=test_input($_POST["religion"]);
	   				$valid++;
	   			}
		   	}

	  	if (empty($_POST["school"])) {

			$schoolErr = "*Last School Attended is required";
	     	
	   		} else {

				$school = test_input($_POST["school"]);
		      	$valid++;
		   	}

	   	if (empty($_POST["contactnumber"])) {

			$contactnumberErr = "*Contact Number is required";
	     	
	   		} else {

				$contactnumber = test_input($_POST["contactnumber"]);
		      	$valid++;
		   	}

		if ($valid >= 13) {

				$test=get_db("SELECT lastname from tbl_studentinfo WHERE lrn=$lrn ");
				$test=$test['lastname'];
				
				if(count($test)==1) {
					$confirm=("LRN " .$lrn. " is not available!");
					echo "<script type='text/javascript'>alert(\"$confirm\");</script>";

				}
				else{
		      $app_state = "processed";
		   }
		}
	}

	// Sanitize data
	function test_input($data) {

		$data = trim($data);
	   	$data = stripslashes($data);
	   	$data = htmlspecialchars($data);
	   	return $data;
	}