<?php
	include_once'connect.php';
			
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<label id="header">List of User Access</label>
		</div>
		<div class="col-md-1"></div>
	</div><br>
	
	<div class="row">
		<form class="form-horizontal" method="post" action="grantaccessupdate.php">
			<table id="grantaccesstable" class="display" cellspacing="0" width="100%" style="background-color: gold">	
		        <thead>
		            <tr>
		                <th>Name</th>   
		                <th>Administrator <input type="checkbox"  id="admincheckall"  style="margin-left:20px;"  unchecked></th>
		                <th>Registrar <input type="checkbox" id="regchechall"  style="margin-left:20px;" unchecked></th>       
		               	<th>Principal <input type="checkbox" id="princheckall"   style="margin-left:20px;" unchecked></th>   
		               	<th>Faculty <input type="checkbox" id="faccheckall" style="margin-left:20px;"  unchecked></th> 
		               	<th>Guidance <input type="checkbox" id="guicheckall"  style="margin-left:20px;"  unchecked></th>                      
		            </tr>		                
		        </thead>		 
		   
		        <tbody>
		        <?php  

					$result = mysql_query("SELECT a.emp_no, lastname,firstname,middlename,administrator,registrar,principal,faculty,guidance 
										FROM tbl_loginemployee a LEFT JOIN tbl_facultyinfo b on a.emp_no=b.emp_no WHERE a.emp_no!=1413914");

					$access=array('administrator','registrar','principal','faculty','guidance');
		  					
					if(mysql_num_rows($result) > 0) {
													
						while ($row = mysql_fetch_assoc($result)){

							$name=$row['lastname'].', '.$row['firstname']." ".$row['middlename'];

							$emp_no=$row['emp_no'];
							if($emp_no!=1417918){
						?>
				            <tr>
				                <td><?php echo $name; ?></td>
				                <?php 
				                	foreach ($access as $key => $value) {

					                	if ($row[$value]=='1'){
					                		?> 
					                		<td style="text-align:center;" >
					                			<input type="checkbox" 

					                			class="<?php 
						                			
						                			switch ($value) {
						                			 	case 'admin':
						                			 		echo "admincheckbox"; 
						                			 		break;

						                			 	case 'registrar':
						                			 		echo "registrarcheckbox"; 
						                			 		break;

						                			 	case 'principal':
						                			 		echo "principalcheckbox"; 
						                			 		break;

						                			 	case 'faculty':
						                			 		echo "facultycheckbox"; 
						                			 		break;

						                			 	case 'guidance':
						                			 		echo "guidancecheckbox"; 
						                			 		break;
						                			 	
						                			 	default:
						                			 		# code...
						                			 		break;
						                			}
						                									                
					                			?>" name="access[<?php echo $value;?>][<?php echo $emp_no; ?>]"  checked>
					                		</td>
					                		<?php
					                	}
					                	else{
					                		?>
					                		<td style="text-align:center;" >
					                			<input type="checkbox" 

					                			class="<?php 

						                			switch ($value) {
						                			 	case 'admin':
						                			 		echo "admincheckbox"; 
						                			 		break;

						                			 	case 'registrar':
						                			 		echo "registrarcheckbox"; 
						                			 		break;

						                			 	case 'principal':
						                			 		echo "principalcheckbox"; 
						                			 		break;

						                			 	case 'faculty':
						                			 		echo "facultycheckbox"; 
						                			 		break;

						                			 	case 'guidance':
						                			 		echo "guidancecheckbox"; 
						                			 		break;
						                			 	
						                			 	default:
						                			 		echo($key);
						                			 		break;
						                			}
					                			?>" name="access[<?php echo $value;?>][<?php echo $emp_no; ?>]"   unchecked>
					                		</td>
					                		<?php
					                	}
					                }
				                	?>
					                
					        </tr>
					        
					    	<?php
							}
					    }
					}
				?>
				</tbody>
			</table>

			<div class="form-group">
			    <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
			</div>
			
			<div class="form-group " style="alighn:right;">
			    <button type="submit" class="btn btn-success success left-margin" name="submit" style="float: right;" onclick="return confirm('Are you sure you want to submit?');">Submit </button>
			</div>
		</form>
	</div>
</div>

	<script type="text/javascript">

		$("#admincheckall").click(function(){
	    	$('input.admincheckbox').not(this).prop('checked', this.checked);
		});

		$("#regchechall").click(function(){
	    	$('input.registrarcheckbox').not(this).prop('checked', this.checked);
		});

		$("#princheckall").click(function(){
	    	$('input.principalcheckbox').not(this).prop('checked', this.checked);
		});

		$("#faccheckall").click(function(){
	    	$('input.facultycheckbox').not(this).prop('checked', this.checked);
		});

		$("#guicheckall").click(function(){
	    	$('input.guidancecheckbox').not(this).prop('checked', this.checked);
		});
		
	</script>