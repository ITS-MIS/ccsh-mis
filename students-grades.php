<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Student's Grade</label>
			</div>
			<div class="col-md-1"></div>
		</div>	

	<div class="row">
		<form method="post">
			<table id="student" class="display" cellspacing="0" width="100%" style="background-color:gold;">		     
		        <thead>		        	
		        	<tr>
		        		<th>LRN</th>
		        		<th>Last Name</th>
		        		<th>First Name</th>
		        		<th>Middle Name</th>
		        		<th>Gender</th>
		        	</tr>							
				</thead>

		       	<tbody>					
					<?php
						
						$studentarray = get_db_array("SELECT lrn, lastname, firstname, middlename, gender from tbl_studentinfo WHERE lrn IN
														(SELECT lrn FROM tbl_studentstatus WHERE remarks IN('New Student', 'Old Student', 'Transferee'))");				
						if(count($studentarray)>0){
	 					
	 					foreach ($studentarray as $key => $fieldname) {
					?>

		           	<tr  data-toggle="modal" data-traget="#studentmodal" data-lrn="<?php echo $studentarray[$key]['lrn']; ?>" data-data="true" class="data">
		     			
						<?php
							
							foreach ($fieldname as $key2 => $value) {
						?>
	            				<td><?php echo $value ?></td>
	            		<?php
	            			}
	            		?>	                             
		            </tr>
	        	<?php
			        
			        	}
			        }
			    	
			    ?>

				</tbody>

			</table>
		</form>
	</div>
</div>

	<div id="studentmodal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			</div>

		</div>		
	</div>	
	
<script type="text/javascript">

	$(document).ready(function() {
		var table=$('#student').dataTable({
			bInfo: true,
			"bFilter": true,
			"ordering": false
			});

		$('#student tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

    		$(".data").removeClass('selected');

	        if ( $(this).removeClass('selected') ) {
	            $(this).addClass('selected');
	         }
	       

	        if($(this).data('data')){
	        	$("#studentmodal .modal-content").html('');
			

				$.post('modal-students-grades.php',{lrn:$(this).data('lrn')},
					 function(html){
					 	$(".modal-content").html(html);
					 	$('#studentmodal').modal('show');
					 }
				 ); 
	        }
				
	
	});
});

</script>

			   

