<?php
	include 'connect.php';
	include 'current-year.php';	
	
	$url=$_POST['url'];

	if(isset($_POST['btnGenerate'])){
		$year_id=$_POST['year_id'];
		$totalsection=$_POST['totalsection'];

	// select all student base in year_id and sy_id-------------------------------------------------------------------------------------

		$lrns=array();

		$result=get_db_array("SELECT lrn FROM tbl_studentstatus where year_id=$year_id and sy_id=$sy_id");
		
		foreach ($result as $key => $column) {
			foreach ($column as $key => $lrn) {
				array_push($lrns,$lrn);
			}
		}

	// rank student by GWA-------------------------------------------------------------------------------------
		
		$lrns=implode (", ", $lrns);
		
		$lrnlist=array();
		$result=get_db_array("SELECT lrn,(avg(quarter1)+ avg(quarter2)+ avg(quarter3)+ avg(quarter4))/4 as avg from tbl_studentgrade where lrn in ($lrns) GROUP BY lrn order by avg desc"); 
		
		foreach ($result as $key => $column) {
			array_push($lrnlist, $column['lrn']);

		}

	//  Select section_id base in no. of section -------------------------------------------------------------------------------------


		$sections=get_db_array("SELECT section_id from tbl_section where section_id in(SELECT section_id from tbl_section_yearlevel where year_id=$year_id and sy_id =$sy_id) limit ".$totalsection);
		$sectionsarray=array();
		foreach ($sections as $key => $column) {
			foreach ($column as $key => $value) {
				array_push($sectionsarray, $value);
			}
			
		}

	// distribute student evenly by rank- tbl_studentstatus section_id update-------------------------------------------------------------------------------------
		
		$arrays = array_chunk($lrnlist, $totalsection);

		foreach ($arrays as $array_num => $array) {
			
			foreach ($array as $item_num => $lrn) {
			 	query_db("UPDATE tbl_studentstatus set section_id=$sectionsarray[$item_num] where lrn=$lrn and year_id=$year_id and sy_id=$sy_id");

			}
		}


		include('generate-advisers.php');

	}

// move student to section---------------------------------------------------------------------------------------

	if(isset($_POST['moveTo'])){
		$section=$_POST['section'];
		$lrn=$_POST['lrn'];

		$section_id=get_db("SELECT section_id from tbl_section where section_name='$section'");
		$section_id=$section_id['section_id'];
		
		query_db("UPDATE tbl_studentstatus set section_id = $section_id where lrn=$lrn and sy_id =$sy_id");
		
	}

//remove sections base on year_id------------------------------------------------------------------------------
	
	if(isset($_POST['btnUnallocate'])){
		$year_id=$_POST['year_id'];

		query_db("UPDATE tbl_studentstatus set section_id = 0 where sy_id =$sy_id and year_id=$year_id");
		query_db("DELETE FROM tbl_advisers where sy_id =$sy_id and year_id=$year_id");
		
	}

	redirect($url);
?>	