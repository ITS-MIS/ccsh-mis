<?php
	include'connect.php';
	include'current-year.php';
		$lrn=$_POST['lrn'];

		$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
							FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
							WHERE b.year_id = c.year_id and b.section_id = d.section_id and a.lrn = b.lrn and a.lrn=$lrn";
										
		$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
		
		if (mysql_num_rows($result)>0) {		
			while($row = mysql_fetch_row($result)) {			
				$lrn = $row[0];
				$lastname = $row[1];
				$firstname = $row[2];
				$middlename = $row[3];
				$year = $row[4];
				$section = $row[5];				
			}
		}
		$iquery = "SELECT image FROM tbl_images WHERE lrn = $lrn AND verify=1";			
					$result = mysql_query($iquery);
						
						$row = mysql_fetch_row($result);
						$image=($row[0]);	
?>

<div class="modal-content">
    <div class="modal-header" style="background-color:gold;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Student's Academic Record</h4>
    </div>
    
    <div class="modal-body  modal-height"> 

    	<div class="row">
			<div class="col-md-11" style="text-align:right;">
				<div class="form-group">
					<img id="image" class="image" src="<?php echo $image; ?>" alt="" height="150px" width="150px"/> 
				</div>
			</div>			
		</div>
		
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-7">
				<div class="form-group">
					<label for="lrn">LRN: <?php echo $lrn; ?></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="text-align: right;">
					<label for="year">Year Level: <?php echo $year; ?></label>
				</div>
			</div>
			<div class="col-md-1"></div>							
		</div>
		
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-7">
				<div class="form-group">
					<label for="name">Student's Name: <?php echo $lastname .', '. $firstname .' '. $middlename ?></label>		
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="text-align: right;">
					<label for="section">Section: <?php echo $section; ?></label>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div><br>

		<div class="row">
			<div class="col-md-1"></div>
			
			<div class="col-sm-3 col-md-10">		
				<table id="under-probation-spec" data-page-length="10" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Subject</th>
							<th>1st Quarter</th>
							<th>2nd Quarter</th>
							<th>3rd Quarter</th>
							<th>4th Quarter</th>
						</tr>
					</thead>
					
					<tbody>
					
						<?php	
							$sql_load = "SELECT subject_title, quarter1, quarter2, quarter3, quarter4 FROM tbl_studentgrade a, tbl_subject b 
											WHERE a.subject_code = b.subject_code AND lrn = $lrn AND a.sy_id=$sy_id";
											
							$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
							
							if (mysql_num_rows($result)>0) {
								while($row = mysql_fetch_row($result)) {								
									$subject_code = $row[0];
									$quarter1 = $row[1];
									$quarter2 = $row[2];
									$quarter3 = $row[3];
									$quarter4 = $row[4];								
						?>
								<tr>
									<td><?php echo $subject_code;  ?></td>
									
									<td align="center" style="<?php //echo $style; ?>" ><?php echo $quarter1; ?></td>
									<td align="center"><?php echo $quarter2; ?></td>
									<td align="center"><?php echo $quarter3; ?></td>
									<td align="center"><?php echo $quarter4; ?></td>
								</tr>
					
						<?php
								}
							}						
						?>

					</tbody>
				</table><br>	
			</div>			
			<div class="col-md-1"></div>
		</div>
	</div>

    <div class="modal-footer" style="background-color:gold;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
	
<script type="text/javascript">

	$(document).ready(function() {
		$('#under-probation-spec').dataTable({
			bInfo: false,
			bFilter: false,
			paging: false,
			"ordering": false
		});
	} );

</script>