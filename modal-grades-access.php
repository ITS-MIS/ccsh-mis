<?php
	include'connect.php';
	$emp_no=$_POST['emp_no'];

	include('current-year.php');
	include('quartercheck.php');					
?>

<div 
	<?php if(!isset($_GET['request'])){ echo 'style="display:none"';}?> 
	class="alert alert-danger" role="alert">Request Failed: Double check existing access 
</div>

 	<div class="modal-content">
        <div class="modal-header" style="background-color:gold;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Add Grade Access</h4>
        </div>        

        <div class="modal-body  modal-height"> 
			<form class="form-horizontal" method="post" action="grade-access-submit.php">
				<p><span style="color:red;" >*</span> required field</p>
				<br><br>
			
				<div class="form-group">
					<div class="col-md-2 ">
					</div>
					<div class="col-md-8 ">
						<div class="col-md-5 ">
							<label for="lrn" class=" control-label">Employee No <span style="color:red;" >*</span></label>
						</div>

						<div class="col-md-3 ">
							<input type="text" style="width:200px;"class="form-control " name="info[emp_no]" placeholder="Employee No." maxlength="12" onkeypress="return isNumber(event)" required value="<?php echo $emp_no?>" readonly>
						</div>
			  		</div>
			  	</div>

			  	<div class="form-group">
					<div class="col-md-2 ">
					</div>
					<div class="col-md-8 ">
						<div class="col-md-5 ">
							<label for="quarter" class=" control-label">Quarter </label>
						</div>

						<div class="col-md-3 ">
							<select class="form-control"  name="info[quarter]" >							
							<option></option>

							<?php if (isset($oq1)) echo'<option value="1st">1st</option>';
								  if (isset($oq2)) echo'<option value="2nd">2nd</option>';
								  if (isset($oq3)) echo'<option value="3rd">3rd</option>';
								  if (isset($oq4)) echo'<option value="4th">4th</option>';
							?>
										
							</select>
						</div>
			  		</div>
			  	</div>

			  	<div class="form-group">
					<div class="col-md-2 ">
					</div>
					<div class="col-md-8 ">
						<div class="col-md-5 ">
							<label for="emp_no" class=" control-label">Subject Code <span style="color:red;" >*</span></label>
						</div>

						<div class="col-md-3 ">
							<select id="selectedsubject" style="width:200px;"class="form-control " name="info[subject_code]" required>
								<?php
								$subjectlist=array();
									$sql_major = "SELECT subject_code FROM tbl_employee_subject WHERE emp_no=$emp_no AND sy_id=$sy_id ORDER BY subject_code";
									$result = mysql_query($sql_major);

										while($row = mysql_fetch_array($result)) {
								?> 
											<option><?php echo $row['subject_code']; ?> </option>
											<?php 	
											 array_push($subjectlist,"'".$row['subject_code']."'");
								
										} 
										print_r($subjectlist);
									?>
							</select>

						</div>
			  		</div>
			  	</div>

			  	<div class="form-group">
					<div class="col-md-2 ">
					</div>
					<div class="col-md-8 ">
						<div class="col-md-5 ">
							<label for="emp_no" class=" control-label">Section Name <span style="color:red;" >*</span></label>
						</div>

						<div class="col-md-3 ">
							<select id="drpsection" type="text" style="width:200px;"class="form-control " name="info[section_name]" required>
							
								<?php
								$subjectlist=implode(',',$subjectlist);

									$sql_major = "SELECT DISTINCT subject_code,section_name from tbl_studentstatus a LEFT JOIN tbl_section b on a.section_id=b.section_id LEFT JOIN tbl_subject_yearlevel c on a.year_id=c.year_id where subject_code in ($subjectlist) and c.sy_id=$sy_id and section_name in(0,null)";
									$result = mysql_query($sql_major);

										while($row = mysql_fetch_array($result)) {
								?> 
											<option class="<?php echo $row['subject_code']?> option" style="display:none" ><?php echo $row['section_name']; ?> </option>
										
									<?php 	
										}
									?>
							</select>
						</div>
			  		</div>
			  	</div>

			  	<div class="form-group">
					<div class="col-md-2 ">
					</div>
					<div class="col-md-8 ">
						<div class="col-md-5 ">
							<label for="emp_no" class=" control-label">LRN </label>
						</div>

						<div class="col-md-3 ">
							<input type="text" style="width:200px;"class="form-control " name="info[lrn]" placeholder="LRN " maxlength="12" onkeypress="return isNumber(event)" >
						</div>
			  		</div>
			  	</div>

			  	<div class="form-group">
					<div class="col-md-2 ">
					</div>
					<div class="col-md-8 ">
						<div class="col-md-5 ">
							<label for="quarter" class="control-label">Duration <span style="color:red;" >*</span></label>
						</div>

						<div class="col-md-3 ">
							<select class="form-control"  name="info[duration]"  required>							
							<option></option>
							<option value="1 Day">1 Day</option>
							<option value="2 Days">2 Days</option>
							<option value="3 Days">3 Days</option>
							<option value="4 Days">4 Days</option>
							<option value="5 Days">5 Days</option>
										
							</select>
						</div>
			  		</div>
			  	</div><br>
				
				<div class="form-group">
					<div class="col-sm-4"></div>					
					<div class="col-sm-4" style="text-align: center;">
						<button type="submit" class="btn btn-success success" name="btnInsert" onclick="return confirm('Are you sure you want to submit?');">Add</button>
						<button type="reset" class="btn btn-default">Reset</button>						
					</div>					
					<div class="col-sm-4"></div>					
				</div>

				<div class="form-group">																			
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
					<input type="hidden" id="empno2" name="emp_no" value="<?php echo $_SERVER['REQUEST_URI'] ?>">					
				</div>				
			</form>
		</div>

	    <div class="modal-footer" style="background-color:gold;">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>	      
        </div>
	</div>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>           
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="students-record-submit.php">
            		<input type="hidden" id="updatelrn" name="ulrn">
            		<input type="hidden" id="updateoffense"name="offense">
            		<input type="hidden" id="updateremarks" name="remarks" >					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnSubmit">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		$('#update').modal({
	        keyboard: true,
	        show:false,

		}).on('show.bs.modal', function(){

	         var getlrnFromRow =$('#emp').val();;
				$('#update').val(getlrnFromRow);

	        var getoffenseFromRow = $('#uoffense').val();;	         
	         	$('#updateoffense').val(getoffenseFromRow);
	         
	        var getremarksFromRow = $('#uremarks').val();;
	         	$('#updateremarks').val(getremarksFromRow);	 
	 			
		});

		var subject=$('#selectedsubject').val();
		$('.'+subject).show();
		console.info('.'+subject);
		$('#selectedsubject').on('click',function(){
			$('.option').hide();
			$('#drpsection').val('');
			var subject=$(this).val();
			$('.'+subject).show();
			console.info('.'+subject);
		});
		
	});

</script>