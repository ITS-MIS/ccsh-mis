<?php						
	$passq = "SELECT password FROM tbl_loginstudent WHERE lrn = $lrn";			
	$result = mysql_query($passq) or die('SQL Error :: '.mysql_error());
		
		$row = mysql_fetch_row($result);
		$p=($row[0]);
?>
<style type="text/css">
			body {
				background-color: black;
				background-image:url("images/bg2.jpg");
				background-size: 100% 100%;
				background-attachment: fixed;
				background-repeat:no-repeat;
				position: absolute; 
				top: 0;
				right: 0;
				left: 0;
				bottom: 0;	
				}
</style>

<?php
	// Initialize variables and set to empty strings
	$opass = $npass = $npass2 = "";
	$opassErr = $npassErr = $npass2Err = $matchErr = $matchErr2 = "";

	// Control variables
	$app_state = "empty";  //empty, processed, logged in
	$valid = 0;

	// Validate input and sanitize
	if ($_SERVER['REQUEST_METHOD']== "POST") {

   		if (empty($_POST["opass"])) {

			$opassErr = "*Password is required";
	     	$opass = $npass = $npass2 = "";
   		
   		} else {

			$opass = test_input($_POST["opass"]);
	      	$valid++;
	   	}
   		
   		if (empty($_POST["npass"])) {

			$npassErr = "*New Password is required";
	     	$npass = $npass2 = "";
   		
   		} else {

			$npass = test_input($_POST["npass"]);
	      	$valid++;
	   	}

	   	if (empty($_POST["npass2"])) {
	  		
	  		$npass2Err = "Confirm Password";
	     	$npass = $npass2 = "";
	   	
	   	} else {

    		$npass2 = test_input($_POST["npass2"]);
	      	$valid++;
    	}
    	
    	if (!empty($_POST["opass"]) && $p != $opass) {
			$matchErr = "Wrong Password! Try again!";
			$opass = $npass = $npass2 = "";

		} else {

			$opass = test_input($_POST["opass"]);
			$npass = test_input($_POST["npass"]);
			$npass2 = test_input($_POST["npass2"]);
			$valid++;
		}

		if (!empty($_POST["npass"]) && !empty($_POST["npass2"]) && $npass != $npass2) {
			$matchErr = "Password don't match! Try again!";
			$npass = $npass2 = "";

		} else {

			$opass = test_input($_POST["opass"]);	
			$npass = test_input($_POST["npass"]);
			$npass2 = test_input($_POST["npass2"]);
			$valid++;
		}

		if ($valid >= 5) {
		      $app_state = "processed";
		   }
	}

	// Sanitize data
	function test_input($data) {

		$data = trim($data);
	   	$data = stripslashes($data);
	   	$data = htmlspecialchars($data);
	   	return $data;
	}

	if ($app_state == "empty") {

	getDatatablesLink();
	getCcshsPageLink();
	getDatatablesScript();
?>			
		<div class="container-fluid">
			<div class="row">			
				
			<?php
				include('current-year.php');
				$select = get_db("SELECT section_id FROM tbl_studentstatus WHERE lrn=$lrn AND sy_id=$sy_id");
				$select=$select['section_id'];

				if($select==0) {

					$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level FROM tbl_studentinfo a 
								LEFT JOIN tbl_studentstatus b ON a.lrn=b.lrn
								LEFT JOIN tbl_yearlevel c ON b.year_id=c.year_id WHERE a.lrn = $lrn AND b.sy_id=$sy_id";
								
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
				
					if (mysql_num_rows($result)>0) {
				
						while($row = mysql_fetch_row($result)) {
					
							$lrn2 = $row[0];
							$lastname = $row[1];
							$firstname = $row[2];
							$middlename = $row[3];
							$year = $row[4];
						}
						$section="Unassigned";
					}	
				}
				else {
					$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
								WHERE b.year_id = c.year_id AND b.section_id = d.section_id AND a.lrn = b.lrn AND a.lrn = $lrn AND b.sy_id=$sy_id";
								
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
				
					if (mysql_num_rows($result)>0) {
				
						while($row = mysql_fetch_row($result)) {
					
							$lrn2 = $row[0];
							$lastname = $row[1];
							$firstname = $row[2];
							$middlename = $row[3];
							$year = $row[4];
							$section = $row[5];							
						}
					}	
				}		
			?>
				
				<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><label id="header">Edit Account</label></div>
						<div class="col-md-1"></div>
				</div>
				
				<div class="col-sm-3 col-md-2"></div>
				
				<form action="<?php ($_SERVER['PHP_SELF']);?>" method="post">
					
				<div class="col-sm-3 col-md-8" style="background-color: rgb(255,255,50); border: 3px solid; border-color: gold"><br>	
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="lrn2">LRN: </label>
								<label for="lrn2"><?php echo $lrn2; ?></label>
							</div>
						</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="yr">Year Level: </label>
									<label for="lrn2"><?php echo $year; ?></label>
								</div>
							</div>
					</div>
					
						<div class="row">				
							<div class="col-md-8">
								<div class="form-group">
									<label for="lname">Student's Name: </label>
									<label for="lrn2"><?php echo $lastname .", ". $firstname ." ". $middlename; ?></label>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="section">Section: </label>
									<label for="section"><?php echo $section; ?></label>
								</div>
							</div>							
						</div><br><br><br>

						<div class="row">						
							<div class="col-md-1"></div>						
							<div class="col-md-4">
								<label for="pass">Input Password</label>	
							</div>
						
							<div class="col-md-4">
								<div class="form-group">		
									<input type="password" class="form-control" id="opass" name="opass" placeholder="Old Password"
										value="<?php echo $opass; ?>" oncopy="return false" onpaste="return false" onkeypress="return isPassword(event)">
										<span class="error"><?php echo $opassErr; ?></span>
								</div>
							</div>							
						</div>
						
						<div class="row">						
							<div class="col-md-1"></div>						
							<div class="col-md-4">
								<label for="npass">Input New Password</label>	
							</div>
						
							<div class="col-md-4">
								<div class="form-group">		
									<input type="password" class="form-control" id="npass" name="npass" placeholder="New Password"
										value="<?php echo $npass; ?>" oncopy="return false" onpaste="return false" onkeypress="return isPassword(event)">
										<span class="error"><?php echo $npassErr; ?></span>
								</div>
							</div>							
						</div>
						
						<div class="row">
						
							<div class="col-md-1"></div>						
							<div class="col-md-4">
								<label for="npass2">Retype New Password</label>
							</div>						
							<div class="col-md-4">
								<div class="form-group">
									
									<input type="password" class="form-control" id="npass2" name="npass2" placeholder="Confirm Password"
										value="<?php echo $npass2; ?>" oncopy="return false" onpaste="return false" onkeypress="return isPassword(event)">
										<span class="error"><?php echo $npass2Err; ?></span>
								</div>
							</div>							
						</div>
							
						<div class="row">						
							<div class="col-md-5"></div>							
							<div class="col-md-5">
								<button type="submit" class="btn btn-success" name="btnSubmit" onclick="return confirm('Are you sure you want to Submit?');">Change Password</button>
							</div>							
						</div><br>

						<div class="row">						
							<div class="col-md-4"></div>						
							<div class="col-md-5"><span class="error"> <?php echo $matchErr; ?></span></div>
						</div>

						<div class="row">						
							<div class="col-md-4"></div>						
							<div class="col-md-5"><span class="error"> <?php echo $matchErr2; ?></span></div>
						</div><br>
					</div>					
				</form>
				<div class="col-sm-3 col-md-2"></div>					
			</div>
		</div>
<?php
}
	elseif ($app_state == "processed") {
	 
		$npass = $_POST['npass'];
		$npass2 = $_POST['npass2'];
		   			
		$update = "UPDATE tbl_loginstudent set password='$npass' where lrn = $lrn";
		mysql_query($update) or die("error updating in sql");
				
		$app_state = "good";			
	}

	if ($app_state == "good") {    
?>	   
    	<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-5">
				<label for="lname"></label>
				<?php
					$confirm=("successfully changed the password!");
					echo "<script type='text/javascript'>alert(\"$confirm\");</script>";
				?>
				<script type="text/javascript">window.location.href="index2.php?mode=Student&category=Home&page=0";</script>
			</div>
		</div>
	<?php
	}
