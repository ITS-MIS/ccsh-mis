<?php	
	getDatatablesLink();
	getDatatablesScript();

		$arraysy=get_db_array("SELECT sy_id, start, end FROM tbl_sy WHERE sy_id=$sy_id");

		foreach ($arraysy as $key => $fieldname) {
			$id = $arraysy[$key]['sy_id'];
			$start = $arraysy[$key]['start'];
			$end = $arraysy[$key]['end'];
		}

		$start = DateTime::createFromFormat("Y-n-d", $start);
		$syear=$start->format("Y"); //year
		$smonth=$start->format("F"); //month
		$sday=$start->format("n"); //day

		$end = DateTime::createFromFormat("Y-n-d", $end);
		$eyear=$end->format("Y"); //year
		$emonth=$end->format("F"); //month
		$eday=$end->format("n") //day
?>

<div class="container-fluid">
	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Dates to Remember</label>
			</div>
			<div class="col-md-1"></div>
	</div>	
	
	<div class="row">
			<h4><b>School Year : <?php echo $currentsy; ?> </b></h4><br>
			<Label><a data-toggle="modal" data-target="#syclass" data-sdate="<?php echo $arraysy[$key]['start']; ?>" data-edate="<?php echo $arraysy[$key]['end']; ?>" data-id="<?php echo $array1[$key]['sy_id']; ?>" class="data">
			start of class: <?php echo $smonth .' '. $sday .', '. $syear ?> <br>
			end of class: <?php echo $emonth .' '. $eday .', '. $eyear ?> </a> 
			<i>&nbsp;click to change</i>
			</Label>
	</div><br>
	
	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<Label id="header2">Submission of Grades Schedule</Label>
			</div>
			<div class="col-md-1"></div>
	</div>

	<div class="row">
		<form method="post">
			<table id="dates" class="display" cellspacing="0" width="100%" style="background-color:gold;">
		     
		        <thead>
		            <tr>
		            	<th>Quarter</th>
		            	<th>ID</th>
		                <th>Start</th>
		                <th>End</th>
		            </tr>		                
		        </thead>
		 			
		        <tbody>
			       <?php
						$array1 = get_db_array("SELECT id, sq1, eq1 FROM tbl_curriculum_sy where sy_id = $sy_id");
						if(count($array1)>0){
			 				foreach ($array1 as $key => $fieldname) {
								?>
				           		<tr  data-toggle="modal" data-target="#datemodal1" data-id1="<?php echo $array1[$key]['id']; ?>" 
				           				data-sq1="<?php echo $array1[$key]['sq1']; ?>" data-eq1="<?php echo $array1[$key]['eq1']; ?>" data-data="true" class="data">
				     				<td>1st Quarter</td>
									<?php
									foreach ($fieldname as $key2 => $value) {
										?>
			            				<td><?php echo $value ?></td>
			            				<?php
			            			}
			            			?>	                             
				            	</tr>
				            <?php					 
					        }
					    }
				    ?>
				    <?php
						$array2 = get_db_array("SELECT id, sq2, eq2 FROM tbl_curriculum_sy where sy_id = $sy_id");
						if(count($array2)>0){
			 				foreach ($array2 as $key => $fieldname) {
								?>
				           		<tr  data-toggle="modal" data-target="#datemodal2" data-id2="<?php echo $array2[$key]['id']; ?>" 
				           				data-sq2="<?php echo $array2[$key]['sq2']; ?>" data-eq2="<?php echo $array2[$key]['eq2']; ?>" data-data="true" class="data">
				     				<td>2nd Quarter</td>
									<?php
									foreach ($fieldname as $key2 => $value) {
										?>
			            				<td><?php echo $value ?></td>
			            				<?php
			            			}
			            			?>	                             
				            	</tr>
				            <?php
					        }
					    }
				    ?>
				    <?php
						$array3 = get_db_array("SELECT id, sq3, eq3 FROM tbl_curriculum_sy where sy_id = $sy_id");
						if(count($array3)>0){
			 				foreach ($array3 as $key => $fieldname) {
								?>
				           		<tr  data-toggle="modal" data-target="#datemodal3" data-id3="<?php echo $array3[$key]['id']; ?>" 
				           				data-sq3="<?php echo $array3[$key]['sq3']; ?>" data-eq3="<?php echo $array3[$key]['eq3']; ?>" data-data="true" class="data">
				     				<td>3rd Quarter</td>
									<?php
									foreach ($fieldname as $key2 => $value) {
										?>
			            				<td><?php echo $value ?></td>
			            				<?php
			            			}
			            			?>	                             
				            	</tr>
				            <?php
					        }
					    }
				    ?>
				    <?php
						$array4 = get_db_array("SELECT id, sq4, eq4 FROM tbl_curriculum_sy where sy_id = $sy_id");
						if(count($array4)>0){
			 				foreach ($array4 as $key => $fieldname) {
								?>
				           		<tr  data-toggle="modal" data-target="#datemodal4" data-id4="<?php echo $array4[$key]['id']; ?>" 
				           				data-sq4="<?php echo $array4[$key]['sq4']; ?>" data-eq4="<?php echo $array4[$key]['eq4']; ?>" data-data="true" class="data">
				     				<td>4th Quarter</td>
									<?php
									foreach ($fieldname as $key2 => $value) {
										?>
			            				<td><?php echo $value ?></td>
			            				<?php
			            			}
			            			?>	                             
				            	</tr>
				            <?php
					        }
					    }
				    ?>

				</tbody>
			</table>
		</form>
	</div><br><br>

	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-4"><Label id="header2">Important Dates</Label></div>
	</div>

	<div class="row">
		<div class="col-sm-3 col-md-4" style="background-color: rgb(255,255,50); border: 3px solid; border-color: gold">
			<form method="post" action="holiday-submit.php"><br><br><br>
				<label for="holidays" class="col-sm-12 control-label"><i>Add Holidays, Special Occasion or Class Suspensions</i></label><br><br><br>
				
				<div class="form-group">
					<label for="holidays" class="col-sm-4 control-label">Input Holidays</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="description" name="description" placeholder="Holiday Name" onkeypress="return isLetterDashPoint(event)" required>
					</div>
				</div><br><br>
				
				<div class="form-group">
					<label for="holidays" class="col-sm-4 control-label">Date</label>
					<div class="col-sm-8">
						<input type="date" class="form-control" id="date" name="date" required>
					</div>
				</div><br><br>

				<div class="form-group">
					<label for="type" class="col-sm-4 control-label">Type</label>
					<div class="col-sm-8">
						<select class="form-control" id="type" name="type" required>							
								<option>Holiday</option>
								<option>Special</option>
								<option>Suspension</option>
						</select>
					</div>
				</div><br><br>

				<div class="form-group">																	
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
				</div>

				<div class="form-group">
					<div class="col-sm-4"></div>					
					<div class="col-sm-4" style="text-align: center;">
						<button type="submit" class="btn btn-success success" name="btnHoliday" onclick="return confirm('Are you sure you want to submit?');">Add</button>
					</div>					
					<div class="col-sm-4"></div>
					<br><br><br>
				</div>
			</form>
		</div>

		<div class="col-md-8">

			<div class="bs-studgrade"><!--=============START of TABS--======================-->
			    <ul class="nav nav-tabs" id="holidayTab">
			    	<li class="active"><a href="#holiday">Regular Holidays</a></li>
			        <li class><a href="#special">Special Holidays</a></li>
			        <li class><a href="#suspension">Class Suspension</a></li>
			    </ul><br>

			    <div class="tab-content">
			        <div id="holiday" class="tab-pane fade in active"><!--=============START of  Regular Holiday--======================-->	
						<form method="post">
							<table id="holidays" class="display" cellspacing="0" width="100%" style="background-color:gold;">
						        <thead>
						            <tr>
						            	<th>ID</th>
						            	<th>Date</th>
						                <th>Description</th>
						                <th>Type</th>
						            </tr>	                
						        </thead>
						 			
						        <tbody>
							       <?php
										$holiday = get_db_array("SELECT id, date, description, type FROM tbl_holidays where sy_id = $sy_id AND type='Holiday'");
										
										if(count($holiday)>0){
							 				
							 				foreach ($holiday as $key => $fieldname) {
												?>
								           		
								           		<tr>
								     				<?php
													foreach ($fieldname as $key2 => $value) {
														?>
							            				<td><?php echo $value ?></td>
							            				<?php
							            			}
							            			?>	                             
								            	</tr>
								            <?php
									        
									        }
									    }
								    ?>
								</tbody>
							</table>
						</form>
			        </div><!--===============================================END of TAB7=========================-->
			       
			        <div id="special" class="tab-pane fade in"><!--=============START of  TAB8--======================-->	
						<form method="post">
							<table id="specials" class="display" cellspacing="0" width="100%" style="background-color:gold;">
						        <thead>
						            <tr>
						            	<th>ID</th>
						            	<th>Date</th>
						                <th>Description</th>
						                <th>Type</th>
						            </tr>
						                
						        </thead>
						 			
						        <tbody>

							       <?php
										$holiday = get_db_array("SELECT id, date, description, type FROM tbl_holidays where sy_id = $sy_id AND type='Special'");
										
										if(count($holiday)>0){
							 				
							 				foreach ($holiday as $key => $fieldname) {
												?>
								           		
								           		<tr>
								     				<?php
													foreach ($fieldname as $key2 => $value) {
														?>
							            				<td><?php echo $value ?></td>
							            				<?php
							            			}
							            			?>	                             
								            	</tr>
								            <?php
									        
									        }
									    }
								    ?>
								</tbody>
							</table>
						</form>
			        </div><!--===============================================END of TAB8=========================-->

			        <div id="suspension" class="tab-pane fade in"><!--=============START of  TAB9--======================-->
			        	<form method="post">
							<table id="suspensions" class="display" cellspacing="0" width="100%" style="background-color:gold;">
						        <thead>
						            <tr>
						            	<th>ID</th>
						            	<th>Date</th>
						                <th>Description</th>
						                <th>Type</th>
						            </tr>						                
						        </thead>
						 			
						        <tbody>
							       <?php
										$holiday = get_db_array("SELECT id, date, description, type FROM tbl_holidays where sy_id = $sy_id AND type='Suspension'");
										
										if(count($holiday)>0){
							 				
							 				foreach ($holiday as $key => $fieldname) {
												?>
								           		
								           		<tr>
								     				<?php
													foreach ($fieldname as $key2 => $value) {
														?>
							            				<td><?php echo $value ?></td>
							            				<?php
							            			}
							            			?>	                             
								            	</tr>
								            <?php
									        
									        }
									    }
								    ?>
								</tbody>
							</table>
						</form>
			        </div><!--===============================================END of TAB9=========================-->

			    </div>
			</div><!--=============END of TABS--======================-->
			
		</div>
	</div>
</div>

<!---========================== dateModal1 =====================-->
<div id="datemodal1" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Date Of Grades Submission 1st Quarter</h4>
	            </div>
	            

	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="date-grades-submit.php"><br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id1" name="id1">
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Start Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="sq1" name="sq1">
							</div>
						</div>

						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">End Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="eq1" name="eq1">	
							</div>
						</div><br>
						
						<div class="form-group">															
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div>

						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnUpdate1" onclick="return confirm('Are you sure you want to submit?');">Update</button>
							</div>						
							<div class="col-sm-4"></div>							
						</div>											
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>

	   		</div>
	   	</div>
	</div>
</div>

<!---========================== dateModal2 =====================-->
<div id="datemodal2" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Date Of Grades Submission 2nd Quarter</h4>
	            </div>
	            
	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="date-grades-submit.php"><br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id2" name="id2">
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Start Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="sq2" name="sq2">
							</div>
						</div>

						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">End Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="eq2" name="eq2">	
							</div>
						</div><br>
						
						<div class="form-group">															
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div>

						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnUpdate2" onclick="return confirm('Are you sure you want to submit?');">Update</button>
							</div>							
							<div class="col-sm-4"></div>							
						</div>												
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!---========================== dateModal3 =====================-->
<div id="datemodal3" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Date Of Grades Submission 3rd Quarter</h4>
	            </div>
	            
	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="date-grades-submit.php"><br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id3" name="id3">
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Start Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="sq3" name="sq3">
							</div>
						</div>

						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">End Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="eq3" name="eq3">	
							</div>
						</div><br>
						
						<div class="form-group">															
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div>

						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnUpdate3" onclick="return confirm('Are you sure you want to submit?');">Update</button>
							</div>							
							<div class="col-sm-4"></div>							
						</div>												
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!---========================== dateModal4 =====================-->
<div id="datemodal4" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Date Of Grades Submission 4th Quarter</h4>
	            </div>
	            
	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="date-grades-submit.php"><br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id4" name="id4">
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Start Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="sq4" name="sq4">
							</div>
						</div>

						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">End Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="eq4" name="eq4">	
							</div>
						</div><br>
						
						<div class="form-group">															
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div>

						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnUpdate4" onclick="return confirm('Are you sure you want to submit?');">Update</button>
							</div>							
							<div class="col-sm-4"></div>							
						</div>												
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!---========================== SY Class Modal =====================-->
<div id="syclass" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Start & End of Class</h4>
	            </div>
	            
	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="date-grades-submit.php"><br><br>
					  
						<div class="form-group">
							<label for="sdate" class="col-sm-4 control-label">Start Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="sdate" name="sdate" required>
							</div>
						</div>

						<div class="form-group">
							<label for="edate" class="col-sm-4 control-label">End Date</label>
							<div class="col-sm-7">
								<input type="date"  class="form-control" id="edate" name="edate" required>	
							</div>
						</div><br>
						
						<div class="form-group">															
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div>

						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnClassSY" onclick="return confirm('Are you sure you want to submit?');">Update SY Schedule</button>
							</div>							
							<div class="col-sm-4"></div>							
						</div>												
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<script type="text/javascript">

//----------------datatables
	$(document).ready(function() {
		var table=$('#holidays').dataTable({
			"info": true,
			"bFilter": true,
			"ordering": false,
			"paging": true
		} );
	});

	$(document).ready(function() {
		var table=$('#specials').dataTable({
			"info": true,
			"bFilter": true,
			"ordering": false,
			"paging": true
		} );
	});

	$(document).ready(function() {
		var table=$('#suspensions').dataTable({
			"info": true,
			"bFilter": true,
			"ordering": false,
			"paging": true
		} );
	});


	$(document).ready(function() {
		var table=$('#dates').dataTable({
			"info": false,
			"bFilter": false,
			"ordering": false,
			"paging": false
    } );

    	$('#dates tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );

//----------------MODAL

	$(function(){
	 	console.info('getIdFromRow');
	    $('#datemodal1').modal({
	        keyboard: true,
	        show:false,

	    }).on('show.bs.modal', function(){
	        var getIdFromRow = $(event.target).closest('tr').data('id1');
     			$('#id1').val(getIdFromRow);

	        var getsq1FromRow = $(event.target).closest('tr').data('sq1');	         
	         	$('#sq1').val(getsq1FromRow);	  

	        var geteq1FromRow = $(event.target).closest('tr').data('eq1');	         
	         	$('#eq1').val(geteq1FromRow);
 
		 });
	});

	$(function(){
	 	console.info('getIdFromRow');
	    $('#datemodal2').modal({
	        keyboard: true,
	        show:false,


	    }).on('show.bs.modal', function(){
	        var getId2FromRow = $(event.target).closest('tr').data('id2');
     			$('#id2').val(getId2FromRow);

	        var getsq2FromRow = $(event.target).closest('tr').data('sq2');	         
	         	$('#sq2').val(getsq2FromRow);	  

	        var geteq2FromRow = $(event.target).closest('tr').data('eq2');	         
	         	$('#eq2').val(geteq2FromRow);
 
		 });
	});

	$(function(){
	 	console.info('getIdFromRow');
	    $('#datemodal3').modal({
	        keyboard: true,
	        show:false,


	    }).on('show.bs.modal', function(){
	        var getId3FromRow = $(event.target).closest('tr').data('id3');
     			$('#id3').val(getId3FromRow);

	        var getsq3FromRow = $(event.target).closest('tr').data('sq3');	         
	         	$('#sq3').val(getsq3FromRow);	  

	        var geteq3FromRow = $(event.target).closest('tr').data('eq3');	         
	         	$('#eq3').val(geteq3FromRow);
 
		 });
	});

	$(function(){
	 	console.info('getIdFromRow');
	    $('#datemodal4').modal({
	        keyboard: true,
	        show:false,


	    }).on('show.bs.modal', function(){
	        var getId4FromRow = $(event.target).closest('tr').data('id4');
     			$('#id4').val(getId4FromRow);

	        var getsq4FromRow = $(event.target).closest('tr').data('sq4');	         
	         	$('#sq4').val(getsq4FromRow);	  

	        var geteq4FromRow = $(event.target).closest('tr').data('eq4');	         
	         	$('#eq4').val(geteq4FromRow);
 
		 });
	});

	$(function(){
	 	console.info('getIdFromRow');
	    $('#syclass').modal({
	        keyboard: true,
	        show:false,


	    }).on('show.bs.modal', function(){
	    	var getIdFromRow = $(event.target).closest('tr').data('id');
     			$('#id').val(getIdFromRow);

	        var getsdateFromRow = $(event.target).closest('tr').data('sdate');	         
	         	$('#sdate').val(getsdateFromRow);	  

	        var getedateFromRow = $(event.target).closest('tr').data('edate');	         
	         	$('#edate').val(getedateFromRow);
 
		 });
	});

//----------------TAB

	$(document).ready(function(){ 
    	$("#holidayTab a").click(function(e){
    		e.preventDefault();
    		$(this).tab('show');
    	});
	});

</script>