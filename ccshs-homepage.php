		<style type="text/css"> 
			#header {
				background-image:url("images/ccshs.JPG");
				background-size: 100% 100%;
				background-attachment: absolute;
				background-repeat:no-repeat;
				position: absolute;
				opacity: 255,255,255,0.4;				
				height: 80%;
				width: 100%;
				top: 50px;
				right: 0;
				left: 0;
				bottom: 0;	
			}

			#mainframe {
				position: absolute; 
				top: 85%;
				right: 110px;
				left: 110px;
				bottom: 0;
				height: 100%;
				font-family: "Open sans", Helvetica, Arial;
			}
			
			#contact{
				background-color: black;
				background-image:url("images/bg2.jpg");
				background-size: 100% 100%;
				background-attachment: absolute;
				background-repeat:no-repeat;
				position: absolute; 
				font-size: 13px;
				width: 100%;
				height: 200px;
				color: black;
				top: 200%;
				right: 0;
				left: 0;
				bottom: 0;
			}
			
			#title {
				text-shadow: 2px 2px black;
				height:60px;
				width: 620px;
				font-family: "Open sans", Helvetica, Arial;
			}
			
		</style>
		
<div id="container-fluid">
	<div id="header"></div>

	<div id="mainframe">	
		<div <?php if(!isset($_GET['loginfailed'])){ echo 'style="display:none"';}?> class="alert alert-danger" role="alert">Login failed: username and password mismatched.</div>
		<br>	
			<div id="title"> <font color="gold" size="6" style="border-bottom-style: inset; border-bottom-color: goldenrod;">
			<b>Student Information System</b></font> </div>
				<p style="margin-left: 25px">Sign In: Kindly choose in the upper right corner (Student/Employee) and click the type of user to login.</p>
				<p style="margin-left: 25px; margin-right: 25px"><font color="#e50000"><b>***WARNING***</b></font><br>
				This is a private system. Unathorized access of this system is strictly prohibited.
				By continuing, you acknowledge your awareness of and concurrence with the Use Policy of Caloocan City Science High School.
				Unauthorized users may be subject to criminal prosecution under the law and are subject to disciplinary action under the school policies.</p>		
		<br><hr>
			<div id="title"> <font color="gold" size="6" style="border-bottom-style: inset; border-bottom-color: goldenrod;">
			<b>About CCSHS</b></font> </div>
			<p style="margin-left: 25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Caloocan City Science High School stands behind the Division Office along 10th. Avenue Caloocan City corner P. Sevilla Street in Grace Park. The school came into existence through the efforts of Dr. Victoria Q. Fuentes, the Schools Division Superintendent then the benevolence of the City Mayor, the Honorable Reynaldo O. Malonzo, who saw the need for a Science High School that would cater to the underprivileged but talented students of the city. </p>
			<p style="margin-left: 25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; On June 1, 1998, the dream of the City Mayor and the Schools Division Superintendent became a reality when the 360 students of the DOST Special Science Classes from the Caloocan High School were housed on a newly renovated Sangguniang Panglungsod, a 3-storey building at the back of the Division Office. The building has eight academic rooms, a library, stockroom two computer rooms, a small guidance office, an Audio Visual Room where programs are held, the Office of the Principal and a Faculty room. </p>
			<p style="margin-left: 25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; On October 23, 1998, the Caloocan City Science High School was inaugurated with the Honorable City Mayor Reynaldo O. Malonzo, and Dr. Victoria Q. Fuentes as principal sponsors, the visionaries of CCSHS. </p>
			<p style="margin-left: 25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; On September of the same year, upon the promotion to Assistant Schools Division Superintendent of Mrs. Lourdes Faustino, the newly appointed Principal Mrs. Esmilita P. Ulangca and Ms. Maria Paz Marcelo, the designated Assistant to the Principal, promulgated the guidelines for the admission of freshmen to CCSHS. Thereafter, a selection test was administered to incoming first year students. To qualify for admission, the applicants should pass the examination, which consists of proficiency tests in English, Science and Mathematics and Logical Reasoning for the first round, and an interview for the second round. A 60–40 percentage aggregate scores of the examinees decides their admission to the school. Through the local school board funding, the school was able to acquire science equipment and 50 computers with printers.</p>
			<p style="margin-left: 25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In 2000 the school ranked first in the Division of Caloocan, sixth in the National Capital Region, and fourteen in the National Level in the 2000 National Secondary Aptitude test. In July of the same year, the school established its autonomous Student Government with Ms. Mary Jane Toledana as the first president and Mr. Ferdinand A. de Leon in his capacity as the Coordinator of the Student government. </p> 
			<p style="margin-left: 25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; On January 2009, Caloocan City Science High School was officially recognized as an independent science high school by the Department of Education – National Capital Region (DepEd-NCR) and by virtue of the Republic Act No. 9676, Caloocan City Science High School was recognized as an independent Science High School by the Republic of the Philippines. </p>
			<p style="margin-left: 25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Today, the school consistently maintains its standing as one of the Top 10 Outstanding Schools based from National Achievement Test in the entire National Capital Region. </p>		
	</div>
	<div id="contact"><br>
		<h4 style="margin-left: 110px">Contact</h4>
		<p style="margin-left: 110px">P. Sevilla St. Corner, 10th Avenue<br>
		Caloocan City, Metro Manila Philippines<br>
		Telefax: (02) 990-77-11<br>
		Email: ccshs.deped@gmail.com</p>
	</div>
</div>

	