<?php
	getDatatablesLink();
	getDatatablesScript();
	getRegistrarPageScript();
?>	
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Entrance Exam Result</label>
			</div>
			<div class="col-md-1"></div>
		</div>

	<div class="row">
	    <form method="POST" action="reports-entranceexam.php" class="form-horizontal"> 
	    		<a type="button" href="reports-entranceexam.php" class="btn btn-success success left-margin" target="_blank style="float: right;"">Print result</a>
		</form>	
	</div><br>

	<div class="row">
		<?php 
			$sy_id=get_db("SELECT sy_id FROM tbl_sy ORDER BY sy_id DESC LIMIT 1");
			$sy_id=$sy_id['sy_id'];

			$result=get_db_array("SELECT id, lastname, firstname, middlename,score from tbl_applicantinfo where score!='' and sy_id=$sy_id order by score desc");	
		?>
	
		<form method="post" action="admission-input-entrance-exam-submit.php" class="form-horizontal" >
			<table id="applicants" class="display" cellspacing="0" width="100%" style="background-color:gold;" >
				<thead >
					<tr>
						<th>ID</th>
						<th>Last Name</th>
						<th>First Name</th>
						<th>Middle Name</th>
						<th>Score</th>
						
					</tr>													
				</thead>			
				<tbody>
			
					<?php
					if(count($result)>0){
						foreach ($result as $key => $fieldname) {
							?>
							<tr>
							<?php
							foreach ($fieldname as $key2 => $value) {

								if ($key2=='score') {
									if ($value==0 or $value =='' or $value==null) {
										?>
										<td >
											<input type="text" class="form-control" id="score" placeholder="Score" name="grade[<?php echo $result[$key]['id'] ?>]" style="width:80px;height:25px" maxlength="5" onkeypress="return isNumber(event)">
										           	
										<?php
									}
									else{
										?>												
										<td class="grade" style="width:200px;" >									
											
											<?php echo $value; ?>
											
									    </td>
										<?php
									}
								}
								else{
									?>
									<td ><?php echo $value;  ?></td>
									<?php										
								}
							}
							
							?>
							</tr>
							<?php								
						}
					}
					?>
						
								
				</tbody>
					
			</table>	
		
			<div class="form-group">															
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
			</div>
		</form>		
	</div>	
	
<script type="text/javascript">
		
	$(document).on("click", "td.grade a", function() {
	    var parent = $(this).parent();
	    $(this).replaceWith("<input type='text' class='form-control'  value='" + $(this).data('grade') + "' name='grade[" + $(this).data('id') + "]' style='margin-left:20px;width:80px;height:25px;' maxlength='5' >"); //closing angle bracket added
	    parent.children(":text").focus();
	    return false;
	});

</script>	
<script type="text/javascript" src="./js/number.js"></script>