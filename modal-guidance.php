<?php
	include'connect.php';
	
	$lrn=$_POST['lrn'];
	$currentdate = date("Y-m-d");
	$cureentdatet=strtotime($currentdate);

	$today = date("Y-m-d H:i:s");
					
	$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
					FROM tbl_studentinfo a left join  tbl_studentstatus b on a.lrn=b.lrn left join  tbl_yearlevel c on b.year_id=c.year_id 
					left join tbl_section d on b.section_id=d.section_id 
					where a.lrn=$lrn";
					
	$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());	
	if (mysql_num_rows($result)>0) {
		while($row = mysql_fetch_assoc($result)) {
			$lrn = $row['lrn'];
			$lastname = $row['lastname'];
			$firstname = $row['firstname'];
			$middlename = $row['middlename'];
			$year = $row['year_level'];
			$section = $row['section_name'];
		}
	}

	$iquery = "SELECT image FROM tbl_images WHERE lrn = $lrn AND verify=1";			
	$result = mysql_query($iquery);
		
	$row = mysql_fetch_row($result);
	$image=($row[0]);
	$recordminor=get_db_array("SELECT date from tbl_guidancerecord where lrn=$lrn and date like '{$currentdate}%' and end_date is null");
	foreach($recordminor as $key => $columnname){
		foreach ($columnname as $key => $value) {
			$intervaltime=strtotime($value)+(60*5);
			$todayt=strtotime($today);
			if($intervaltime>$todayt){
				$pause=true;
			}
		}
	}

	$resultenddate=get_db_array("SELECT end_date FROM tbl_guidancerecord where lrn=$lrn and end_date is not null");
	foreach ($resultenddate as $key => $columnname) {
		foreach ($columnname as $key => $value) {
			$end_datet=strtotime($value);
			if ($cureentdatet<$end_datet) {
				$suspended=true;
			}		
		}		
	}			
?>
		<div class="modal-content">
            <div class="modal-header" style="background-color:gold;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lrn">Student's Disciplinary Record</h4>
            </div>
            
            <div class="modal-body  modal-height">
            	<form role="form" method="post" action="students-record-submit.php">

            	<div class="row">
					<div class="col-md-11" style="text-align:right;">
						<div class="form-group">
							<img id="image" src="<?php echo $image; ?>" alt="" height="150px" width="150px"/> 
						</div>
					</div>					
				</div>
			
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-5" style="text-align: left;">
						<div class="form-group">
							<label for="lrn">LRN: <?php echo $lrn; ?></label>
						</div>
					</div>

					<div class="col-md-5" style="text-align: right;">
						<div class="form-group">
							<label for="year">Year Level: <?php echo $year; ?></label>
						</div>
					</div>
					<div class="col-md-1"></div>							
				</div>
				
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-5" style="text-align: left;">
						<div class="form-group">
							<label for="name">Student's Name: <?php echo $lastname .', '. $firstname .' '. $middlename ?></label>		
						</div>
					</div>

					<div class="col-md-5" style="text-align: right;">
						<div class="form-group">
							<label for="section">Section: <?php echo $section; ?></label>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div><br>

			<?php
			if(isset($suspended)){
				?>
				<span style="margin-left:100px;color:red;">Curently suspended</span>
				<?php
			}
			elseif (isset($pause) ) {
				?>
				<span style="margin-left:100px;color:red;">5 minutes interval</span>
				<?php
			}
			else{
				?><br>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="offense">Offense</label>
						</div>			
					</div>

					<div class="col-md-8">
						<select id="uoffense" name="offense" size="1" style="width: 300px;">
						
								<?php
									$sql_minor = "SELECT offense_desc FROM tbl_offense WHERE offense_type = 'Minor' order by offense_id desc";
									$result = mysql_query($sql_minor);
								?>
								
							<optgroup label="Minor Offense">
							
							<?php
								while($row = mysql_fetch_array($result)) {
							?> 
									<option><?php echo $row['offense_desc']; ?> </option>
							<?php		
								}
							?>
							</optgroup>
							<optgroup label="Major Offense">
							
								<?php
									$sql_major = "SELECT offense_desc FROM tbl_offense WHERE offense_type = 'Major' order by offense_id desc";
									$result = mysql_query($sql_major);

										while($row = mysql_fetch_array($result)) {
								?> 
											<option><?php echo $row['offense_desc']; ?> </option>
										
									<?php 	
										} 
									?>
							</optgroup>
						</select>						
					</div>
					
				</div>
								
				<div class="row">
					<div class="col-md-2"></div>					
					<div class="col-md-8">
						<div class="form-group">
						  <label for="remarks">Remarks:</label>
						  <textarea class="form-control" rows="2" id="uremarks" name="remarks" style="resize:none"></textarea>
						</div>
					</div>								
					<div class="col-md-2"></div>					
				</div>
				
				<div class="form-group">															
					<input type="hidden" id="ulrn" name="lrn2" value="<?php echo $lrn; ?>">
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">					
				</div>

				<div class="row">
					<div class="col-md-4"></div>					
					<div class="col-md-4" style="text-align: center;">
						<button data-target="#update" data-toggle="modal" type="button" class="btn btn-success success">Add Record</button>
						<button type="reset" class="btn btn-default" name="btnReset">Reset</button>
					</div>					
					<div class="col-md-4"></div>
				</div>

			<?php
				}
			?>
				<br><br>
			</form>
				<table id="guidance" class="display" cellspacing="0" width="100%" style="background-color:gold;">
			        <thead>
			            <tr>
			            	<th>Date</th>
			                <th>Offense</th>
			                <th>Penalty</th>
			                <th>End date</th>
			                <th>Remarks</th>
			            </tr>
			        </thead>

			        <tbody>
				        <?php  
							$query = "SELECT a.id, a.lrn, date, c.offense_desc, d.penalty_desc, a.remarks,end_date
										FROM tbl_guidancerecord a LEFT JOIN tbl_offense c ON a.offense_id=c.offense_id
										LEFT JOIN tbl_penalty d ON a.penalty_id=d.penalty_id
										WHERE lrn=$lrn ORDER By date desc";
							
							$result = mysql_query($query) or die(mysql_error());
							if(mysql_num_rows($result) > 0) {								
								while ($row = mysql_fetch_assoc($result)) {														
									$id = $row['id'];
									$lrn = $row['lrn'];
									$dater = $row['date'];
									$offense_desc = $row['offense_desc'];
									$penalty_desc = $row['penalty_desc'];
									$remarks = $row['remarks'];
									$end_date = $row['end_date'];
						?>
					            <tr>
					                <td width="15%"><?php echo $dater; ?></td>
					                <td><?php echo $offense_desc; ?></td>
					                <td><?php echo $penalty_desc ?></td>
					                <td width="15%"><?php echo $end_date; ?></td>
					        		<td width="15%"><?php echo $remarks; ?></td>
					            </tr>
						
						<?php
					            }
					        }
					    ?>
					</tbody>
				</table>
			</div>

		    <div class="modal-footer" style="background-color:gold;">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
	        </div>
   		</div>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="students-record-submit.php">
            		<input type="hidden" id="updatelrn" name="ulrn">
            		<input type="hidden" id="updateoffense"name="offense">
            		<input type="hidden" id="updateremarks" name="remarks" >					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnSubmit">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
		$('#guidance').dataTable( {
			bInfo: false,
			"bFilter": false,
			"ordering": false,
			paging:false
		});
	} );

	$(function(){
    	$('#update').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	         var getlrnFromRow =$('#ulrn').val();;
				$('#updatelrn').val(getlrnFromRow);

	        var getoffenseFromRow = $('#uoffense').val();;	         
	         	$('#updateoffense').val(getoffenseFromRow);
	         
	        var getremarksFromRow = $('#uremarks').val();;
	         	$('#updateremarks').val(getremarksFromRow);	 
	 			
		        console.info(getlrnFromRow);
		});
	});
	
</script>