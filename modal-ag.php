<?php
	include'connect.php';

		$lrn=$_POST['lrn'];
		$id=$_POST['id'];

		$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
									FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
									WHERE b.year_id = c.year_id and b.section_id = d.section_id and a.lrn = b.lrn and a.lrn=$lrn";
									
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
					
					if (mysql_num_rows($result)>0) {					
						while($row = mysql_fetch_assoc($result)) {						
							$lrn = $row['lrn'];
							$lastname = $row['lastname'];
							$firstname = $row['firstname'];
							$middlename = $row['middlename'];
							$year = $row['year_level'];
							$section = $row['section_name'];							
						}
					} 

					$iquery = "SELECT image FROM tbl_images WHERE lrn = $lrn AND verify=1";			
					$result = mysql_query($iquery);
						
						$row = mysql_fetch_row($result);
						$image=($row[0]);	
?>

 				<div class="modal-content">
		            <div class="modal-header" style="background-color:gold;">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title"> Disciplinary Record</h4>
		            </div><br>

		            <div class="row">
						<div class="col-md-11" style="text-align:right;">
							<div class="form-group">
								<img id="image" src="<?php echo $image; ?>" alt="" height="150px" width="150px"/> 
							</div>
						</div>						
					</div>

		            <div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-5" style="text-align: left;">
							<div class="form-group">
								<label for="lrn">LRN: <?php echo $lrn; ?></label>
							</div>
						</div>

						<div class="col-md-5" style="text-align: right;">
							<div class="form-group">
								<label for="year">Year Level: <?php echo $year; ?></label>
							</div>
						</div>
						<div class="col-md-1"></div>							
					</div>
						
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-5" style="text-align: left;">
							<div class="form-group">
								<label for="name">Student's Name: <?php echo $lastname .', '. $firstname .' '. $middlename ?></label>		
							</div>
						</div>

						<div class="col-md-5" style="text-align: right;">
							<div class="form-group">
								<label for="section">Section: <?php echo $section; ?></label>
							</div>
						</div>
						<div class="col-md-1"></div>
					</div><br><br>
		            
		            <div class="modal-body  modal-height"> 
						<table id="guidance" class="display" cellspacing="0" width="100%" style="background-color:gold;">
					        <thead>
					            <tr>
					            	<th>Date</th>
					                <th>Offense</th>
					                <th>Penalty</th>
					                <th>Remarks</th>
					            </tr>							                
					        </thead>

					        <tbody>
					        <?php  
								$query = "SELECT a.lrn, date, c.offense_desc, d.penalty_desc, a.remarks
											FROM tbl_guidancerecord a LEFT JOIN tbl_offense c ON a.offense_id=c.offense_id
											LEFT JOIN tbl_penalty d ON a.penalty_id=d.penalty_id
											WHERE lrn=$lrn ORDER By date desc";
								$result = mysql_query($query) or die(mysql_error());
								if(mysql_num_rows($result) > 0) {
																
									while ($row = mysql_fetch_assoc($result)) {
										$lrn = $row['lrn'];
										$dater = $row['date'];
										$offense_desc = $row['offense_desc'];
										$penalty_desc = $row['penalty_desc'];
										$remarks = $row['remarks'];												
							?>
						            <tr>								                
						                <td width="15%"><?php echo $dater; ?></td>
						                <td><?php echo $offense_desc; ?></td>
						                <td><?php echo $penalty_desc ?></td>
						        		<td width="15%"><?php echo $remarks; ?></td>
						            </tr>									
							<?php
						            }
						        }
						    ?>
							</tbody>
						</table>
					</div>
					<form method="post" action="principal-guidance.php">
				    <div class="modal-footer" style="background-color:;">
				    	<p class="text-warning" style="text-align:center"><small>Click <b>Close</b> button to update the notification!</small></p>
				        <button id="closeview" type="submit" class="btn btn-success success" data-dismiss="modal" name="close">Close</button>
			        </div>
			        </form>
		   		</div>
<?php
	query_db("UPDATE tbl_guidancerecord SET ptd2='' where id=$id");	
?>

<script type="text/javascript">

	$(document).ready(function() {
		$('#guidance').dataTable( {
			bInfo: false,
			"bFilter": false,
			"ordering": false,
			paging:false
		});
	});

	$('#closeview').on('click',function(){
		window.location.href="index2.php?mode=Faculty&category=Guidance%20Record&page=4";
	});

</script>