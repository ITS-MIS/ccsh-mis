<?php	
	getDatatablesLink();
	getDatatablesScript();	

	include('current-year.php');

	$section_names=array();			
?>

<style type="text/css">
	#contextMenu {
  		position: absolute;
  		display:none;
	}
</style>

<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<label id="header">Student-Section Loading</label>
	</div>
	<div class="col-md-1"></div>
</div>

<div class="row"  style="line-height:50px;">	
	<div class="col-md-3">	
		<Label>School Year : <?php echo $sy; ?> </Label>
	</div>
	<div class="col-md-9">
		<label>Year Level</label>
		<div class="btn-group btn-left-padding"> <a class="btn btn-default dropdown-toggle btn-select" data-toggle="dropdown" href="#">
			
			<?php 
			 	$result=get_db_array("SELECT year_level FROM tbl_yearlevel ");

				if(!isset($_GET['year_level'])) {
				
					$_GET['year_level'] = $result[0]['year_level'];
				
					echo $_GET['year_level'];
					$yearlvl=$_GET['year_level'];		

				}
				else {
				
					echo $_GET['year_level'];	
					$yearlvl=$_GET['year_level'];	 												
				}
			?>		

			<span class="caret"></span></a>
	            <ul class="dropdown-menu">
		        
		            <?php

					 	foreach ($result as $key => $filedname) {
					 		foreach ($filedname as $key => $value) {
					 			?>
						 		<li>
			                		<a href="index2.php?mode=<?php echo $_GET['mode']?>&category=<?php echo $_GET['category']?>&page=<?php echo $_GET['page']?>&year_level=<?php echo $value; ?>"><?php echo $value; ?></a>
			                	</li>
			                <?php
	           		
					 		}					
					 	}
		            ?>
           	 	</ul>
        </div>
    </div>
</div>

<?php

//TAB---------------------------------------------------------------------------------------------------------------------
	
	$result=get_db_array("SELECT year_id FROM tbl_yearlevel where year_level='$yearlvl' ")or die(mysql_error());

	$year_id=$result[0]['year_id'];
	$section_ids=get_db_array("SELECT DISTINCT section_id FROM tbl_studentstatus where sy_id=$sy_id AND year_id =$year_id and section_id!=0  order by section_id");
	$tabno=count($section_ids);
	
	if($tabno>0){
		
		foreach ($section_ids as $key => $section_id) {
			foreach ($section_id as $key => $value) {
				$section=get_db("SELECT section_name FROM tbl_section where section_id=$value")	;
				array_push($section_names,$section['section_name']);
			}
		}
	}

?>
<div>

  <!-- Nav tabs -->
  	<ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#unallocated" aria-controls="home" role="tab" data-toggle="tab"><?php if ($tabno==0) echo "List of Students"; else echo "unallocated Students"?></a></li>
	    <?php
	    if (isset($section_names)) {
	    	   
	    	foreach ($section_names as $key => $value) {
	    		
	    		?>
	    		<li role="presentation">
	    		<a href="#<?php echo "tab-".$value;?>" aria-controls="<?php echo "tab-".$value;?>" role="tab" data-toggle="tab"><?php echo $value; ?></a>
	    		</li>
    			<?php    		
	    	}
	    }
	    ?>
	</ul>

		

  <!-- Tab panes -->
	<div class="tab-content">

	    <div role="tabpanel" class="tab-pane active" id="unallocated">
	    	<br>
			<div class="row">
				<div class="col-md-2">
					<label style="margin-left:20px">Total Student : </label>
					<label id="totalno"></label>
				</div>

				<form method="post" action="student-section-loading-submit.php" style="text-align:left;">
					<input type="hidden" style="width:50px" name="year_id" value="<?php echo $year_id;?>" >
					<input type="hidden" style="width:50px" name="sy_id" value="<?php echo $sy_id;?>" >
					<input type="hidden" style="width:50px" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
								
					<?php 
					if ($tabno==0){
						?>

							<div class="col-md-4">
								<label style="margin-left:20px">Total No. of Section : </label>
								<input id="sectionno" type="number" style="width:50px" maxlength="1"  max="4" min="1" name="totalsection" oninput="maxLengthCheck(this)" onkeypress="return isNumber1234(event)" onpaste="return false" required> limit : 4
							</div>

							<div class="col-md-2">
							
								
								
									<button type="submit" class="btn btn-success success " name="btnGenerate" onclick="return confirm('Are you sure you want to submit?');">Generate Sections</button>
							</div>	
						
					<?php
					}
					else{
						?>
						<button type="submit" class="btn pull-right btn-danger danger "  name="btnUnallocate" onclick="return confirm('Are you sure you want to unallocate?');">Unallocate all students</button>
						
						<?php

					}
					?>
				</form>
				<div class="col-md-5">
				</div>
				
			</div>

			<br><br>
	    	<form method="post" >
				<table id="unallocated-student-table" class="display" cellspacing="0" width="100%" /*data-page-length="25"*/ style="background-color:gold;">		
			        <thead>
			            <tr> 
			                <th>LRN</th>
			                <th>Last Name</th>
			                <th>First Name</th>
			                <th>Middle Name</th>
			                <th>Status</th>					        
			            </tr>			                
			        </thead>

			        <tbody>

			        <?php  
			      
					$totalno=0;

					$result =get_db_array("SELECT a.id,a.lrn,lastname,firstname,middlename,b.remarks from tbl_studentinfo a left join tbl_studentstatus b on a.lrn=b.lrn 
						where b.sy_id=$sy_id and b.year_id =(SELECT year_id from tbl_yearlevel where year_level='$yearlvl') and section_id=0 order by lastname asc"); 
					
						if(count($result)>0){
							foreach ($result as $key => $fieldname) {

								?>

				           		<tr <?php if (count($section_names)!=0)  echo 'class="data" data-toggle="context" data-target="#sectionlist" ';?>
				           		data-section0="<?php if (count($section_names)>0 ) echo $section_names[0];?>" 
				 				data-section1="<?php if (count($section_names)>1 ) echo $section_names[1];?>"
				 				data-section2="<?php if (count($section_names)>2 ) echo $section_names[2];?>"
								data-section3="<?php if (count($section_names)>3 ) echo $section_names[3];?>" 
			     				
				           		data-id="<?php echo $result[$key]['id']; ?>" data-lrn="<?php echo $result[$key]['lrn']; ?>" data-yrlvl="<?php echo $yearlvl; ?>">
				     			
									<?php
									

										foreach ($fieldname as $key2 => $value) {
											if ($key2!='id') {
													
												?>
						        				<td><?php echo $value ?></td>
						        				<?php
											}
											
					        			}
					        		
					        		?>	                             
				            	</tr>

				          		<?php
					          	$totalno++;
			            	
				            }
				        }       
				      ?>
					</tbody>

				</table>
			</form>
	    </div>

	 <?php

	//section tabs-------------------------------------------------------------------------------------------------------------
		//if (count($section_names)>0) {
		    $totalno0=0;
			$totalno1=0;
			$totalno2=0;
			$totalno3=0;
			foreach ($section_names as $section => $sectionvalue) {
				$emp_no_info=get_db("SELECT emp_no,lastname,firstname,middlename from tbl_facultyinfo where emp_no=(select emp_no from tbl_advisers where section_id=(select section_id from tbl_section where section_name='$sectionvalue' ) and sy_id=$sy_id) ;");
				
				?>				
	    		<div role="tabpanel" class="tab-pane" id="<?php echo "tab-".$sectionvalue;?>">
	    			<br>
	    			<div class="row">
						<div class="col-md-4">
							<label style="margin-left:20px">Total Student : </label>
							<label id="totalno<?php echo $section ?>"></label>
						</div>
						<div class="col-md-8">
							<label style="margin-left:20px">Adviser : <?php if($emp_no_info['emp_no']!=0 || $emp_no_info['emp_no']!=null) echo $emp_no_info['lastname'].', '.$emp_no_info['firstname'].' '.$emp_no_info['middlename']; else echo "No Adviser";?></label>
					
							<button  data-sectionname="<?php echo $sectionvalue ?>"data-toggle="modal" data-url="<?php echo $_SERVER['REQUEST_URI']; ?>" data-target="#subjectmodal" style="margin-left:20px" class="btn btn-danger danger change">Change Adviser</button>
						</div>
					</div>
					<br><br>
	    		 	<table id="<?php echo "table-".$sectionvalue ?>" class="display" cellspacing="0" width="100%" /*data-page-length="25"*/ style="background-color:gold;" >
				        <thead>
				            <tr> 
				                <th>LRN</th>
				                <th>Last Name</th>
				                <th>First Name</th>
				                <th>Middle Name</th>
				                <th>Status</th>		        
				            </tr>
				                
				        </thead>

				        <tbody>

				        <?php  
				            
						$result =get_db_array("SELECT a.id,a.lrn,lastname,firstname,middlename,year_id,b.remarks from tbl_studentinfo a 
							left join tbl_studentstatus b on a.lrn=b.lrn
							where b.sy_id=$sy_id and b.year_id =$year_id and section_id=(select section_id from tbl_section where section_name='$sectionvalue') order by lastname asc "); 
							        
						foreach ($result as $key => $fieldname) {
			
								?>

				           		<tr   class="data "  data-toggle="context" data-target="#sectionlist" 
				           		
				 				data-section0="<?php if (count($section_names)>0 and $sectionvalue!=$section_names[0]) echo $section_names[0];?>" 
				 				data-section1="<?php if (count($section_names)>1 and $sectionvalue!=$section_names[1]) echo $section_names[1];?>"
				 				data-section2="<?php if (count($section_names)>2 and $sectionvalue!=$section_names[2]) echo $section_names[2];?>"
								data-section3="<?php if (count($section_names)>3 and $sectionvalue!=$section_names[3]) echo $section_names[3];?>" 
			     				data-currentsection="<?php echo $sectionvalue?>"
				           		data-id="<?php echo $result[$key]['id']; ?>" data-lrn="<?php echo $result[$key]['lrn']; ?>" data-yrlvl="<?php echo $yearlvl; ?>">
				     			
									<?php
									$lrn='';
									foreach ($fieldname as $key2 => $value) {
										if ($key2!='id' and $key2!='year_id') {
											
											
											?>
					        				<td><?php echo $value ?></td>
					        				<?php
										}
										
				        			}
				        			?>	                             
				            	</tr>

				          	<?php
				          		if ($section==0){
				          			$totalno0++;

				          		}
				          		elseif($section==1){
				          			$totalno1++;

				          		}
				          		elseif($section==2){
				          			$totalno2++;

				          		}
				          		elseif($section==3){
				          			$totalno3++;

				          		}
				          		
			            }
	        
						?>
						
						</tbody>

					</table>


	    		</div>
			    	
				<?php		
			}
		
		?>
	</div>

</div>

	<input type="hidden" id="recordno" value="<?php echo $totalno ?>">

	<input type="hidden" id="recordno0" value="<?php echo $totalno0 ?>">
	<input type="hidden" id="recordno1" value="<?php echo $totalno1 ?>">
	<input type="hidden" id="recordno2" value="<?php echo $totalno2 ?>">
	<input type="hidden" id="recordno3" value="<?php echo $totalno3 ?>">

	
<div id="sectionlist">
	 <ul class="dropdown-menu" role="menu">
	 	<li><label> Move to</label></li>
	  	<form id="form" action="student-section-loading-submit.php" method="post">
	  		<input type="hidden"  name="moveTo" value="true">
	  		<input type="hidden" id="lrn" name="lrn">
	  		<input type="hidden" id="section" name="section">
	  		<input type="hidden"  name="url" value="<?php echo $_SERVER['REQUEST_URI'];?>">

		 	<li id="li0" onclick="form.submit();"><a id="section0" tabindex="-1" href="#"></a></li>
		  	<li id="li1" onclick="form.submit();"><a id="section1" tabindex="-1" href="#"></a></li>
		  	<li id="li2" onclick="form.submit();"><a id="section2" tabindex="-1" href="#"></a></li>
		  	<li id="li3" onclick="form.submit();"><a id="section3" tabindex="-1" href="#"></a></li>
  		</form> 
  	</ul>
</div>

<div id="subjectmodal" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title" id="sectionname" ></h4>
	               
	            </div>            

	            <div class="modal-body  modal-height"> 
						<br><br>		
					<table id="faculty" class="display" cellspacing="0" width="100%">			     
				        <thead>
				            <tr>
				            	<th style="text-align: center;">Employee no.</th>
				                <th style="text-align: center;">Name</th>		               
				            </tr>			                
				        </thead>
				 
				        <tbody>

				        <?php  

							$result = get_db_array("SELECT emp_no,lastname,firstname, middlename from tbl_facultyinfo WHERE emp_no!=1413914");

							foreach ($result as $key => $columnname) {
								?>
								<tr  data-toggle="modal" data-target="#confirm" data-subjectcode="" data-emp="<?php echo $result[$key]['emp_no']?>" class="data">
					            <?php
								foreach ($columnname as $key => $value) {
			
									$name= $columnname['lastname'] . ", " . $columnname['firstname'] . " " . $columnname['middlename'];
									if($key=='emp_no'){

										?>
						             	<td  style="text-align: center;"><?php echo $value; ?></td>
						                <td ><?php echo $name; ?></td>
						        		<?php
					        		}
				        	 
					        	}
					        ?>
				        	</tr>
				        	<?php	
				        }
					    ?>

						</tbody>

					</table><br>
				</div>

				<div class="form-group">
					<input type="hidden" id="sectionname2" value="">
					<input type="hidden" id="url" value="">							
												
				</div><br>
			    <div class="modal-footer" style="background-color:gold;">
			       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================  Confirm Modal =================================================================-->

<div id="confirm" class="modal fade" >
    <div class="modal-dialog">
    	<div id="confirmmodal"class="modal-content">
      </div>  
    </div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		$('#unallocated-student-table').dataTable();
		$('#table-ARCHIMEDES').dataTable();
		$('#table-COPERNICUS').dataTable();
		$('#table-EDISON').dataTable();
		$('#table-GALILEO').dataTable();
		$('#table-DARWIN').dataTable();
		$('#table-LINNAEUS').dataTable();
		$('#table-MENDEL').dataTable();
		$('#table-CURIE').dataTable();
		$('#table-DALTON').dataTable();
		$('#table-KELVIN').dataTable();
		$('#table-EINSTEIN').dataTable();
		$('#table-NEWTON').dataTable();
		$('#faculty').dataTable();

		//additional section
		$('#table-TESLA').dataTable();
		$('#table-PASTEUR').dataTable();
		$('#table-FARADAY').dataTable();
		$('#table-KEPLER').dataTable();

		var record=$('#recordno').val();
		var record0=$('#recordno0').val();
		var record1=$('#recordno1').val();
		var record2=$('#recordno2').val();
		var record3=$('#recordno3').val();

		//console.info(record);
		$('#totalno').text(record);	
		$('#totalno0').text(record0);
		$('#totalno1').text(record1);
		$('#totalno2').text(record2);
		$('#totalno3').text(record3);

		//$('a[data-toggle="tab"]').on('show.bs.tab',function(e){
			
		 	$('#sectionlist').on('show.bs.context',function(event){

		 		$('#li0').css('display','');
		 		$('#li1').css('display','');
		 		$('#li2').css('display','');
		 		$('#li3').css('display','');

				var sectionname0=$(event.target).closest('tr').data('section0');
				var sectionname1=$(event.target).closest('tr').data('section1');
				var sectionname2=$(event.target).closest('tr').data('section2');
				var sectionname3=$(event.target).closest('tr').data('section3');
				var currentsection=$(event.target).closest('tr').data('currentsection');
				var lrn=$(event.target).closest('tr').data('lrn');

				$('#lrn').val(lrn);			
				if(sectionname0 && (sectionname0!=currentsection && sectionname0!='') ){	
					$('#section0').text(sectionname0);	
						console.info(sectionname0+' 0');
						console.info(currentsection);
						 console.info(lrn);
				}
				else{
					$('#li0').css('display','none');
				}

				if(sectionname1 && (sectionname1!=currentsection && sectionname1!='') ){
					$('#section1').text(sectionname1);	
					console.info(sectionname1+' 1');
					console.info(currentsection);
					console.info(lrn);
									
				}
				else{
					$('#li1').css('display','none');
				}

				if( sectionname2 || (sectionname2!=currentsection && sectionname2!='') ){
					$('#section2').text(sectionname2);	
					console.info(sectionname2);
						console.info(currentsection);
								
				}
				else{
					$('#li2').css('display','none');
				}

				if(sectionname3 || (sectionname3!=currentsection && sectionname3!='') ){
					$('#section3').text(sectionname3);
					console.info(sectionname3);
						console.info(currentsection);
						
				}
				else{			
					$('#li3').css('display','none');

				}
		
		});

	$('#section0').on('click',function(){
		var section=$(this).text();
		$('#section').val(section);

	});


	$('#section1').on('click',function(){
		var section=$(this).text();
		$('#section').val(section);

	});


	$('#section2').on('click',function(){
		var section=$(this).text();
		$('#section').val(section);

	});


	$('#section03').on('click',function(){
		var section=$('#this').text();
		$('#section').val(section);

	});


	$('.change').on('click',function(){
		var sectioname=$(this).data('sectionname');
		var url=$(this).data('url');
		$('#sectionname2').val(sectioname);
		$('#url').val(url);
		$('#sectionname').text("Change Adviser for "+ sectioname );	
		console.info(sectioname);

	});
	
	$('#faculty tbody').on( 'click', 'tr', function () {
   		var emp_no =$(event.target).closest('tr').data('emp');
   		var section_name=$('#sectionname2').val();
   		var url=$('#url').val();

		$("#confirmmodal").html('');
			
		$.post('modal-adviser-change-confirm.php',{emp_no:emp_no,section_name:section_name,url:url},
			function(html){
			 	$("#confirmmodal").html(html);
			 	$('#confirm').modal('show');
			 }
		); 
	});
	  
});

</script>
