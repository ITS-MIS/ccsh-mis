<?php	
	getother();
	include('transferee-validation.php');

	if ($app_state == "empty") {	
?>

<style type="text/css">
	
	.divider {
		width:500px;
		text-align:center;
	}

	.divider hr {

		width:50%;
		color: black;
	}
	
	hr { 
		display: block;
		margin-top: 0.5em;
		margin-bottom: 0.5em;
		margin-left: auto;
		margin-right: auto;
		border-style: inset;
		border-width: 1px;s
	}
	.error {color: #FF0000;}
	
</style>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Transferee's Form</label>
			</div>
			<div class="col-md-1"></div>
		</div>	
		
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-sm-6 col-md-10" style="background-color:rgb(255,255,50); border: 3px solid; border-color: gold; padding-left:30px; padding-right:30px;"><br>
					<form role="form" method="post" action="<?php ($_SERVER['PHP_SELF']);?>">
						<label for="lrn">Student's Personal Information</label><hr><br>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="lrn">LRN</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="lrn" name="info[lrn]" placeholder="LRN" maxlength="12" onkeypress="return isNumber(event)" value="<?php if(isset($_POST['info'])) echo $info['lrn']?>"> 
									<span class="error"><?php if (isset($emptyErr['lrn'])) echo $errlist['lrn']; ?></span> 
								</div>
							</div>

							<div class="col-md-4"></div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="enroll">Transferee for</label>&nbsp;<span class="error">*</span>

									<select  class="form-control" id="year_level" placeholder="Year level" name="info[year_level]">
											<option value="<?php if(isset($info['year_level']) and $info['year_level'] !='' ) echo $info['year_level'];?>"><?php if(isset($info['year_level']) and $info['year_level'] !='' ) echo $info['year_level'];else echo'--Select Year Level--'; ?></option>	
											<option value="Grade 7">Grade 7</option>
											<option value="Grade 8">Grade 8</option>
											<option value="Grade 9">Grade 9</option>
											<option value="Grade 10">Grade 10</option>
											
									</select>
									<span class="error"><?php if (isset($emptyErr['year_level'])) echo $errlist['year_level']; ?></span> 
								</div>
							</div>
			
						</div>
						
						<div class="row">						
							<div class="col-md-4">
								<div class="form-group">
									<label for="lastname">Last Name</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="lastname" name="info[lastname]" placeholder="Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['lastname']?>"> 
									<span class="error"><?php if (isset($emptyErr['lastname'])) echo $errlist['lastname']; ?></span> 								
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fname">First Name</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="firstname" name="info[firstname]" placeholder="First Name" maxlength="50" onkeypress="return isLetterPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['firstname']?>">
									<span class="error"><?php if (isset($emptyErr['firstname'])) echo $errlist['firstname']; ?></span> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="mname">Middle Name</label>
									<input type="text" class="form-control" id="middlename" name="info[middlename]" placeholder="Middle Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['middlename']?>">
								</div>
							</div>							
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="birthdate">Birthdate</label>&nbsp;<span class="error">*</span>
									<input type="date" class="form-control" id="date" name="info[birthdate]" placeholder="Birthdate" value="<?php if(isset($_POST['info'])) echo $info['birthdate']?>">
									<span class="error"><?php if (isset($emptyErr['birthdate'])) echo $errlist['birthdate']; ?></span> 
								</div>
							</div>
						
						
							<div class="col-md-4">
								<div class="form-group">									
									<label for="gender">Gender:</label>&nbsp;<span class="error">*</span><br>
									<div class="col-md-4">
										<label class="radio-inline">
										<input type="radio" value="M" id="genderm" name="info[gender]" <?php if (isset($info['gender']) && $info['gender']=="M") echo "checked";?>> Male</label>
									</div>
									
									<div class="col-md-8">
										<label class="radio-inline">	
										<input type="radio" value="F" id="genderf" name="info[gender]" <?php if (isset($info['gender']) && $info['gender']=="F") echo "checked";?>> Female</label>
									</div>
										<span class="error"><?php if (isset($emptyErr['gender'])) echo $errlist['gender']; ?></span> 	
								</div>
							</div>
							<div class="col-md-4"></div>								
						</div><br>
						
						<div class="row">						
							<div class="col-md-4">
								<div class="form-group">
									<label for="Address">Address</label>
								</div>
							</div>							
						</div>
						
						<div class="row">
							<div class="col-md-7">
								<div class="form-group">
									<label for="add_no_st">House No./Street/ Sitio/Purok</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="addnost" name="info[addnost]" placeholder="House No./Street/ Sitio/Purok" maxlength="30" onkeypress="return isAdd(event)" value="<?php if(isset($_POST['info'])) echo $info['addnost']?>"> 
									<span class="error"><?php if (isset($emptyErr['addnost'])) echo $errlist['addnost']; ?></span> 
								</div>
							</div>
					
							<div class="col-md-5">
								<div class="form-group">
									<label for="brgy">Barangay </label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="brgy" name="info[brgy]" placeholder="Barangay" maxlength="20" onkeypress="return isAdd(event)" value="<?php if(isset($_POST['info'])) echo $info['brgy']?>">
									<span class="error"><?php if (isset($emptyErr['brgy'])) echo $errlist['brgy']; ?></span> 
								</div>
							</div>		
						</div>

						<div class="row">
							<div class="col-md-1"></div>							
							<div class="col-md-5">
								<div class="form-group">
									<label for="city">City</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="city" name="info[city]" placeholder="City" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['city']?>"> 
									<span class="error"><?php if (isset($emptyErr['city'])) echo $errlist['city']; ?></span> 
								</div>
							</div>
							
							<div class="col-md-5">
								<div class="form-group">
									<label for="province">Province</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="province" name="info[province]" placeholder="Province" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['province']?>">
									<span class="error"><?php if (isset($emptyErr['province'])) echo $errlist['province']; ?></span> 
								</div>
							</div>
							
							<div class="col-md-1"></div>							
						</div><br>
						
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-5">
								<div class="form-group">
									<label for="mother_tounge">Mother Tongue</label>&nbsp;<span class="error">*</span>
									
									<select class="form-control" id="mothertounge" name="info[mothertounge]" onchange="showfield1(this.options[this.selectedIndex].value)" >
												<option value="<?php echo $info['mothertounge'] ?>"><?php if (isset($info['mothertounge'])) {print $info['mothertounge'];} ?></option>
												<option>Tagalog</option>
												<option>Bikol</option>
												<option>Cebuano</option>
												<option>Chabacano</option>
												<option>Hiligaynon</option>
												<option>Iloko</option>
												<option>Kapampangan</option>
												<option>Maguindanaoan</option>
												<option>Maranao</option>
												<option>Pangasinense</option>												
												<option>Tausug</option>
												<option>Waray</option>
												<option>Other</option>
										</select>
										
										 <div id="div1"><input  id="mothertounge2" type="hidden" name="info[mothertounge]" /></div>
										 <span class="error"><?php if (isset($emptyErr['mothertounge'])) echo $errlist['mothertounge']; ?></span> 
								</div>
								
							</div>
								
							<div class="col-md-5">
								<div class="form-group">
									<label for="ip">Ethnic group</label>&nbsp;<span class="error">*</span>
									
									<select class="form-control" id="ip" placeholder="Ethnic Group" name="info[ip]" onchange="showfield2(this.options[this.selectedIndex].value)" >
												<option value="<?php echo $info['ip'] ?>"><?php if (isset($info['ip'])) {print $info['ip'];} ?></option>
												<option>Tagalog</option>						
												<option>Bikolano</option>
												<option>Gaddang</option>
												<option>Ibanag</option>
												<option>Ilokano</option>
												<option>Ivatan</option>
												<option>Kapampangan</option>
												<option>Moro</option>
												<option>Pangasinan</option>												
												<option>Sambal</option>
												<option>Subanon</option>
												<option>Visayan</option>
												<option>Zamboangueño</option>
												<option>Other</option>
										</select>
											 <div id="div2"><input id="ip2"  type="hidden" name="info[ip]" /></div>
											 <span class="error"><?php if (isset($emptyErr['ip'])) echo $errlist['ip']; ?></span> 
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
								
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-5">
								<div class="form-group">
									<label for="religion">Religion</label>&nbsp;<span class="error">*</span>
								
									<select class="form-control" id="religion" name="info[religion]" onchange="showfield3(this.options[this.selectedIndex].value)" >
												<option value="<?php echo $info['religion'] ?>"><?php if (isset($info['religion'])) {print $info['religion'];} ?></option>
												<option>Catholic</option>						
												<option>JW</option>
												<option>INC</option>
												<option>Muslim</option>
												<option>SDA</option>
												<option>LDS</option>
												<option>Born again</option>
												<option>Other</option>
												
										</select>
										<span class="error"><?php if (isset($emptyErr['religion'])) echo $errlist['religion']; ?></span>
										<div id="div3"><input id="religion2"  type="hidden" name="info[religion]" /></div>
										 
								</div>
							</div>
								<div class="col-md-6"></div>
						</div><br>

						<div class="row">
							<div class="col-md-8">		
									<label for="lrn">Other Information</label>
									</div>							
								<div class="col-md-2"></div>								
							</div>

						<hr><br>
						
						<div class="row">						
							<div class="col-md-5">
								<div class="form-group">
									<label for="Father">Father's Name:</label>									
								</div>
							</div>
							
						</div>
						
						
						<div class="row">
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="flastname">Last Name</label>
									<input type="text" class="form-control" id="flastname" name="info[flastname]" placeholder="Father's Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['flastname']?>">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="ffirstname">First Name</label>
									<input type="text" class="form-control" id="ffirstname" name="info[ffirstname]" placeholder="Father's First Name" maxlength="50" onkeypress="return isLetterPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['ffirstname']?>">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fmiddlename">Middle Name</label>
									<input type="text" class="form-control" id="fmiddlename" name="info[fmiddlename]" placeholder="Father's Middle Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['fmiddlename']?>">
								</div>
							</div>							
						</div><br>
						
						<div class="row">						
							<div class="col-md-5">
								<div class="form-group">
									<label for="Mother">Mother's Maiden Name:</label>
								</div>
							</div>							
						</div>
						
						<div class="row">							
							<div class="col-md-4">
								<div class="form-group">
									<label for="mlastname">Last Name</label>
									<input type="text" class="form-control" id="mlastname" name="info[mlastname]" placeholder="Mother's Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['mlastname']?>">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="mfirstname">First Name</label>
									<input type="text" class="form-control" id="mfirstname" name="info[mfirstname]" placeholder="Mother's First Name" maxlength="50" onkeypress="return isLetterPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['mfirstname']?>">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fmiddlename">Middle Name</label>
									<input type="text" class="form-control" id="mmiddlename" name="info[mmiddlename]" placeholder="Mother's Middle Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['mmiddlename']?>">
								</div>
							</div>							
						</div><br>
						
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="guardian_name">Name of Guardian:<i> if no parent(s)</i></label>
									<input type="text" class="form-control" id="guardianname" name="info[guardianname]" placeholder="Guardian Name" maxlength="50" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['guardianname']?>">
									<span class="error"><?php if (isset($emptyErr['guardianname'])) echo $errlist['guardianname']; ?></span> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="relationship">Relationship</label>&nbsp;<span class="error">*</span>
										<select class="form-control" id="relationship" name="info[relationship]"  onchange="showfield4(this.options[this.selectedIndex].value)">
												<option value="<?php echo $info['relationship'] ?>"><?php if (isset($info['relationship'])) {print $info['relationship'];} ?></option>
												<option>mother</option>						
												<option>father</option>
												<option>aunt</option>						
												<option>uncle</option>
												<option>brother</option>
												<option>sister</option>
												<option>grandmother</option>
												<option>grandfather</option>
												<option>godmother</option>
												<option>godfather</option>
												<option>Other</option>
										</select>
									<span class="error"><?php if (isset($emptyErr['relationship'])) echo $errlist['relationship']; ?></span> 
									<div id="div4"><input id="relationship2"  type="hidden" name="info[relationship]" /></div>
								</div>
							</div>
						
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="contactnumber">Contact Number</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="contactnumber" name="info[contactnumber]" placeholder="Contact Number" onkeypress="return isContact(event)" value="<?php if(isset($_POST['info'])) echo $info['contactnumber']?>">
									<span class="error"><?php if (isset($emptyErr['contactnumber'])) echo $errlist['contactnumber']; ?></span> 
								</div>
							</div>
					
						</div>
						
						<div class="row">						
							<div class="col-md-1"></div>							
							<div class="col-md-10">
								<div class="form-group">
									<input type="hidden" class="form-control" id="status" name="status" value="Enrolled">
									<input type="hidden" class="form-control" id="remarks_id" name="remarks_id" value="2">
								</div>
							</div>							
							<div class="col-md-1"></div>							
						</div><br>

					<div class="row">						
						<div class="col-md-4">						
							<div class="form-group">
								<label for="schoolyear">School:</label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="school" placeholder="Last School Attended" name="info[school]"  maxlength="50" onkeypress="return isLetterDashPoint(event)" value="<?php if(isset($_POST['info'])) echo $info['school']?>">
								<span class="error"><?php if (isset($emptyErr['school'])) echo $errlist['school']; ?></span> 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="gwa">GWA:</label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="gwa" placeholder="GWA" name="info[gwa]"  maxlength="6" onkeypress="return isFloat(event)" value="<?php if(isset($_POST['info'])) echo $info['gwa']?>">
								<span class="error"><?php if (isset($emptyErr['gwa'])) echo $errlist['gwa']; ?></span> 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label for="guardianmail">Guardian E-mail Address:</label>
								<input type="email" class="form-control" id="guardianmail" placeholder="Guardian E-Mail address" name="info[guardianmail]"  maxlength="50" value="<?php if(isset($_POST['info'])) echo $info['guardianmail']?>"> 
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6 req">
								<label class="heading">Requirements:</label><br/>
								<label class="checkbox-inline"><input type="checkbox" name="nso" >NSO(Birth Certificate)</label><br/>
								<label class="checkbox-inline"><input type="checkbox" name="f137" >Form 137</label><br/>
								<label class="checkbox-inline"><input type="checkbox" name="f138" >Form 138</label><br/>
								<label class="checkbox-inline"><input type="checkbox" name="cgmc" >Certificate of Good Moral</label><br/>
								<label class="checkbox-inline"><input type="checkbox" name="idpic">Id Picture</label><br/>
								<label class="checkbox-inline"><input type="checkbox" name="medc">Medical Certificate</label><br/><br/>														
						</div>
					</div><br><br>

					<div class="row">						
						<div class="col-md-4"></div>
						<div class="col-md-4" style="text-align:center">
							<div class="form-group">								
								<input id="submit" type="submit" class="btn btn-success success" value="Submit" name="btnSubmit" onclick="return confirm('Are you sure you want to submit?');">
								<input type="reset" class="btn btn-default" value="Reset" name="btnReset">							
							</div>
						</div>

						<div class="col-md-4">
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div>
					</div>														
				</form>					
			</div>
		</div>		
	</div>
		
<?php
}
	elseif ($app_state == "processed") {
	 
		include('enrollment-transferee-submit.php');			
		
		$app_state = "good";
	}

	if ($app_state == "good") {
	    
	?>
	   
    	<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-5">
				<label for="lname"></label>
				<?php
					$confirm=("successfully added the record!");
					echo "<script type='text/javascript'>alert(\"$confirm\");</script>";
				?>
				<script type="text/javascript">window.location.href="index2.php?mode=Registrar&category=Enrollment&page=2";</script>
			</div>
		</div>
	<?php
	}