<?php	
	getDatatablesLink();
	getModalPageLink();
	getDatatablesScript();

	$subject='';
?>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<label id="header">Teacher-Subject Loading</label>
		</div>
		<div class="col-md-1"></div>
	</div>

<button data-target="#generatemodal" data-toggle="modal" type="button" class="btn btn-success success"> <font face="arial">Generate Employee with Subject</font></span></button>
<!--<button data-target="#generateadvisermodal" data-toggle="modal" type="button" class="btn btn-success success"> <font face="arial">Generate Advisers</font></span></button>-->

<div class="row">
	<form method="post"><br>
		<table id="subject" class="display" cellspacing="0" width="100%" style="background-color:gold;">
	     
	        <thead>
	            <tr>
	            	<th style="text-align: left;">Code</th>
	                <th style="text-align: left;">Title</th>
	                <th style="text-align: left;">Grade level</th>
	                <th style="text-align: left;">emp_no</th>
	            </tr>
	        </thead>
	 
	        <tbody>
	        <?php  
	        	include('current-year.php');
				$query = "SELECT a.subject_code,subject_title, year_id,c.emp_no,lastname,firstname,middlename 
							FROM tbl_subject a left join tbl_subject_yearlevel b on a.subject_code=b.subject_code 
							left join tbl_employee_subject c on a.subject_code=c.subject_code AND c.sy_id=$sy_id
							left join tbl_facultyinfo d on c.emp_no=d.emp_no  WHERE b.sy_id=$sy_id order by year_id;";

				$result = mysql_query($query) or die(mysql_error());

				if(mysql_num_rows($result) > 0) {												
					while ($row = mysql_fetch_assoc($result)) {
						$subject_code = $row['subject_code'];
						$subject_title = $row['subject_title'];
						$year_id = $row['year_id'];
						$emp_no = $row['emp_no'];
						$lastname = $row['lastname'];
						$firstname = $row['firstname'];
						$middlename = $row['middlename'];
						$name='';
						$subjects='';

						if($lastname==''){
							$name= 'none';
							$subjects='none';
						}
						else{
							$c=0;
							$result2=mysql_query("SELECT subject_code from tbl_employee_subject where emp_no=$emp_no AND sy_id = $sy_id order by subject_code");
							
							while ($row = mysql_fetch_assoc($result2)) {
								if($c==0){

									$subjects=$row['subject_code'];

								}
								else{
									$subjects=$subjects .", ".$row['subject_code'];
								}
								
								$c++;
							}

							$name= $lastname . ", " . $firstname . " " . $middlename;

						}
						
						
			?>

	            <tr  data-toggle="modal"  data-target="#subjectmodal" 
	            		data-subjectcode="<?php echo $subject_code?>" data-name="<?php echo $name ?>"
	            		data-subjects="<?php echo $subjects ?>" class="data">
	                
	                
	                <td><?php echo $subject_code; ?></td>
	                <td ><?php echo $subject_title; ?></td>
	                <td style="text-align: left;"><?php echo $year_id; ?></td>
	                <td style="text-align: left;"><?php echo $emp_no; ?></td>
	            </tr>
			
			<?php
		            }
		        }

		    ?>

			</tbody>

		</table>
	</form><br>
</div>

<div id="subjectmodal" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title" id="subjectcode" ></h4>
	            </div>
	            

	            <div class="modal-body  modal-height"> <br><br>
	
						<div class="form-group">
							<label id="teachby" class="col-sm-12 ">Assigned Faculty :</label>			
					  	</div>
					  	<div class="form-group">
							<label id="subjectteach" class="col-sm-12 ">Subject(s) Assigned :</label>	
							<br><br>	

					  	</div>

						<div class="form-group">

							<div class="col-sm-4"></div>
							
							<div class="col-sm-4" style="text-align:center;">
								<button  id="change" class="btn btn-success success" name="btnUpdate" >Change</button>
								<a  href="#remove" data-toggle="modal" >
									<button id="btnremove" type="button" class="btn btn-danger warning "  >Remove</button>
								</a>

							</div>
							
							<div class="col-sm-4"></div>
						</div>
						<br><br>
						<div id="facultylist" style="display:none;">

							<form method="post" >
								<table id="faculty" class="display" cellspacing="0" width="100%">
							     
							        <thead>
							            <tr>
							            	<th style="text-align: center;">Employee no.</th>
							                <th style="text-align: center;">Name</th>		               
							            </tr>
							                
							        </thead>
							 
							        <tbody>
							        <?php  

										$query = "SELECT emp_no,lastname,firstname, middlename from tbl_facultyinfo WHERE emp_no!=1413914";

										$result = mysql_query($query) or die(mysql_error());


										if(mysql_num_rows($result) > 0) {
											while ($row = mysql_fetch_assoc($result)) {																	
												$emp_no = $row['emp_no'];
												$lastname = $row['lastname'];
												$firstname = $row['firstname'];
												$middlename = $row['middlename'];
												$name= $lastname . ", " . $firstname . " " . $middlename;												
									?>
								            <tr id="confirmcode" data-toggle="modal"   data-target="#confirm" data-subjectcode="" data-emp="<?php echo $emp_no?>" class="data">
								               
								                <td  style="text-align: center;"><?php echo $emp_no; ?></td>
								                <td ><?php echo $name; ?></td>
								        
								            </tr>
									
									<?php
								            }
								        }

								    ?>

									</tbody>
								</table>
							</form><br>
						</div>
							
						<div class="form-group">															
						
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
							
						</div>
						<br>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================  Confirm Modal =================================================================-->

<div id="confirm" class="modal fade" >
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Confirm change?</p>
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="teacher-subject-submit.php">

            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
            		<input id="assignedsubject" type="hidden" name="subject_code" >
            		<input id="changeemp" type="hidden" name="emp_no">
                	<button type="submit"class="btn btn-success success">Confirm</button>  	
		   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================  Remove Modal =================================================================-->

<div id="remove" class="modal fade" >
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Confirm Remove?</p>
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="teacher-subject-submit.php">

            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
            		
            		<input id="removesubject" type="hidden" name="subject_code" >

                	<button type="submit"class="btn btn-success success" name="remove">Confirm</button>  	
		   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================ generatemodal Confirm Modal =================================================================-->
<div id="generatemodal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Generate List of Employee-Subjects</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to generate a new list of employees with subjects for new SY?</p>               
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="generate-employee-subject.php">

            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-insert-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-insert" name="btnGenerate">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================ generateadvisermodal Confirm Modal =================================================================-->
<div id="generateadvisermodal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Generate Adviser</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to generate a new list of advisers for new SY?</p>               
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="generate-advisers.php">

            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-insert-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-insert" name="btnGenerateAdviser">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>
 		
<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#subject').dataTable();
 
    	$('#subject tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#faculty').dataTable({
	     
	       "bPaginate": false
	     });
 
    	$('#faculty tbody').on( 'click', 'tr', function () {


	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }
	       		
	      		
	       		var emp_no =$(event.target).closest('tr').data('emp');

	    		$("#changeemp").val(emp_no);

	  
	      		console.info($("#assignedsubject").val());
				console.info($("#changeemp").val());
	       
    	} );
	} );


	  $(function(){
	  	 
	    $('#subjectmodal').modal({
	        keyboard: true,
	        show:false,       

	    }).on('show.bs.modal', function(){
	    	var subjectcode = $(event.target).closest('tr').data('subjectcode');
	    	var emp_no = $(event.target).closest('tr').data('emp');
	    	var name = $(event.target).closest('tr').data('name');
	    	var subjects = $(event.target).closest('tr').data('subjects');
	         $('#subjectcode').text(subjectcode);
	         $('#assignedsubject').val(subjectcode);
	         console.info(subjectcode);
	         console.info($("#assignedsubject").val());
	         $('#teachby').text("Assigned Faculty : "+name);
	 		 $('#subjectteach').text("Subject(s) Assigned : "+subjects);

		     $('#confirmcode').data('subjectcode',subjectcode);
		  
	 			if(name=='none'){
	 				$('#btnremove').hide();
	 			}
	 			else{
	 				$('#btnremove').show();
	 			}


		}).on('hidden.bs.modal', function(){

			 $('#facultylist').hide();
			 $("#change").show();
		});
	});

	  $("#change").click(function(){

		$('#facultylist').show();
		 $("#change").hide();
		 $('.modal-backdrop.fade.in').css('height','1450px')

	 
	});

	  $(function(){
	  	 
	    $('#confirm').modal({
	        keyboard: true,
	        show:false,
	       
	    }).on('show.bs.modal', function(){

			$("#subjectmodal").modal('hide');

		});
	});

	   $(function(){
	  	 
	    $('#remove').modal({
	        keyboard: true,
	        show:false,
	       
	    }).on('show.bs.modal', function(){

			$("#subjectmodal").modal('hide');

	    	var subject_code = $('#confirmcode').data('subjectcode');
	    
	    	$("#removesubject").val(subject_code);
	    	
	      		console.info(subject_code);
   		
	      		console.info($("#removesubject").val());
	
		});
	});

</script>
			   

