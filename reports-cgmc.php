<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');
	$lrn=$_POST['lrn'];
	
	date_default_timezone_set('Asia/Taipei');
	$currentmonth = date('F');
	$cmonth = date('m');
	$currentyear = date('Y');
	$currentday = date('d');
	$currentdate=$currentyear."-".$cmonth."-".$currentday;
	$day=1;

	$pdf = new FPDI();

	$pdf->AddPage('P','letter','c'); 

	$pageCount = $pdf->setSourceFile("forms/cgmc-sample.pdf");
	$tplIdx = $pdf->importPage(1); 

	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true); 

// schoolyear-------------------------------------------------------------

	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(53, 115.3);
	$pdf->SetFont('Arial','', '9');
	$pdf->Write(0, $sy);

//month---------------------------------------------------------------------

$pdf->SetXY(45, 247);
$pdf->SetFont('Arial','', '9');
$pdf->Write(0, $currentdate);

//student--------------------------------------------------------------------
	$result=mysql_query("SELECT a.lrn,lastname,firstname,middlename,gender,year_id
						from tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and sy_id=$sy_id  and b.lrn='$lrn'");
	
	while($row=mysql_fetch_assoc($result)){
		$name=$row['firstname'].' '.$row['middlename']."   ".$row['lastname'];
		$pdf->SetXY(75, 94);
		$pdf->SetFont('Arial', 'I', '14');
	 
		$pdf->Write(0, $name);
	
		$year_id=$row['year_id'];
		$pdf->SetXY(77, 108);
		$pdf->SetFont('Arial', 'b', '10');
	 
		$pdf->Write(0, $year_id);	
	}

	$pdf->Output();