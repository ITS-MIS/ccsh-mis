function isLetter(event) {
            var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode > 64 && chCode <91) || (chCode > 96 && chCode <123 || chCode==8) || (chCode==32);
        }


function isLetterDash(event) {
            var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode > 64 && chCode <91) || (chCode > 96 && chCode <123 || chCode==8) || (chCode==45) || (chCode==32);
        }


function isLetterPoint(event) {
            var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode > 64 && chCode <91) || (chCode > 96 && chCode <123 || chCode==8) || (chCode==46) || (chCode==32);
        }


function isLetterDashPoint(event) {
            var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode > 64 && chCode <91) || (chCode > 96 && chCode <123 || chCode==8) || (chCode==45) || (chCode==46) || (chCode==32);
        }


function isAdd(event) {
			 var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode > 64 && chCode <91) || (chCode > 96 && chCode <123 || chCode==8) || (chCode > 47 && chCode < 58) || (chCode==44) || (chCode==45) || (chCode==46) ||(chCode==32);
		}


function isTitle(event) {
			 var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode > 64 && chCode <91) || (chCode > 96 && chCode <123 || chCode==8) || (chCode > 47 && chCode < 58) || (chCode==32);
		}


function isPassword(event) {
			 var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode > 64 && chCode <91) || (chCode > 96 && chCode <123 || chCode==8) || (chCode > 47 && chCode < 58);
		}


function isNumber(event) {
			//evt = (evt) ? evt : window.event;
			var charCode = (event.which) ? event.which : event.keyCode;
			
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
			}
			return true;
		}


function isFloat(event)
       {
          var charCode = (event.which) ? event.which : event.keyCode
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;
 
          return true;
       }


function isContact(event)
  {
     var charCode = (event.which) ? event.which : event.keyCode
     if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

     return true;
  }

//for input type of section-loading
function isNumber1234(event) {
       var chCode = ('charCode' in event) ? event.charCode : event.keyCode;

                // returns false if a numeric character has been entered
            return (chCode==49) || (chCode==50) || (chCode==51) || (chCode==52) || (chCode==16);
    }


 function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }



function caps(element){
      element.value = element.value.toUpperCase();
  }