 <?php
	include 'connect.php';
	
	getMeta();
	getBsLink();
	getCcshsPageLink();
	getJqueryScript();
	getBsScript();

	if( !isset($_POST['lrn']) || empty($_POST['lrn']) || !isset($_POST['pass']) || empty($_POST['pass'])){
		
?>
		<div class="alert alert-warning" role="alert">Check username and password or fill all fields
			<a href="index.php"><input type="submit" class="btn btn-warning" value="Try Again" name="btnLoad" id="btnload"></a>
		</div>
						
<?php
			
	}
	else {
		$user = verify_user_pass2($_POST['lrn'], $_POST['pass']);
   	 	
		if ($user){
   	 			set_logged_in_user2($user); 		
				?>
				<div class="alert alert-success" role="alert">Login success!</div>
				<?php redirect('index2.php'); 
				
		} else {
							
			redirect('index.php?loginfailed');
		}
	}