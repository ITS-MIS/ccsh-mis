<style type="text/css"> .error {color: #FF0000;} </style>

<?php
	include_once'connect.php';
	getValidationScipt();
?>

<div id="insertmodal" class="modal fade">
	<div id="student-content">
		<div class="modal-dialog modal-lg">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Add Student Record</h4>
	            </div>

	            <div class="modal-body" style="padding-left:50px; padding-right:50px;"> 
					<form class="form-horizontal" method="post" action="students-submit.php"><br><br>
						<div class="row">	
							<p><span class="error">&nbsp;* required field</span></p>
							<div class="col-md-4">
								<div class="form-group">
									<label for="lrn">LRN</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="alrn" name="lrn" placeholder="LRN" maxlength="12" onkeypress="return isNumber(event)" required> 
								</div>
							</div>							
						</div>
					
						<div class="row">						
							<div class="col-md-4">
								<div class="form-group">
									<label for="lastname">Last Name</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="alastname" name="lastname" placeholder="Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" required> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fname">First Name</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="afirstname" name="firstname" placeholder="First Name" maxlength="50" onkeypress="return isLetterPoint(event)" required>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="mname">Middle Name</label>
									<input type="text" class="form-control" id="amiddlename" name="middlename" placeholder="Middle Name" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>							
						</div><br>
							
						<div class="row">						
							<div class="col-md-4">
								<div class="form-group">
									<label for="gender">Gender</label>&nbsp;<span class="error">*</span>
										<select class="form-control" id="agender" name="gender" required>							
												<option>M</option>
												<option>F</option>
										</select>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="birthdate">Birthdate</label>&nbsp;<span class="error">*</span>
									<input type="date" class="form-control" id="abirthdate" name="birthdate" required>
								</div>
							</div>					
						</div>
		
						<div class="row">						
							<div class="col-md-4">
								<div class="form-group">
									<label for="mothertounge">Mother Tounge</label>
										<select class="form-control" id="amothertounge" name="mothertounge" >							
												<option>Tagalog</option>
												<option>Bikol</option>
												<option>Cebuano</option>
												<option>Chabacano</option>
												<option>Hiligaynon</option>
												<option>Iloko</option>
												<option>Kapampangan</option>
												<option>Maguindanaoan</option>
												<option>Maranao</option>
												<option>Pangasinense</option>												
												<option>Tausug</option>
												<option>Waray</option>
												<option>Other</option>
										</select>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="ip">Ethnic Group</label>
										<select class="form-control" id="aip" name="ip" >
												<option>Tagalog</option>						
												<option>Bikolano</option>
												<option>Gaddang</option>
												<option>Ibanag</option>
												<option>Ilokano</option>
												<option>Ivatan</option>
												<option>Kapampangan</option>
												<option>Moro</option>
												<option>Pangasinan</option>												
												<option>Sambal</option>
												<option>Subanon</option>
												<option>Visayan</option>
												<option>Zamboangueño</option>
												<option>Other</option>
										</select>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="religion">Religion</label>
										<select class="form-control" id="areligion" name="religion" >
												<option>Catholic</option>						
												<option>Born Again</option>
												<option>INC</option>
												<option>JW</option>
												<option>LDS</option>
												<option>Muslim</option>
												<option>Protestant</option>
												<option>SDA</option>
												<option>Other</option>
										</select>
								</div>
							</div>	
						</div><br>
						
						<div class="row">						
							<div class="col-md-4">
								<div class="form-group">
									<label for="Address">Address</label>
								</div>
							</div>							
						</div>
						
						<div class="row">							
							<div class="col-md-1"></div>							
							<div class="col-md-6">
								<div class="form-group">
									<label for="add_no_st">House No. / Street / Sitio / Purok</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="aaddnost" name="addnost" maxlength="30" onkeypress="return isAdd(event)" required> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="brgy">Barangay</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="abrgy" name="brgy" maxlength="20" onkeypress="return isAdd(event)" required>
								</div>
							</div>
							
							<div class="col-md-1"></div>							
						</div>
						
						<div class="row">							
							<div class="col-md-1"></div>							
							<div class="col-md-6">
								<div class="form-group">
									<label for="city">City</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="acity" name="city" maxlength="30" onkeypress="return isLetterDashPoint(event)" required> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="province">Province</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="aprovince" name="province" maxlength="30" onkeypress="return isLetterDashPoint(event)" required>
								</div>
							</div>
							
							<div class="col-md-1"></div>							
						</div><br>
						
						<div class="row divider">							
							<div class="col-md-4"></div>							
							<div class="col-md-4" style="text-align:center">
								<div class="divider">
									<label for="lrn">Other Information</label>
								</div>
							</div>							
							<div class="col-md-4"></div>							
						</div><hr><br>
						
						<div class="row">						
							<div class="col-md-3">
								<div class="form-group">
									<label for="Father">Father's Name:</label>
								</div>
							</div>							
						</div>
						
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="ffirstname">First Name</label>
									<input type="text" class="form-control" id="affirstname" name="ffirstname" placeholder="Father's First Name" maxlength="50" onkeypress="return isLetterPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fmiddlename">Middle Name</label>
									<input type="text" class="form-control" id="afmiddlename" name="fmiddlename" placeholder="Father's Middle Name" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="flastname">Last Name</label>
									<input type="text" class="form-control" id="aflastname" name="flastname" placeholder="Father's Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>							
						</div><br>
						
						<div class="row">						
							<div class="col-md-5">
								<div class="form-group">
									<label for="Mother">Mother's Maiden Name:</label>
								</div>
							</div>							
						</div>
						
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="mfirstname">First Name</label>
									<input type="text" class="form-control" id="amfirstname" name="mfirstname" placeholder="Mother's First Name" maxlength="50" onkeypress="return isLetterPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fmiddlename">Middle Name</label>
									<input type="text" class="form-control" id="ammiddlename" name="mmiddlename" placeholder="Mother's Middle Name" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="mlastname">Last Name</label>
									<input type="text" class="form-control" id="amlastname" name="mlastname" placeholder="Mother's Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>							
						</div><br>
						
						<div class="row">							
							<div class="col-md-8">
								<div class="form-group">
									<label for="guardian_name">Name of Guardian:&nbsp;<i>if no parent(s)</i></label>
									<input type="text" class="form-control" id="aguardianname" name="guardianname" placeholder="Guardian's Full Name" maxlength="50" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="relationship">Relationship</label>
										<select class="form-control" id="arelationship" name="relationship">
												<option></option>
												<option>aunt</option>						
												<option>uncle</option>
												<option>brother</option>
												<option>sister</option>
												<option>grandmother</option>
												<option>grandfather</option>
												<option>godmother</option>
												<option>godfather</option>
												<option>Other</option>
										</select>
								</div>
							</div>						
						</div>
						
						<div class="row">							
							<div class="col-md-4"></div>							
							<div class="col-md-4">
								<div class="form-group">
									<label for="contact_number">Contact Number</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="acontactnumber" name="contactnumber" placeholder="in case of emergency..." maxlength="15" onkeypress="return isContact(event)" required>
								</div>
							</div>							
							<div class="col-md-6"></div>							
						</div><br>

						<div class="row">							
							<div class="col-md-3">
								<div class="form-group">
									<label for="gwa">Elementary GWA</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="agwa" name="gwa" placeholder="GWA" maxlength="5" value="<?php echo '0.00'; ?>" onkeypress="return isFloat(event)" required>
								</div>
							</div>
							
							<div class="col-md-9">
								<div class="form-group">
									<label for="school">Elementary School</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="aschool" name="school" placeholder="Last School Attended" maxlength="50" onkeypress="return isLetterDashPoint(event)" required>
								</div>
							</div>						
						</div>
						
						<div class="row">						
							<div class="col-md-4"></div>							
							<div class="col-md-4" style="text-align:center">
								<div class="form-group">
									<label for="remarks">Remarks</label>
									<input type="text" class="form-control" id="aremarks" name="remarks" maxlength="30">
								</div>
							</div>
							<div class="col-md-4"></div>							
						</div>
						
						<div class="form-group">
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div><br>
						
						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnInsert">Add</button>
								<!--<button data-target="#insert" data-toggle="modal" type="button" class="btn btn-success success">Add</button>-->
								<button type="reset" class="btn btn-default">Reset</button>
							</div>							
							<div class="col-sm-4"></div>							
						</div>						
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================ Insert Confirm Modal =================================================================-->
<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Record</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to add the record?</p>          
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="students-submit.php">
            		<input type="hidden" id="insertlrn"name="lrn">
            		<input type="hidden" id="insertlastname" name="lastname">
            		<input type="hidden" id="insertfirstname" name="firstname">
            		<input type="hidden" id="insertmiddlename" name="middlename">
            		<input type="hidden" id="insertgender" name="gender">
            		<input type="hidden" id="insertbirthdate" name="birthdate">
            		<input type="hidden" id="insertmothertounge" name="mothertounge">
            		<input type="hidden" id="insertip" name="ip">
            		<input type="hidden" id="insertreligion" name="religion">
            		<input type="hidden" id="insertaddnost" name="addnost">
            		<input type="hidden" id="insertbrgy" name="brgy">
            		<input type="hidden" id="insertcity" name="city">
            		<input type="hidden" id="insertprovince" name="province">
            		<input type="hidden" id="insertflastname" name="flastname">
            		<input type="hidden" id="insertffirstname" name="ffirstname">
            		<input type="hidden" id="insertfmiddlename" name="fmiddlename">
            		<input type="hidden" id="insertmlastname" name="mlastname">
            		<input type="hidden" id="insertmfirstname" name="mfirstname">
            		<input type="hidden" id="insertmmiddlename" name="mmiddlename">
            		<input type="hidden" id="insertguardianname" name="guardianname">
            		<input type="hidden" id="insertrelationship" name="relationship">
            		<input type="hidden" id="insertcontactnumber" name="contactnumber">
            		<input type="hidden" id="insertgwa" name="gwa">
            		<input type="hidden" id="insertschool" name="school">
            		<input type="hidden" id="insertremarks" name="remarks">            		
            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnInsert">Confirm</button>                	
             	</form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	
	$(function(){
    	$('#insert').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

			var getlrnFromRow = $('#alrn').val();;
 				$('#insertlrn').val(getlrnFromRow);
			
			var getlastnameFromRow = $('#alastname').val();;
 				$('#insertlastname').val(getlastnameFromRow);
			
			var getfirstnameFromRow = $('#afirstname').val();;
 				$('#insertfirstname').val(getfirstnameFromRow);
			
			var getmiddlenameFromRow = $('#amiddlename').val();;
 				$('#insertmiddlename').val(getmiddlenameFromRow);
			
			var getgenderFromRow = $('#agender').val();;
 				$('#insertgender').val(getgenderFromRow);
			
			var getbirthdateFromRow = $('#abirthdate').val();;
 				$('#insertbirthdate').val(getbirthdateFromRow);

			var getmothertoungeFromRow = $('#amothertounge').val();;
 				$('#insertmothertounge').val(getmothertoungeFromRow);
			
			var getipFromRow = $('#aip').val();;
 				$('#insertip').val(getipFromRow);
			
			var getreligionFromRow = $('#areligion').val();;
 				$('#insertreligion').val(getreligionFromRow);
			
			var getaddnostFromRow = $('#aaddnost').val();;
 				$('#insertaddnost').val(getaddnostFromRow);

			var getbrgyFromRow = $('#abrgy').val();;
 				$('#insertbrgy').val(getbrgyFromRow);

			var getcityFromRow = $('#acity').val();;
 				$('#insertcity').val(getcityFromRow);

			var getprovinceFromRow = $('#aprovince').val();;
 				$('#insertprovince').val(getprovinceFromRow);

			var getflastnameFromRow = $('#aflastname').val();;
 				$('#insertflastname').val(getflastnameFromRow);

			var getffirstnameFromRow = $('#affirstname').val();;
 				$('#insertffirstname').val(getffirstnameFromRow);
			
			var getfmiddlenameFromRow = $('#afmiddlename').val();;
 				$('#insertfmiddlename').val(getfmiddlenameFromRow);

			var getmlastnameFromRow = $('#amlastname').val();;
 				$('#insertmlastname').val(getmlastnameFromRow);

			var getmfirstnameFromRow = $('#amfirstname').val();;
 				$('#insertmfirstname').val(getmfirstnameFromRow);

			var getmmiddlenameFromRow = $('#ammiddlename').val();;
 				$('#insertmmiddlename').val(getmmiddlenameFromRow);

			var getguardiannameFromRow = $('#aguardianname').val();;
 				$('#insertguardianname').val(getguardiannameFromRow);

			var getrelationshipFromRow = $('#arelationship').val();;
 				$('#insertrelationship').val(getrelationshipFromRow);

			var getcontactnumberFromRow = $('#acontactnumber').val();;
 				$('#insertcontactnumber').val(getcontactnumberFromRow);

			var getgwaFromRow = $('#agwa').val();;
 				$('#insertgwa').val(getgwaFromRow);

			var getschoolFromRow = $('#aschool').val();;
 				$('#insertschool').val(getschoolFromRow);

			var getremarksFromRow = $('#aremarks').val();;
 				$('#insertremarks').val(getremarksFromRow);

		});
	});

</script>