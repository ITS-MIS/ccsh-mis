<?php
	include'connect.php';
	include('current-year.php');

		$lrn=$_POST['lrn'];

			$studentq = "SELECT id, lrn, lastname, firstname, middlename, addnost, brgy, city, province, guardianname, relationship, contactnumber, guardianmail
							FROM tbl_studentinfo WHERE lrn=$lrn";
						
			$result = mysql_query($studentq) or die(mysql_error());
						
				if (mysql_num_rows($result)>0) {
				
					while(($row = mysql_fetch_assoc($result))!= null) {
						$id = $row['id'];
						$lrn = $row['lrn'];
						$lastname = $row['lastname'];
						$firstname = $row['firstname'];
						$middlename = $row['middlename'];
						$addnost = $row['addnost'];
						$brgy = $row['brgy'];
						$city = $row['city'];
						$province = $row['province'];
						$guardianname = $row['guardianname'];
						$relationship = $row['relationship'];
						$contactnumber = $row['contactnumber'];
						$guardianmail = $row['guardianmail'];

						if ($lrn==0){
							$lrn=$id;
						}
					}
				}

			$iquery = "SELECT image FROM tbl_images WHERE lrn = $lrn AND verify=1";			
						$result = mysql_query($iquery);
							
							$row = mysql_fetch_row($result);
							$image=($row[0]);
?>

	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Student's Information</h4>
	            </div>

	            <div class="modal-body" style="padding-left:50px; padding-right:50px;"> 
					<form class="form-horizontal" method="post" action="advisory-class-submit.php">

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="id"></label>
									<input type="hidden" class="form-control" id="id" name="id">	
								</div>
							</div>	
						</div>	

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="lrn">LRN</label>
									<input type="text" class="form-control" id="lrn" name="lrn" maxlength="12" value="<?php echo $lrn; ?>" onkeypress="return isNumber(event)">
								</div>
							</div>
							<div class="col-md-8" style="text-align:right;">
								<div class="form-group">
									<img id="image" class="image" src="<?php echo $image; ?>" alt="" height="150px" width="150px"/> 
								</div>
							</div>							
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="lastname">Last Name</label>
									<input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $lastname; ?>" readonly>	
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="firstname">First Name</label>
									<input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" readonly>	
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="middlename">Middle Name</label>
									<input type="text" class="form-control" id="middlename" name="middlename" value="<?php echo $middlename; ?>" readonly>	
								</div>
							</div>							
						</div><br>
							
						<div class="row">						
							<div class="col-md-4">
								<div class="form-group">
									<label for="Address">Address</label>
								</div>
							</div>							
						</div>
						
						<div class="row">							
							<div class="col-md-1"></div>							
							<div class="col-md-6">
								<div class="form-group">
									<label for="add_no_st">House No. / Street / Sitio / Purok</label>
									<input type="text" class="form-control" id="addnost" name="addnost" maxlength="30" onkeypress="return isAdd(event)" value="<?php echo $addnost; ?>"> 
								</div>
							</div>							
							<div class="col-md-4">
								<div class="form-group">
									<label for="brgy">Barangay</label>
									<input type="text" class="form-control" id="brgy" name="brgy" maxlength="20" onkeypress="return isAdd(event)" value="<?php echo $brgy; ?>">
								</div>
							</div>
							<div class="col-md-1"></div>							
						</div>
						
						<div class="row">							
							<div class="col-md-1"></div>							
							<div class="col-md-6">
								<div class="form-group">
									<label for="city">City</label>
									<input type="text" class="form-control" id="city" name="city" maxlength="30" value="<?php echo $city; ?>"> 
								</div>
							</div>							
							<div class="col-md-4">
								<div class="form-group">
									<label for="province">Province</label>
									<input type="text" class="form-control" id="province" name="province" maxlength="30" value="<?php echo $province; ?>">
								</div>
							</div>							
							<div class="col-md-1"></div>							
						</div><br>
						
						<div class="row divider">

							<div class="col-md-5"></div>							
							<div class="col-md-4">
								<div class="divider">
									<label for="lrn">Other Information</label>
								</div>
							</div>							
							<div class="col-md-3"></div>							
						</div><hr>
						
						<div class="row">							
							<div class="col-md-7">
								<div class="form-group">
									<label for="guardian_name">Name of Guardian:&nbsp;<i>if no parent(s)</i></label>
									<input type="text" class="form-control" id="guardianname" name="guardianname" maxlength="50" onkeypress="return isLetterDashPoint(event)" value="<?php echo $guardianname; ?>">
								</div>
							</div>							
							<div class="col-md-1"></div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="relationship">Relationship</i></label>
										<select class="form-control" id="relationship" placeholder="Relationship" name="relationship" value="<?php echo $relationship; ?>">
												<option></option>
												<option>mother</option>						
												<option>father</option>
												<option>aunt</option>						
												<option>uncle</option>
												<option>brother</option>
												<option>sister</option>
												<option>grandmother</option>
												<option>grandfather</option>
												<option>godmother</option>
												<option>godfather</option>
												<option>Other</option>
										</select>
										<div id="div4"></div>
								</div>
							</div>						
						</div>
						
						<div class="row">							
							<div class="col-md-3">
								<div class="form-group">
									<label for="contact_number">Contact Number</label>
									<input type="text" class="form-control" id="contactnumber" name="contactnumber" maxlength="15" onkeypress="return isContact(event)" value="<?php echo $contactnumber; ?>">
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="contact_number">Guardian E-Mail</label>
									<input type="email" class="form-control" id="guardianmail" name="guardianmail" maxlength="50" value="<?php echo $guardianmail; ?>">
								</div>
							</div>

							<div class="col-md-1"></div>

							<div class="col-md-3">
								<div class="form-group"><br>
									<label class="checkbox-inline"><input type="checkbox" id="send" name="send" value="1">Send E-mail Notification to Parent?</label><br/>
								</div>
							</div>												
						</div>

						<div class="form-group">
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						</div>
						
						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#update" data-toggle="modal" type="button" class="btn btn-success success">Update</button>				
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>							
							</div>							
							<div class="col-sm-4"></div>							
						</div>						
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;"></div>
	   		</div>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="advisory-class-submit.php">
            		<input type="hidden" id="updateid"name="id"> 
            		<input type="hidden" id="updatelrn"name="lrn">           		
            		<input type="hidden" id="updateaddnost" name="addnost">
            		<input type="hidden" id="updatebrgy" name="brgy">
            		<input type="hidden" id="updatecity" name="city">
            		<input type="hidden" id="updateprovince" name="province">
            		<input type="hidden" id="updateguardianname" name="guardianname">
            		<input type="hidden" id="updaterelationship" name="relationship">
            		<input type="hidden" id="updatecontactnumber" name="contactnumber">
            		<input type="hidden" id="updateremarks" name="remarks">
            		<input type="hidden" id="updateguardianmail" name="guardianmail">
            		<input type="hidden" id="updatesend" name="send">
            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnUpdate">Confirm</button>                	
             	</form>
            </div>
        </div>
    </div>
</div>		
	
<script type="text/javascript">
	
	$(document).ready(function() {

		$(function(){
	    	$('#delete').modal({
		        keyboard: true,
		        show:false,

	    	}).on('show.bs.modal', function(){
	         	var getIdFromRow = $('#id').val();
	 				$('#deleteid').val(getIdFromRow);

	 			var getlrnFromRow = $('#lrn').val();
	 				$('#deletelrn').val(getlrnFromRow); 

		        	console.info(getIdFromRow);
			});
		});
  	
	  	$(function(){
	    	$('#update').modal({
		        keyboard: true,
		        show:false,

	    	}).on('show.bs.modal', function(){

	    		var getIdFromRow =$('#id').val();
					$('#updateid').val(getIdFromRow);

				var getlrnFromRow = $('#lrn').val();
	 				$('#updatelrn').val(getlrnFromRow);
				
				var getaddnostFromRow = $('#addnost').val();
	 				$('#updateaddnost').val(getaddnostFromRow);

				var getbrgyFromRow = $('#brgy').val();
	 				$('#updatebrgy').val(getbrgyFromRow);

				var getcityFromRow = $('#city').val();
	 				$('#updatecity').val(getcityFromRow);

				var getprovinceFromRow = $('#province').val();
	 				$('#updateprovince').val(getprovinceFromRow);

				var getguardiannameFromRow = $('#guardianname').val();
	 				$('#updateguardianname').val(getguardiannameFromRow);

				var getrelationshipFromRow = $('#relationship').val();
	 				$('#updaterelationship').val(getrelationshipFromRow);

				var getcontactnumberFromRow = $('#contactnumber').val();
	 				$('#updatecontactnumber').val(getcontactnumberFromRow);

				var getremarksFromRow = $('#remarks').val();
	 				$('#updateremarks').val(getremarksFromRow);

	 			var getguardianmailFromRow = $('#guardianmail').val();
	 				$('#updateguardianmail').val(getguardianmailFromRow);

	 			var getsendFromRow = $('#send').val();;
	 				$('#updatesend').val(getsendFromRow);


			});
		});

	});

</script>