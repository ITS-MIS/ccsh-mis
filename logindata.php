<?php 
	include'connect.php';
	getMeta();
	getBsLink();
	getJqueryScript();
	getBsScript();
	getBsValidator();
?>

	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:gold;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Employee Login</h4>
            </div>          

            <div class="modal-body  modal-height"> 
				<form  id="empForm" class="form-horizontal" method="post" action="loginverification.php"><br><br>
					<div class="form-group">
						<label for="usernamer" class="col-sm-4 control-label">Username</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="username" placeholder="Employee No." name="emp_no">
						</div>
				  </div>
				  
					<div class="form-group">
						<label for="inputpass" class="col-sm-4 control-label">Password</label>
						<div class="col-sm-7">
							<input type="password" class="form-control" id="inputpass" placeholder="Password" name="pass">
						</div>
					</div><br>
					
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-10">
							<button type="submit" class="btn btn-success success left-margin" name="btnSubmit" >Login</button>
							<button type="reset" class="btn btn-default" name="reset" id="resetBtn">Reset</button>
						</div>
					</div>					
				</form>
			</div>

		    <div class="modal-footer" style="background-color:gold;">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
   		</div>
	</div>