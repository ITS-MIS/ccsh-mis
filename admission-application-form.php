<?php 
	include('admission-validation.php');
	if ($app_state == "empty") {
	getother();					
?>
		<div class="container-fluid">	
		
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Student's Application Form</label>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-sm-6 col-md-10" style="background-color: rgb(255,255,50); border: 3px solid; border-color: gold; padding-left:30px; padding-right:30px;">
			<br>	

				<form action="<?php ($_SERVER['PHP_SELF']);?>" method="post">
					<label for="lrn">Student's Personal Information</label>
					<hr><br>

					<div class="row">
						<p><span class="error">* required field</span></p>
						<div class="col-md-4">
							<div class="form-group">
								<label for="lrn">LRN</label>
								<input type="text" class="form-control" id="lrn" name="lrn" placeholder="LRN" maxlength="12" onkeypress="return isNumber(event)" value="<?php echo $lrn; ?>"> 
							</div>
						</div>
					</div>
						
					<div class="row">						
						<div class="col-md-4">
							<div class="form-group">
								<label for="lastname">Last Name</label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="lastname" name="lname" placeholder="Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php echo $lname; ?>"> 
								<span class="error"><?php echo $lnameErr; ?></span>
							</div>
						</div>
							
						<div class="col-md-4">
							<div class="form-group">
								<label for="fname">First Name</label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="firstname" name="fname" placeholder="First Name" maxlength="50" onkeypress="return isLetterPoint(event)" value="<?php echo $fname; ?>">
								<span class="error"><?php echo $fnameErr; ?></span>
							</div>
						</div>
							
						<div class="col-md-4">
							<div class="form-group">
								<label for="mname">Middle Name</label>
								<input type="text" class="form-control" id="middlename" name="mname" placeholder="Middle Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" value="<?php echo $mname; ?>">
							</div>
						</div>							
					</div>
							
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="birthdate">Birthdate</label>&nbsp;<span class="error">*</span>
								<input type="date" class="form-control" id="bdate" name="bdate" placeholder="Birthdate" value="<?php echo $bdate; ?>">
								<span class="error"><?php echo $bdateErr; ?></span>
							</div>
						</div>
							
						<div class="col-md-4">
							<div class="form-group">
								<label for="gender">Gender:</label>&nbsp;<span class="error">*</span><br>
								<div class="col-md-3">
									<label class="radio-inline">
										<input type="radio" value="M" id="genderm" name="rdo_gender" 
										<?php if (isset($rdo_gender) && $rdo_gender=="M") echo "checked";?>>Male
									</label>
								</div>

								<div class="col-md-9">
									<label class="radio-inline">	
										<input type="radio" value="F" id="genderf" name="rdo_gender" 
										<?php if (isset($rdo_gender) && $rdo_gender=="F") echo "checked";?>> Female
									</label><span class="error">&nbsp;<?php echo $genderErr; ?></span>
								</div>
							</div>							
						</div>
					</div><br>

					<div class="row">		
						<div class="col-md-4">
							<div class="form-group">
								<label for="Address">Address</label>
							</div>
						</div>
					</div>
						
					<div class="row">
						<div class="col-md-1"></div>					
						<div class="col-md-4">
							<div class="form-group">
								<label for="add_no_st">House No./Street/ Sitio/Purok</label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="addnost" name="addnost" placeholder="House No./Street/ Sitio/Purok" maxlength="30" onkeypress="return isAdd(event)" value="<?php echo $addnost; ?>"> 
								<span class="error">&nbsp;<?php echo $addnostErr; ?></span>
							</div>
						</div>
					
						<div class="col-md-4">
							<div class="form-group">
								<label for="brgy">Barangay </label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="brgy" name="brgy" placeholder="Barangay" maxlength="20" onkeypress="return isAdd(event)" value="<?php echo $brgy; ?>">
								<span class="error">&nbsp;<?php echo $brgyErr; ?></span>
							</div>
						</div>			
					</div>

					<div class="row">
							<div class="col-md-1"></div>							
							<div class="col-md-4">
								<div class="form-group">
									<label for="city">City</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="city" name="city" maxlength="30" placeholder="City" onkeypress="return isLetterDashPoint(event)" value="<?php echo $city; ?>">
									<span class="error">&nbsp;<?php echo $cityErr; ?></span>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="province">Province</label>&nbsp;<span class="error">*</span>
									<input type="text" class="form-control" id="province" name="province" maxlength="30" placeholder="Province" onkeypress="return isLetterDashPoint(event)" value="<?php echo $province; ?>">
									<span class="error">&nbsp;<?php echo $provinceErr; ?></span>
								</div>
							</div>
							
							<div class="col-md-1"></div>							
					</div><br><br>
						
					<label for="lrn">Other Information</label><hr><br>
						
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="mother_tounge">Mother Tongue</label>&nbsp;<span class="error">*</span>
								
								<select class="form-control" id="mothertounge" name="mothertounge" onchange="showfield1(this.options[this.selectedIndex].value)">							
											<option value="<?php echo $mothertounge ?>"><?php if (isset($_POST['mothertounge'])) {print "$mothertounge";} ?></option>
											<option>Tagalog</option>
											<option>Bikol</option>
											<option>Cebuano</option>
											<option>Chabacano</option>
											<option>Hiligaynon</option>
											<option>Iloko</option>
											<option>Kapampangan</option>
											<option>Maguindanaoan</option>
											<option>Maranao</option>
											<option>Pangasinense</option>												
											<option>Tausug</option>
											<option>Waray</option>
											<option>Other</option>
									</select>
									<span class="error">&nbsp;</span>
									<div id="div1"><input class="form-control" id="mothertounge2" onkeypress="return isLetterDashPoint(event)"
										<?php if (!isset($mothertounge2) or $mothertounge2=='') echo 'type="hidden"'; else echo 'type="text" value="'.$mothertounge2.'"';?> name="mothertounge2" /></div>
										<span class="error">&nbsp;<?php echo $mothertoungeErr; ?></span>
							</div>
						</div>
								
							<div class="col-md-4">
								<div class="form-group">
									<label for="ip">Ethnic group</label>&nbsp;<span class="error">*</span>
							
									<select class="form-control" id="ip" placeholder="Ethnic Group" name="ip" onchange="showfield2(this.options[this.selectedIndex].value)" value="<?php echo $ip; ?>">
												<option value="<?php echo $ip ?>"><?php if (isset($_POST['ip'])) {print "$ip";} ?></option>
												<option>Tagalog</option>						
												<option>Bikolano</option>
												<option>Gaddang</option>
												<option>Ibanag</option>
												<option>Ilokano</option>
												<option>Ivatan</option>
												<option>Kapampangan</option>
												<option>Moro</option>
												<option>Pangasinan</option>												
												<option>Sambal</option>
												<option>Subanon</option>
												<option>Visayan</option>
												<option>Zamboangueño</option>
												<option>Other</option>
										</select>
											<span class="error">&nbsp;</span>
											<div id="div2"><input  class="form-control" id="ip" onkeypress="return isLetterDashPoint(event)"
												 <?php if (!isset($ip2) or $ip2=='') echo 'type="hidden"'; else echo 'type="text" value="'.$ip2.'"';?>  name="ip2" /></div>
												 <span class="error">&nbsp;<?php echo $ipErr; ?></span>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="religion">Religion</label>&nbsp;<span class="error">*</span>
							
									<select class="form-control" id="religion" placeholder="Religion" name="religion" onchange="showfield3(this.options[this.selectedIndex].value)" value="<?php echo $religion; ?>">
												<option value="<?php echo $religion ?>"><?php if (isset($_POST['religion'])) {print "$religion";} ?>
												</option>
												<option>Catholic</option>						
												<option>JW</option>
												<option>INC</option>
												<option>Muslim</option>
												<option>SDA</option>
												<option>LDS</option>
												<option>Born again</option>
												<option>Other</option>
												
										</select>
											<span class="error">&nbsp;</span>
											<div id="div3"><input  class="form-control" id="religion2" onkeypress="return isLetterDashPoint(event)"
												<?php if (!isset($religion2) or $religion2=='') echo 'type="hidden"'; else echo 'type="text" value="'.$religion2.'"';?>  name="religion2" onkeypress="return isContact(event)"/></div>
												<span class="error">&nbsp;<?php echo $religionErr; ?></span>
								</div>
							</div>	
		
					</div><br>				
						
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-4">

							<div class="form-group">
								<label for="schoolyear">School:</label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="school" placeholder="Last School Attended" name="school"  maxlength="50" onkeypress="return isLetterDashPoint(event)" value="<?php echo $school; ?>">
								<span class="error">&nbsp;<?php echo $schoolErr; ?></span>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group">
								<label for="contact_number">Contact Number</label>&nbsp;<span class="error">*</span>
								<input type="text" class="form-control" id="contactnumber" name="contactnumber" placeholder="Contact Number" maxlength="15" onkeypress="return isContact(event)" value="<?php echo $contactnumber; ?>">
								<span class="error">&nbsp;<?php echo $contactnumberErr; ?></span>
							</div>
						</div>
					</div><br><br>
							
					<div class="row">
						<div class="col-md-4"></div>
						<div class="col-md-4" style="text-align:center">
							<button data-target="#insert" data-toggle="modal" type="button" class="btn btn-success success">Submit</button>
							<input type="reset" class="btn btn-default" value="Reset" name="btnReset">						
						</div>
						<div class="col-md-4"></div>						
					</div><br>
				</form>	
			</div>
				<div class="col-md-1"></div>
		</div>
	</div>
		
<!--================ Insert Confirm Modal =================================================================-->
<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Applicant's Record</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to add the record?</p>
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" action="<?php ($_SERVER['PHP_SELF']);?>" method="post">
            		<input type="hidden" id="insertlrn"name="lrn">
            		<input type="hidden" id="insertlastname" name="lname">
            		<input type="hidden" id="insertfirstname" name="fname">
            		<input type="hidden" id="insertmiddlename" name="mname">
            		<input type="hidden" id="insertbirthdate" name="bdate">
            		<input type="hidden" id="insertgender" name="rdo_gender">
            		<input type="hidden" id="insertaddnost" name="addnost">
            		<input type="hidden" id="insertbrgy" name="brgy">
            		<input type="hidden" id="insertcity" name="city">
            		<input type="hidden" id="insertprovince" name="province">
            		<input type="hidden" id="insertmothertounge" name="mothertounge">
            		<input type="hidden" id="insertip" name="ip">
            		<input type="hidden" id="insertreligion" name="religion">
            		<input type="hidden" id="insertmothertounge2" name="mothertounge2">
            		<input type="hidden" id="insertip2" name="ip2">
            		<input type="hidden" id="insertreligion2" name="religion2">
            		<input type="hidden" id="insertschool" name="school">		
            		<input type="hidden" id="insertcontactnumber" name="contactnumber">
            		<input type="hidden" name="status" value="Not yet Enrolled">
            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnSubmit">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<?php
}

	elseif ($app_state == "processed") {
	 
		include('admission-application-submit.php');		

		$app_state = "good";	
	}

	if ($app_state == "good") {	    
?>
    	<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-5">
				<label for="lname"></label>

				<?php
					$confirm=("successfully added the record!");
					echo "<script type='text/javascript'>alert(\"$confirm\");</script>";
				?>
				
				<script type="text/javascript">window.location.href="index2.php?mode=Registrar&category=Admission&page=0";</script>
			</div>
		</div>

<?php } ?>

<script type="text/javascript">
	
	$(document).ready(function(){
    	$('#insert').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

			var getlrnFromRow = $('#lrn').val();;
 				$('#insertlrn').val(getlrnFromRow);
			
			var getlastnameFromRow = $('#lastname').val();;
 				$('#insertlastname').val(getlastnameFromRow);
			
			var getfirstnameFromRow = $('#firstname').val();;
 				$('#insertfirstname').val(getfirstnameFromRow);
			
			var getmiddlenameFromRow = $('#middlename').val();;
 				$('#insertmiddlename').val(getmiddlenameFromRow);
			
			var getbirthdateFromRow = $('#bdate').val();;
 				$('#insertbirthdate').val(getbirthdateFromRow);

			var getgenderFromRow = $( "input:checked" ).val();;
 				$('#insertgender').val(getgenderFromRow);

 			var getaddnostFromRow = $('#addnost').val();;
 				$('#insertaddnost').val(getaddnostFromRow);

			var getbrgyFromRow = $('#brgy').val();;
 				$('#insertbrgy').val(getbrgyFromRow);

 			var getcityFromRow = $('#city').val();;
 				$('#insertcity').val(getcityFromRow);

 			var getprovinceFromRow = $('#province').val();;
 				$('#insertprovince').val(getprovinceFromRow);

			var getmothertoungeFromRow = $('#mothertounge').val();;
 				$('#insertmothertounge').val(getmothertoungeFromRow);
			
			var getipFromRow = $('#ip').val();;
 				$('#insertip').val(getipFromRow);
			
			var getreligionFromRow = $('#religion').val();;
 				$('#insertreligion').val(getreligionFromRow);

 			var mothertounge2 = $('#mothertounge2').val();;
 				$('#insertmothertounge2').val(mothertounge2);
			
			var ip2 = $('#ip2').val();;
 				$('#insertip2').val(ip2);
			
			var religion2 = $('#religion2').val();;
 				$('#insertreligion2').val(religion2);
			
			var getschoolFromRow = $('#school').val();;
 				$('#insertschool').val(getschoolFromRow);

			var getcontactnumberFromRow = $('#contactnumber').val();;
 				$('#insertcontactnumber').val(getcontactnumberFromRow);

		});
	});

</script>
