<?php
include_once'connect.php';

	if(isSet($_POST['btnEnroll'])){	

		$info=$_POST['info'];
		print_r($info);
		$columns=array();
		$values=array();
		print_r($columns);
		foreach ($info as $fieldname => $value) {
			if ($fieldname!='id') {
				if(!empty($value)){

					if ($fieldname=='age' || $fieldname=='lrn' ) {
						$value = trim($value);		
						array_push($values,  $value);
					}
					elseif ($fieldname=='guardianmail') {
						//$value = trim($value);		
						array_push($values,  "'".$value."'");
					}
					
					else{
						$value = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($value))));
						$value = trim($value);	
						array_push($values,  "'".$value."'");
					}
					array_push($columns, $fieldname);				
				}
			}
		}
		print_r($columns);

//--Insert _tbl_studentinfo------------------------------------------------------------------------------------------------------------------		
		$columns = implode (", ", $columns);
	
		$values = implode (", ", $values);
		
		echo "<br><br>";
		echo "INSERT INTO tbl_studentinfo(".$columns.") VALUES(".$values.")";
		query_db("INSERT INTO tbl_studentinfo(".$columns.") VALUES(".$values.")");

		$result=get_db("SELECT id, lrn FROM tbl_studentinfo ORDER BY id DESC LIMIT 1");
		$id = $result['id'];
		$lrn = $result['lrn']; 
		echo "<br><br>";
		echo $id;

		if($lrn==null || $lrn=='' || $lrn==0){
			query_db("UPDATE tbl_studentinfo SET lrn=$id WHERE id=$id");
		}

//--Insert _tbl_requirement------------------------------------------------------------------------------------------------------------------		
		
		if(isset($_POST['nso'])) $nso=1; else $nso =0;
		if(isset($_POST['f137'])) $f137=1; else $f137 =0;
		if(isset($_POST['f138'])) $f138=1; else $f138 =0;
		if(isset($_POST['cgmc'])) $cgmc=1; else $cgmc =0;
		if(isset($_POST['idpic'])) $idpic=1; else $idpic =0;
		if(isset($_POST['medc'])) $medc=1; else $medc =0;

		query_db("INSERT INTO tbl_requirement (student_id,nso,f137,f138,cgmc,idpic,medc) values($id,$nso,$f137,$f138,$cgmc,$idpic,$medc)");

//--Insert _tbl_studentstatus------------------------------------------------------------------------------------------------------------------		
		
		$sy_id=get_last_sy_id();

		if($lrn==null || $lrn=='' || $lrn==0){
			
			query_db("INSERT INTO tbl_studentstatus(lrn,year_id,sy_id,remarks) values($id,7,$sy_id,'New Student')");

		}
		else{
			query_db("INSERT INTO tbl_studentstatus(lrn,year_id,sy_id,remarks) values($lrn,7,$sy_id,'New Student')");
			
		}
	}

//--Insert _tbl_studentgrade------------------------------------------------------------------------------------------------------------------		

	$subjects=get_db("SELECT seven FROM tbl_curriculum where c_id=(SELECT c_id FROM tbl_curriculum_sy where sy_id=$sy_id)");
	$subjectsarray=str_getcsv($subjects['seven']);
	print_r($subjectsarray);

	if($lrn==null || $lrn=='' || $lrn==0){
		foreach ($subjectsarray as $key => $value) {
		
			query_db("INSERT INTO tbl_studentgrade (lrn,subject_code,quarter1,quarter2,quarter3,quarter4,sy_id) VALUES($id,'".$value."',0,0,0,0,$sy_id)");
		}
	}
	else{
		foreach ($subjectsarray as $key => $value) {
			query_db("INSERT INTO tbl_studentgrade (lrn,subject_code,quarter1,quarter2,quarter3,quarter4,sy_id) VALUES($lrn,'".$value."',0,0,0,0,$sy_id)");
		}
	}

//--Insert _tbl_applicant------------------------------------------------------------------------------------------------------------------		

	query_db("UPDATE tbl_applicantinfo set enrolled=1 where id={$info['id']}");

//--Insert _tbl_studentlogin----------------------------------------------------------------------------------------------------------------
		
		$sy_id=get_last_sy_id();
		
		if($lrn==null || $lrn=='' || $lrn==0){
			
			query_db("INSERT INTO tbl_loginstudent(lrn,username,password) values($id,$id,'$id')");

		}
		else{
			query_db("INSERT INTO tbl_loginstudent(lrn,username,password) values($lrn,$lrn,'$lrn')");	
		}

//--Insert _tbl_parentlogin------------------------------------------------------------------------------------------------------------------		
		
		$sy_id=get_last_sy_id();		

		if($lrn==null || $lrn=='' || $lrn==0){
			
			query_db("INSERT INTO tbl_loginparent(lrn,username,password) values($id,$id,'$id')");

		}
		else{
			query_db("INSERT INTO tbl_loginparent(lrn,username,password) values($lrn,$lrn,'$lrn')");
			
		}

//--Insert _tbl_audit------------------------------------------------------------------------------------------------------------------	

	$emp_type = 'Registrar';
	$table = 'tbl_studentinfo';
	$action = 'ADD';
	include('audit-query.php');

// parent mail send--------------------------------------------------------------------------------------
	if($lrn!=null || $lrn!='' || $lrn!=0){
		if($info['guardianmail'] !=null || $info['guardianmail'] =='' || !empty($info['guardianmail']) ){

			$guardianmail = $info['guardianmail'];
			include('mail.php');
		}
	}

	redirect('index2.php?mode=Registrar&category=Enrollment&page=0');