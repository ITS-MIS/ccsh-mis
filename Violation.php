<?php	
	getDatatablesLink();
	getModalPageLink();
	getDatatablesScript();
?>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Violation Report</label>
			</div>
			<div class="col-md-1"></div>
		</div>

<div class="row">
	<form id="form" method="post" action="reports-violation.php" target="_blank">
		<input type="hidden"  id="id" name="id">
		<input type="hidden"  id="lrn" name="lrn">
	
		<table id="student" class="display" cellspacing="0" width="100%" /*data-page-length="25"*/ style="background-color:gold;">
	        <thead>
	            <tr> 
	                <th>LRN</th>
	                <th>Last Name</th>
	                <th>First Name</th>
	                <th>Middle Name</th>
	            </tr>	                
	        </thead>

	        <tbody>

	        <?php  
	      
				$result =get_db_array("SELECT a.lrn, lastname, firstname,middlename FROM tbl_guidancerecord a, tbl_studentinfo b
								
									WHERE a.lrn=b.lrn and a.penalty_id>=5 order by date ");
				
				foreach ($result as $key => $fieldname) {
			?>

	           		<tr class="data" data-toggle="modal" data-traget="#studentmodal" data-id="<?php echo $result[$key]['id']; ?>" data-lrn="<?php echo $result[$key]['lrn']; ?>">
	     			
						<?php
							foreach ($fieldname as $key2 => $value) {
								if ($key2!='id') {
										
									?>
			        				<td><?php echo $value ?></td>
			        				<?php
								}
								
		        			}
	        			?>	                             
	            	</tr>
	        <?php
            	}
			?>

			</tbody>
		</table>
	</form><br>
</div>

<script type="text/javascript">

	$(document).ready(function() {
		$('#student').dataTable({
			bInfo: true,
			"bFilter": true,
			"ordering": false
			});
		
		$('#student').dataTable();
		 
		$('#student tbody').on('click', 'tr', function (e) {
			e.preventDefault();

			var id=$(this).data('id');
			var lrn=$(this).data('lrn');
			var yearlevel=$(this).data('yearlevel');
			$('#id').val(id);
			$('#lrn').val(lrn);
			$('#form').submit(); 
		});
				       
	    $(function(){
		    $('#delete').modal({
		        keyboard: true,
		        backdrop: "static",
		        show:false,

		    }).on('show.bs.modal', function(){
		        var getIdFromRow = $(event.target).closest('a').data('id');
		 			$('#deleteid2').val(getIdFromRow);

			        console.info(getIdFromRow);

			});

		});

	});

</script>