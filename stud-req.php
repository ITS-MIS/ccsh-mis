<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<style type="text/css">
			body {
				background-color: black;
				background-image:url("images/bg2.jpg");
				background-size: 100% 100%;
				background-attachment: fixed;
				background-repeat:no-repeat;
				position: absolute; 
				top: 0;
				right: 0;
				left: 0;
				bottom: 0;	
				}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<label id="header">Requirements to Submit</label>
		</div>
		<div class="col-sm-1"></div>
	</div>
	
	<div class="row"><br>
		<form method="post">
			<table id="student" class="display" cellspacing="0" width="100%" style="background-color:gold;">   
		        <thead>
		            <tr>
		                <th>Form 137</th>
						<th>Form 138</th>
		                <th>Certificate of Good Moral</th>
		                <th>NSO</th>
						<th>Medical Certificate</th>
						<th>ID Picture</th>		               
		            </tr>		                
		        </thead>
		 
		        <tbody>
			        <?php  
			        	$id=get_db("SELECT id from tbl_studentinfo where lrn=$lrn");
			        	$id=$id['id'];
						$query = "SELECT nso,f137,f138,cgmc,idpic,medc FROM tbl_requirement a 
									LEFT JOIN tbl_studentinfo b ON a.id=b.id WHERE a.student_id=$id";

						$result = mysql_query($query) or die(mysql_error());


						if(mysql_num_rows($result) > 0) {
														
							while ($row = mysql_fetch_assoc($result)) {

								$nso= $row['nso'];
								$f138= $row['f138'];
								$f137= $row['f137'];
								$cgmc= $row['cgmc'];
								$idpic= $row['idpic'];
								$medc= $row['medc'];					
					?>

		            <tr>               
					 	<td style="text-align:center"><?php echo $f137; ?></td>
						<td style="text-align:center"><?php echo $f138; ?></td>
						<td style="text-align:center"><?php echo $cgmc; ?></td>
						<td style="text-align:center"><?php echo $nso; ?></td>
						<td style="text-align:center"><?php echo $medc; ?></td>
						<td style="text-align:center"><?php echo $idpic; ?></td>						
		            </tr>			
					<?php
				            }
				        }
				    ?>

				</tbody>
			</table>
		</form>
	</div>

	<div class="row">
		<div class="col-sm-8">
			<br>	
			<label><i>Legend:</i></label> <br>
				<p style="text-indent: 30px; color:#FFFAF0; text-shadow: 1px 1px black; font-size:15px">1 - completed</p>
				<p style="text-indent: 30px; color:#FFFAF0; text-shadow: 1px 1px black; font-size:15px">0 - need to submit immediately</p>
		</div>	
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#student').dataTable({
		 	bInfo: false,
			"bFilter": false,
			"ordering": false,
			"paging":false
    	} );
	} );

</script>