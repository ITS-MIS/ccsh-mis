<?php
	getDatatablesLink();
	getDatatablesScript();
	getother();

	$advisoryclass=get_db("SELECT section_name FROM tbl_advisers a LEFT JOIN tbl_section b ON a.section_id=b.section_id WHERE a.emp_no=$emp_no AND a.sy_id=$sy_id");
	$advisoryclass=$advisoryclass['section_name'];
?>

<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Advisory Class List - <?php echo $advisoryclass; ?></label>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
		<div class="col-sm-6 col-md-12">			
			<table id="student" class="display" cellspacing="0" width="100%" /*data-page-length='25'*/ style="background-color: gold">
				<thead>
					<tr>
						<th style="display: none;">ID</th>
						<th>LRN</th>
						<th>Name</th>
						<th>First Name</th>
						<th>Middle Name</th>
						<th style="display: none;">No. St.</th>
						<th style="display: none;">Barangayrgy</th>
						<th style="display: none;">City</th>
						<th style="display: none;">Province</th>
						<th style="display: none;">Guardian Name</th>
						<th style="display: none;">Relationship</th>
						<th style="display: none;">Contact Number</th>
						<th style="display: none;">Year Level</th>
						<th style="display: none;">Section</th>
						<th style="display: none;">Remarks</th>
					</tr>
				</thead>
				
				<tbody>
				<?php 
					$studentq = "SELECT a.id, a.lrn, lastname, firstname, middlename, addnost, brgy, city, province, guardianname, relationship, contactnumber,
									c.year_level, d.section_name, a.remarks FROM tbl_studentinfo a 
									LEFT JOIN tbl_studentstatus b ON a.lrn = b.lrn
									LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id
									LEFT JOIN tbl_section d ON b.section_id = d.section_id 
									LEFT JOIN tbl_advisers e ON b.section_id = e.section_id 
									WHERE e.emp_no = $emp_no AND b.sy_id=$sy_id AND a.remarks!='TO' AND a.remarks!='EXPELLED'
										GROUP BY a.lrn ORDER BY gender desc, lastname asc, firstname asc";
								
					$result = mysql_query($studentq) or die(mysql_error());
						if (mysql_num_rows($result)>0) {
							while(($row = mysql_fetch_assoc($result))!= null) {

								$id = $row['id'];
								$lrn = $row['lrn'];
								$lastname = $row['lastname'];
								$firstname = $row['firstname'];
								$middlename = $row['middlename'];
								$addnost = $row['addnost'];
								$brgy = $row['brgy'];
								$city = $row['city'];
								$province = $row['province'];
								$guardianname = $row['guardianname'];
								$relationship = $row['relationship'];
								$contactnumber = $row['contactnumber'];
								$year_level = $row['year_level'];
								$section_name = $row['section_name'];
								$remarks = $row['remarks'];

								if ($lrn==0){
									$lrn=$id;
								}
				?>
							<tr data-id="<?php echo $id ?>" data-lrn="<?php echo $lrn ?>" class="data">	        
						        <td style="display: none;"></td>
						        <td><?php echo $lrn; ?></td>
						        <td><?php echo $lastname; ?></td>
						        <td><?php echo $firstname; ?></td>
						        <td><?php echo $middlename; ?></td>
						        <td style="display: none;"><?php echo $addnost; ?></td>
						        <td style="display: none;"><?php echo $brgy; ?></td>
						        <td style="display: none;"><?php echo $city; ?></td>
						        <td style="display: none;"><?php echo $province; ?></td>
						        <td style="display: none;"><?php echo $guardianname; ?></td>
						        <td style="display: none;"><?php echo $relationship; ?></td>
						        <td style="display: none;"><?php echo $contactnumber; ?></td>
						        <td style="display: none;"><?php echo $year_level; ?></td>
						        <td style="display: none;"><?php echo $section_name; ?></td>
						        <td style="display: none;"><?php echo $remarks; ?></td>
						    </tr>
					<?php
							}
						}
					?>
				
				</tbody>
			</table>
		</div>
	</div>
</div>	

<div id="ag" class="modal fade">
	<div id="yearlevel-content">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">       
		   	</div>
		</div>
	</div>
</div>
		
<script type="text/javascript">

	$(document).ready(function() {
		var table=$('#student').dataTable({
			bInfo: true,
			"bFilter": true,
			"ordering": false
		});
 		
    	$('#student tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }


			$(".modal-content").html('');
			$('#ag').modal('show');


			 $.post('modal-advisory-class.php',{lrn:$(this).attr('data-lrn')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ) 
			var lrn= $(this).attr('data-lrn');
			 console.info(lrn);
		});
	} );

</script>