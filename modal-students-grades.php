<?php
	include'connect.php';
	include('current-year.php');

		$lrn=$_POST['lrn'];

		$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
						FROM tbl_studentinfo a, tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
						WHERE b.year_id = c.year_id AND b.section_id = d.section_id AND a.lrn = b.lrn AND a.lrn=$lrn AND b.sy_id=$sy_id";
										
		$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
		
		if (mysql_num_rows($result)>0) {
			while($row = mysql_fetch_row($result)) {			
				$lrn = $row[0];
				$lastname = $row[1];
				$firstname = $row[2];
				$middlename = $row[3];
				$year = $row[4];
				$section = $row[5];				
			}
		}
?>

<form method="post" action="submitgrades.php">
	<div class="modal-content">
	    <div class="modal-header" style="background-color:gold;">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">Student's Academic Record</h4>
	    </div>
	    
	    <div class="modal-body  modal-height"><!--=============START of Modal Body--======================-->

	    	<div class="row"><!--=============START of  Student Info--======================-->
				<div class="col-md-1"></div>
				<div class="col-md-7">
					<div class="form-group">
						<label for="lrn">LRN: <?php echo $lrn; ?></label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group" style="text-align: right;">
						<label for="year">Year Level: <?php echo $year; ?></label>
					</div>
				</div>
				<div class="col-md-1"></div>							
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-7">
					<div class="form-group">
						<label for="name">Student's Name: <?php echo $lastname .', '. $firstname .' '. $middlename ?></label>		
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group" style="text-align: right;">
						<label for="section">Section: <?php echo $section; ?></label>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div><!--=============END of  Student Info--======================-->


			<div class="bs-studgrade"><!--=============START of TABS--======================-->
			    <ul class="nav nav-tabs" id="form137Tab">
			    	<?php
			    		
			    		$count7 = get_db("SELECT count(lrn) as c7 FROM tbl_studentstatus WHERE year_id = 7 AND lrn = $lrn;");
			    		$count7 = $count7['c7'];
			    		
			    		if ($count7 == 1) { 
			        		echo "<li class='active'><a href='#grade7'>Grade 7 Grades</a></li>";
			        	}


			        	$count8 = get_db("SELECT count(lrn) as c8 FROM tbl_studentstatus WHERE year_id = 8 AND lrn = $lrn;");
			    		$count8 = $count8['c8'];
			    		
			    		if ($count8 == 1) { 
			        		echo "<li><a href='#grade8'>Grade 8 Grades</a></li>";
			        	}


			        	$count9 = get_db("SELECT count(lrn) as c9 FROM tbl_studentstatus WHERE year_id = 9 AND lrn = $lrn;");
			    		$count9 = $count9['c9'];
			    		
			    		if ($count9==1){ 
			        		echo "<li><a href='#grade9'>Grade 9 Grades</a></li>";
			        	}


			        	$count10 = get_db("SELECT count(lrn) as c10 FROM tbl_studentstatus WHERE year_id = 10 AND lrn = $lrn;");
			    		$count10 = $count10['c10'];
			    		
			    		if ($count10==1){ 
			        		echo "<li><a href='#grade10'>Grade 10 Grades</a></li>";
			        	}
			        ?>
			        
			    </ul><br>

			    <div class="tab-content">
			        <div id="grade7" class="tab-pane fade in active"><!--=============START of  TAB7--======================-->		
						<table id="grade-spec7" data-page-length="10" class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>Subject_code</th>
									<th>Description</th>
									<th>1st Quarter</th>
									<th>2nd Quarter</th>
									<th>3rd Quarter</th>
									<th>4th Quarter</th>
								</tr>
							</thead>
							
							<tbody>
								<?php			
									$studentgradearray = get_db_array("SELECT a.subject_code, subject_title, quarter1, quarter2, quarter3, quarter4 
														FROM tbl_studentgrade a 
														LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code 
														WHERE lrn = $lrn AND a.subject_code IN 
															(SELECT c.subject_code from tbl_subject_yearlevel c 
																LEFT JOIN tbl_studentgrade a ON a.sy_id=c.sy_id WHERE year_id = 7) ORDER BY b.subject_title asc");
																			
									if (count($studentgradearray)>0) {

										foreach ($studentgradearray as $key => $fieldname) {
								?>
								
								<tr>
	           	  			
									<?php
						
										foreach ($fieldname as $key2 => $value) {

											if($key2=='quarter1'){
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter2') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter3') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter4') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>"  data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											else{
												
												?>
												<td ><?php echo $value; ?></td>
												<?php	
												
											}

	  
	        							}
	        						?>	                             
	            				</tr>
	    						
	    						<?php		        
		        						}
		        					}		    					
		    					?>
		    					<input type="hidden" name="year_id">
		    				</tbody>
						</table>
			        </div><!--===============================================END of TAB7=========================-->
			       
			        <div id="grade8" class="tab-pane fade in"><!--=============START of  TAB8--======================-->		
						<table id="grade-spec8" data-page-length="10" class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>Subject_code</th>
									<th>Subject</th>
									<th>1st Quarter</th>
									<th>2nd Quarter</th>
									<th>3rd Quarter</th>
									<th>4th Quarter</th>
								</tr>
							</thead>
							
							<tbody>
							
								<?php
									$lrn=$_POST['lrn'];
									$studentgradearray = get_db_array("SELECT a.subject_code,subject_title, quarter1, quarter2, quarter3, quarter4 
														FROM tbl_studentgrade a 
														LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code 
														WHERE lrn = $lrn AND a.subject_code IN 
															(SELECT c.subject_code from tbl_subject_yearlevel c 
																LEFT JOIN tbl_studentgrade a ON a.sy_id=c.sy_id WHERE year_id = 8) ORDER BY b.subject_title asc");
																								
									if (count($studentgradearray)>0) {

										foreach ($studentgradearray as $key => $fieldname) {
								?>
								
								<tr>
	           	  			
									<?php
						
										foreach ($fieldname as $key2 => $value) {
											if($key2=='quarter1'){
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter2') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter3') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter4') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>"  data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											else{
												
												?>
												<td ><?php echo $value; ?></td>
												<?php	
												
											}
	        							}
	        						?>	                             
	            				</tr>
	    						
	    						<?php		        
		        						}
		        					}		    	
		    					?>
		    				</tbody>
						</table>
			        </div><!--===============================================END of TAB8=========================-->

			        <div id="grade9" class="tab-pane fade in"><!--=============START of  TAB9--======================-->		
						<table id="grade-spec9" data-page-length="10" class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>Subject_code</th>
									<th>Subject</th>
									<th>1st Quarter</th>
									<th>2nd Quarter</th>
									<th>3rd Quarter</th>
									<th>4th Quarter</th>
								</tr>
							</thead>
							
							<tbody>
							
								<?php
									$lrn=$_POST['lrn'];
									$studentgradearray = get_db_array("SELECT a.subject_code, subject_title, quarter1, quarter2, quarter3, quarter4 
														FROM tbl_studentgrade a 
														LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code 
														WHERE lrn = $lrn AND a.subject_code IN 
															(SELECT c.subject_code from tbl_subject_yearlevel c 
																LEFT JOIN tbl_studentgrade a ON a.sy_id=c.sy_id WHERE year_id = 9) ORDER BY b.subject_title asc");
																			
									if (count($studentgradearray)>0) {

										foreach ($studentgradearray as $key => $fieldname) {
								?>
								
								<tr>
	           	  			
									<?php
						
										foreach ($fieldname as $key2 => $value) {
											if($key2=='quarter1'){
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter2') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter3') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter4') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>"  data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											else{
												
												?>
												<td ><?php echo $value; ?></td>
												<?php	
												
											}
	        							}
	        						?>	                             
	            				</tr>
	    						
	    						<?php		        
		        						}
		        					}		    	
		    					?>
		    				</tbody>
						</table>
			        </div><!--===============================================END of TAB9=========================-->

			        <div id="grade10" class="tab-pane fade in"><!--=============START of  TAB10--======================-->		
						<table id="grade-spec10" data-page-length="10" class="display" cellspacing="0" width="100%" style="background-color:gold;">
							<thead>
								<tr>
									<th>Subject_code</th>
									<th>Subject</th>
									<th>1st Quarter</th>
									<th>2nd Quarter</th>
									<th>3rd Quarter</th>
									<th>4th Quarter</th>
								</tr>
							</thead>
							
							<tbody>
							
								<?php
									$lrn=$_POST['lrn'];
									$studentgradearray = get_db_array("SELECT a.subject_code, subject_title, quarter1, quarter2, quarter3, quarter4 
														FROM tbl_studentgrade a 
														LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code 
														WHERE lrn = $lrn AND a.subject_code IN 
															(SELECT c.subject_code from tbl_subject_yearlevel c 
																LEFT JOIN tbl_studentgrade a ON a.sy_id=c.sy_id WHERE year_id = 10) ORDER BY b.subject_title asc");
																			
									if (count($studentgradearray)>0) {

										foreach ($studentgradearray as $key => $fieldname) {
								?>
								
								<tr>
	           	  			
									<?php
						
										foreach ($fieldname as $key2 => $value) {
											if($key2=='quarter1'){
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter2') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter3') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>"  data-subjectc="<?php echo  $fieldname['subject_code']; ?>" data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											elseif ($key2=='quarter4') {
												?>
												<td class="grade"><?php if($value <80) echo '<span style="color:red;">'.$value.'</span>';else echo $value; ?>
				        							<a  style=" margin-left:20px;" data-lrn="<?php echo $lrn; ?>" data-subjectc="<?php echo  $fieldname['subject_code']; ?>"  data-q="<?php echo $key2;?>" data-grade="<?php echo $value; ?>" class="edit" href="#">edit</a></td>
												</td>
												<?php
											}
											else{
												
												?>
												<td ><?php echo $value; ?></td>
												<?php	
												
											}
	        							}
	        						?>	                             
	            				</tr>
	    						
	    						<?php		        
		        						}
		        					}		    	
		    					?>
		    				</tbody>
						</table>
			        </div><!--===============================================END of TAB10=========================-->
			    </div>
			</div><!--=============END of TABS--======================-->
				
	    </div><!--=============END of Modal Body--======================-->
	   
	    <div class="modal-footer" style="background-color:gold;">
	   		 <input id="submit" type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
	    	<input id="submit" type="hidden" name="lrn" value="<?php echo $lrn ?>">
	    	<button id="submit" type="submit" class="btn btn-success" name="btnResgistrarSubmit" onclick="return confirm('Are you sure you want to submit?');">Submit</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    </div>

	</div>
</form>

<script type="text/javascript">

	$(document).ready(function() {
		$('#grade-spec7').dataTable({
			bInfo: false,
			bFilter: false,
			paging: false,
		});

		$("td.grade a").on("click",  function() {
		    var parent = $(this).parent();
		    $(this).replaceWith("<input type='text' value='" + $(this).data('grade') + "' name='grade[" + $(this).data('q') + "][" + $(this).data('subjectc') + "]' style='margin-left:20px;width:80px; height:25px;' onkeypress='return isNumberKey(this);'>"); //closing angle bracket added
		    parent.children(":text").focus();
		    return false;
		  
		});
	} );

	$(document).ready(function() {
		$('#grade-spec8').dataTable({
			bInfo: false,
			bFilter: false,
			paging: false,
		});
	} );

	$(document).ready(function() {
		$('#grade-spec9').dataTable({
			bInfo: false,
			bFilter: false,
			paging: false,
		});
	} );

	$(document).ready(function() {
		$('#grade-spec10').dataTable({
			bInfo: false,
			bFilter: false,
			paging: false,
		});
	} );

		
	$(document).ready(function(){ 
    	$("#form137Tab a").click(function(e){
    		e.preventDefault();
    		$(this).tab('show');
    	});
	});

</script>