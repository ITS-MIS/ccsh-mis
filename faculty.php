<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">List of Faculty</label>
			</div>
			<div class="col-md-1"></div>
	</div>	

	<button data-target="#insertmodal" data-toggle="modal" type="button" class="btn btn-success success"> <font face="arial">Add Faculty</font></span></button>

	<div class="row"><br>
		<form method="post">
			<table id="faculty" class="display" cellspacing="0" width="100%" style="background-color: gold;">		    
		        <thead>
		            <tr>
		            	<th style="display: none;">ID</th>
		                <th>Employee No.</th>
		                <th>Last Name</th>
		                <th>First Name</th>
		                <th>Middle Name</th>
		                <th style="display: none;">Password</th>
		            </tr>		                
		        </thead>

		        <tbody>
			        <?php  
						$query = "SELECT a.*, b.password FROM tbl_facultyinfo a 
									LEFT JOIN tbl_loginemployee b ON a.emp_no=b.emp_no where a.emp_no !=1413914
									ORDER BY lastname, firstname";

						$result = mysql_query($query) or die(mysql_error());

						if(mysql_num_rows($result) > 0) {
														
							while ($row = mysql_fetch_assoc($result)) {

								$id = $row['id'];
								$emp_no = $row['emp_no'];
								$lastname = $row['lastname'];
								$firstname = $row['firstname'];
								$middlename = $row['middlename'];
								$password = $row['password'];
					?>

		            <tr data-toggle="modal" data-id="<?php echo $id ?>" data-empno="<?php echo $emp_no ?>"
		            						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
		            						data-middlename="<?php echo $middlename ?>" data-password="<?php echo $password ?>"
		            						data-target="#facultymodal" class="data">
		                
		                <td style="display: none;"></td>
		                <td><?php echo $emp_no; ?></td>
		                <td><?php echo $lastname; ?></td>
		                <td><?php echo $firstname; ?></td>
		                <td><?php echo $middlename; ?></td>
		                <td style="display: none;"></td>
		            </tr>
				
					<?php
			            	}
			        	}
			    	?>

				</tbody>

			</table>
		</form><br>
	</div>
</div>

<div id="facultymodal" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Faculty Information</h4>
	            </div>
	            

	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="faculty-submit.php"><br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id" name="id" >
							</div>
					  	</div>
					
						<div class="form-group">
							<label for="emp_no" class="col-sm-4 control-label">Employee No.</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="emp_no" name="emp_no" placeholder="Employee No." maxlength="12" onkeypress="return isNumber(event)">
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Last Name</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" maxlength="30" onkeypress="return isLetterDashPoint(event)">
							</div>
						</div>

						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">First Name</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" maxlength="50" onkeypress="return isLetterPoint(event)">
							</div>
						</div>

						<div class="form-group">
							<label for="middlename" class="col-sm-4 control-label">Middle Name</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="middlename" name="middlename" placeholder="Middle Name" maxlength="30" onkeypress="return isLetterDashPoint.(event)">
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="col-sm-4 control-label">Password</label>
							<div class="col-sm-7">
								<input type="password"  class="form-control" id="password" name="password" placeholder="Password" maxlength="20" onkeypress="return isPassword(event)">
							</div>
						</div><br>
						
						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#update" data-toggle="modal" type="button" class="btn btn-success success">Update</button>
								<input type="hidden"  class="form-control" id="deleteid" >						
								<button  data-target="#delete" data-toggle="modal" type="button" class="btn btn-danger warning confirm-delete-modal" name="btnDelete">Delete</button>							
							</div>							
							<div class="col-sm-4"></div>							
						</div>

						<div class="form-group">																					
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">							
						</div>						
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================ Add Record Modal =================================================================-->
<div id="insertmodal" class="modal fade">
	<div id="faculty-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Add Faculty Record</h4>
	            </div>	            

	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="faculty-submit.php"><br><br>
						<div class="form-group">
							<label for="emp_no" class="col-sm-4 control-label">Employee No.</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="aemp_no" name="emp_no" placeholder="Employee No." maxlength="12" onkeypress="return isNumber(event)" required>
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Last Name</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="alastname" name="lastname" placeholder="Last Name" maxlength="30" onkeypress="return isLetterDashPoint (event)" required>
							</div>
						</div>

						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">First Name</label>&nbsp;<span class="error">*</span>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="afirstname" name="firstname" placeholder="First Name" maxlength="50" onkeypress="return isLetterPoint (event)" required>
							</div>
						</div>

						<div class="form-group">
							<label for="middlename" class="col-sm-4 control-label">Middle Name</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="amiddlename" name="middlename" placeholder="Middle Name" maxlength="30" onkeypress="return isLetterDashPoint (event)">
							</div>
						</div><br>
						
						<div class="form-group">
							<div class="col-sm-4"></div>					
							<div class="col-sm-4" style="text-align: center;">
								<button type="submit" class="btn btn-success success" name="btnInsert" onclick="return confirm('Are you sure you want to submit?');">Add</button>
								<button type="reset" class="btn btn-default">Reset</button>						
							</div>							
							<div class="col-sm-4"></div>							
						</div>

						<div class="form-group">																					
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">							
						</div>						
					</form>
				</div>
				
				<p><span class="error">&nbsp;* required field</span></p>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>

	   		</div>
	   	</div>
	</div>
</div>

<!--================ Insert Confirm Modal =================================================================-->
<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Record</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to add the record?</p>               
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="faculty-submit.php">
            		<input type="hidden" id="insertempno"name="emp_no">
            		<input type="hidden" id="insertlastname" name="lastname"  >		
					<input type="hidden" id="insertfirstname" name="firstname" >							
					<input type="hidden" id="insertmiddlename" name="middlename"  >																	
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-insert-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-insert" name="btnInsert">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="faculty-submit.php">
            		<input type="hidden" id="updateid" name="id">
            		<input type="hidden" id="updateempno"name="emp_no">
            		<input type="hidden" id="updatelastname" name="lastname"  >		
					<input type="hidden" id="updatefirstname" name="firstname" >							
					<input type="hidden" id="updatemiddlename" name="middlename"  >												
					<input type="hidden" id="updatepassword" name="password"> 					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnUpdate">Confirm</button>                	
             	</form>
            </div>
        </div>
    </div>
</div>

<!--================ Delete Confirm Modal =================================================================-->
<div id="delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to delete the record?</p>
                <p class="text-warning"><small>If you proceed, deletion will be immediate!</small></p>
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="faculty-submit.php">
            		<input type="hidden"  id="deleteid2" name="id">
            		<input type="hidden" id="deleteempno"name="emp_no">
            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-delete-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-danger warning modal-delete" name="btnDelete">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>
		
<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#faculty').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    	});

    	$('#faculty tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }
	       
    	} );
	} );

	$(function(){
	 	console.info('getIdFromRow');
	    $('#facultymodal').modal({
	        keyboard: true,
	        show:false,

	    }).on('show.bs.modal', function(){
	        var getIdFromRow = $(event.target).closest('tr').data('id');
     			$('#id').val(getIdFromRow);

	        var getempnoFromRow = $(event.target).closest('tr').data('empno');	         
	         	$('#emp_no').val(getempnoFromRow);
	         	$('#deleteempno').val(getempnoFromRow);	  

	        var getlastnameFromRow = $(event.target).closest('tr').data('lastname');
	         	$('#lastname').val(getlastnameFromRow);	 

	        var getfirstnameFromRow = $(event.target).closest('tr').data('firstname');
	         	$('#firstname').val(getfirstnameFromRow);

	        var getmiddlenameFromRow = $(event.target).closest('tr').data('middlename');
	         	$('#middlename').val(getmiddlenameFromRow);

	        var getpasswordFromRow = $(event.target).closest('tr').data('password');
	         	$('#password').val(getpasswordFromRow); 
	        
			$('#deleteid').val(getIdFromRow);
			         	console.info(getIdFromRow);
				        
		 });
	});

	$(function(){
    	$('#insert').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getempnoFromRow = $('#aemp_no').val();;	         
	         	$('#insertempno').val(getempnoFromRow);
	         
	        var getlastnameFromRow = $('#alastname').val();;
	         	$('#insertlastname').val(getlastnameFromRow);	 

	        var getfirstnameFromRow = $('#afirstname').val();;
	         	$('#insertfirstname').val(getfirstnameFromRow);

	        var getmiddlenameFromRow = $('#amiddlename').val();;
	         	$('#insertmiddlename').val(getmiddlenameFromRow);

		});
	});

  	$(function(){
    	$('#delete').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){
         	var getIdFromRow = $('#deleteid').val();
       
 			$('#deleteid2').val(getIdFromRow); 			
	        console.info(getIdFromRow);
		});
	});

  	$(function(){
    	$('#update').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getIdFromRow =$('#id').val();;
				$('#updateid').val(getIdFromRow);

	        var getempnoFromRow = $('#emp_no').val();;	         
	         	$('#updateempno').val(getempnoFromRow);
	         
	        var getlastnameFromRow = $('#lastname').val();;
	         	$('#updatelastname').val(getlastnameFromRow);	 

	        var getfirstnameFromRow = $('#firstname').val();;
	         	$('#updatefirstname').val(getfirstnameFromRow);

	        var getmiddlenameFromRow = $('#middlename').val();;
	         	$('#updatemiddlename').val(getmiddlenameFromRow);

	        var getpasswordFromRow = $('#password').val();;
	         	$('#updatepassword').val(getpasswordFromRow); 
	 			
		        console.info(getIdFromRow);
		});
	});

</script>