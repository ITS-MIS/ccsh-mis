<?php
	getDatatablesLink();
	getCcshsPageLink();
	getDatatablesScript();
?>
	
<style type="text/css">
	body {
		background-color: black;
		background-image:url("images/bg2.jpg");
		background-size: 100% 100%;
		background-attachment: fixed;
		background-repeat:no-repeat;
		position: absolute; 
		top: 0;
		right: 0;
		left: 0;
		bottom: 0;	
		}
		
	.badge{color:white;
			background-color: red;
	}
</style>

<div class="container-fluid">
	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Online Grades of Child</label>
			</div>
			<div class="col-md-1"></div>
	</div>
	
	<div class="row">
		<div class="col-sm-3 col-md-1"></div>			
			<?php
				include('current-year.php');
				$select = get_db("SELECT section_id FROM tbl_studentstatus WHERE lrn=$lrn2 AND sy_id=$sy_id");
				$select=$select['section_id'];

				if($select==0) {

					$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level 
								FROM tbl_studentinfo a 
								LEFT JOIN tbl_studentstatus b ON a.lrn=b.lrn
								LEFT JOIN tbl_yearlevel c ON b.year_id=c.year_id 
								WHERE a.lrn = $lrn2 AND b.sy_id=$sy_id";
								
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
				
					if (mysql_num_rows($result)>0) {				
						while($row = mysql_fetch_row($result)) {					
							$lrn = $row[0];
							$lastname = $row[1];
							$firstname = $row[2];
							$middlename = $row[3];
							$year_level = $row[4];							
						}
						$section_name="Unassigned";
					}	
				} else {
					$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
								FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
								WHERE b.year_id = c.year_id AND b.section_id = d.section_id AND a.lrn = b.lrn AND a.lrn = $lrn2 AND b.sy_id=$sy_id";
								
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
				
					if (mysql_num_rows($result)>0) {				
						while($row = mysql_fetch_row($result)) {					
							$lrn = $row[0];
							$lastname = $row[1];
							$firstname = $row[2];
							$middlename = $row[3];
							$year_level = $row[4];
							$section_name = $row[5];							
						}
					}	
				}				
			?>
				
		<div class="col-sm-3 col-md-10" style="border: 3px solid; border-color: gold;" id="divToPrint" style="display:none;"><br>

			<div class="row">
				<div class="col-md-2">
					<img src="images/logo.png" height=80px; width=80px;>
				</div>
						
				<div class="col-md-10">
		
					<div class="row">			
						<div class="col-md-4">
							<div class="form-group">
								<label for="lrn2">LRN:</label>
								<label for="lrn2"><?php echo $lrn; ?></label>
							</div>
						</div>						
					</div>

					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="name">Name:</label>
								<label for="name"><?php echo $lastname .", ". $firstname ." ". $middlename; ?></label>
							</div>
						</div>
					</div>

					
					<div class="row">	
						<div class="col-md-4">
							<div class="form-group">
								<label for="yr">Year Level:</label>
								<label for="lrn2"><?php echo $year_level; ?></label>
							</div>
						</div>
						
					</div>
						
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="section">Section:</label>
								<label for="section"><?php echo $section_name; ?></label>
							</div>
						</div>						
					</div>
				</div>

				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<table id="online-grade" data-page-length="20" class="display" cellspacing="0" width="100%" style="background-color: gold">
							<thead>
								<tr>
									<th>Subject</th>
									<th>1st Quarter</th>
									<th>2nd Quarter</th>
									<th>3rd Quarter</th>
									<th>4th Quarter</th>
								</tr>
							</thead>
							
							<tbody>
								<?php
									$sql_load = "SELECT subject_title, quarter1, quarter2, quarter3, quarter4 FROM tbl_studentgrade a
													LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code
													LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn
													WHERE a.lrn = $lrn AND a.sy_id=$sy_id GROUP BY a.subject_code ORDER BY subject_title";
													
									$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
									
									if (mysql_num_rows($result)>0) {
										while($row = mysql_fetch_row($result)) {										
											$subject_code = $row[0];
											$quarter1 = $row[1];
											$quarter2 = $row[2];
											$quarter3 = $row[3];
											$quarter4 = $row[4];										
								?>								
										<tr>
											<td><?php echo $subject_code;  ?></td>
											<td align="center"><?php  if ($quarter1 == 0) { echo ' '; } else {echo round($quarter1);} ?></td>
											<td align="center"><?php  if ($quarter2 == 0) { echo ' '; } else {echo round($quarter2);} ?></td>
											<td align="center"><?php  if ($quarter3 == 0) { echo ' '; } else {echo round($quarter3);} ?></td>
											<td align="center"><?php  if ($quarter4 == 0) { echo ' '; } else {echo round($quarter4);} ?></td>
										</tr>
							
								<?php
										}
									}								
								?>
							</tbody>
						</table>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div><br><br>

			<div class="row">
				<div class="col-md-5"></div>					
				<div class="col-md-7">				
					<input type="submit" class="btn btn-success" id="btnprint" value="Print a copy" onclick="PrintDiv();" />
				</div>						
			</div><br>	
		</div>
		<div class="col-md-1"></div>
	</div>
</div>

	<script type="text/javascript">	

		/* Datatable Student's Grade -------------------------------------------*/
			$(document).ready(function() {
				$('#online-grade').dataTable({
					bInfo: false,
					bFilter: false,
					paging: false,
					"ordering": false

				});
			} );	
	     

		function PrintDiv() {    
			
			var divToPrint = document.getElementById('divToPrint');
			
			var ButtonControl = document.getElementById("btnprint");
				ButtonControl.style.visibility = "hidden";
		   
			var popupWin = window.open('', '_blank', 'width=1000,height=500');
				popupWin.document.open();
		  
				popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
				divToPrint.style.visibility = 'hidden';
				popupWin.document.close();
				
				window.location = 'index2.php?mode=Parent&category=Online%20Grades&page=1';
			}
				
	 </script>