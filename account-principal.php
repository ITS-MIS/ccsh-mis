<?php						
	$passq = "SELECT password FROM tbl_loginemployee WHERE emp_no = $emp_no";			
	$result = mysql_query($passq) or die('SQL Error :: '.mysql_error());
		
		$row = mysql_fetch_row($result);
		$p=($row[0]);

	// Initialize variables and set to empty strings
	$opass = $npass = $npass2 = "";
	$opassErr = $npassErr = $npass2Err = $matchErr = $matchErr2 = "";

	// Control variables
	$app_state = "empty";  //empty, processed, logged in
	$valid = 0;

	// Validate input and sanitize
	if ($_SERVER['REQUEST_METHOD']== "POST") {

   		if (empty($_POST["opass"])) {

			$opassErr = "*Password is required";
	     	$opass = $npass = $npass2 = "";
   		
   		} else {

			$opass = test_input($_POST["opass"]);
	      	$valid++;
	   	}
   		
   		if (empty($_POST["npass"])) {

			$npassErr = "*New Password is required";
	     	$npass = $npass2 = "";
   		
   		} else {

			$npass = test_input($_POST["npass"]);
	      	$valid++;
	   	}
	   
	   	if (empty($_POST["npass2"])) {
	  		
	  		$npass2Err = "Confirm Password";
	     	$npass = $npass2 = "";
	   	
	   	} else {

    		$npass2 = test_input($_POST["npass2"]);
	      	$valid++;
    	}
    	
    	if (!empty($_POST["opass"]) && $p != $opass) {
			$matchErr = "Wrong Password! Try again!";
			$opass = $npass = $npass2 = "";

		} else {

			$opass = test_input($_POST["opass"]);
			$npass = test_input($_POST["npass"]);
			$npass2 = test_input($_POST["npass2"]);
			$valid++;
		}

		if (!empty($_POST["npass"]) && !empty($_POST["npass2"]) && $npass != $npass2) {
			$matchErr = "Password don't match! Try again!";
			$npass = $npass2 = "";

		} else {

			$opass = test_input($_POST["opass"]);	
			$npass = test_input($_POST["npass"]);
			$npass2 = test_input($_POST["npass2"]);
			$valid++;
		}

		if ($valid >= 5) {
		      $app_state = "processed";
		   }
	}

	// Sanitize data
	function test_input($data) {

		$data = trim($data);
	   	$data = stripslashes($data);
	   	$data = htmlspecialchars($data);
	   	return $data;
	}

	if ($app_state == "empty") {

	getDatatablesLink();	
	getDatatablesScript();		
?>
		<div class="container-fluid">
			<div class="row">
			
				<?php						
					$sql_load = "SELECT a.emp_no, lastname, firstname, middlename, password b 
									FROM tbl_facultyinfo a, tbl_loginemployee b 
									WHERE a.emp_no = b.emp_no and a.emp_no = $emp_no";
									
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
					
					if (mysql_num_rows($result)>0) {
					
						while($row = mysql_fetch_row($result)) {
						
							$emp_no2 = $row[0];
							$lname = $row[1];
							$fname = $row[2];
							$mname = $row[3];
							$pass = $row[4];
						}
					}						
				?>
				
				<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><label id="header">Edit Account</label></div>
						<div class="col-md-1"></div>
				</div>
				
				<div class="col-sm-3 col-md-2"></div>
				
				<form action="<?php ($_SERVER['PHP_SELF']);?>" method="post">
					
				<div class="col-sm-3 col-md-8" style="background-color: rgb(255,255,50); border: 3px solid; border-color: gold"><br>		
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="empno2">Employee No.:</label>
								<label for="empno2"><?php echo $emp_no2; ?></label>
							</div>
						</div>	
					</div>
					
						<div class="row">						
							<div class="col-md-8">
								<div class="form-group">
									<label for="name">Name:</label>
									<label for="name"><?php echo $lname .", ". $fname ." ". $mname; ?></label>
								</div>
							</div>							
						</div><br>

						<div class="row">						
							<div class="col-md-1"></div>						
							<div class="col-md-4">
								<label for="pass">Input Password</label>	
							</div>						
							<div class="col-md-4">
								<div class="form-group">		
									<input type="password" class="form-control" id="opass" name="opass" placeholder="Old Password" maxlength="20"
										value="<?php echo $opass; ?>" oncopy="return false" onpaste="return false" onkeypress="return isPassword(event)">
										<span class="error"><?php echo $opassErr; ?></span>
								</div>
							</div>							
						</div>
						
						<div class="row">						
							<div class="col-md-1"></div>						
							<div class="col-md-4">
								<label for="npass">Input New Password</label>	
							</div>						
							<div class="col-md-4">
								<div class="form-group">		
									<input type="password" class="form-control" id="npass" name="npass" placeholder="New Password" maxlength="20"
										value="<?php echo $npass; ?>" oncopy="return false" onpaste="return false" onkeypress="return isPassword(event)">
										<span class="error"><?php echo $npassErr; ?></span>
								</div>
							</div>							
						</div>
						
						<div class="row">						
							<div class="col-md-1"></div>						
							<div class="col-md-4">
								<label for="npass2">Retype New Password</label>
							</div>						
							<div class="col-md-4">
								<div class="form-group">
									<input type="password" class="form-control" id="npass2" name="npass2" placeholder="Confirm Password" maxlength="20"
										value="<?php echo $npass2; ?>" oncopy="return false" onpaste="return false" onkeypress="return isPassword(event)">
										<span class="error"><?php echo $npass2Err; ?></span>
								</div>
							</div>							
						</div>
							
						<div class="row">						
							<div class="col-md-5"></div>							
							<div class="col-md-5">
								<button type="submit" class="btn btn-success" name="btnSubmit" onclick="return confirm('Are you sure you want to submit?');">Change Password</button>
							</div>							
						</div>

						<div class="row">						
							<div class="col-md-4"></div>						
							<div class="col-md-5">
								<span class="error"> <?php echo $matchErr; ?></span>
							</div>
						</div>

						<div class="row">						
							<div class="col-md-4"></div>			
							<div class="col-md-5">
								<span class="error"> <?php echo $matchErr2; ?></span>
							</div>
						</div><br>
					</div>
				</form>
				<div class="col-sm-3 col-md-2"></div>					
			</div>
		</div>
<?php
}
	elseif ($app_state == "processed") {
	 
		$npass = $_POST['npass'];
		$npass2 = $_POST['npass2'];
		   			
		$update = "UPDATE tbl_loginemployee set password='$npass' where emp_no = $emp_no";
		mysql_query($update) or die("error updating in sql");
				
		$app_state = "good";			
	}

	if ($app_state == "good") {    
?>	   
    	<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-5">
				<label for="lname"></label>
				<?php
					$confirm=("successfully changed the password!");
					echo "<script type='text/javascript'>alert(\"$confirm\");</script>";
				?>
				<script type="text/javascript">window.location.href="index2.php?mode=Principal&category=Account&page=0";</script>
			</div>
		</div>
	<?php
	}
