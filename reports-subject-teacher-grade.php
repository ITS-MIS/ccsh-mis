<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');
	include('current-year.php');

		 $subject = $_POST['subject'];
		 $section = $_POST['section'];
		 $emp_no = $_POST['emp_no'];
	
	$pdf = new FPDI();

	$pdf->AddPage('P','legal');
	$pageCount = $pdf->setSourceFile("forms/subject-teacher.pdf");
	$tplIdx = $pdf->importPage(1);

	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);

	$pdf->SetFont('Arial', '', '9');
	$pdf->SetTextColor(0,0,0);

	$x=20;
	$ymale=20;
	$y=62;
	$n=6;

// schoolyear-------------------------------------------------------------
	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(175, 17);
	$pdf->SetFont('Arial','', '9');
	$pdf->Write(0, $sy);

//student list--------------------------------------------------------------------
	$y=37;
	$n=5.5;

	$xarraymale=array(26,57,78,107,132,148,164,180,187);

	$pdf->SetFont('Arial', '', '9'); 
	
	$result=get_db_array("SELECT c.lrn, lastname,firstname,middlename,quarter1,quarter2,quarter3,quarter4  FROM tbl_studentstatus a left join tbl_studentgrade b on a.lrn=b.lrn 
							left join tbl_studentinfo c on a.lrn=c.lrn 
							left join tbl_studentstatus d on a.lrn=d.lrn 
							LEFT JOIN tbl_section e ON e.section_id=d.section_id 
							LEFT JOIN tbl_employee_subject f ON f.subject_code=b.subject_code
							WHERE f.emp_no=$emp_no and e.section_name ='$section' and d.sy_id=$sy_id and b.subject_code='$subject' AND c.remarks!='TO' group by b.lrn ORDER BY c.lastname, c.firstname");
			
	foreach ($result as $key => $columnname) {

			$c=0;
		foreach ($columnname as $key2 => $value) {
			
			$pdf->SetXY($xarraymale[$c], $y);
			$pdf->Write(0, $value)  ; 
	
			$c++;			
		}
		
		$y=$y+$n;
	}

	$pdf->Output();

?>