<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<style type="text/css">
	body{
		margin-right:200px;
		margin-left:200px;
	}
</style>

<div class="container-fluid">
	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">List of Section</label>
			</div>
			<div class="col-md-1"></div>
		</div>

	<!--<button data-target="#insertmodal" data-toggle="modal" type="button" class="btn btn-success success">Add Section</button>-->
	<button data-target="#generatesectionmodal" data-toggle="modal" type="button" class="btn btn-success success"> <font face="arial">Generate Sections</font></span></button>

	<div class="bs-studgrade"><!--=============START of TABS--======================-->
		    <ul class="nav nav-tabs" id="sectionTab">
		    	<li class="active"><a href="#grade7">Grade 7</a></li>
		        <li class><a href="#grade8">Grade 8</a></li>
		        <li class><a href="#grade9">Grade 9</a></li>
		        <li class><a href="#grade10">Grade 10</a></li>
		    </ul><br>

		    <div class="tab-content">
		        <div id="grade7" class="tab-pane fade in active"><!--=============START of  TAB7--======================-->
		        	<form method="post" >
						<table id="sections7" class="display" cellspacing="0" width="100%" style="background-color:gold;">
					        <thead>
					            <tr>
					                <th style="display: none;">Section ID</th>
					                <th>Section Name</th>
					                <th>Year Level</th>
					            </tr>					                
					        </thead>
					   
					        <tbody>

					        <?php  

					        	$sy = (date('Y').'-'.(date('Y')+1));
					        	
								$query = "SELECT a.section_id, section_name, c.year_level from tbl_section a 
											LEFT JOIN tbl_section_yearlevel b ON a.section_id=b.section_id 
											LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id
											LEFT JOIN tbl_sy d ON b.sy_id=d.sy_id
											WHERE c.year_id=7 AND d.sy = '$sy'";

								$result = mysql_query($query) or die(mysql_error());


								if(mysql_num_rows($result) > 0) {
																
									while ($row = mysql_fetch_assoc($result)) {

										$section_id=$row['section_id'];
										$section_name=$row['section_name'];
										$year_level = $row['year_level'];
								
							?>
							            <tr  data-toggle="modal" data-id="<?php echo $section_id ?>" data-name="<?php echo $section_name ?>" 
							            		data-year="<?php echo $year_level ?>" data-target="#sectionmodal" class="data">
							                
							                <td style="display: none;"></td>
							                <td><?php echo $section_name ?></td>
							               	<td><?php echo $year_level ?></td>
							            </tr>
							<?php
						            }
						        }
						    ?>
							</tbody>
						</table>
					</form>
		        </div><!--===============================================END of TAB7=========================-->
		       
		        <div id="grade8" class="tab-pane fade in"><!--=============START of  TAB8--======================-->
		        	<form method="post" >
						<table id="sections8" class="display" cellspacing="0" width="100%" style="background-color:gold;">					     
					        <thead>
					            <tr>
					                <th style="display: none;">Section ID</th>
					                <th>Section Name</th>
					                <th>Year Level</th>
					            </tr>					                
					        </thead>					
					   
					        <tbody>
					        <?php  

					        	$sy = (date('Y').'-'.(date('Y')+1));					        	

								$query = "SELECT a.section_id, section_name, c.year_level from tbl_section a 
											LEFT JOIN tbl_section_yearlevel b ON a.section_id=b.section_id 
											LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id
											LEFT JOIN tbl_sy d ON b.sy_id=d.sy_id
											WHERE c.year_id=8 AND d.sy = '$sy'";

								$result = mysql_query($query) or die(mysql_error());


								if(mysql_num_rows($result) > 0) {
																
									while ($row = mysql_fetch_assoc($result)) {

										$section_id=$row['section_id'];
										$section_name=$row['section_name'];
										$year_level = $row['year_level'];								
							?>

							            <tr  data-toggle="modal" data-id="<?php echo $section_id ?>" data-name="<?php echo $section_name ?>" 
							            		data-year="<?php echo $year_level ?>" data-target="#sectionmodal" class="data">
							                
							                <td style="display: none;"></td>
							                <td><?php echo $section_name ?></td>
							               	<td><?php echo $year_level ?></td>
							            </tr>
							<?php
						            }
						        }
						    ?>
							</tbody>
						</table>
					</form>
		        </div><!--===============================================END of TAB8=========================-->

		        <div id="grade9" class="tab-pane fade in"><!--=============START of  TAB9--======================-->
					<form method="post" >
						<table id="sections9" class="display" cellspacing="0" width="100%" style="background-color:gold;">					     
					        <thead>
					            <tr>
					                <th style="display: none;">Section ID</th>
					                <th>Section Name</th>
					                <th>Year Level</th>
					            </tr>
					        </thead>			
					   
					        <tbody>
					        <?php  
					        	$sy = (date('Y').'-'.(date('Y')+1));					       

								$query = "SELECT a.section_id, section_name, c.year_level from tbl_section a 
											LEFT JOIN tbl_section_yearlevel b ON a.section_id=b.section_id 
											LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id
											LEFT JOIN tbl_sy d ON b.sy_id=d.sy_id
											WHERE c.year_id=9 AND d.sy = '$sy'";

								$result = mysql_query($query) or die(mysql_error());

								if(mysql_num_rows($result) > 0) {
																
									while ($row = mysql_fetch_assoc($result)) {
										$section_id=$row['section_id'];
										$section_name=$row['section_name'];
										$year_level = $row['year_level'];
							?>
							            <tr  data-toggle="modal" data-id="<?php echo $section_id ?>" data-name="<?php echo $section_name ?>" 
							            		data-year="<?php echo $year_level ?>" data-target="#sectionmodal" class="data">
							                
							                <td style="display: none;"></td>
							                <td><?php echo $section_name ?></td>
							               	<td><?php echo $year_level ?></td>
							            </tr>
							<?php
						            }
						        }
						    ?>
							</tbody>
						</table>
					</form>		        	
		        </div><!--===============================================END of TAB9=========================-->

		        <div id="grade10" class="tab-pane fade in"><!--=============START of  TAB10--======================-->
		        	<form method="post" >
						<table id="sections10" class="display" cellspacing="0" width="100%" style="background-color:gold;">					     
					        <thead>
					            <tr>
					                <th style="display: none;">Section ID</th>
					                <th>Section Name</th>
					                <th>Year Level</th>
					            </tr>					                
					        </thead>					 
					   
					        <tbody>
					        <?php  
					        	$sy = (date('Y').'-'.(date('Y')+1));					        	

								$query = "SELECT a.section_id, section_name, c.year_level from tbl_section a 
											LEFT JOIN tbl_section_yearlevel b ON a.section_id=b.section_id 
											LEFT JOIN tbl_yearlevel c ON b.year_id = c.year_id
											LEFT JOIN tbl_sy d ON b.sy_id=d.sy_id
											WHERE c.year_id=10 AND d.sy = '$sy'";

								$result = mysql_query($query) or die(mysql_error());


								if(mysql_num_rows($result) > 0) {																
									while ($row = mysql_fetch_assoc($result)) {
										$section_id=$row['section_id'];
										$section_name=$row['section_name'];
										$year_level = $row['year_level'];								
							?>
							            <tr  data-toggle="modal" data-id="<?php echo $section_id ?>" data-name="<?php echo $section_name ?>" 
							            		data-year="<?php echo $year_level ?>" data-target="#sectionmodal" class="data">
							                
							                <td style="display: none;"></td>
							                <td><?php echo $section_name ?></td>
							               	<td><?php echo $year_level ?></td>
							            </tr>
							<?php
						            }
						        }
						    ?>
							</tbody>
						</table>
					</form>
		        </div><!--===============================================END of TAB10=========================-->
		    </div>
		</div><!--=============END of TABS--======================-->

</div>

<div id="sectionmodal" class="modal fade">
	<div id="section-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Section Information</h4>
	            </div>	            

	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="sections-submit.php"><br><br>
						<div class="form-group">
							<label for="usernamer" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="section_id" placeholder="Section ID" name="section_id" readonly>
							</div>
					  	</div>
					  
						<div class="form-group">
							<label for="inputpass" class="col-sm-4 control-label">Name</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="section_name" placeholder="Section Name" name="section_name" maxlength="20" onkeypress="return isLetter(event)" onkeyup="caps(this)" readonly>
							</div>
						</div>

						<div class="form-group">
							<label for="yearlevel" class="col-sm-4 control-label">Year Level</label>
							<div class="col-sm-7">
						
								<select style="width: 320px;" class="form-control" id="year_level" placeholder="Year Level" name="year_level" >
								
										<?php
											$yl = "SELECT year_level FROM tbl_yearlevel";
											$result = mysql_query($yl);
										?>										
									
									<?php
										while($row = mysql_fetch_array($result)) {
									?> 
											<option><?php echo $row['year_level']; ?> </option>
									<?php		
										}
									?>
									
								</select>
							</div>
						</div><br>
					
						<div class="form-group">

							<div class="col-sm-4"></div>
							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#update" data-toggle="modal" type="button" class="btn btn-success success"  >Update</button>				
								<!--<button  data-target="#delete" data-toggle="modal" type="button" class="btn btn-danger warning confirm-delete-modal" name="btnDelete">Delete</button>-->
							</div>
							
							<div class="col-sm-4"></div>
							
						</div>

						<div class="form-group">															
						
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
							
						</div>
						
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================ Add Record Modal =================================================================-->
<div id="insertmodal" class="modal fade">
	<div id="section-content">
		<div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Add Section Record</h4>
	            </div>
	            
	            <div class="modal-body  modal-height"> 
					<form class="form-horizontal" method="post" action="sections-submit.php"><br><br>
					  
						<div class="form-group">
							<label for="inputpass" class="col-sm-4 control-label">Name</label>
							<div class="col-sm-7">
								<input type="text"  class="form-control" id="asection_name" placeholder="Section Name" name="section_name" maxlength="20" onkeypress="return isLetter(event)" onkeyup="caps(this)">
							</div>
						</div>

						<div class="form-group">
							<label for="yearlevel" class="col-sm-4 control-label">Year Level</label>
							<div class="col-sm-7">
						
								<select style="width: 320px;" class="form-control" id="ayear_level" placeholder="Year Level" name="year_level" >
								
										<?php
											$yl = "SELECT year_level FROM tbl_yearlevel";
											$result = mysql_query($yl);
										?>									
									
									<?php
										while($row = mysql_fetch_array($result)) {
									?> 
											<option><?php echo $row['year_level']; ?> </option>
									<?php		
										}
									?>
									
								</select>
							</div>
						</div><br>
						
						<div class="form-group">
							<div class="col-sm-4"></div>							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#insert" data-toggle="modal" type="button" class="btn btn-success success">Add</button>
								<button type="reset" class="btn btn-default">Reset</button>									
							</div>							
							<div class="col-sm-4"></div>							
						</div>

						<div class="form-group">																					
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">							
						</div>						
					</form>
				</div>

			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			     
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<!--================ Insert Confirm Modal =================================================================-->
<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Record</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>          
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="sections-submit.php">
            		<input type="hidden" id="insertname"name="section_name">
            		<input type="hidden" id="insertyear" name="year_level"  >		 					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnInsert">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to add the record?</p>          
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="sections-submit.php">
            		<input type="hidden" id="updateid" name="section_id">
            		<input type="hidden" id="updatename"name="section_name">
            		<input type="hidden" id="updateyear" name="year_level"  >		 					
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnUpdate">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<!--================ Delete Confirm Modal =================================================================-->
<div id="delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Do you want to delete the record?</p>
                <p class="text-warning"><small>If you proceed, deletion will be immediate!</small></p>
            </div>

            <div class="modal-footer">      
            	<form method="post" action="sections-submit.php">
		            <input type="hidden" id="deleteid" name="section_id">
		            <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-danger warning" name="btnDelete">Confirm</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!--================ generateSectionmodal Confirm Modal =================================================================-->
<div id="generatesectionmodal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Generate Sections</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to generate a new list of sections for new SY?</p>               
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="generate-section-yearlevel.php">
            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			<button type="button" class="btn btn-default modal-insert-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-insert" name="btnGenerateSection">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#sections7').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#sections7 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#sections8').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#sections8 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }
	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#sections9').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#sections9 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }
	       
    	} );
	} );

	$(document).ready(function() {
		var table=$('#sections10').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    } );

    	$('#sections10 tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }
	       
    	} );
	} );

	 $(function(){
	    $('#sectionmodal').modal({
	        keyboard: true,
	        show:false,


	    }).on('show.bs.modal', function(){
	        var getIdFromRow = $(event.target).closest('tr').data('id');   	
	         	$('#section_id').val(getIdFromRow);
	        
	        var getnameFromRow = $(event.target).closest('tr').data('name'); 
	        	$('#section_name').val(getnameFromRow);

	        var getyearFromRow = $(event.target).closest('tr').data('year'); 
	        	$('#year_level').val(getyearFromRow);
	        
	    });
	});

	$(function(){
    	$('#insert').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getnameFromRow =$('#asection_name').val();;
				$('#insertname').val(getnameFromRow);
	 		
	 		var getyearFromRow =$('#ayear_level').val();;
				$('#insertyear').val(getyearFromRow);

		});
	});

  	$(function(){
    	$('#delete').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){
         	var getIdFromRow = $('#section_id').val();
 				$('#deleteid').val(getIdFromRow); 			
	        	console.info(getIdFromRow);
		});
	});
  	
  	$(function(){
    	$('#update').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getIdFromRow =$('#section_id').val();;
				$('#updateid').val(getIdFromRow);

			var getnameFromRow =$('#section_name').val();;
				$('#updatename').val(getnameFromRow);
	 		
	 		var getyearFromRow =$('#year_level').val();;
				$('#updateyear').val(getyearFromRow);
	 			
		        console.info(getIdFromRow);
		});
	});

  	$(document).ready(function(){ 
    	$("#sectionTab a").click(function(e){
    		e.preventDefault();
    		$(this).tab('show');
    	});
	});

</script>