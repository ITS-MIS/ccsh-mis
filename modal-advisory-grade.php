<head>
	<style type="text/css">
		a.accordion-toggle {
		  display: block;
		  padding: 5px  5px;
		  background-color: #ffe34c;
		  text-decoration: none;
		  //text-align: center;
		}

		div.panel-heading {
		  padding: 0;
		}
	</style>
</head>

<?php
	include'connect.php';
	include('current-year.php');

		$lrn=$_POST['lrn'];

		$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
							FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d
							WHERE b.year_id = c.year_id AND b.section_id = d.section_id AND a.lrn = b.lrn AND a.lrn=$lrn AND b.sy_id=$sy_id";
										
		$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
		
		if (mysql_num_rows($result)>0) {		
			while($row = mysql_fetch_row($result)) {			
				$lrn = $row[0];
				$lastname = $row[1];
				$firstname = $row[2];
				$middlename = $row[3];
				$year = $row[4];
				$section = $row[5];
			}
		}
?>

<div class="modal-content">
    <div class="modal-header" style="background-color:gold;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Student's Academic Record</h4>
    </div>
    
    <div class="modal-body modal-height"> 

    	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-7">
				<div class="form-group">
					<label for="lrn">LRN: <?php echo $lrn; ?></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="text-align: right;">
					<label for="year">Year Level: <?php echo $year; ?></label>
				</div>
			</div>
			<div class="col-md-1"></div>							
		</div>
		
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-7">
				<div class="form-group">
					<label for="name">Student's Name: <?php echo $lastname .', '. $firstname .' '. $middlename ?></label>		
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="text-align: right;">
					<label for="section">Section: <?php echo $section; ?></label>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div><br>

	    <div class="bs-accord"><!--===================START of Accordion=========================-->
		    <div class="panel-group" id="accordion">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="accordion-toggle" id="change">
		                    	&nbsp;&nbsp;Input Core Values <span class="caret" id="caretpos"></span>
		                    </a>
		                </h4>
		            </div>
		            <div id="collapseOne" class="panel-collapse collapse">
		                <div class="panel-body">
		                    <div class="row">
								<div class="col-md-1"></div>

								<div class="col-md-10">
									<table id="values" class="display" cellspacing="0" width="100%" style="background-color:gold;">
								        <thead>
								            <tr>
								            	<th style="text-align:center">Quarter</th>
								            	<th style="text-align:center">Maka Diyos</th>
								                <th style="text-align:center">Makatao</th>
								                <th style="text-align:center">Makakalikasan</th>
								                <th style="text-align:center">Makabansa</th>
								                <th></th>
								            </tr>								                
								        </thead>

								        <tbody>       
									            <tr>
									            	<form role="form" method="post" action="core-values-submit.php">
									            	<td width="10%">1st</td>
									                <td width="20%"><select class="form-control" id="uv11" name="v11">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
									                <td width="20%"><select class="form-control" id="uv21" name="v21">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv31" name="v31">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv41" name="v41">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<div class="form-group">															
														<input type="hidden" id="ulrn1" name="lrn2" value="<?php echo $lrn; ?>">					
													</div>
													<td width="10%"><button data-target="#update1" data-toggle="modal" type="button" class="btn btn-success success">Submit</button></td>
													</form>		                
									            </tr>

									            <tr>
									            	<form role="form" method="post" action="core-values-submit.php">
									            	<td width="10%">2nd</td>
									                <td width="20%"><select class="form-control" id="uv12" name="v12">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
									                <td width="20%"><select class="form-control" id="uv22" name="v22">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv32" name="v32">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv42" name="v42">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<div class="form-group">															
														<input type="hidden" id="ulrn2" name="lrn3" value="<?php echo $lrn; ?>">					
													</div>
													<td width="10%"><button data-target="#update2" data-toggle="modal" type="button" class="btn btn-success success">Submit</button></td>
													</form>		                
									            </tr>

									            <tr>
									            	<form role="form" method="post" action="core-values-submit.php">
									            	<td width="10%">3rd</td>
									                <td width="20%"><select class="form-control" id="uv13" name="v13">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
									                <td width="20%"><select class="form-control" id="uv23" name="v23">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv33" name="v33">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv43" name="v43">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<div class="form-group">															
														<input type="hidden" id="ulrn3" name="lrn4" value="<?php echo $lrn; ?>">					
													</div>
													<td width="10%"><button data-target="#update3" data-toggle="modal" type="button" class="btn btn-success success">Submit</button></td>
													</form>		                
									            </tr>

									            <tr>
									            	<form role="form" method="post" action="core-values-submit.php">
									            	<td width="10%">4th</td>
									                <td width="20%"><select class="form-control" id="uv14" name="v14">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
									                <td width="20%"><select class="form-control" id="uv24" name="v24">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv34" name="v34">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<td width="20%"><select class="form-control" id="uv44" name="v44">
															<option>AO</option>
															<option>SO</option>
															<option>RO</option>
															<option>NO</option>					
														</select>
													</td>
													<div class="form-group">															
														<input type="hidden" id="ulrn4" name="lrn5" value="<?php echo $lrn; ?>">					
													</div>
													<td width="10%"><button data-target="#update4" data-toggle="modal" type="button" class="btn btn-success success">Submit</button></td>
													</form>		                
									            </tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-1"></div>
							</div>
							
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4"> <br>   
								  <table class="table table-condensed">
									<thead align="center">
									  <tr>
										<th>Marking</th>
										<th>Non Numerical Rating</th>
									  </tr>
									</thead>
									<tbody>
									  <tr>
										<td>AO	</td>
										<td>Always Observed</td>
									  </tr>
									  <tr>
										<td>SO</td>
										<td>Sometimes Observed</td>
									  </tr>
									  <tr>
										<td>RO</td>
										<td>Rarely Observed</td>
									  </tr>
									  <tr>
									  	<td>NO</td>
										<td>Not Observed</td>
									  </tr>
									</tbody>
								  </table>
								</div>
								<div class="col-md-4"></div>								
		                </div>
		            </div>
		        </div>
		    </div>
		</div> <!--===================END of Accordion=========================--><br>

		<div class="row">
			<div class="col-md-1"></div>			
			<div class="col-sm-3 col-md-10">		
				<table id="grade-spec" data-page-length="10" class="display" cellspacing="0" width="100%" style="background-color:gold;">
					<thead>
						<tr>
							<th>Subject</th>
							<th>1st Quarter</th>
							<th>2nd Quarter</th>
							<th>3rd Quarter</th>
							<th>4th Quarter</th>
						</tr>
					</thead>
					
					<tbody>					
						<?php
							include('current-year.php');
							
							$sql_load = "SELECT subject_title, quarter1, quarter2, quarter3, quarter4 FROM tbl_studentgrade a, tbl_subject b 
											WHERE a.subject_code = b.subject_code AND lrn = $lrn AND a.sy_id = $sy_id ORDER BY b.subject_title asc";
											
							$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
							
							if (mysql_num_rows($result)>0) {
								while($row = mysql_fetch_row($result)) {								
									$subject_code = $row[0];
									$quarter1 = $row[1];
									$quarter2 = $row[2];
									$quarter3 = $row[3];
									$quarter4 = $row[4];								
						?>	
								<tr>
									<td><?php echo $subject_code;  ?></td>
									<td align="center"><?php echo $quarter1; ?></td>
									<td align="center"><?php echo $quarter2; ?></td>
									<td align="center"><?php echo $quarter3; ?></td>
									<td align="center"><?php echo $quarter4; ?></td>
								</tr>					
						<?php
								}
							}						
						?>
					</tbody>
				</table><br>	
			</div>			
			<div class="col-md-1"></div>
		</div><br>
    </div>

    <div class="modal-footer" style="background-color:gold;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>

<!--================ Update Confirm Modal 1 =================================================================-->
<div id="update1" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Core Values Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record in 1st Quarter?</p>          
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="core-values-submit.php">
            		<input type="hidden" id="updatelrn1" name="ulrn1">
            		<input type="hidden" id="updatev11" name="v11">
            		<input type="hidden" id="updatev21" name="v21">
            		<input type="hidden" id="updatev31" name="v31">
            		<input type="hidden" id="updatev41" name="v41">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnSubmit1">Confirm</button>                
             	</form>
            </div>
        </div>
    </div>
</div>

<!--================ Update Confirm Modal 2 =================================================================-->
<div id="update2" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Core Values Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record in 2nd Quarter?</p>
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="core-values-submit.php">
            		<input type="hidden" id="updatelrn2" name="ulrn2">
            		<input type="hidden" id="updatev12" name="v12">
            		<input type="hidden" id="updatev22" name="v22">
            		<input type="hidden" id="updatev32" name="v32">
            		<input type="hidden" id="updatev42" name="v42">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnSubmit2">Confirm</button>   
             	</form>
            </div>
        </div>
    </div>
</div>

<!--================ Update Confirm Modal 3 =================================================================-->
<div id="update3" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Core Values Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record in 3rd Quarter?</p>          
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="core-values-submit.php">
            		<input type="hidden" id="updatelrn3" name="ulrn3">
            		<input type="hidden" id="updatev13" name="v13">
            		<input type="hidden" id="updatev23" name="v23">
            		<input type="hidden" id="updatev33" name="v33">
            		<input type="hidden" id="updatev43" name="v43">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnSubmit3">Confirm</button>     
             	</form>
            </div>
        </div>
    </div>
</div>

<!--================ Update Confirm Modal 3 =================================================================-->
<div id="update4" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Core Values Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record in 4th Quarter?</p>
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="core-values-submit.php">
            		<input type="hidden" id="updatelrn4" name="ulrn4">
            		<input type="hidden" id="updatev14" name="v14">
            		<input type="hidden" id="updatev24" name="v24">
            		<input type="hidden" id="updatev34" name="v34">
            		<input type="hidden" id="updatev44" name="v44">
		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnSubmit4">Confirm</button>
             	</form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
		$('#grade-spec').dataTable({
			bInfo: false,
			bFilter: false,
			paging: false,
			"ordering": false
		});
	} );

	$(document).ready(function() {
		$('#values').dataTable({
			bInfo: false,
			bFilter: false,
			paging: false,
			"ordering": false
		});
	} );

	$(function(){
    	$('#update1').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getlrnFromRow =$('#ulrn1').val();;
				$('#updatelrn1').val(getlrnFromRow);

			var getv1FromRow =$('#uv11').val();;
				$('#updatev11').val(getv1FromRow);

			var getv2FromRow =$('#uv21').val();;
				$('#updatev21').val(getv2FromRow);

			var getv3FromRow =$('#uv31').val();;
				$('#updatev31').val(getv3FromRow);

			var getv4FromRow =$('#uv41').val();;
				$('#updatev41').val(getv4FromRow);
	        
		});
	});

	$(function(){
    	$('#update2').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getlrnFromRow =$('#ulrn2').val();;
				$('#updatelrn2').val(getlrnFromRow);

			var getv1FromRow =$('#uv12').val();;
				$('#updatev12').val(getv1FromRow);

			var getv2FromRow =$('#uv22').val();;
				$('#updatev22').val(getv2FromRow);

			var getv3FromRow =$('#uv32').val();;
				$('#updatev32').val(getv3FromRow);

			var getv4FromRow =$('#uv42').val();;
				$('#updatev42').val(getv4FromRow);
	        
		});
	});

	$(function(){
    	$('#update3').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getlrnFromRow =$('#ulrn3').val();;
				$('#updatelrn3').val(getlrnFromRow);

			var getv1FromRow =$('#uv13').val();;
				$('#updatev13').val(getv1FromRow);

			var getv2FromRow =$('#uv23').val();;
				$('#updatev23').val(getv2FromRow);

			var getv3FromRow =$('#uv33').val();;
				$('#updatev33').val(getv3FromRow);

			var getv4FromRow =$('#uv43').val();;
				$('#updatev43').val(getv4FromRow);
	        
		});
	});

	$(function(){
    	$('#update4').modal({
	        keyboard: true,
	        show:false,

    	}).on('show.bs.modal', function(){

	        var getlrnFromRow =$('#ulrn4').val();;
				$('#updatelrn4').val(getlrnFromRow);

			var getv1FromRow =$('#uv14').val();;
				$('#updatev14').val(getv1FromRow);

			var getv2FromRow =$('#uv24').val();;
				$('#updatev24').val(getv2FromRow);

			var getv3FromRow =$('#uv34').val();;
				$('#updatev34').val(getv3FromRow);

			var getv4FromRow =$('#uv44').val();;
				$('#updatev44').val(getv4FromRow);
	        
		});
	});

 $("#change").click(function(){

		$('#grade-spec').show();
		 $("#change").show();
		 $('.modal-backdrop.fade.in').css('height','1200px')
	 
	});

</script>