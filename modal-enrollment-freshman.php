<style type="text/css"> .error {color: #FF0000;} </style>

<?php
	include 'connect.php';
	getValidationScipt();

	$id=$_POST['id'];

	$result=get_db_array2("SELECT * From tbl_applicantinfo where id=$id");
	$lrn=$result[0][1];
	$studentfields=array("ID","LRN", "Lastname","Firstname", "Middlename","Gender","Birth Date","Age","Mother-tongue","IP","Religion","Address no. & street","Brg.","City","Province", "Father: Lastname", "Firstname", "Middlename", "Mother: Lastname", "Firstname","Middlename", "Guardian name", "Relationship","Contact-number","GWA","School","Guardian E-Mail Address");
	$dbfields=array(      "id","lrn","lastname","firstname","middlename","gender","birthdate","age","mothertounge",     "ip","religion","addnost",             "brgy","city","province","flastname","ffirstname","fmiddlename","mlastname","mfirstname","mmiddlename","guardianname","relationship","contactnumber","gwa","school","guardianmail");
?>

    <div class="modal-content">
        <div class="modal-header" style="background-color:gold;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Student's Information</h4>
        </div>

        <div class="modal-body" > 
			<form class="form-horizontal" method="post" action="enrollment-freshman-submit.php"><br><br>

				<?php 
				$c=0;
				foreach ($studentfields as $key => $fieldlabel) {
			
					if($key<=14 || $key==23 || $key==2 || $key==25){
						?>
						<div class="form-group" >
							<div class="col-md-1">
							</div>
							
							<div class="col-md-4">
								<label ><?php echo $fieldlabel;?></label>
							</div>

							<div  class="col-md-3">
								<?php
							 	if($fieldlabel!='GWA'){
							 		?>
							 		<label  style="margin-top:5px;" class><?php echo $result[0][$c];?></label>
							 		<input type="hidden" name="info[<?php echo $dbfields[$key] ?>]" value="<?php echo $result[0][$c];?>" >
							 		
							 		<?php
							 		$c++;
							 	}			 
							 	else{
							 		?>
							 		<input type="text" class="form-control" name="info[<?php echo $dbfields[$key] ?>]" style="width:200px" value="<?php echo $result[0][$c];?>" required>							 		
							 		<?php
							 		$c++;
							 	}							 								 	
							 	?>
							</div>
						</div>
						<?php
					}
					elseif($fieldlabel=='Guardian name'){
						?>
						<div class="form-group" >
							<div class="col-md-1">
							</div>
							
							<div class="col-md-4">
								<label ><?php echo $fieldlabel;?></label>&nbsp;<span class="error">*</span>
							</div>

							<div  class="col-md-3">

								<input type="text" style="width:200px" class="form-control" name="info[<?php echo $dbfields[$key] ?>]" placeholder="Guardian Name" maxlength="30" onkeypress="return isLetterDashPoint(event)" required>
							
							</div>
						</div>
						<?php
					}
					elseif($fieldlabel=='Relationship'){
						?>
						<div class="form-group" >
							<div class="col-md-1">
							</div>
							
							<div class="col-md-4">
								<label ><?php echo $fieldlabel;?></label>&nbsp;<span class="error">*</span>
							</div>

							<div  class="col-md-3">
								<select style="width:200px" class="form-control" name="info[<?php echo $dbfields[$key] ?>]" required>
									<option></option>
									<option>mother</option>
									<option>father</option>
									<option>aunt</option>						
									<option>uncle</option>
									<option>brother</option>
									<option>sister</option>
									<option>grandmother</option>
									<option>grandfather</option>
									<option>godmother</option>
									<option>godfather</option>
									<option>Other</option>
								</select>
							</div>
						</div>
						<?php
					}
					elseif($fieldlabel=='GWA'){
						?>
						<div class="form-group" >
							<div class="col-md-1">
							</div>
							
							<div class="col-md-4">
								<label ><?php echo $fieldlabel;?></label>&nbsp;<span class="error">*</span>
							</div>

							<div  class="col-md-3">

								<input type="text" style="width:200px" class="form-control" name="info[<?php echo $dbfields[$key] ?>]" placeholder="GWA" onkeypress="return isFloat(event)" maxlength="5" required>
							
							</div>
						</div>
						<?php
					}
					elseif($fieldlabel=='Guardian E-Mail Address'){
						?>
						<div class="form-group" >
							<div class="col-md-1">
							</div>
							
							<div class="col-md-4">
								<label ><?php echo $fieldlabel;?></label>&nbsp;<span class="error">*</span>
							</div>

							<div  class="col-md-3">

								<input type="email" style="width:200px" class="form-control" name="info[<?php echo $dbfields[$key] ?>]" placeholder="Guardian E-mail Address" maxlength="50">
							
							</div>
						</div>
						<?php
					}
					else{
						?>
						<div class="form-group" >
							<div class="col-md-1">
							</div>
							
							<div class="col-md-4">
								<label ><?php echo $fieldlabel;?></label>
							</div>

							<div  class="col-md-3">

								<input type="text" style="width:200px" class="form-control" name="info[<?php echo $dbfields[$key] ?>]" onkeypress="return isLetterDashPoint(event)" maxlength="30">
							
							</div>
						</div>
						<?php
					}
				}
				?>
		
				<div class="col-md-6 ">		
						<label class="heading">Enrollment Requirements:</label><br/>
						<label class="checkbox-inline"><input type="checkbox" id="nso" name="nso"  >NSO(Birth Certificate)</label><br/>
						<label class="checkbox-inline"><input type="checkbox" id="f138" name="f138" >Form 138</label><br/>
						<label class="checkbox-inline"><input type="checkbox" id="idpic"name="idpic" >Id Picture</label><br/>
						<label class="checkbox-inline"><input type="checkbox" id="medc" name="medc" >Medical Certificate</label><br/><br/>
						
						<label class="heading">Requirements:</label><br/>
						<label class="checkbox-inline"><input type="checkbox" id="f137" name="f137" >Form 137</label><br/>
						<label class="checkbox-inline"><input type="checkbox" id="cgmc" name="cgmc" >Certificate of Good Moral</label><br/>									
				</div>

				<div class="form-group">
					<input type="hidden" id="lrn2" name="lrn" value="<?php echo $lrn ?>">
				</div><br>
				
				<div class="form-group text-center">
					<div class="col-sm-4"></div>					
					<div class="col-sm-4">
						<button id="enroll" type="submit" style="display:none" class="btn btn-success success" name="btnEnroll" onclick="return confirm('Are you sure you want to submit?');">Enroll</button>
					</div>					
					<div class="col-sm-4"></div>					
				</div>				
			</form>
		</div>

	    <div class="modal-footer" style="background-color:gold;">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>	      
        </div>
	</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		$("input[type=checkbox]").on("click",function(){

			if($('#nso').is(":checked") && $('#f138').is(":checked") && $('#idpic').is(":checked") && $('#medc').is(":checked")){		
				$("#enroll").show();
			}else{
				
				$("#enroll").hide();

			}
			
		});
		$("#enroll").on("click",function(){
			var lrn =$("#lrn").val();
			$("#lrn2").val(lrn);

		});

	});

</script>