<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
	
	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">List of Student</label>
			</div>
			<div class="col-md-1"></div>
		</div>	

	<button data-target="#insertmodal" data-toggle="modal" type="button" class="btn btn-success success">Add Student</button>

	<div class="row"><br>
		<form method="post">
			<table id="student" class="display" cellspacing="0" width="100%" style="background-color:gold;">		     
		        <thead>
		            <tr>
		            	<th style="display: none;">ID</th>
		                <th>LRN</th>
		                <th>Last Name</th>
		                <th>First Name</th>
		                <th>Middle Name</th>
		                <th>Gender</th>
		                <th style="display: none;">Birthdate</th>
		                <th style="display: none;">Age</th>
		                <th style="display: none;">Mother Tongue</th>
		                <th style="display: none;">Ethnic Group</th>
		                <th style="display: none;">Religion</th>
		                <th style="display: none;">House No. / Street</th>
		                <th style="display: none;">Barangay</th>
		                <th style="display: none;">City</th>
		                <th style="display: none;">Province</th>
		                <th style="display: none;">Father Lastname</th>
		                <th style="display: none;">Father Firstname</th>
		                <th style="display: none;">Father Middlename</th>
		                <th style="display: none;">Mother Lastname</th>
		                <th style="display: none;">Mother Firstname</th>
		                <th style="display: none;">Mother Middlename</th>
		                <th style="display: none;">Guardian Name</th>
		                <th style="display: none;">Relationship</th>
		                <th style="display: none;">Contact Number</th>
		                <th style="display: none;">Elementary GWA</th>
		                <th style="display: none;">Elementary School</th>
		                <th style="display: none;">Remarks</th>  
		                <th style="display: none;">Password</th> 
		            </tr>		                
		        </thead>
		 
		        <tbody>

			        <?php  

						$query = "SELECT a.*, password FROM tbl_studentinfo a 
									LEFT JOIN tbl_loginstudent b ON a.lrn = b.lrn ORDER BY lastname, firstname, middlename";

						$result = mysql_query($query) or die(mysql_error());


						if(mysql_num_rows($result) > 0) {
														
							while ($row = mysql_fetch_assoc($result)) {

								$id = $row['id'];
								$lrn= $row['lrn'];
								$lastname = $row['lastname'];
								$firstname = $row['firstname'];
								$middlename = $row['middlename'];
								$gender = $row['gender'];	
								$birthdate = $row['birthdate'];	
								$age = $row['age'];
								//$birthplace = $row['birthplace'];
								$mothertounge = $row['mothertounge'];	
								$ip = $row['ip'];	
								$religion = $row['religion'];	
								$addnost = $row['addnost'];
								$brgy = $row['brgy'];	
								$city = $row['city'];	
								$province = $row['province'];	
								$flastname = $row['flastname'];	
								$ffirstname = $row['ffirstname'];
								$fmiddlename = $row['fmiddlename'];	
								$mlastname = $row['mlastname'];	
								$mfirstname = $row['mfirstname'];	
								$mmiddlename = $row['mmiddlename'];	
								$guardianname = $row['guardianname'];
								$relationship = $row['relationship'];	
								$contactnumber = $row['contactnumber'];
								$gwa = $row['gwa'];
								$school =$row['school'];
								$remarks = $row['remarks'];
								$password = $row['password'];
						
								
					?>

		            <tr data-toggle="modal" data-id="<?php echo $id ?>" data-lrn="<?php echo $lrn ?>"
						data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
						data-middlename="<?php echo $middlename ?>" data-gender="<?php echo $gender ?>"
						data-birthdate="<?php echo $birthdate ?>" data-age="<?php echo $age ?>"
						data-mothertounge="<?php echo $mothertounge ?>"
						data-ip="<?php echo $ip ?>" data-religion="<?php echo $religion ?>"
						data-addnost="<?php echo $addnost ?>" data-brgy="<?php echo $brgy ?>"
						data-city="<?php echo $city ?>" data-province="<?php echo $province ?>"
						data-flastname="<?php echo $flastname ?>" data-ffirstname="<?php echo $ffirstname ?>"
						data-fmiddlename="<?php echo $fmiddlename ?>" data-mlastname="<?php echo $mlastname ?>"
						data-mfirstname="<?php echo $mfirstname ?>" data-mmiddlename="<?php echo $mmiddlename ?>"
						data-guardianname="<?php echo $guardianname ?>" data-relationship="<?php echo $relationship ?>"
						data-contactnumber="<?php echo $contactnumber ?>" data-gwa="<?php echo $gwa ?>"
						data-school="<?php echo $school ?>" data-remarks="<?php echo $remarks ?>"
						data-password="<?php echo $password ?>" data-target="#studentmodal" class="data">
		                
		                <td style="display: none;"></td>
		                <td><?php echo $lrn; ?></td>
		                <td><?php echo $lastname; ?></td>
		                <td><?php echo $firstname; ?></td>
		                <td><?php echo $middlename; ?></td>
		                <td><?php echo $gender; ?></td>
						<td style="display: none;"><?php echo $birthdate; ?></td>
		                <td style="display: none;"><?php echo $age; ?></td>
		                <td style="display: none;"><?php echo $mothertounge; ?></td>
		                <td style="display: none;"><?php echo $ip; ?></td>
		                <td style="display: none;"><?php echo $religion; ?></td>
		                <td style="display: none;"><?php echo $addnost; ?></td>
		                <td style="display: none;"><?php echo $brgy; ?></td>
		                <td style="display: none;"><?php echo $city; ?></td>
		                <td style="display: none;"><?php echo $province; ?></td>
		                <td style="display: none;"><?php echo $flastname; ?></td>
		                <td style="display: none;"><?php echo $ffirstname; ?></td>
		                <td style="display: none;"><?php echo $fmiddlename; ?></td>
		                <td style="display: none;"><?php echo $mlastname; ?></td>
		                <td style="display: none;"><?php echo $mfirstname; ?></td>
		                <td style="display: none;"><?php echo $mmiddlename; ?></td>
		                <td style="display: none;"><?php echo $guardianname; ?></td>
		                <td style="display: none;"><?php echo $relationship; ?></td>
		                <td style="display: none;"><?php echo $contactnumber; ?></td>
		                <td style="display: none;"><?php echo $gwa; ?></td>
		                <td style="display: none;"><?php echo $school; ?></td>
		                <td style="display: none;"><?php echo $remarks; ?></td>
		                <td style="display: none;"><?php echo $password; ?></td>
		            </tr>
				
					<?php
				            }
				        }
				    ?>

				</tbody>
			</table>
		</form>
	</div>
</div>

<div id="studentmodal" class="modal fade">
	<div id="student-content">
		<div class="modal-dialog modal-lg">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color:gold;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Student's Information</h4>
	            </div>

	            <div class="modal-body" style="padding-left:50px; padding-right:50px;"> 
					<form class="form-horizontal" method="post" action="students-submit.php">
						
						<br><br>

						<div class="form-group">
							<label for="code" class="col-sm-4 control-label"></label>
							<div class="col-sm-7">
								<input type="hidden" class="form-control" id="id" name="id" >
							</div>
					  	</div>
					
						<div class="row">
						
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="lrn">LRN</label>
									<input type="text" class="form-control" id="lrn" name="lrn" maxlength="12" onkeypress="return isNumber(event)"> 
								</div>
							</div>
							
						</div>
					
						<div class="row">
						
							<div class="col-md-4">
								<div class="form-group">
									<label for="lastname">Last Name</label>
									<input type="text" class="form-control" id="lastname" name="lastname" maxlength="30" onkeypress="return isLetterDashPoint(event)"> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fname">First Name</label>
									<input type="text" class="form-control" id="firstname" name="firstname" maxlength="50" onkeypress="return isLetterPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="mname">Middle Name</label>
									<input type="text" class="form-control" id="middlename" name="middlename" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
						</div>
						
						<br>
							
						<div class="row">
						
							<div class="col-md-4">
								<div class="form-group">
									<label for="gender">Gender</label>
									<select class="form-control" id="gender" name="gender">							
								
											<option>M</option>
											<option>F</option>
									
								</select>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="birthdate">Birthdate</label>
									<input type="date" class="form-control" id="birthdate" name="birthdate">
								</div>
							</div>

						</div>

						<div class="row">
						
							<div class="col-md-4">
								<div class="form-group">
									<label for="mothertounge">Mother Tongue</label>
										<select class="form-control" id="mothertounge" name="mothertounge" onchange="showfield1(this.options[this.selectedIndex].value)">							
												<option>Tagalog</option>
												<option>Bikol</option>
												<option>Cebuano</option>
												<option>Chabacano</option>
												<option>Hiligaynon</option>
												<option>Iloko</option>
												<option>Kapampangan</option>
												<option>Maguindanaoan</option>
												<option>Maranao</option>
												<option>Pangasinense</option>												
												<option>Tausug</option>
												<option>Waray</option>
												<option>Other</option>
										</select>
									 <div id="div1"></div>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="ip">IP</label>
										<select class="form-control" id="ip" name="ip" onchange="showfield2(this.options[this.selectedIndex].value)">
												<option>Tagalog</option>						
												<option>Bikolano</option>
												<option>Gaddang</option>
												<option>Ibanag</option>
												<option>Ilokano</option>
												<option>Ivatan</option>
												<option>Kapampangan</option>
												<option>Moro</option>
												<option>Pangasinan</option>												
												<option>Sambal</option>
												<option>Subanon</option>
												<option>Visayan</option>
												<option>Zamboangueño</option>
												<option>Other</option>
										</select>
										<div id="div2"></div>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="religion">Religion</label>
										<select class="form-control" id="religion" name="religion" onchange="showfield3(this.options[this.selectedIndex].value)">
												<option>Catholic</option>						
												<option>Born Again</option>
												<option>INC</option>
												<option>JW</option>
												<option>LDS</option>
												<option>Muslim</option>
												<option>Protestant</option>
												<option>SDA</option>
												<option>Other</option>
										</select>
										<div id="div3"></div>
								</div>
							</div>
						
						</div>
						
						<br>
						
						<div class="row">
						
							<div class="col-md-4">
								<div class="form-group">
									<label for="Address">Address</label>
								</div>
							</div>
							
						</div>
						
						<div class="row">
							
							<div class="col-md-1"></div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="add_no_st">House No. / Street / Sitio / Purok</label>
									<input type="text" class="form-control" id="addnost" name="addnost" maxlength="30" onkeypress="return isAdd(event)"> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="brgy">Barangay</label>
									<input type="text" class="form-control" id="brgy" name="brgy" maxlength="20" onkeypress="return isAdd(event)">
								</div>
							</div>
							
							<div class="col-md-1"></div>
							
						</div>
						
						<div class="row">
							
							<div class="col-md-1"></div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="city">City</label>
									<input type="text" class="form-control" id="city" name="city" maxlength="30" onkeypress="return isLetterDashPoint(event)"> 
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="province">Province</label>
									<input type="text" class="form-control" id="province" name="province" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
							<div class="col-md-1"></div>
							
						</div>
						
						<br>
						
						<div class="row divider">
							
							<div class="col-md-4"></div>
							
							<div class="col-md-4" style="text-align:center">
								<div class="divider">
									<label for="lrn">Other Information</label>
								</div>
							</div>
							
							<div class="col-md-4"></div>
							
						</div>
						
						<hr><br>
						
						<div class="row">
						
							<div class="col-md-3">
								<div class="form-group">
									<label for="Father">Father's Name:</label>
								</div>
							</div>
							
						</div>
						
						<div class="row">

							<div class="col-md-4">
								<div class="form-group">
									<label for="ffirstname">First Name</label>
									<input type="text" class="form-control" id="ffirstname" name="ffirstname" maxlength="50" onkeypress="return isLetterPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fmiddlename">Middle Name</label>
									<input type="text" class="form-control" id="fmiddlename" name="fmiddlename" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="flastname">Last Name</label>
									<input type="text" class="form-control" id="flastname" name="flastname" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
						</div>
						
						<br>
						
						<div class="row">
						
							<div class="col-md-5">
								<div class="form-group">
									<label for="Mother">Mother's Maiden Name:</label>
								</div>
							</div>
							
						</div>
						
						<div class="row">

							<div class="col-md-4">
								<div class="form-group">
									<label for="mfirstname">First Name</label>
									<input type="text" class="form-control" id="mfirstname" name="mfirstname" maxlength="50" onkeypress="return isLetterPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="fmiddlename">Middle Name</label>
									<input type="text" class="form-control" id="mmiddlename" name="mmiddlename" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="mlastname">Last Name</label>
									<input type="text" class="form-control" id="mlastname" name="mlastname" maxlength="30" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
						</div>
						
						<br>
						
						<div class="row">
							
							<div class="col-md-8">
								<div class="form-group">
									<label for="guardian_name">Name of Guardian:&nbsp;<i>if no parent(s)</i></label>
									<input type="text" class="form-control" id="guardianname" name="guardianname" maxlength="50" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="relationship">Relationship</i></label>
										<select class="form-control" id="relationship" name="relationship" onchange="showfield4(this.options[this.selectedIndex].value)">
												<option></option>
												<option>mother</option>						
												<option>father</option>
												<option>aunt</option>						
												<option>uncle</option>
												<option>brother</option>
												<option>sister</option>
												<option>grandmother</option>
												<option>grandfather</option>
												<option>godmother</option>
												<option>godfather</option>
												<option>Other</option>
										</select>
										<div id="div4"></div>
								</div>
							</div>
						
						</div>
						
						<div class="row">
							
							<div class="col-md-4"></div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="contact_number">Contact Number</label>
									<input type="text" class="form-control" id="contactnumber" name="contactnumber" maxlength="13" onkeypress="return isContact(event)">
								</div>
							</div>
							
							<div class="col-md-6"></div>
							
						</div>

						<br>

						<div class="row">
							
							<div class="col-md-3">
								<div class="form-group">
									<label for="gwa">Elementary GWA</label>
									<input type="text" class="form-control" id="gwa" name="gwa" maxlength="5" onkeypress="return isFloat(event)">
								</div>
							</div>
							
							<div class="col-md-9">
								<div class="form-group">
									<label for="school">Elementary School</label>
									<input type="text" class="form-control" id="school" name="school" maxlength="50" onkeypress="return isLetterDashPoint(event)">
								</div>
							</div>
						
						</div>
						
						<div class="row">
						
							<div class="col-md-1"></div>
							
							<div class="col-md-7">
								<div class="form-group">
									<label for="remarks">Remarks</label>
									<input type="text" class="form-control" id="remarks" name="remarks" maxlength="30">
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<label for="password">Password</label>
									<input type="password" class="form-control" id="password" name="password" maxlength="20" onkeypress="return isPassword(event)">
								</div>
							</div>

							<div class="col-md-1"></div>
							
						</div>
						
						<div class="form-group">
																		
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

						</div>
						
						

						<br>
						
						<div class="form-group">

							<div class="col-sm-4"></div>
							
							<div class="col-sm-4" style="text-align: center;">
								<button data-target="#update" data-toggle="modal" type="button" class="btn btn-success success"  >Update</button>				
								<button  data-target="#delete" data-toggle="modal" type="button" class="btn btn-danger warning confirm-delete-modal" name="btnDelete">Delete</button>							
							</div>
							
							<div class="col-sm-4"></div>
							
						</div>

						
					</form>
				</div>


			    <div class="modal-footer" style="background-color:gold;">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      
		        </div>
	   		</div>
	   	</div>
	</div>
</div>

<?php include('modal-add-student.php'); ?>

<!--================ Update Confirm Modal =================================================================-->
<div id="update" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to update the record?</p>
                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="students-submit.php">

            		<input type="hidden" id="updateid" name="id">
            		<input type="hidden" id="updatelrn"name="lrn">
            		<input type="hidden" id="updatelastname" name="lastname">
            		<input type="hidden" id="updatefirstname" name="firstname">
            		<input type="hidden" id="updatemiddlename" name="middlename">
            		<input type="hidden" id="updategender" name="gender">
            		<input type="hidden" id="updatebirthdate" name="birthdate">
            		<input type="hidden" id="updatemothertounge" name="mothertounge">
            		<input type="hidden" id="updateip" name="ip">
            		<input type="hidden" id="updatereligion" name="religion">
            		<input type="hidden" id="updateaddnost" name="addnost">
            		<input type="hidden" id="updatebrgy" name="brgy">
            		<input type="hidden" id="updatecity" name="city">
            		<input type="hidden" id="updateprovince" name="province">
            		<input type="hidden" id="updateflastname" name="flastname">
            		<input type="hidden" id="updateffirstname" name="ffirstname">
            		<input type="hidden" id="updatefmiddlename" name="fmiddlename">
            		<input type="hidden" id="updatemlastname" name="mlastname">
            		<input type="hidden" id="updatemfirstname" name="mfirstname">
            		<input type="hidden" id="updatemmiddlename" name="mmiddlename">
            		<input type="hidden" id="updateguardianname" name="guardianname">
            		<input type="hidden" id="updaterelationship" name="relationship">
            		<input type="hidden" id="updatecontactnumber" name="contactnumber">
            		<input type="hidden" id="updategwa" name="gwa">
            		<input type="hidden" id="updateschool" name="school">
            		<input type="hidden" id="updateremarks" name="remarks">
            		<input type="hidden" id="updatepassword" name="password">

            		<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">

		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-success success modal-update" name="btnUpdate">Confirm</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<!--================ Delete Confirm Modal =================================================================-->
<div id="delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Do you want to delete the record?</p>
                <p class="text-warning"><small>If you proceed, deletion will be immediate!</small></p>
            </div>

            <div class="modal-footer">
            
            	<form method="post" action="students-submit.php">
		            <input type="hidden"  id="deleteid" name="id">
		            <input type="hidden"  id="deletelrn" name="lrn">
		            <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
		   			
		   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-danger warning" name="btnDelete">Confirm</button>
                </form>

            </div>

        </div>
    </div>
</div>
 		
<script type="text/javascript">

	$(document).ready(function() {
		var table=$('#student').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    	} );

    	$('#student tbody').on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }

	       
    	} );

	 	$(function(){
	    	$('#studentmodal').modal({
		        keyboard: true,
		        show:false,


		    }).on('show.bs.modal', function(){
		        var getIdFromRow = $(event.target).closest('tr').data('id');
	 				$('#id').val(getIdFromRow);

				var getlrnFromRow = $(event.target).closest('tr').data('lrn');
	 				$('#lrn').val(getlrnFromRow);
	 				//$('#deletelrn').val(getlrnFromRow);
				
				var getlastnameFromRow = $(event.target).closest('tr').data('lastname');
	 				$('#lastname').val(getlastnameFromRow);
				
				var getfirstnameFromRow = $(event.target).closest('tr').data('firstname');
	 				$('#firstname').val(getfirstnameFromRow);
				
				var getmiddlenameFromRow = $(event.target).closest('tr').data('middlename');
	 				$('#middlename').val(getmiddlenameFromRow);
				
				var getgenderFromRow = $(event.target).closest('tr').data('gender');
	 				$('#gender').val(getgenderFromRow);
				
				var getbirthdateFromRow = $(event.target).closest('tr').data('birthdate');
	 				$('#birthdate').val(getbirthdateFromRow);
				
				var getmothertoungeFromRow = $(event.target).closest('tr').data('mothertounge');
	 				$('#mothertounge').val(getmothertoungeFromRow);
				
				var getipFromRow = $(event.target).closest('tr').data('ip');
	 				$('#ip').val(getipFromRow);
				
				var getreligionFromRow = $(event.target).closest('tr').data('religion');
	 				$('#religion').val(getreligionFromRow);
				
				var getaddnostFromRow = $(event.target).closest('tr').data('addnost');
	 				$('#addnost').val(getaddnostFromRow);

				var getbrgyFromRow = $(event.target).closest('tr').data('brgy');
	 				$('#brgy').val(getbrgyFromRow);

				var getcityFromRow = $(event.target).closest('tr').data('city');
	 				$('#city').val(getcityFromRow);

				var getprovinceFromRow = $(event.target).closest('tr').data('province');
	 				$('#province').val(getprovinceFromRow);

				var getflastnameFromRow = $(event.target).closest('tr').data('flastname');
	 				$('#flastname').val(getflastnameFromRow);

				var getffirstnameFromRow = $(event.target).closest('tr').data('ffirstname');
	 				$('#ffirstname').val(getffirstnameFromRow);
				
				var getfmiddlenameFromRow = $(event.target).closest('tr').data('fmiddlename');
	 				$('#fmiddlename').val(getfmiddlenameFromRow);

				var getmlastnameFromRow = $(event.target).closest('tr').data('mlastname');
	 				$('#mlastname').val(getmlastnameFromRow);

				var getmfirstnameFromRow = $(event.target).closest('tr').data('mfirstname');
	 				$('#mfirstname').val(getmfirstnameFromRow);

				var getmmiddlenameFromRow = $(event.target).closest('tr').data('mmiddlename');
	 				$('#mmiddlename').val(getmmiddlenameFromRow);

				var getguardiannameFromRow = $(event.target).closest('tr').data('guardianname');
	 				$('#guardianname').val(getguardiannameFromRow);

				var getrelationshipFromRow = $(event.target).closest('tr').data('relationship');
	 				$('#relationship').val(getrelationshipFromRow);

				var getcontactnumberFromRow = $(event.target).closest('tr').data('contactnumber');
	 				$('#contactnumber').val(getcontactnumberFromRow);

				var getgwaFromRow = $(event.target).closest('tr').data('gwa');
	 				$('#gwa').val(getgwaFromRow);

				var getschoolFromRow = $(event.target).closest('tr').data('school');
	 				$('#school').val(getschoolFromRow);

				var getremarksFromRow = $(event.target).closest('tr').data('remarks');
	 				$('#remarks').val(getremarksFromRow);

	 			var getpasswordFromRow = $(event.target).closest('tr').data('password');
	 				$('#password').val(getpasswordFromRow);

				         	console.info(getIdFromRow);
		        
		    });
		});

	  	$(function(){
	    	$('#delete').modal({
		        keyboard: true,
		        show:false,

	    	}).on('show.bs.modal', function(){
	         	var getIdFromRow = $('#id').val();
	 				$('#deleteid').val(getIdFromRow);

	 			var getlrnFromRow = $('#lrn').val();
	 				$('#deletelrn').val(getlrnFromRow); 

		        	console.info(getIdFromRow);
			});
		});
	  	
	  	$(function(){
	    	$('#update').modal({
		        keyboard: true,
		        show:false,

	    	}).on('show.bs.modal', function(){

	    		var getIdFromRow =$('#id').val();;
					$('#updateid').val(getIdFromRow);

				var getlrnFromRow = $('#lrn').val();;
	 				$('#updatelrn').val(getlrnFromRow);
				
				var getlastnameFromRow = $('#lastname').val();;
	 				$('#updatelastname').val(getlastnameFromRow);
				
				var getfirstnameFromRow = $('#firstname').val();;
	 				$('#updatefirstname').val(getfirstnameFromRow);
				
				var getmiddlenameFromRow = $('#middlename').val();;
	 				$('#updatemiddlename').val(getmiddlenameFromRow);
				
				var getgenderFromRow = $('#gender').val();;
	 				$('#updategender').val(getgenderFromRow);
				
				var getbirthdateFromRow = $('#birthdate').val();;
	 				$('#updatebirthdate').val(getbirthdateFromRow);
				
				var getmothertoungeFromRow = $('#mothertounge').val();;
	 				$('#updatemothertounge').val(getmothertoungeFromRow);
				
				var getipFromRow = $('#ip').val();;
	 				$('#updateip').val(getipFromRow);
				
				var getreligionFromRow = $('#religion').val();;
	 				$('#updatereligion').val(getreligionFromRow);
				
				var getaddnostFromRow = $('#addnost').val();;
	 				$('#updateaddnost').val(getaddnostFromRow);

				var getbrgyFromRow = $('#brgy').val();;
	 				$('#updatebrgy').val(getbrgyFromRow);

				var getcityFromRow = $('#city').val();;
	 				$('#updatecity').val(getcityFromRow);

				var getprovinceFromRow = $('#province').val();;
	 				$('#updateprovince').val(getprovinceFromRow);

				var getflastnameFromRow = $('#flastname').val();;
	 				$('#updateflastname').val(getflastnameFromRow);

				var getffirstnameFromRow = $('#ffirstname').val();;
	 				$('#updateffirstname').val(getffirstnameFromRow);
				
				var getfmiddlenameFromRow = $('#fmiddlename').val();;
	 				$('#updatefmiddlename').val(getfmiddlenameFromRow);

				var getmlastnameFromRow = $('#mlastname').val();;
	 				$('#updatemlastname').val(getmlastnameFromRow);

				var getmfirstnameFromRow = $('#mfirstname').val();;
	 				$('#updatemfirstname').val(getmfirstnameFromRow);

				var getmmiddlenameFromRow = $('#mmiddlename').val();;
	 				$('#updatemmiddlename').val(getmmiddlenameFromRow);

				var getguardiannameFromRow = $('#guardianname').val();;
	 				$('#updateguardianname').val(getguardiannameFromRow);

				var getrelationshipFromRow = $('#relationship').val();;
	 				$('#updaterelationship').val(getrelationshipFromRow);

				var getcontactnumberFromRow = $('#contactnumber').val();;
	 				$('#updatecontactnumber').val(getcontactnumberFromRow);

				var getgwaFromRow = $('#gwa').val();;
	 				$('#updategwa').val(getgwaFromRow);

				var getschoolFromRow = $('#school').val();;
	 				$('#updateschool').val(getschoolFromRow);

				var getremarksFromRow = $('#remarks').val();;
	 				$('#updateremarks').val(getremarksFromRow);

	 			var getpasswordFromRow = $('#password').val();;
	 				$('#updatepassword').val(getpasswordFromRow);
	 				
				    console.info(getIdFromRow)
			});
		});
		
		function showfield1(name){
		  if(name=='Other')document.getElementById('div1').innerHTML='<input type="text" class="form-control" id="mothertounge" name="mothertounge" placeholder="Mother Tounge"maxlength="20">';
		  else document.getElementById('div1').innerHTML='';
		}

		function showfield2(name){
		  if(name=='Other')document.getElementById('div2').innerHTML='<input type="text" class="form-control" id="ip" name="ip" placeholder="Ethnic Group" maxlength="20">';
		  else document.getElementById('div2').innerHTML='';
		}

		function showfield3(name){
		  if(name=='Other')document.getElementById('div3').innerHTML='<input type="text" class="form-control" id="religion" name="religion" placeholder="Religion" maxlength="20">';
		  else document.getElementById('div3').innerHTML='';
		}

		function showfield4(name){
		  if(name=='Other')document.getElementById('div4').innerHTML='<input type="text" class="form-control" id="relationship" name="relationship" placeholder="Relationship" maxlength="20">';
		  else document.getElementById('div4').innerHTML='';
		}
	});

</script>


