
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:gold;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Log-out</h4>
            </div>            

            <div class="modal-body">
            	<br>
					<h5>Confirm log-out?</h5>
				<br>		
			</div>

		    <div class="modal-footer" style="background-color:gold;">
				<form method="post" action="logout.php">				  
					<button type="submit" class="btn btn-success success"  name="confirm" value="confirm">Confirm</button>						
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		     	</form> 
	        </div>
   		</div>
	</div>