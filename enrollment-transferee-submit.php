<?php
		$url= $_POST['url'];
		$info=$_POST['info'];
		$year_level=$info['year_level'];
		
		$columns=array();
		$values=array();

		foreach ($info as $fieldname => $value) {
			
			if(!empty($value)){

				if ($fieldname=='age' || $fieldname=='lrn' || $fieldname=='gwa' ) {	
					$value = trim($value);		
					array_push($values,  $value);
				}
				elseif ($fieldname=='guardianmail') {
					$value = trim($value);		
					array_push($values, "'". $value."'");
				}
				else{
					if ($fieldname!='year_level') {
						$value = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($value))));
						$value = trim($value);	
						array_push($values,  "'".$value."'");
					}
				}

				
				if ($fieldname!='year_level') {
					array_push($columns, $fieldname);
				}
			}
		}

//--Insert _tbl_studentinfo------------------------------------------------------------------------------------------------------------------		
	$columns = implode (", ", $columns);

	$values = implode (", ", $values);
	
	query_db("INSERT INTO tbl_studentinfo(".$columns.") VALUES(".$values.")");

	$result=get_db("SELECT id, lrn FROM tbl_studentinfo ORDER BY id DESC LIMIT 1") or die(mysql_error());
	$id = $result['id'];
	$lrn = $result['lrn']; 
	
//--Insert _tbl_requirement------------------------------------------------------------------------------------------------------------------		
	
	if(isset($_POST['nso'])) $nso=1; else $nso =0;
	if(isset($_POST['f137'])) $f137=1; else $f137 =0;
	if(isset($_POST['f138'])) $f138=1; else $f138 =0;
	if(isset($_POST['cgmc'])) $cgmc=1; else $cgmc =0;
	if(isset($_POST['idpic'])) $idpic=1; else $idpic =0;
	if(isset($_POST['medc'])) $medc=1; else $medc =0;

	query_db("INSERT INTO tbl_requirement (student_id,nso,f137,f138,cgmc,idpic,medc) values($id,$nso,$f137,$f138,$cgmc,$idpic,$medc)");

//--Insert _tbl_studentstatus------------------------------------------------------------------------------------------------------------------		
	
	$sy_id=get_last_sy_id();

	$result=get_db("SELECT year_id FROM tbl_yearlevel WHERE year_level='$year_level'");
	$year_id=$result['year_id'];

	if($info['lrn']==null || $info['lrn']=='' || $info['lrn']==0){
		
		query_db("INSERT INTO tbl_studentstatus(lrn,year_id,sy_id,remarks) values($id,$year_id,$sy_id,'Transferee')");
	}
	else{
		query_db("INSERT INTO tbl_studentstatus(lrn,year_id,sy_id,remarks) values({$info['lrn']},$year_id,$sy_id,'Transferee')");
	}

//--Insert _tbl_studentgrade------------------------------------------------------------------------------------------------------------------	
	
	switch ($year_level) {
		case 'Grade 7':
			$curriculum='seven';
			break;
		case 'Grade 8':
			$curriculum='eight';
			break;
		case 'Grade 9':
			$curriculum='nine';
			break;
		case 'Grade 10':
			$curriculum='ten';
			break;
		
		default:
			# code...
			break;
	}
	
	$subjects=get_db("SELECT ".$curriculum." FROM tbl_curriculum where c_id=(SELECT c_id FROM tbl_curriculum_sy where sy_id=$sy_id)")or die(mysql_error());
	$subjectsarray=str_getcsv($subjects[$curriculum]);

	if($lrn==null || $lrn=='' || $lrn==0){
		foreach ($subjectsarray as $key => $value) {
			query_db("INSERT INTO tbl_studentgrade (lrn,subject_code,quarter1,quarter2,quarter3,quarter4,sy_id) VALUES($id,'".$value."',0,0,0,0,$sy_id)");
		}
	}
	else{
		foreach ($subjectsarray as $key => $value) {
		
			query_db("INSERT INTO tbl_studentgrade (lrn,subject_code,quarter1,quarter2,quarter3,quarter4,sy_id) VALUES($lrn,'".$value."',0,0,0,0,$sy_id)");
		}
	}

	//--Insert _tbl_studentlogin------------------------------------------------------------------------------------------------------------------		
		
		$sy_id=get_last_sy_id();

		if($lrn==null || $lrn=='' || $lrn==0){
			
			query_db("INSERT INTO tbl_loginstudent(lrn,username,password) values($id,$id,'$id')");

		}
		else{
			query_db("INSERT INTO tbl_loginstudent(lrn,username,password) values($lrn,$lrn,'$lrn')");
		}

//--Insert _tbl_parentlogin------------------------------------------------------------------------------------------------------------------		

		$sy_id=get_last_sy_id();
		
		if($lrn==null || $lrn=='' || $lrn==0){
			
			query_db("INSERT INTO tbl_loginparent(lrn,username,password) values($id,$id,'$id')");
		}
		else{
			query_db("INSERT INTO tbl_loginparent(lrn,username,password) values($lrn,$lrn,'$lrn')");
		}
		
// parent mail send--------------------------------------------------------------------------------------
	if($lrn!=null || $lrn!='' || $lrn!=0){
		if($info['guardianmail'] !=null || $info['guardianmail'] =='' || !empty($info['guardianmail']) ){

			$guardianmail = $info['guardianmail'];
			include('mail.php');
			echo 'hello';
		}
	}

redirect('index2.php?mode=Registrar&category=Enrollment&page=2');