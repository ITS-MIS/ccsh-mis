<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');

	date_default_timezone_set('Asia/Taipei');
	$currentmonth = date('F');
	$cmonth = date('m');
	$currentyear = date('Y');
	$currentday = date('d');
	$currentdate=$currentyear."-".$cmonth."-".$currentday;
	$month=$currentmonth;
	


	$emp_no=$_SESSION['emp_no'];
	
	$pdf = new FPDI();

	$pdf->AddPage('P','letter');
	
	$pageCount = $pdf->setSourceFile("forms/sf2.pdf");
	$tplIdx = $pdf->importPage(1);
	$pdf->useTemplate($tplIdx, 8, 0, 0, 0, true);

	$pdf->SetFont('Arial', '', '4');
	$pdf->SetTextColor(0,0,0);

	$x=19;
	$y=37.5;
	$y2=138.5;
	$n=3.9;

// schoolyear-------------------------------------------------------------

	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(100, 16.5);
	$pdf->SetFont('Arial','', '6');
	$pdf->Write(0, $sy);

//month---------------------------------------------------------------------

	$pdf->SetXY(157, 16.5);
	$pdf->SetFont('Arial','b', '7');
	$pdf->Write(0, $currentmonth);

// grade level--------------------------------------------------------------

	$year=get_db("SELECT year_id FROM tbl_yearlevel WHERE year_id = (SELECT year_id FROM tbl_advisers WHERE emp_no=$emp_no and sy_id = $sy_id)");
	$year=$year['year_id'];

	$pdf->SetXY(153, 21);
	$pdf->SetFont('Arial','b', '7');
	$pdf->Write(0, $year);

// section--------------------------------------------------------------
	$section=get_db("SELECT section_name FROM tbl_section WHERE section_id = (SELECT section_id FROM tbl_advisers WHERE emp_no=$emp_no and sy_id = $sy_id)");
	$section=$section['section_name'];

	$pdf->SetXY(175, 21);
	$pdf->SetFont('Arial','b', '7');
	$pdf->Write(0, $section);

//day and weekday--------------------------------------------------------------------
	$day=1;
	$daylabel=array('M'=>'Monday','T'=>'Tuesday','W'=>'Wednesday','TH'=>'Thursday','F'=>'Friday');
	$month = date('m', strtotime($month));
	$totalday = cal_days_in_month(CAL_GREGORIAN, $month, $currentyear);
	$day_x=63.8;
	$label_x=64.3;			
    while ($totalday!=$day) {
    	if($day<10){
           $day='0'.$day;
       }
		$date = $currentyear."-".$month."-".$day;
		$weekday=date('l', strtotime( $date));
		if($weekday!='Saturday' && $weekday!='Sunday'){
			$dlabel=array_search($weekday, $daylabel); 
			$pdf->SetXY($day_x,29.7);
			$pdf->Write(0, $day);

			if ($dlabel=='TH') {
				$label_x=$label_x-0.85;
				$pdf->SetXY($label_x,33.7);
				$pdf->Write(0, $dlabel);
				$label_x=$label_x+0.85;
			}
			elseif ($dlabel=='W' || $dlabel=='M' ) {
				$label_x=$label_x-0.45;
				$pdf->SetXY($label_x,33.7);
				$pdf->Write(0, $dlabel);
				$label_x=$label_x+0.45;
			}
			
			else{
				$pdf->SetXY($label_x,33.7);
				$pdf->Write(0, $dlabel);
			}
			$label_x=$label_x+4.31;
			$day_x=$day_x+4.31;
      	}
     
      	$day++;
    }

	$result=get_db_array("SELECT lrn,lastname,firstname,middlename,gender
							from tbl_studentinfo where lrn in
							(SELECT lrn FROM tbl_studentstatus WHERE sy_id=$sy_id and section_id =
							(SELECT section_id FROM tbl_advisers WHERE emp_no=$emp_no and sy_id=$sy_id)) and remarks!='TO' order by  gender, lastname, firstname");

	$pdf->SetFont('Arial', '', '7'); 
	$pdf->SetTextColor(0,0,0);
	
	foreach ($result as $key => $columnname) {

		$lrn=$result[$key]['lrn'];
		$day2=1;
		$attend_x=64.3;
		$ctotal=0;
		while ($totalday!=$day2) {
	    	if($day2<10){
	           $day2='0'.$day2;    
	    	}

			$date2 = $currentyear."-".$month."-".$day2;
			$weekday2=date('l', strtotime($date2));
			$date3 = $currentyear."-".$cmonth."-".$day2;

			if($weekday2!='Saturday' && $weekday2!='Sunday'){
		 		
				$date_present=get_db_array("SELECT date_present from tbl_attendance where lrn=$lrn and emp_no=$emp_no and date_present = '$date3'");
				if($result[$key]['gender']=='M'){
			
					if(count($date_present)==1){
						$pdf->SetXY($attend_x,$y);
						$pdf->Write(0, "/");
						$attend_x=$attend_x+4.31;
					}
							
					else{
						$pdf->SetXY($attend_x,$y);
						$pdf->Write(0, "x");
						$attend_x=$attend_x+4.31;
						$ctotal++;
					}
				}
				else{
					if(count($date_present)==1){
						$pdf->SetXY($attend_x,$y2);
						$pdf->Write(0, "/");
						$attend_x=$attend_x+4.31;
					}
							
					else{
						$pdf->SetXY($attend_x,$y2);
						$pdf->Write(0, "x");
						$attend_x=$attend_x+4.31;
						$ctotal++;

					}
				}
				
			}
			$day2++;
		}
		if($result[$key]['gender']=='M'){
					
			$pdf->SetXY(177,$y);
			$pdf->Write(0, $ctotal);
		}
		else{
			$pdf->SetXY(177,$y2);
			$pdf->Write(0, $ctotal);
		}
					
		foreach ($columnname as $key4 => $value2) {
			if($key4=='lastname'){
				$name=$value2.', '.$columnname['firstname'].' '.$columnname['middlename'];

				if ($columnname['gender']=='M'){

					$pdf->SetXY($x, $y);
					$y=$y+$n;
				}
				else{
					$pdf->SetXY($x, $y2);
					$y2=$y2+$n;
				}
				$pdf->Write(0, $name);

			}	
		}
	}
	
	$pdf->Output();