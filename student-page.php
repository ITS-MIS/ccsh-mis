<?php
	getDatatablesLink();
	getCcshsPageLink();
	getDatatablesScript();
?>

<link rel="stylesheet" type="text/css" href="css/f_clone_Notify.css">
<script type="text/javascript" src="js/f_clone_Notify.js"></script>

<style type="text/css">
	body {
		background-color: black;
		background-image:url("images/bg2.jpg");
		background-size: 100% 100%;
		background-attachment: fixed;
		background-repeat:no-repeat;
		position: absolute; 
		top: 0;
		right: 0;
		left: 0;
		bottom: 0;	
		}
		
	.badge{color:white;
			background-color: red;
	}

	.container{
    margin-top:20px;
	}

	.image-preview-input {
	    position: relative;
		overflow: hidden;
		margin: 0px;    
	    color: #333;
	    background-color: #fff;
	    border-color: #ccc;    
	}

	.image-preview-input input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 20px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}

	.image-preview-input-title {
	    margin-left:2px;
	}
</style>

<?php
	$id=get_db("SELECT id from tbl_studentinfo where lrn=$lrn");
	$id=$id['id'];
	$passq=get_db("SELECT count(a.id) as count FROM tbl_requirement a 
				
				WHERE student_id=$id AND (nso=0 OR f137=0 OR f138=0 OR cgmc=0 OR idpic=0 OR medc=0)");	
		$p=$passq['count'];	

			if ($p==1) {
			$message="Welcome Student! 
						You have pending requirement(s) that needs to submit, please check requirement tab!";
		} else {
			$message='Welcome Student!';
		}

?>

<div class="container-fluid">
<br><br><br><br><br><br>
				<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<label id="header">Welcome to CCSHS Student Page!</label>
						</div>
						<div class="col-md-1"></div>
				</div>	
	<div class="row">
		<div class="col-sm-3 col-md-1"></div>
			<?php
				
				include('current-year.php');
				$select = get_db("SELECT section_id FROM tbl_studentstatus WHERE lrn=$lrn AND sy_id=$sy_id");
				$select=$select['section_id'];

				if($select==0) {

					$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level 
								FROM tbl_studentinfo a 
								LEFT JOIN tbl_studentstatus b ON a.lrn=b.lrn
								LEFT JOIN tbl_yearlevel c ON b.year_id=c.year_id 
								WHERE a.lrn = $lrn AND b.sy_id=$sy_id";
								
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
				
					if (mysql_num_rows($result)>0) {
				
						while($row = mysql_fetch_row($result)) {
					
							$lrn = $row[0];
							$lastname = $row[1];
							$firstname = $row[2];
							$middlename = $row[3];
							$year_level = $row[4];
					}
						$section_name="Unassigned";
					}	

				} else {
					$sql_load = "SELECT a.lrn, a.lastname, a.firstname, a.middlename, c.year_level, d.section_name 
								FROM tbl_studentinfo a,tbl_studentstatus b, tbl_yearlevel c, tbl_section d 
								WHERE b.year_id = c.year_id AND b.section_id = d.section_id AND a.lrn = b.lrn AND a.lrn = $lrn AND b.sy_id=$sy_id";
								
					$result = mysql_query($sql_load) or die('SQL Error :: '.mysql_error());
				
					if (mysql_num_rows($result)>0) {			
						while($row = mysql_fetch_row($result)) {					
							$lrn = $row[0];
							$lastname = $row[1];
							$firstname = $row[2];
							$middlename = $row[3];
							$year_level = $row[4];
							$section_name = $row[5];							
						}
					}	
				}
			?>			

		<div class="col-sm-3 col-md-10" style="border: 3px solid; border-color: gold;" id="divToPrint" style="display:none;"><br>

			<div class="row">

				<div class="col-md-2">
						<img src="images/logo.png" height="80px; width=80px;">
				</div>
						
				<div class="col-md-10">	

					<div class="row">
						<div class="col-md-10" style="text-align:right;">
							<div class="form-group">

									<?php

										$iquery = "SELECT image FROM tbl_images WHERE lrn = $lrn AND verify=1";			
										$result = mysql_query($iquery);
											
											$row = mysql_fetch_row($result);
											$image=($row[0]);
									?>

								<img id="image" class="image" src="<?php echo $image; ?>" alt="Upload of image is &nbsp;&nbsp;&nbsp; subject to &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								 verification of the &nbsp;&nbsp;&nbsp; administrator!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" height="150px" width="150px"/>

							</div>
						</div>
						<div class="col-md-2"></div>
					</div>	

					<div class="row">
						<div class="col-md-6"></div>
						<div class="col-md-6" style="text-align:right;">
							<div class="form-group">
								<div class="input-group image-preview">
					                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
					                <span class="input-group-btn">
					                	<form action="saveimage.php" enctype="multipart/form-data" method="post">
						                    <!-- image-preview-clear button -->
						                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
						                        <span class="glyphicon glyphicon-remove"></span> Clear
						                    </button>
						                    <!-- image-preview-input -->
						                    <div class="btn btn-default image-preview-input">
						                        <span class="glyphicon glyphicon-folder-open"></span>
						                        <span class="image-preview-input-title">Browse</span>
						                        <input type="file" accept="image/png" class="btn btn-success" name="uploadedimage" required/> <!-- rename it -->

						                        <input type="hidden" name="lrn" value="<?php echo $lrn; ?>">
						                        <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
						                    </div>
						                       	<button type="submit" class="btn btn-success" name="btnSubmit" onclick="return confirm('Are you sure you want to Submit?');">Submit</button>
														
					                    </form>
					            </div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6"></div>
						<div class="col-md-6" style="text-align:right; color:#FFFAF0; text-shadow: 2px 1px black;">
							<div class="form-group">
								<label><i>Upload of image is subject to verification of the administrator!</i></label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6" style="text-align:left;">
							<div class="form-group">
								<label for="name">Student's Name:</label><br>
								<label for="name" style="color:white; text-indent: 40px; text-shadow: 2px 1px black; font-size:17px"><?php echo $lastname .", ". $firstname ." ". $middlename; ?></label>
							</div>
						</div>
					</div>

					
					<div class="row">	
						<div class="col-md-4">
							<div class="form-group">
								<label for="yr">Year Level:</label><br>
								<label for="lrn2" style="color:white; text-indent: 40px; text-shadow: 2px 1px black; font-size:17px"><?php echo $year_level; ?></label>
							</div>
						</div>
						
					</div>
						
					<div class="row">

						<div class="col-md-4">
							<div class="form-group">
								<label for="section">Section:</label><br>
								<label for="section" style="color:white; text-indent: 40px; text-shadow: 2px 1px black; font-size:17px"><?php echo $section_name; ?></label>
							</div>
						</div>
						
					</div>

				</div>
			</div>
				
			<br>	
		</div>
		<div class="col-md-1"></div>
	</div>
</div>
	
<script type="text/javascript">
	
	$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});


		sNotify.addToQueue('<?php echo preg_replace("/\r?\n/", "\\n", addslashes($message)); ?>');
	    sNotify.alterNotifications('chat_msg');

</script>