<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<div class="container-fluid">
	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Audit Trail</label>
			</div>
			<div class="col-md-1"></div>
		</div>	
	
	<div class="row"><br>
		<form method="post" >
			<table id="audit" class="display" cellspacing="0" width="100%" style="background-color:gold;">		     
		        <thead>
		            <tr>
		                <th>ID</th>
		                <th>Date</th>
		                <th>Employee No.</th>
		                <th>Employee Type</th>
		                <th>LRN</th>
		                <th>Table Affected</th>
		                <th>Action</th>
		            </tr>	                
	        	</thead>
		   
		        <tbody>
		        <?php  
	        		$studentgradearray = get_db_array("SELECT * FROM tbl_audit order by id desc limit 1000");

	        		if (count($studentgradearray)>0) {

						foreach ($studentgradearray as $key => $fieldname) {
					?>
							
					<tr>
           	  			
					<?php
					
						foreach ($fieldname as $key2 => $value) {
					?>
        						
        				<td><?php echo $value ?></td>
        						
        			<?php
        				}
        			?>	                             
            		
            		</tr>
    						
				<?php
						}
					}
				?>

				</tbody>
			</table>
		</form>
	</div>
</div>

<!--================ Reset Confirm Modal =================================================================-->
<div id="reset" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Audit Trail Confirmation</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to reset the record?</p>
                <p class="text-warning"><small>If you proceed, changes will be immediate!</small></p>
                
            </div>

            <div class="modal-footer">
            	<form class="form-horizontal" method="post" action="audit-trail-submit.php">

		   			<button type="button" class="btn btn-default modal-update-cancel" data-dismiss="modal">Close</button>
                	<button type="submit" class="btn btn-danger warning modal-update" name="btnReset">Reset</button>
                	
             	</form>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
		var table=$('#audit').dataTable({
		 	bInfo: true,
			"bFilter": true,
			"ordering": false
    	});
	});

</script>