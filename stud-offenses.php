<?php	
	getDatatablesLink();
	getDatatablesScript();
?>

<style type="text/css">
			body {
				background-color: black;
				background-image:url("images/bg2.jpg");
				background-size: 100% 100%;
				background-attachment: fixed;
				background-repeat:no-repeat;
				position: absolute; 
				top: 0;
				right: 0;
				left: 0;
				bottom: 0;	
				}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<label id="header">Disciplinary Action Record</label>
		</div>
		<div class="col-sm-1"></div>
	</div>

	<br>

	<div class="row">

	    <?php
			$sql="SELECT * FROM tbl_uploads ORDER BY id DESC LIMIT 1";
			$result_set=mysql_query($sql);
			while($row=mysql_fetch_array($result_set)) {
		    	
		?>
		    	<label><a href="uploads/<?php echo $row['file'] ?>" target="_blank"><font face="arial">Download CCSHS School Policies </font><span class="glyphicon glyphicon-download-alt"></span></a></label>
		<?php
		    }
		 ?>
  	</div>
	
	<div class="row"><br>
		<form method="post">
			<table id="da" class="display" cellspacing="0" width="100%" style="background-color:gold;">
		     
		        <thead>
		            <tr>
		            	<th>Date </th>
		            	<th>Offense</th>
		                <th>Penalty</th>
		                <th>Remarks</th>		      
		            </tr>
		                
		        </thead>

		        <tbody>

			        <?php  


						$query = "SELECT a.date, b.offense_desc, c.penalty_desc, remarks from tbl_guidancerecord a 
									LEFT JOIN tbl_offense b ON a.offense_id=b.offense_id
									LEFT JOIN tbl_penalty c ON a.penalty_id=c.penalty_id
									WHERE a.lrn=$lrn ORDER BY date desc";



						$result = mysql_query($query) or die(mysql_error());


						if(mysql_num_rows($result) > 0) {
														
							while ($row = mysql_fetch_assoc($result)) {

								$dater = $row['date'];
								$offense_desc = $row['offense_desc'];
								$penalty_desc = $row['penalty_desc'];
								$remarks = $row['remarks'];

					?>
					
			            <tr> 
			                <td width="15%"><?php echo $dater; ?></td>
			                <td width="40%"><?php echo $offense_desc; ?></td>
			                <td width="25%"><?php echo $penalty_desc; ?></td>
			                <td width="10%"><?php echo $remarks; ?></td>
			            </tr>				
				<?php
			            }
			        }
			    ?>
				</tbody>
			</table>
		</form>
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#da').dataTable({
		 	bInfo: false,
			"bFilter": false,
			"ordering": false,
			"paging":false
    	} );
	} );

</script>