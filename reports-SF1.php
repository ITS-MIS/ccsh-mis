<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');
	include('current-year.php');

	$emp_no=$_SESSION['emp_no'];
	
	$pdf = new FPDI();

	$pdf->AddPage('L','legal');
	$pageCount = $pdf->setSourceFile("forms/sf1-new.pdf");
	$tplIdx = $pdf->importPage(1);

	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);

	$pdf->SetFont('Arial', 'b', '6');
	$pdf->SetTextColor(0,0,0);

	$x=18.5;
	$ymale=33.8;
	$yfemale=106;
	$n=4;

	$pdf->SetXY(270, 15);
	$pdf->SetFont('Arial','', '6');
	$pdf->Write(0, 'II');
	

// schoolyear-------------------------------------------------------------
	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(200, 19);
	$pdf->SetFont('Arial','', '6');
	$pdf->Write(0, $sy);

// grade level--------------------------------------------------------------
	$year=get_db("SELECT year_id FROM tbl_yearlevel WHERE year_id = (SELECT year_id FROM tbl_advisers WHERE emp_no=$emp_no and sy_id=$sy_id)");
	$year=$year['year_id'];

	$pdf->SetXY(256, 19);

	$pdf->SetFont('Arial','', '6');
	$pdf->Write(0, $year);

// section--------------------------------------------------------------
	$section=get_db("SELECT section_name FROM tbl_section WHERE section_id = (SELECT section_id FROM tbl_advisers WHERE emp_no=$emp_no and sy_id=$sy_id)");
	$section=$section['section_name'];

	$pdf->SetXY(310, 19);

	$pdf->SetFont('Arial','', '6');
	$pdf->Write(0, $section);
	
//student list--------------------------------------------------------------------
	$c=0;

	$xarraymale=array(17.9,39,81,87,102,110,127,145,160,192,210,226,242,267,287,305,320);

	$y=$ymale;
	
	$result=get_db_array("SELECT lrn,lastname,firstname,middlename,gender,birthdate,age,mothertounge,ip,religion,addnost,brgy,city,
							province,flastname,ffirstname,fmiddlename,mlastname,mfirstname,mmiddlename,guardianname,relationship,contactnumber
							from tbl_studentinfo where lrn in
							(SELECT lrn FROM tbl_studentstatus WHERE sy_id=$sy_id and section_id =
							(SELECT section_id FROM tbl_advisers WHERE emp_no=$emp_no and sy_id=$sy_id)) and remarks!='TO' order by  gender desc, lastname, firstname");
			
	foreach ($result as $key => $columnname) {

		foreach ($columnname as $key2 => $value) {
			if($key2=='firstname'or $key2=='middlename' or $key2=='ffirstname'or $key2=='fmiddlename' or $key2=='mfirstname'or $key2=='mmiddlename'){
				
			}
			else{
				
				if($key2=='lastname'){
					$pdf->SetXY($xarraymale[$c], $y);
					$pdf->Write(0, $value.", ".$result[$key]['firstname']." ".$result[$key]['middlename']);
					$pdf->SetFont('Arial', '', '6'); 
					
					
				}
				else{
				
					if($key2=='addnost'or $key2=='relationship' or $key2=='contactnumber' or $key2=='province'){
						$pdf->SetFont('Arial', '', '6'); 
					}
					else{
						$pdf->SetFont('Arial', '', '6'); 
					}
					$pdf->SetXY($xarraymale[$c], $y);
					$pdf->Write(0, $value);
					
				}
				$c++;
			}
			
		}
		$c=0;
		$y=$y+$n;
	}
	
	$pdf->AddPage('L','legal','C');
	$pageCount = $pdf->setSourceFile("forms/sf1-new.pdf");
	$tplIdx = $pdf->importPage(2);
	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);

	$pdf->SetFont('Arial', 'b', '4');
	$pdf->SetTextColor(0,0,0);

	$pdf->Output();

	$pdf->AddPage('L','legal','C');
?>