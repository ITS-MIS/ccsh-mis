<?php
	include_once'connect.php';
	date_default_timezone_set('Asia/Taipei');

	getDatatablesLink();
	getDatatablesScript();

	$currentmonth = date('F');
	$cmonth = date('m');
	$currentyear = date('Y');
	$currentday = date('d');
	$currentdate=$currentyear."-".$cmonth."-".$currentday;
	$day=1;
?>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Attendance Monitoring</label>
			</div>
			<div class="col-md-1"></div>
		</div>

<div>
	<form role="form" class="form-horizontal" method="post" action="submit-attendance.php">
		<div>
			<div class="col-md-3" style="margin-bottom:50px;">	
					
				<label>Month</label>

				<div class="btn-group btn-left-padding"> <a class="btn btn-default dropdown-toggle btn-select" data-toggle="dropdown" href="#">
					<?php 

					if(!isset($_GET['month'])) {
						echo $currentmonth;
						$_GET['month']=$currentmonth;
						$month=$currentmonth;
					}

					else{
						$month=$_GET['month'];
						echo $_GET['month'];	
						
					}
					?>
						
					<span class="caret"></span></a>
			            <ul class="dropdown-menu">
				            
			            	<?php
			            	$months=array("January","February","March","April","May","June","July","August","September","October","November","December");

			            	foreach ($months as $value) {
			            		?>
			            		<li><a href="index2.php?mode=<?php echo $_GET['mode']?>&category=<?php echo $_GET['category']?>&page=<?php echo $_GET['page']?>&month=<?php echo $value ?>"><?php echo $value ?></a></li>
			            		<?php
			            	}

			            	?>

		           	 	</ul>
		        </div>
		    </div>

	        <div class="col-md-2" style="margin-bottom:50px;">	

	        	<label>Edit All</label>
	        	
	        		<input id="editall" name="editall" type="checkbox" style="margin-left:20px;">
	        	
	        </div>
		</div>

		<div>
			
			<table id="attendance" class="display" cellspacing="0" width="100%" style="background-color: gold">     
			        <thead>
			            <tr>
			                <th>Name</th>  
			                <?php
			               	$month = date('m', strtotime($month));
							$totalday = cal_days_in_month(CAL_GREGORIAN, $month, $currentyear);
										
			                while ($totalday!=$day) {

			                	if($day<10){
					               $day='0'.$day;
					           }


								$date = $currentyear."-".$month."-".$day;
								$weekday=date('l', strtotime( $date));
								if($weekday!='Saturday' && $weekday!='Sunday'){
									
									if($_GET['month']==$currentmonth && $day==$currentday){

										?>
									 	<th style="min-width:55px;"<?php if ($weekday=='Monday') echo'style="border-left: 1px solid green"';?> ><?php  echo $day ?><input type="checkbox"  id="allpresent"  style="margin-left:20px;"  unchecked></th>
					                  	<?php
									}
									else{

										?>
										<th <?php if ($weekday=='Monday') echo'style="border-left: 1px solid green"';?> ><?php  echo $day ?></th>
				                  		<?php
									}		 	
			                  	}
			                  	$day++;
			                }
			                $day=1;
			                ?>
			                	          
			            </tr>			                
			        </thead>			 

			        <tbody>
			        	<?php  
			        						
						$result = mysql_query("SELECT lrn,lastname,firstname,middlename FROM tbl_studentinfo where lrn in (SELECT lrn FROM tbl_studentstatus WHERE sy_id=$sy_id AND section_id=(SELECT section_id FROM tbl_advisers WHERE emp_no= {$_SESSION['emp_no']} and sy_id=$sy_id )) AND remarks!='TO' AND remarks!='EXPELLED' order by gender desc, lastname, firstname") or die(mysql_error());
			  					
						if(mysql_num_rows($result) > 0) {
														
							while ($row = mysql_fetch_assoc($result)){

								$name=$row['lastname'].', '.$row['firstname']." ".$row['middlename'];
								$lrn=$row['lrn'];
							?>
					            <tr>
					                
					                <td><?php echo $name; ?></td>
					                <?php 

					            
					                while($totalday!=$day){
					                	if($day<10){
					                		$day='0'.$day;
					                	}
										$date = $currentyear."-".$month."-".$day;
										$weekday=date('l', strtotime( $date));


										if($weekday!='Saturday' && $weekday!='Sunday'){


										  	$result2=mysql_query("SELECT * FROM tbl_attendance where lrn=$lrn AND date_present='$date' ");
										  	
				               				if(mysql_num_rows($result2) > 0) {				
										
			                					if($month <$cmonth || ($month ==$cmonth && $day<$currentday) ){
													?> 
				                					<td <?php if ($weekday=='Monday') echo'style="border-left: 1px solid green"'; ?>>
									                	<input type="checkbox" class="pastattendance " name="attendance[<?php echo $lrn?>][<?php echo $date?>]" disabled="disabled" checked="checked">
								                		
								                	</td>
								                	<?php

												} 
												else{

													?> 
				                					<td  <?php if ($weekday=='Monday') echo'style="border-left: 1px solid green"'; ?>>
									                	<input style="margin-left:35px;"type="checkbox" class="currentattendance " name="attendance[<?php echo $lrn?>][<?php echo $date?>]" checked="checked">
								                		
								                	</td>
								                	<?php
													
												}

											}
											else{
												if($month <$cmonth || ($month ==$cmonth && $day < $currentday) ){
													?> 
				                					<td <?php if ($weekday=='Monday') echo'style="border-left: 1px solid green"'; ?>>
									                	<input type="checkbox" class="pastattendance " name="attendance[<?php echo $lrn?>][<?php echo $date?>]" disabled="disabled"  unchecked>
								                		
								                	</td>
								                	<?php
								                }
								                elseif($month >$cmonth || ($month ==$cmonth && $day>$currentday) ){
								                	?> 
				                					<td <?php if ($weekday=='Monday') echo'style="border-left: 1px solid green"'; ?>>
									                	<input type="checkbox"  name="attendance[<?php echo $lrn?>][<?php echo $date?>]" Style="display:none;">
									                	
								                	</td>
								                	<?php
								                }
								              	else{

								              		?> 
				                					<td <?php if ($weekday=='Monday') echo'style="border-left: 1px solid green"'; ?>>
									                	<input style="margin-left:35px;"type="checkbox" class="currentattendance" name="attendance[<?php echo $lrn?>][<?php echo $date?>]" unchecked>
									                	
								                	</td>
								                	<?php
													
								              	}
											}

				                		}
				                		$day++;
				                	}
			                	$day=1;
			                	?>
					            </tr>
					            <?php

					        }
		
					    }			                	
						                		
						?>
						                
					</tbody>

			</table>
			
			<div class="form-group">														    
			    <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
			</div> 

			<div class="form-group">														   
			    <input type="hidden" name="date" value="<?php echo $currentdate ?>">
			</div>

			<div class="form-group">														    
			    <input type="hidden" name="selectedmonth" value="<?php echo $_GET['month'] ?>">
			</div>

			<div class="form-group">														    
			    <input type="hidden" name="currentyear" value="<?php echo $currentyear ?>">
			</div>

			
			<div class="form-group " style="alighn:right;">
			    <button type="submit" class="btn btn-success success left-margin" name="submit" style="float: right;" onclick="return confirm('Are you sure you want to submit?');">Submit </button>			   
			</div>
		
		</div>
	</form>
</div>

	<script type="text/javascript">
		
		$("#editall").click(function(){
	    	if (this.checked) {
		    	$("input.pastattendance ").removeAttr("disabled");
		  	} else {
		    	$("input.pastattendance ").attr("disabled", true);
		  }
		});

		$("#allpresent").click(function(){
	    	$('input.currentattendance').not(this).prop('checked', this.checked);
		});

	</script>