<?php
	getDatatablesLink();	
	getDatatablesScript();
?>
		
<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<label id="header">Ranking</label>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
			<label>Year Level</label>
			<div class="btn-group btn-left-padding"> <a class="btn btn-default dropdown-toggle btn-select" data-toggle="dropdown" href="#">
				
				<?php 
				 	$result=get_db_array("SELECT year_level FROM tbl_yearlevel")or die(mysql_error());

					if(!isset($_GET['year_level'])) {
					
						$_GET['year_level'] = $result[0]['year_level'];
					
						echo $_GET['year_level'];		

					}
					else {
					
						echo $_GET['year_level'];		 												
					}
				?>

				<span class="caret"></span></a>
		            <ul class="dropdown-menu">
			            <?php
			           		$result=mysql_query("SELECT year_level FROM tbl_yearlevel")or die(mysql_error());
			            
			            	while ($row = mysql_fetch_assoc($result)) {
			            ?>
			                	<li>
			                		<a href="index2.php?mode=<?php echo $_GET['mode']?>&category=<?php echo $_GET['category']?>&page=<?php echo $_GET['page']?>&year_level=<?php echo $row['year_level']?>"><?php echo $row['year_level']?></a>
			                	</li>
		           		
		           		<?php
			            	}
			            ?>
	           	 	
	           	 	</ul>
	        </div>

       	 	<label> Quarter</label>
			<div class="btn-group btn-center-padding"> <a class="btn btn-default dropdown-toggle btn-select" data-toggle="dropdown" href="#">
		    	    
					<?php


            $c=1;
            
	        $arrayq=array('1st', '2nd', '3rd', '4th');


			if(!isset($_GET['quarter'])) {
				echo $arrayq[0];
				$_GET['quarter']=$arrayq[0];	 
		 	
			}
		
			else {

				echo $_GET['quarter']; 			
			}
		?>

		<span class="caret"></span></a>
            <ul class="dropdown-menu">
	        
           		<?php

	            	foreach($arrayq as $key => $value) {

	            ?>
	                <li><a href="index2.php?mode=<?php echo $_GET['mode'] ?>&category=<?php echo $_GET['category'] ?>&page=<?php echo $_GET['page'] ?>&year_level=<?php echo $_GET['year_level']?>&quarter=<?php echo $value ?>"><?php echo $value ?></a>
	                </li>
	        	
	        	<?php
	            	}
	            ?>

       	 	</ul>
	        </div>

		</div>
					
					

	<?php

		if(isset($_GET['year_level'])) {

			$year_level = $_GET['year_level'];
			
			$ranking = "SELECT * FROM tbl_yearlevel where year_level = '$year_level'";
			$result = mysql_query($ranking) or die(mysql_error());

			if (mysql_num_rows($result)>0) {
						
				while(($row = mysql_fetch_assoc($result))!= null) {

					$id = $row['id'];
					$year_id = $row['year_id'];
					$year_level = $row['year_level'];
					$year_desc = $row['year_desc'];
				}
			}
	?>

	<div class="row">
		
		<div class="row"><br>
			<form method="post" >
				<table id="ranking" class="display" cellspacing="0" width="100%" style="background-color:gold;">
						<thead>
							<tr>
								<th>LRN</th>
								<th>Name</th>
								<th>Ranking</th>
								<th>Average</th>
							</tr>
						</thead>
						
						<tbody>
						
						<?php 
							switch($_GET['quarter']){
								case '1st': $q='quarter1';
											break;

								case '2nd': $q='quarter2';
											break;

								case '3rd': $q='quarter3';
											break;

								case '4th': $q='quarter4';
											break;
							}

							include('current-year.php');

							$sql_search = "SELECT a.lrn, lastname, firstname, middlename, avg(" .$q. " ) as average
											FROM tbl_studentgrade a, tbl_studentinfo b 
											WHERE a.lrn = b.lrn AND  a.sy_id=$sy_id AND b.remarks!='TO' AND a.lrn in
												(SELECT lrn from tbl_studentstatus c, tbl_sy d where year_id=$year_id and c.sy_id=$sy_id)
												AND b.remarks!='EXPELLED' GROUP BY a.lrn ORDER BY average desc";				
			
							$result = mysql_query($sql_search) or die(mysql_error());							
							
								if (mysql_num_rows($result)>0) {
								
									$rank = 1;
								
									while(($row = mysql_fetch_assoc($result))!= null) {
										$lrn = $row['lrn'];
										$lastname = $row['lastname'];
										$firstname = $row['firstname'];
										$middlename = $row['middlename'];
										$average = $row['average'];
										
						?>
						
										<tr data-lrn="<?php echo $lrn ?>"
		            							data-lastname="<?php echo $lastname ?>" data-firstname="<?php echo $firstname ?>" 
		            							data-middlename="<?php echo $middlename ?>" class="data">

											<td width="25%"><?php echo $lrn;  ?></td>
											<td><?php echo $lastname .", ". $firstname ." ". $middlename;  ?></td>
											<td width="20%"><?php echo $rank;  ?></td>
											<td width="20%"><?php echo number_format((float)$average, 3, '.', ''); ?></td>
										</tr>
							<?php	
										$rank++;
									}
								}
							?>
						</tbody>
				</table>
			</form>
		</div>

		<div id="da" class="modal fade">
			<div id="yearlevel-content">
				<div class="modal-dialog modal-lg">
			        <div class="modal-content">
			           
			   		</div>
			   	</div>
			</div>
		</div>

	</div>
	
<?php	
		}
		
?>

</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		var table=$('#ranking').dataTable({
			bInfo: true,
			"bFilter": true,
			"ordering": false,
		});
 		
    	$('#ranking tbody').on( 'click', 'tr', function (e) {
    		e.preventDefault();

	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	        }


			$(".modal-content").html('');
			$('#da').modal('show');

			 $.post('modal-ranking.php',{lrn:$(this).attr('data-lrn')},
				 function(html){
				 	$(".modal-content").html(html);
				 }
			 ) 
			var lrn= $(this).attr('data-lrn');
			 console.info(lrn);

	
		});
	} );

</script>