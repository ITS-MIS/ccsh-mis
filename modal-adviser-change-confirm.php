<?php
    include'connect.php';
    include('current-year.php');

    $section_name=$_POST['section_name'];
    $emp_no=$_POST['emp_no'];
    $url=$_POST['url'];
    
    $oldsection_id=get_db("SELECT emp_no,section_id from tbl_advisers where emp_no=$emp_no and sy_id=$sy_id ");
    $oldsection_id=$oldsection_id['section_id'];

    if ($oldsection_id!=0 || $oldsection_id!=null) {
        $oldsection_name=get_db("SELECT section_name from tbl_section where section_id=$oldsection_id ");
        $oldsection_name=$oldsection_name['section_name'];
    }
?>
            
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
    </div>
    
    <div class="modal-body">
        <?php
            if ($oldsection_id!=0 || $oldsection_id!=null) {
                ?>
                <p>Re-assign adviser from  <?php echo $oldsection_name ?> ?</p>
                <?php
            }   
            else{
                ?>
                <p>Assign Adviser?</p>
                <?php
            }
        ?>
    </div>

    <div class="modal-footer">
    	<form class="form-horizontal" method="post" action="submit-adviser-change.php">
    		<input type="hidden" name="url" value="<?php echo $url ?>">		
    		<input type="hidden" id="section_name" name="section_name" value="<?php echo $section_name?>">													
			<input type="hidden" id="emp_no" name="emp_no" value="<?php echo $emp_no?>">
        	<button type="submit"class="btn btn-success success">Confirm</button>  	
   			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>        	
     	</form>
    </div>