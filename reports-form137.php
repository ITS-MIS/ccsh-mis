<?php
	require_once('connect.php');
	require_once('fpdf/fpdf.php');
	require_once('fpdf/fpdi.php');
	include('current-year.php');
	
	$lrn=$_POST['lrn'];	

	$pdf = new FPDI();
	$pdf->AddPage('P','legal');

	$pageCount = $pdf->setSourceFile("forms/Form137.pdf");
	$tplIdx = $pdf->importPage(1);

	$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true); 

	$pdf->SetFont('Arial', '', '7');
	$pdf->SetTextColor(0,0,0);

	$x=43;
	$y=88.3;
	$y2=107.5;
	$n=4;

//schoolyear-------------------------------------------------------------
	$sy=get_db("SELECT sy FROM tbl_sy WHERE sy_id = $sy_id");
	$sy=$sy['sy'];
	
	$pdf->SetXY(153, 57);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, $sy);

//grade7---------------------------------------------------------------------
	$pdf->SetXY(24, 68);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, 'Caloocan City Science High School');
	
	$pdf->SetXY(42, 148);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, '1');

	$result=mysql_query("SELECT a.lrn,year_id,a.section_id,section_name,sy
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c
	WHERE a.section_id=b.section_id and year_id=7  and a.sy_id=c.sy_id and lrn='$lrn'");
	
	while($row=mysql_fetch_assoc($result)){
		
		$sy1=$row['year_id'];
		$pdf->SetXY(78, 72.5);
		$pdf->SetFont('Arial', '', '10');
	 	$pdf->Write(0, $sy1);

		$sn1=$row['section_name'];
		$pdf->SetXY(82, 72.5);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
		
		$sn1=$row['sy'];
		$pdf->SetXY(35, 72.5);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
}

//grade8---------------------------------------------------------------------

	$pdf->SetXY(122, 68);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, 'Caloocan City Science High School');

	$pdf->SetXY(145, 148);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, '2');	

	$result=mysql_query("SELECT a.lrn,year_id,a.section_id,section_name,sy 
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c 
	WHERE a.section_id=b.section_id and year_id=8  and a.sy_id=c.sy_id and lrn='$lrn'");
	
	while($row=mysql_fetch_assoc($result)){
		$sy1=$row['year_id'];
		$pdf->SetXY(177, 72.5);
		$pdf->SetFont('Arial', '', '10');
	 	$pdf->Write(0, $sy1);

		$sn1=$row['section_name'];
		$pdf->SetXY(182, 72.5);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
		
		$sn1=$row['sy'];
		$pdf->SetXY(136, 72.5);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
	}

// grade9--------------------------------------------------------------
	
	$pdf->SetXY(24, 158);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, 'Caloocan City Science High School');
	
	$pdf->SetXY(42, 239);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, '3');
	

	$result=mysql_query("SELECT a.lrn,year_id,a.section_id,section_name,sy 
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c 
	WHERE a.section_id=b.section_id and year_id=9  and a.sy_id=c.sy_id and lrn='$lrn'");
	
	while($row=mysql_fetch_assoc($result)){
		$sy1=$row['year_id'];
		$pdf->SetXY(78, 164);
		$pdf->SetFont('Arial', '', '10');
	 	$pdf->Write(0, $sy1);

		$sn1=$row['section_name'];
		$pdf->SetXY(82, 164);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
		
		$sn1=$row['sy'];
		$pdf->SetXY(35, 164);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
	}

// grade10--------------------------------------------------------------
	$pdf->SetXY(122, 158);
	$pdf->SetFont('Arial','', '10');
	$pdf->Write(0, 'Caloocan City Science High School');	

	$result=mysql_query("SELECT a.lrn,year_id,a.section_id,section_name,sy 
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c 
	WHERE a.section_id=b.section_id and year_id=10  and a.sy_id=c.sy_id and lrn='$lrn'");
	
	while($row=mysql_fetch_assoc($result)) {
		$sy1=$row['year_id'];
		$pdf->SetXY(177, 164);
		$pdf->SetFont('Arial', '', '10');
	 	$pdf->Write(0, $sy1);

		$sn1=$row['section_name'];
		$pdf->SetXY(182, 164);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
		
		$sn1=$row['sy'];
		$pdf->SetXY(136, 164);
		$pdf->SetFont('Arial','', '10');
		$pdf->Write(0, $sn1);
	}	
	
///Grade7 output---------------------------------------------------------------------	

	$xarraymale=array(14,35,47,59,71,83);

	$pdf->SetFont('Arial', '', '9'); 
	
	$result=get_db_array("SELECT b.subject_code,round(quarter1),round(quarter2), round(quarter3), round(quarter4),round((quarter1+quarter2+quarter3+quarter4)/4) as average FROM tbl_studentgrade a
							LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code
							LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn and a.sy_id=c.sy_id
							WHERE a.lrn = $lrn and year_id=7 GROUP BY a.subject_code ORDER BY subject_title");
			
	foreach ($result as $key => $columnname) {
			$c=0;
			$remarks="";
		foreach ($columnname as $key2 => $value) {
			
			if ($key2=='average'){
			
				if ($value>74 ) {															
					$remarks= "Passed";
				} else {
					$remarks = "Failed";		
				}	
			}
				
						$pdf->SetXY(93, $y);
						$pdf->Write(0, $remarks)  ; 

						$pdf->SetXY($xarraymale[$c], $y);
						$pdf->Write(0, $value)  ; 				
			$c++;
		}
		
		$y=$y+$n;
	}

//Grade8 output---------------------------------------------------------------------	
	
	$y=88.3;
	$n=4;

	$xarraymale=array(117,136,148,160,172,184);

	$pdf->SetFont('Arial', '', '9'); 
	
	$result=get_db_array("SELECT a.subject_code, round(quarter1), round(quarter2), round(quarter3), round(quarter4),round((quarter1+quarter2+quarter3+quarter4)/4) as average FROM tbl_studentgrade a
							LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code
							LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn and a.sy_id=c.sy_id
							WHERE a.lrn = $lrn and year_id=8 GROUP BY a.subject_code ORDER BY subject_title");
			
	foreach ($result as $key => $columnname) {

			$c=0;
			$remarks="";
		foreach ($columnname as $key2 => $value) {
			
			if ($key2=='average'){
			
				if ($value>74 ) {			
							
							$remarks= "Passed";
						}
						else {
								
							$remarks = "Failed";
							
						}	
			}		
						$pdf->SetXY(193, $y);
						$pdf->Write(0, $remarks)  ; 

						$pdf->SetXY($xarraymale[$c], $y);
						$pdf->Write(0, $value)  ; 
				
				$c++;		
		}
		
		$y=$y+$n;
	}

	///Grade9 output---------------------------------------------------------------------	
	
	$y=179;
	$n=4;
    
	$xarraymale=array(14,35,47,59,71,83,95);

	$pdf->SetFont('Arial', '', '9'); 
	
	$result=get_db_array("SELECT a.subject_code, round(quarter1), round(quarter2), round(quarter3), round(quarter4),round((quarter1+quarter2+quarter3+quarter4)/4) as average FROM tbl_studentgrade a
								LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code
								LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn and a.sy_id=c.sy_id
								WHERE  a.lrn = $lrn and year_id=9 GROUP BY a.subject_code ORDER BY subject_title");
	foreach ($result as $key => $columnname) {

			$c=0;
			$remarks="";

		foreach ($columnname as $key2 => $value) {
			
			if ($key2=='average'){
			
				if ($value>74 ) {
							
					$remarks= "Passed";
				} else {
					$remarks = "Failed";		
				}	
			}
				
				$pdf->SetXY(92, $y);
				$pdf->Write(0, $remarks)  ; 

				$pdf->SetXY($xarraymale[$c], $y);
				$pdf->Write(0, $value)  ; 
				$c++;
		}
		
		$y=$y+$n;
	}

	///Grade10 output---------------------------------------------------------------------	
	
	$y=179;
	$n=4;
    
	$xarraymale=array(115,136,148,160,172,184,197);

	$pdf->SetFont('Arial', '', '9'); 
	
	$result=get_db_array("SELECT a.subject_code, round(quarter1), round(quarter2), round(quarter3), round(quarter4),(avg(quarter1)+avg(quarter2) + avg(quarter3) + avg(quarter4))/4 as gwa FROM tbl_studentgrade a
							LEFT JOIN tbl_subject b ON a.subject_code = b.subject_code
							LEFT JOIN tbl_studentstatus c ON a.lrn=c.lrn and a.sy_id=c.sy_id
							WHERE a.lrn =$lrn and year_id=10 GROUP BY a.subject_code ORDER BY subject_title");
	foreach ($result as $key => $columnname) {

			$c=0;
			$remarks="";
		foreach ($columnname as $key2 => $value) {
			
			if ($key2=='average'){
			
				if ($value>74 ) {	
					$remarks= "Passed";
				} else {
					$remarks = "Failed";
				}	
			}
				
				$pdf->SetXY(193, $y);
				$pdf->Write(0, $remarks)  ; 

				$pdf->SetXY($xarraymale[$c], $y);
				$pdf->Write(0, $value)  ; 
				$c++;
		}
		
		$y=$y+$n;
	}

//student list--------------------------------------------------------------------

$result=mysql_query("SELECT a.lrn,lastname,firstname,middlename,birthdate,gender,year_id,addnost,brgy,city,province,guardianname,school,gwa
							from tbl_studentinfo a,tbl_studentstatus b WHERE a.lrn=b.lrn and sy_id=$sy_id  and b.lrn=$lrn");
	
	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=$row['firstname'].' '.$row['middlename']."   ".$row['lastname'];
		$pdf->SetXY(37, 44);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $name);
	
		$date=$row['birthdate'];
		$pdf->SetXY(154, 44);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $date);	
		
		$address=$row['addnost'].', '.$row['brgy']." ,  ".$row['city'].' , '.$row['province'];
		$pdf->SetXY(26, 52);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $address);
		
		$school=$row['school'];
		$pdf->SetXY(65, 57);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $school);
		
		$gwa=$row['gwa'];
		$pdf->SetXY(193, 57);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $gwa);
		
		$gender=$row['gender'];
		$pdf->SetXY(193, 44);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $gender);
		
		$lrn=$row['lrn'];
		$pdf->SetXY(180, 61);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $lrn);
		
		$guardian=$row['guardianname'];
		$pdf->SetXY(165, 52);
		$pdf->SetFont('Arial', '', '10');
		$pdf->Write(0, $guardian);
	}

//average grade7-------------------------------------------------
	
	$result=mysql_query("SELECT subject_code, 
	round(avg(quarter1)+avg(quarter2) + avg(quarter3)+ avg(quarter4))/4 as gwa FROM tbl_studentgrade a,tbl_studentstatus b where b.year_id=7 and  a.sy_id=b.sy_id and a.lrn=b.lrn and a.lrn=$lrn");

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=number_format((float)$row['gwa'], 2, '.', '');
		$pdf->SetXY(85, 148);
		$pdf->SetFont('Arial', '', '10');
		if ($name==NULL or $name==0){

		}
		else{
		$pdf->Write(0, $name);}
	
//average grade8-------------------------------------------------

	$result=mysql_query("SELECT subject_code, 
	round(avg(quarter1)+avg(quarter2) + avg(quarter3)+ avg(quarter4))/4 as gwa FROM tbl_studentgrade a,tbl_studentstatus b where b.year_id=8 and  a.sy_id=b.sy_id and a.lrn=b.lrn and a.lrn=$lrn");

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name2=number_format((float)$row['gwa'], 2, '.', '');
		$pdf->SetXY(185, 148);
		$pdf->SetFont('Arial', '', '10');
		if ($name2==NULL or $name2==0){

		}
		else{
		$pdf->Write(0, $name2);}

//average grade9-------------------------------------------------
		$result=mysql_query("SELECT subject_code, 
		round(avg(quarter1)+avg(quarter2) + avg(quarter3)+ avg(quarter4))/4 as gwa FROM tbl_studentgrade a,tbl_studentstatus b where b.year_id=9 and  a.sy_id=b.sy_id  and a.lrn=b.lrn and a.lrn=$lrn");
	

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name3=number_format((float)$row['gwa'], 2, '.', '');
		$pdf->SetXY(85, 239);
		$pdf->SetFont('Arial', '', '10');
		if ($name3==NULL or $name3==0){

		}
		else{
		$pdf->Write(0, $name3);}
//average grade10-------------------------------------------------
		$result=mysql_query("SELECT subject_code, 
		round(avg(quarter1)+avg(quarter2) + avg(quarter3)+ avg(quarter4))/4 as gwa FROM tbl_studentgrade a,tbl_studentstatus b where b.year_id=10 and a.lrn=b.lrn and  a.sy_id=b.sy_id and a.lrn=b.lrn and a.lrn=$lrn");
	

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name4=number_format((float)$row['gwa'], 2, '.', '');
		$pdf->SetXY(185, 239);
		$pdf->SetFont('Arial', '', '10');
		if ($name4==NULL or $name4==0){

		}
		else{


		$pdf->Write(0, $name4);	}

}
}
}
}

//	Adviser grade7-----------------------------------------------------------------

	$result=mysql_query("SELECT distinct a.lrn,a.year_id,a.section_id,section_name,sy,d.emp_no,firstname,middlename,lastname
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c,tbl_advisers d,tbl_facultyinfo e
	WHERE a.section_id=b.section_id and a.year_id=d.year_id and a.section_id=d.section_id  and d.year_id=7  and e.emp_no=d.emp_no and a.sy_id=c.sy_id  and a.lrn=$lrn and d.sy_id=a.sy_id");
	

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=$row['firstname'].'  '.$row['middlename']." ".$row['lastname'];
		$pdf->SetXY(68, 153); 
		$pdf->SetFont('Arial', '', '9');
		$pdf->Write(0, $name);
	
}

//	Adviser grade8-----------------------------------------------------------------
$result=mysql_query("SELECT distinct a.lrn,a.year_id,a.section_id,section_name,sy,d.emp_no,firstname,middlename,lastname
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c,tbl_advisers d,tbl_facultyinfo e
	WHERE a.section_id=b.section_id and a.year_id=d.year_id and a.section_id=d.section_id  and d.year_id=8  and e.emp_no=d.emp_no and a.sy_id=c.sy_id  and a.lrn=$lrn and d.sy_id=a.sy_id");
	

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=$row['firstname'].'  '.$row['middlename']." ".$row['lastname'];
		$pdf->SetXY(170, 153); 
		$pdf->SetFont('Arial', '', '9');
		$pdf->Write(0, $name);
	
}

//	Adviser grade9-----------------------------------------------------------------
$result=mysql_query("SELECT distinct a.lrn,a.year_id,a.section_id,section_name,sy,d.emp_no,firstname,middlename,lastname
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c,tbl_advisers d,tbl_facultyinfo e
	WHERE a.section_id=b.section_id and a.year_id=d.year_id and a.section_id=d.section_id  and d.year_id=9  and e.emp_no=d.emp_no and a.sy_id=c.sy_id  and a.lrn=$lrn and d.sy_id=a.sy_id");
	

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=$row['firstname'].'  '.$row['middlename']." ".$row['lastname'];
		$pdf->SetXY(68, 243); 
		$pdf->SetFont('Arial', '', '9');
		$pdf->Write(0, $name);
	
}
//	Adviser grade10-----------------------------------------------------------------
$result=mysql_query("SELECT distinct a.lrn,a.year_id,a.section_id,section_name,sy,d.emp_no,firstname,middlename,lastname
	FROM tbl_studentstatus a,tbl_section b,tbl_sy c,tbl_advisers d,tbl_facultyinfo e
	WHERE a.section_id=b.section_id and a.year_id=d.year_id and a.section_id=d.section_id  and d.year_id=10  and e.emp_no=d.emp_no and a.sy_id=c.sy_id  and a.lrn=$lrn and d.sy_id=a.sy_id");
	

	$pdf->SetFont('Arial', '', '8'); 
	$pdf->SetTextColor(0,0,0);

	while($row=mysql_fetch_assoc($result)){
		$name=$row['firstname'].'  '.$row['middlename']." ".$row['lastname'];
		$pdf->SetXY(170, 243); 
		$pdf->SetFont('Arial', '', '9');
		$pdf->Write(0, $name);
	
}


	$pdf->Output();

?>




